# Brain Trainer Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Changed
- locked Flutter version to 1.17.5
- Re-did iOS podfiles to get compiling for iOS
- 

### Added
- Diagnostic information to support page
- Diagnostic information to support e-mail body
- Pop-up help dialog when user fails impedance check multiple times in a row
- User information for Crashlytics and Analytics

## [1.5.1] - 2020-08-27

### Fixed
- Fix CU-bmgexy - Haptic was not turning off

## [1.5.0] - 2020-08-27

### Added
- Link to ABM M4 APPT instructional video on YouTube.
- Text Markers in M4 Stream - Starting Training, End Training, Over Threshold, Under Threshold, Start Round, End Round
- File sync test to system page, with snackbar saying how many uploaded
- fileKey in TrainingSession in DB will have the file name!
- Target portal color based on the average z-score threshold since last portal breaking the threshold -- green, if the z-score average was above the threshold, or red if the z-score average was below the threshold.

### Changed
- Hardware page layout -- better use of space.
- Timing on PPR - starts when the instructions are done
- Space game paths extended to meet the 3:00-ish criteria.

### Removed
- "Learn More" placeholder tiles.

### Fixed
- Look to fix CU-bjjxur - less file churning, usually upload only two!
- Fixed CU-bgqgwb again.  Will not crash with bad data in the report.
- Fixed CU-bjm7b6 - system UI disapears after PIN pad now.
- Fixed CU-bmgjf3 - Fixed string issue when training time or zone time is greater than 10 minutes
- Fixed CU-bmgezw - incorrect zone percentage report on training report

## [1.4.0] - 2020-08-26

### Added
- Auto uploading files to firebase storage to a bucket with the subjects email address.
- PIN pad access to Debug page - ask for PIN
- Implemented home tab "Setup Headset" button
- Implemented hardware tab "Start Training" button
- Pre-game countdown timer.

### Changed
- Home page layout and graphics
- Shorter haptic feedback (CU-bjgky5)
- Slider on debug to experiment with haptic.  NOTE: DOES NOT PERSIST
- Updated z-score logic to create a baseline during initial acquisition, and then use that as the basis for all subsequent z-score computations. The baseline window is currently set to 4 minutes.
- Debug Page renamed System

### Deprecated

### Removed
- Settings button (CU-b4k7de)

### Fixed
- Fixed CU-bgqgwb - crash on finish game
- Fixed CU-b4ka6w - typo on hardware page
- Fixed CU-bgmqub - PPR had 9 rounds

### Security

## [1.3.0] - 2020-08-25

### Added
- Haptic feedback for training (always on, m4 only) - 1 second pulses, lighter as you get to threshold, nothing over threshold.
- Haptic test button
- Email support button opens gmail
- Play Again button for line and space games on reporting page
- Cancel buttons in "Universe Master," "Green Seeker," and PPR training.

### Changed
- More tab renamed to Support
- Support information on Support page
- Game names and descriptions
- Reporting format
- PPR text
- PPR rounds remaining
- PPR now shows per-round stats
- Removed putts view from debug page
- Graph game is now 3 minutes
- Graph game graph is longer (3x), and taller (25%)
- In / Out zone labels.
- "Universe Master" and "Green Seeker" now run in full screen.
- User can no longer continue to training until a headset is connected and the impedance is good.

### Fixed
- Session time in zone is now calculated based on sample percentages over time - fix CU-b8mfng
- Play Again plays the game again
- Battery charge level range.

### Removed
- removed upload test from debug page

### Deprecated

### Removed

### Security

## [1.2.2] - 2020-08-20

### Added
- Haptic capability in M4 plugin

### Fixed
- Training Tab error only allowed one game once.
- Upon connecting to the M4 ensure it's not in acquisition mode.

## [1.2.1] - 2020-08-20

Version for Dry Run 2

### Changed
- Z score threshold now goes from 40-90, and starts at 40 for a new subject.  Control on Debug tab updated.

## [1.2.0] - 2020-08-20

Version for Dry Run

### Added
- PPR game
- Debug time button, makes games shorter for testing functionality
- Change zone threshold on Debug page - writes to database, used later
- Battery status indicator (updates upon connecting to a device, periodically every 60 seconds, and real-time during acquisition)

### Changed
- More text for training page

### Known Issues
- There is a chance that invoking an impedance measure or battery status query will cause the acquisition start to fail.  If this happens, leave the training tab and go back to it.

## [1.1.0] - 2020-08-19

### Added
- Training program info on debug page (number of sessions)

### Changed
- The Delete Test Sessions button now deletes all user data and logs the user out, in order to easily test the "new user experience"
- Reporting on the Report page - times, averages
- Renamed TrainingManager to TrainingService
- Training Program database collection - one per subject
- Writing real training results to the database, reflected on debug page
- Training page will not work if no M4 is connected
- Update the headset state indicator (upper-right headset and battery icon) to track impedance measurement level
- New users get GolfSubject and TrainingProgram documents in database

### Deprecated

### Removed
- Add and Load Subject buttons from debug page - this is automatic now, if a user does not have a
  subject, they get one created

### Fixed
- Deleting a training session deletes the putts
- Gracefully handle M4 (real and simulated) spontaneous disconnects

### Security

## [1.0.1] - 2020-08-13

### Added
- Line graph feedback training - 1 minute of lines with automatic end
- Training Page state machine functionality - picking page, play any training, reports, return to training
- Training manager that starts and stops acquisition

### Changed
- Made a debug tab with all that was in the more tab
- Changed content of more tab
- Space game ends when the last portal is reached


### Fixed
- Z score math in Space Game was backwards


## [1.0.0 (798)] - 2020-08-12

### Added
- Updated the M4 simulator server and M4 Flutter plugin to coordinate using DNS-SD (a.k.a. Zeroconf) instead of requiring humans to manually enter network configuration settings. For more information, please refer to the M4 simulator server README.
- Optios branding in resources, including launcher icon

### Changed
- The `m4_network_device_config.json` file is no longer required and can be removed from the mobile device

## Template - use these field for a new entry

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

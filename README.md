# brain_trainer

Optios Expert Brain State Training mobile application.

## Getting Started

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our [online documentation](https://flutter.dev/docs), which offers tutorials, samples, guidance on mobile development, and a full API reference.

## Flutter Front-end Development

### Flutter Version
This app currently is version-locked to Flutter 2.2.1  This version of flutter can be obtained by doing a "git checkout 2.2.1" inside your flutter install, it it comes from git.

If you are using a non-git install, the version can be found [here](https://flutter.dev/docs/development/tools/sdk/releases?tab=macos)

### PR Checklist

* Run `flutter pub get` and commit the resulting `pubspec.lock` (if changes were made to `pubspec.yaml`)
* Run `flutter format` *only* on the files you have changed
* Include updated supporting documentation and/or README
* Include updated tests as needed

### Adding/Changing Dependencies (in `pubspec.yaml`)

Checklist for adding or changing dependencies:
* Do you *really* need it?
* Does it compare favorably over other suitable packages?
* Is the license compatible with commercial development and deployment?
* Is it mature enough for production?
* Is it actively being developed and supported?
* Does it come from a reasonably stable and trusted source?

If changes are made to `pubspec.yaml`, run `flutter pub get` and include the resulting `pubspec.lock` file with the `pubspec.yaml` file in your PR.

## Back End

This project used Goggle Firebase as the backend. It is being used for authentication, analytics, storage, distribution, and databases.

This requires:
- You have been added to the Firebase project as a developer
- You have retrieved and sent your [debug SHA-1 key](https://developers.google.com/android/guides/client-auth) to get added to the key list.

A few resources for Firebase & Flutter
- [Firebase Main Page](http://firebase.google.com)
- [Codelab for Firebase and Flutter](https://codelabs.developers.google.com/codelabs/flutter-firebase/#0)
- [Firebase Flutter Packages, with links to GitHub and Examples](https://pub.dev/publishers/firebase.google.com/packages)

## DevOps
For DevOps, we are using:
- [Github](https://github.com) of course
- [Fastlane](https://fastlane.tools/) for building and distribution
- [CircleCI](http://circleci.com) for online automated building
- [Firebase App Distribution](https://firebase.google.com/docs/app-distribution) for release storage and management with testers and devices.

If you want to set this up locally:
- Install the Firebase CLI: `curl -sL https://firebase.tools | bash`
- Login into Firebase using the CLI `firebase login`
- See the README file in the `./android/fastlane` directory.

If you want to build release builds, ask for the signing keys.

## Circle CI Details
The Android app will automatically deploy to Firebase only from commits in `dev`, `master`, or `feature_*`. Other branches will only build.

The CircleCI build for this uses a custom container at [optios/flutter-fastlane-android:api-28](https://hub.docker.com/repository/registry-1.docker.io/optios/flutter-fastlane-android/tags?page=1).  This image was built from the CircleCI Android 28 image, with Flutter, Fastlane, and FirebaseCLI installed. The Dockerfile is in the infrastructure directory.

If you want to use CircleCI in local mode, be sure to make your Docker installation default to at least 4GB memory for the containers. Android / Gradle builds are memory hogs!

# CGXKitExample

The CGXKit Example application illustrates how to use the [CGXKit SDK](https://github.com/BrainWave-Software/CGXKit) to stream EEG data from a CGX device.

While this demo application is written using Apple's SwiftUI APIs, the CGXKit SDK can be used with UIKit on iOS/iPadOS/tvOS and with Cocoa on macOS.

## Requirements

- macOS Big Sur or later
- Xcode 12.5 or later

## Platform Support

The CGXKit SDK and demo application run on the following Apple operating systems:

- iOS/iPadOS/tvOS
- watchOS
- macOS



//
//  ContentView.swift
//  Shared
//
//  Created by Mark Alldritt on 2021-06-26.
//

import SwiftUI
import CGXKit


let maxVariance = CGFloat(10000)
let goodImpedance = CGFloat(200000) // Ohms
let maxImpedance = CGFloat(1000000) // Ohms


struct ConnectedView: View {
    private let headband: CGXHeadband
    private let eegListener: EEGListener

    @State private var isSettingsPresented: Bool = false

    init(_ headband: CGXHeadband) {
        self.headband = headband
        self.eegListener = EEGListener(headband)
    }

    var body: some View {
        EEGView(listener: eegListener)
        /*
        .onReceive(NotificationCenter.default
                    .publisher(for: UserSettingsNotification, object: nil)) { (notification) in
            isSettingsPresented = true
        }
        */
        .onAppear() {
            #if os(iOS)
            //  Don't allow the device to sleep while connected to a headband
            UIApplication.shared.isIdleTimerDisabled = true
            #endif
        }
        .onDisappear() {
            #if os(iOS)
            //  Allow the device to sleep when disconnected
            UIApplication.shared.isIdleTimerDisabled = false
            #endif
        }
        .sheet(isPresented: $isSettingsPresented, onDismiss: {
            print(self.isSettingsPresented)
        }) {
            //UserSettingsView()
        }
    }
}


struct ContentView: View {
    var body: some View {
        CGXHeadbandView(connect: { (headband) in
            //  Configure the headband as we want it...
            headband.connect()
        }) { headband in
            CGXHeadbandConnectedView(showRSSI: true, headband: headband) { _ in
                ConnectedView(headband)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

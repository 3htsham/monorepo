//
//  DemoApp.swift
//  Shared
//
//  Created by Mark Alldritt on 2021-06-26.
//

import SwiftUI
import CGXKit


@main
struct CGXKitDemoApp: App {

    init() {
        //  Start scanning for CGX devices...
        CGXHeadband.defaultEEGUnits = .microVolts
        CGXHeadbandManager.shared.start()
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

//
//  EEGView.swift
//  PeakAlpha
//
//  Created by Mark Alldritt on 2021-01-08.
//

import SwiftUI
import Charts
import CGXKit


struct QualityInfoView: View {
    //  Textual representation of impedance when out of range
    
    var impedance: CGFloat
    
    var body: some View {
        HStack {
            let impedanceColor = impedance <= goodImpedance ? Color.green : (impedance <= maxImpedance ? Color.orange : Color.red)
                        
            if impedance > goodImpedance {
                Text("I: \(Int(impedance / 1000))kΩ")
                    .font(Font.caption.monospacedDigit())
                    .foregroundColor(impedanceColor)
            }
        }
    }
}


struct LevelView: View {
    //  Vertical bar chart indicating a level
    
    @Binding var value: CGFloat
    var maxValue: CGFloat
    var backgroundColor = Color(white: 0.2)
    var foregroundColor = Gradient(colors: [Color.green])
    
    private func level(value: CGFloat, height: CGFloat) -> CGFloat {
        let percentage = max(0, min(value, maxValue)) / maxValue
        return height * percentage
    }
    
    var body: some View {
        ZStack(alignment: .bottomLeading) {
            GeometryReader { geometryReader in
                let level = level(value: value,
                                  height: geometryReader.size.height)
                let lg = LinearGradient(gradient: foregroundColor, startPoint: .bottom, endPoint: .top)
                
                Rectangle()
                    .foregroundColor(backgroundColor)
                Rectangle()
                    .fill(lg)
                    .mask(Rectangle()
                            .frame(height: level)
                            .offset(x: 0, y: (geometryReader.size.height - level) / 2))
                    .animation(value < 0.001 ? .none : .easeIn)
            }
        }
    }
}


struct EEGChannelView: View {
    //  Display a single EEG channel
    
    private var listener: EEGListener
    private var channel: CGXHeadband.Channel
    private var channelIndex: Int?

    @Binding private var eeg: [[CGXHeadband.SampleType]]
    @Binding private var impedance: [CGXHeadband.SampleType]

    var impedanceGradient: Gradient {
        Gradient(stops: [.init(color: .green, location: 0),
                         .init(color: .orange, location: CGFloat(goodImpedance / maxImpedance)),
                         .init(color: .red, location: 1)])
    }

    init(listener: EEGListener, channel: CGXHeadband.Channel, eeg: Binding<[[CGXHeadband.SampleType]]>, impedance: Binding<[CGXHeadband.SampleType]>) {
        self.listener = listener
        self.channel = channel
        channelIndex = listener.headband.model.eegChannels.firstIndex(of: channel)
        self._eeg = eeg
        self._impedance = impedance
    }

    var body: some View {
        if let channelIndex = channelIndex {
            let impedance = self.impedance.isEmpty ? 0 : CGFloat(self.impedance[channelIndex])
            
            HStack(spacing: 1) {
                Group {
                    if impedance < maxImpedance {
                        ZStack {
                            Chart(data: eeg.isEmpty ? [] : eeg[channelIndex])
                                .chartStyle(
                                    LineChartStyle(.line, lineColor: .blue, lineWidth: 0.5)
                                )
                        }
                    }
                    else {
                        Rectangle()
                            .fill(Color.red.opacity(0.4))
                    }
                }
                .overlay(
                    VStack(spacing: 0) {
                        Spacer()
                        QualityInfoView(impedance: impedance)
                            .offset(x: -4, y: -2)
                    },
                    alignment: .trailing
                )
                .padding(0)
                LevelView(value: .constant(impedance),
                          maxValue: maxImpedance,
                          foregroundColor: impedanceGradient)
                    .frame(width: 6)
            }
        }
        else {
            GeometryReader { (g) in
                HStack(alignment: .center) {
                    Spacer()
                    Text("Not Available")
                        .font(channel == .none ? Font.title3 : Font.caption)
                        .foregroundColor(Color.gray)
                    Spacer()
                }
                .frame(height: g.size.height)
            }
        }
    }
}


struct EEGChannelsView: View {
    //  Display a series of EEG channels
    
    private static let timer = Timer.publish(every: 1.0 / 20, on: .main, in: .common).autoconnect()
    
    let listener: EEGListener
    let channels: [CGXHeadband.Channel] // channels to display

    @State private var isVisible = false
    @State private var update = false
    @State private var eeg: [[CGXHeadband.SampleType]] = [] // [channel][sample] sample
    @State private var impedance: [CGXHeadband.SampleType] = [] // [channel]

    var body: some View {
        GeometryReader { (g) in
            let spacing = CGFloat(2)
            
        VStack(spacing: 1) {
                ZStack {
                    //  Create alternating colored background views.
                    VStack(alignment: .leading, spacing: spacing) {
                        ForEach(0..<channels.count, id: \.self) { (i) in
                            Rectangle()
                                .fill(i % 2 == 0 ? Color(.sRGB, white: 0.07, opacity: 1.0) : Color(.sRGB, white: 0, opacity: 1.0))
                        }
                    }
                    
                    //  Create the EEG Graph view for all channels
                    VStack(alignment: .leading, spacing: spacing) {
                        ForEach(channels) { (channel) in
                            EEGChannelView(listener: listener, channel: channel, eeg: $eeg, impedance: $impedance)
                        }
                    }
                    
                    //  Create EEG channel labels for each channel
                    VStack(alignment: .leading, spacing: spacing) {
                        ForEach(channels) { (channel) in
                            VStack(alignment: .leading, spacing: 0) {
                                Text(channel.name)
                                    .font(Font.caption)
                                    .offset(x: 4, y: 4)
                                Spacer()
                            }
                            .padding(0)
                        }
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .onReceive(Self.timer) { t in
                        if (isVisible) {
                            (eeg, impedance) = listener.eeg(width: Int(g.size.width))
                        }
                    }
                    .onAppear() {
                        isVisible = true
                        (eeg, impedance) = listener.eeg(width: Int(g.size.width))
                    }
                    .onDisappear() {
                        isVisible = false
                    }
                }
                .clipped()
            }
        }
    }
}


struct EEGView: View {
    //  Display all EEG offered by the headband
    
    private let listener: EEGListener
    private let channels: [CGXHeadband.Channel]

    init(listener: EEGListener) {
        self.listener = listener
        
        //  Only show EEG channels
        channels = listener.headband.model.eegChannels.filter { (c) in
            switch c {
            case .A1, .A2:
                return false
                
            case .ExG1, .ExG2:
                return false
                
            default:
                return c.signalType == .EEG || c.signalType == .ExG
            }
        }
    }
    
    var body: some View {
        EEGChannelsView(listener: listener, channels: channels)
    }
}

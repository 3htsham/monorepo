//
//  CGXBluetoothView.swift
//  CGXKitDemo
//
//  Created by Mark Alldritt on 2021-06-27.
//

import SwiftUI
import CGXKit


#if os(iOS)
fileprivate extension UIApplication {
    func openSettings() {
        guard let settingsUrl = URL(string: Self.openSettingsURLString) else { return }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
}
#endif


fileprivate struct NoBluetoothView: View {
    var body: some View {
        #if targetEnvironment(simulator)
        Text("Bluetooth not available in simulator.  You must test on a device.")
            .font(titleFont)
            .multilineTextAlignment(.center)
            .padding()
        #else
        Text("Bluetooth Not Available")
            .font(titleFont)
            .multilineTextAlignment(.center)
            .padding()
            .transition(.opacity)
        #endif
    }
}


fileprivate struct BluetoothPoweredOffView: View {
    var body: some View {
        Group() {
            VStack() {
                Text("Bluetooth is turned off.  Please turn Bluetooth on so you can connect to your EEG headband.")
                    .font(.headline)
                    .multilineTextAlignment(.center)
                    .padding()
                #if os(iOS)
                CGXFilledButton(title: "Settings") {
                    UIApplication.shared.openSettings()
                }
                #endif
            }
        }
        .transition(.opacity)
    }
}


fileprivate struct BluetoothUnauthorizedView: View {
    var body: some View {
        let appName = Bundle.main.infoDictionary!["CFBundleName"] as! String
        
        Group() {
            VStack() {
                Text("Bluetooth access is not authorized.  Please authorize \(appName) to use Bluetooth to connect to your EEG headband.")
                    .font(.headline)
                    .multilineTextAlignment(.center)
                    .padding()
                #if os(iOS)
                CGXFilledButton(title: "Settings", imageName: "") {
                    UIApplication.shared.openSettings()
                }
                #endif
            }
        }
        .transition(.opacity)
    }
}


struct CGXBluetoothViewView<Content: View> : View {
    //  This view monitors Bluetooth state.  Once Bluetooth becomes available, CGX device aquisition begins.
    
    let content: (_ headband: CGXHeadband) -> Content
    let connect: ((_ headband: CGXHeadband) -> Void)?

    @State private var bluetoothState = CGXHeadbandManager.shared.state

    private let bluetoothPublisher = NotificationCenter.default
                .publisher(for: CGXHeadbandManager.BluetoothStateChangedNotification)
        
    var body: some View {
        Group() {
            switch bluetoothState {
            case .poweredOff:
                BluetoothPoweredOffView()
                
            case .unauthorized:
                BluetoothUnauthorizedView()
                
            case .poweredOn:
                CGXHeadbandAquisitionView(connect: connect, content: content)

            default:
                NoBluetoothView()
            }
        }
        .onReceive(bluetoothPublisher) { (output) in
            bluetoothState = CGXHeadbandManager.shared.state
        }
        .environment(\.colorScheme, .dark)
        .preferredColorScheme(.dark)
        .transition(.opacity)
    }
}

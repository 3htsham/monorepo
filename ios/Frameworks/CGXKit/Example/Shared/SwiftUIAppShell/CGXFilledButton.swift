//
//  CGXFilledButton
//  
//
//  Created by Mark Alldritt on 2021-02-22.
//

import SwiftUI
#if os(macOS)
import Cocoa
#endif


#if os(macOS)
struct BlueButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .foregroundColor(configuration.isPressed ? Color.blue : Color.white)
            .background(configuration.isPressed ? Color.white : Color.blue)
            .cornerRadius(6.0)
            .padding()
    }
}
#endif

public struct CGXFilledButton: View {
    private var buttonWidth: CGFloat {
        #if os(watchOS)
        let w = WKInterfaceDevice.current().screenBounds.width
        #elseif os(iOS)
        let w = UIScreen.main.bounds.width
        #elseif os(macOS)
        let w = NSScreen.main!.frame.width
        #endif

        return w * 0.8 // 80%
    }
    private let title: String
    private let imageName: String?
    private let action: () -> Void
    
    public init(title: String, imageName: String? = nil, action: @escaping () -> Void = {}) {
        self.title = title
        self.imageName = imageName
        self.action = action
    }

    public var body: some View {
        #if os(watchOS)
        Button(action: action) {
            if let imageName = imageName, !imageName.isEmpty {
                HStack {
                    Text(title)
                        .foregroundColor(.white)
                    CGXKitImage(imageName)
                        .frame(width: 30, height: 30, alignment: .center)
                        .foregroundColor(.white)
                }
            }
            else {
                Text(title)
                    .foregroundColor(.white)
            }
        }
        #elseif os(macOS)
        Button(action: action) {
            if let imageName = imageName, !imageName.isEmpty {
                HStack {
                    Text(title)
                        .foregroundColor(.white)
                    CGXKitImage(imageName)
                        .renderingMode(.template)
                        .frame(width: 30, height: 30, alignment: .center)
                        .foregroundColor(.white)
                }
                .frame(maxWidth: .infinity)
                .frame(height: 40)
            }
            else {
                Text(title)
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity)
                    .frame(height: 40)
            }
        }
        .buttonStyle(BlueButtonStyle())
        #else
        Button(action: action) {
            if let imageName = imageName, !imageName.isEmpty {
                HStack {
                    Text(title)
                        .font(.headline)
                        .foregroundColor(.white)
                        .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 5))
                    CGXKitImage(imageName)
                        .renderingMode(.template)
                        .frame(width: 30, height: 30, alignment: .center)
                        .foregroundColor(.white)
                        .padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 10))
                }
                .background(Color.accentColor)
                .cornerRadius(10)
                .frame(maxWidth: .infinity)
                .frame(height: 40)
            }
            else {
                Text(title)
                    .font(.headline)
                    .frame(width: buttonWidth * 0.8, height: 40)
                    .foregroundColor(.white)
                    .background(Color.accentColor)
                    .cornerRadius(10)
            }
        }
        #endif
    }
}

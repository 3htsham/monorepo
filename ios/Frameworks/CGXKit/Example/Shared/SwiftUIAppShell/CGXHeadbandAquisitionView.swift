//
//  CGXHeadbandAquisitionView.swift
//  CGXKitDemo
//
//  Created by Mark Alldritt on 2021-06-27.
//

import SwiftUI
import CGXKit


fileprivate struct ConnectButton: View {
    let action: () -> Void
    
    public var body: some View {
        #if os(macOS)
        Button(action: action) {
            Text("Connect")
                .foregroundColor(.white)
                .padding(10)
        }
        .buttonStyle(BlueButtonStyle())
        #else
        Button(action: action) {
            HStack(spacing: 2) {
                Text("Connect")
                    .foregroundColor(.white)
                CGXKitImage("Connect")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 20, height: 20, alignment: .center)
                    .foregroundColor(.white)
            }
            .padding(10)
            .background(Color.accentColor)
            .cornerRadius(8)
        }
        #endif
    }
}


fileprivate struct HeadbandRow: View {
    let showRSSI: Bool
    let headband: CGXHeadband

    @State private var rssi = -1

    var body: some View {
        if let image = headband.model.image {
            HStack {
                CGXKitImage(image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .shadow(color: Color(.sRGB, white: 0.5, opacity: 0.5), radius: 10, x: 0.0, y: 0.0)
                    .frame(width: 40, height: 40)
                    .padding(.trailing, 10)
                VStack(alignment: .leading) {
                    Text(headband.model.manufacturer)
                    Text(headband.model.model)
                    CGXRSSISignalView(rssi: $rssi)
                        .font(.footnote)
                }
            }
            .padding()
            .onReceive(NotificationCenter.default
                        .publisher(for: CGXHeadband.RSSIChangeNotification, object: headband)) { (output) in
                rssi = headband.rssi
            }
            .onAppear {
                rssi = headband.rssi
            }
        }
        else {
            VStack(alignment: .leading) {
                Text(headband.model.manufacturer)
                Text(headband.model.model)
                CGXRSSISignalView(rssi: $rssi)
                    .font(.footnote)
            }
            .padding()
            .onReceive(NotificationCenter.default
                        .publisher(for: CGXHeadband.RSSIChangeNotification, object: headband)) { (output) in
                rssi = headband.rssi
            }
            .onAppear {
                rssi = headband.rssi
            }
        }
    }
}


private struct CGXHeadbandListView <Content> : View where Content : View {
    //  This view handles CGX device acquisition.  As the CGXHeadbandManager reports device list changes, this view
    //  updates its list of available devices.  When the user connects to a particular headband, we initiate a
    //  device connect and monitor the headband's status.
    private let devicesPublisher = NotificationCenter.default
                .publisher(for: CGXHeadbandManager.DevicesChangedNotification)

    let showRSSI: Bool
    let connect: ((_ headband: CGXHeadband) -> Void)?
    let content: (_ headband: CGXHeadband) -> Content

    @State private var headbands = [CGXHeadband]()
    @State private var headband: CGXHeadband?
    @State private var headbandState = CGXHeadband.HeadbandState.disconnected

    var headbandsList: some View {
        List(headbands) { headband in
            HStack {
                HeadbandRow(showRSSI: showRSSI, headband: headband)
                Spacer()
                ConnectButton() {
                    if let connect = connect {
                        connect(headband)
                    }
                    else {
                        headband.connect()
                    }

                    self.headband = headband
                    self.headbandState = headband.state
                }
            }
        }
    }
    
    var body: some View {
        Group {
            if let headband = headband {
                Group {
                    switch headbandState {
                    case .connecting:
                        VStack {
                            ProgressView("Connecting...")
                                .padding()
                            CGXFilledButton(title: "Disconnect", imageName: "Disconnect") {
                                headband.disconnect()
                            }
                        }

                    case .connected:
                        content(headband)
                        
                    case .disconnecting:
                        Text("Disconnecting...")
                        
                    case .disconnected:
                        Text("Disconnected")
                    }
                }
                .onReceive(NotificationCenter.default
                            .publisher(for: CGXHeadband.StateChangeNotification, object: headband)) { (output) in
                    headbandState = headband.state
                    if headbandState == .disconnected {
                        self.headband = nil
                    }
                }
            }
            else if headbands.isEmpty {
                Text("Please turn on your CGX headband.")
                    .font(titleFont)
                    .multilineTextAlignment(.center)
                    .padding()
            }
            //else if headbands.count == 1 {
            //    ConnectToCGXHeadbandView(showRSSI: showRSSI, connect: connect, content: content)
            //}
            else {
                #if os(macOS)
                VStack {
                    Text("CGX Headbands")
                        .padding()
                    headbandsList
                }
                #elseif os(watchOS)
                NavigationView {
                    headbandsList
                        .navigationTitle("Headbands")
                }
                #else
                NavigationView {
                    headbandsList
                        .navigationTitle("CGX Headbands")
                        .navigationBarTitleDisplayMode(.inline)
                }
                .navigationViewStyle(StackNavigationViewStyle())
                #endif
            }
        }
        .onReceive(devicesPublisher) { (output) in
            print("devicesChanged: \(CGXHeadbandManager.shared.headbands)")
            
            headbands = CGXHeadbandManager.shared.headbands
        }
    }
    
}


struct CGXHeadbandAquisitionView<Content: View> : View {

    private let content: (_ headband: CGXHeadband) -> Content
    private let connect: ((_ headband: CGXHeadband) -> Void)?
    
    init(connect: ((_: CGXHeadband) -> Void)? = nil, @ViewBuilder content: @escaping (_ headband: CGXHeadband) -> Content) {
        self.content = content
        self.connect = connect
    }
    
    var body: some View {
        #if os(watchOS)
        ConnectToCGXHeadbandView(showRSSI: false, connect: connect, content: content)
        #else
        CGXHeadbandListView(showRSSI: true, connect: connect, content: content)
        #endif
    }
}

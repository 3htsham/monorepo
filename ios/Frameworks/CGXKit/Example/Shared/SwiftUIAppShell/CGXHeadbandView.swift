//
//  CGXHeadbandView.swift
//  
//
//

import SwiftUI
import CGXKit
import LNSBatteryLevel


extension View {
    var titleFont: Font {
        #if os(watchOS)
        return .headline
        #else
        return .title
        #endif
    }
    
    #if os(macOS)
    func CGXKitImage(_ image: NSImage) -> Image { // return a SwiftUI image for a NSImage.
        return Image(nsImage: image)
    }
    #else
    func CGXKitImage(_ image: UIImage) -> Image { // return a SwiftUI image for a UIImage.
        return Image(uiImage: image)
    }
    #endif
    
    func CGXKitImage(_ name: String) -> Image { // return a SwiftUI image for an asset .
        #if os(watchOS)
        return Image(uiImage: UIImage(named: name, in: .main, with: nil)!)
        #elseif os(macOS)
        return Image(nsImage: Bundle.main.image(forResource: name)!)
        #else
        return Image(uiImage: UIImage(named: name, in: .main, compatibleWith: nil)!)
        #endif
    }
}




public struct CGXHeadbandConnectedView<Content> : View where Content : View {
    //  This view appears when a headband becomes connected and includes the `content` view provided when
    //  CGXHeadbandView was called.  When the device is disconnects or is turned off, this view is removed
    //  and the app reverts to device acquisition mode.
    
    let showRSSI: Bool
    let headband: CGXHeadband
    let content: (_ headband: CGXHeadband) -> Content

    @State private var batteryLevel = CGFloat(0)
    @State private var rssi = -1
    
    public var body: some View {
        #if os(watchOS)
        //  For the small watchOS screen we use a scroll view.  The top region is the live EEG view which
        //  fills the screen.  The user can scroll to see the device details and the Disconnect button.
        ScrollView(.vertical) {
            let screenHeight = WKInterfaceDevice.current().screenBounds.height

            VStack {
                //  Let the content view provided fill the screen
                content(headband)
                    .frame(height: screenHeight)
                //  Add the headband and its status
                HStack {
                    Text(headband.model.model)
                    if headband.model.providesBatteryData {
                        LNSBatteryLevel(level: $batteryLevel, charging: .constant(false), borderColor: .white)
                            .frame(width: 18, height: 18)
                            .onReceive(NotificationCenter.default
                                        .publisher(for: CGXHeadband.BatteryLevelChangeNotification, object: headband)) { (output) in
                                batteryLevel = CGFloat(headband.batteryLevel ?? -1)
                            }
                            .onAppear {
                                batteryLevel = CGFloat(headband.batteryLevel ?? -1)
                            }
                    }
                    if showRSSI {
                        CGXRSSISignalView(rssi: $rssi)
                            .font(.footnote)
                    }
                }
                .padding()
                .onReceive(NotificationCenter.default
                            .publisher(for: CGXHeadband.RSSIChangeNotification, object: headband)) { (output) in
                    rssi = headband.rssi
                }
                .onAppear {
                    rssi = headband.rssi
                }
                //  Add a disconnect button the user can scroll into view
                CGXFilledButton(title: "Disconnect", imageName: "Disconnect") {
                    headband.disconnect()
                }
            }
        }
        #elseif os(macOS)
        VStack {
            HStack(alignment: .center) {
                HStack {
                    Text(headband.model.model).font(.headline)
                    if headband.model.providesBatteryData {
                        LNSBatteryLevel(level: $batteryLevel, charging: .constant(false), borderColor: .white)
                            .frame(width: 18, height: 18)
                            .onReceive(NotificationCenter.default
                                        .publisher(for: CGXHeadband.BatteryLevelChangeNotification, object: headband)) { (output) in
                                batteryLevel = CGFloat(headband.batteryLevel ?? -1)
                            }
                            .onAppear {
                                batteryLevel = CGFloat(headband.batteryLevel ?? -1)
                            }
                    }
                    if showRSSI {
                        CGXRSSISignalView(rssi: $rssi)
                            .font(.footnote)
                    }
                }
                .onReceive(NotificationCenter.default
                            .publisher(for: CGXHeadband.RSSIChangeNotification, object: headband)) { (output) in
                    rssi = headband.rssi
                }
                .onAppear {
                    rssi = headband.rssi
                }
                Spacer()
                Button(action: {
                    headband.disconnect()
                }) {
                    Text("Disconnect")
                        .foregroundColor(.white)
                        .frame(width: 90, height: 30)
                }
                .buttonStyle(BlueButtonStyle())
            }
            .padding(3)
            content(headband)
        }
        #else
        NavigationView {
            content(headband)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        HStack {
                            Text(headband.model.model).font(.headline)
                            if headband.model.providesBatteryData {
                                LNSBatteryLevel(level: $batteryLevel, charging: .constant(false), borderColor: .white)
                                    .frame(width: 18, height: 18)
                                    .onReceive(NotificationCenter.default
                                                .publisher(for: CGXHeadband.BatteryLevelChangeNotification, object: headband)) { (output) in
                                        batteryLevel = CGFloat(headband.batteryLevel ?? -1)
                                    }
                                    .onAppear {
                                        batteryLevel = CGFloat(headband.batteryLevel ?? -1)
                                    }
                            }
                            if showRSSI {
                                CGXRSSISignalView(rssi: $rssi)
                                    .font(.footnote)
                            }
                        }
                    }
                }
                .navigationBarItems(trailing: Button(action: {
                    headband.disconnect()
                }, label: {
                    CGXKitImage("Disconnect")
                }))
                .onReceive(NotificationCenter.default
                            .publisher(for: CGXHeadband.RSSIChangeNotification, object: headband)) { (output) in
                    rssi = headband.rssi
                }
                .onAppear {
                    rssi = headband.rssi
                }
        }
        #endif
    }
}


struct CGXHeadbandDescriptionView : View {
    
    let showRSSI: Bool
    let headband: CGXHeadband

    @State private var rssi = 0

    var body: some View {
        Group {
            if let image = headband.model.image {
                #if os(watchOS)
                VStack(alignment: .center) {
                    CGXKitImage(image)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .shadow(color: Color(.sRGB, white: 0.5, opacity: 0.5), radius: 10, x: 0.0, y: 0.0)
                        .frame(width: 80, height: 80)
                    Text(headband.model.manufacturer)
                    Text(headband.model.model)
                    if showRSSI {
                        CGXRSSISignalView(rssi: $rssi)
                    }
                }
                #else
                HStack {
                    CGXKitImage(image)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .shadow(color: Color(.sRGB, white: 0.5, opacity: 0.5), radius: 10, x: 0.0, y: 0.0)
                        .frame(width: 80, height: 80)
                        .padding(.trailing, 10)
                    VStack(alignment: .leading) {
                        Text(headband.model.manufacturer)
                        Text(headband.model.model)
                        if showRSSI {
                            CGXRSSISignalView(rssi: $rssi)
                        }
                    }
                }
                #endif
            }
            else {
                Text(headband.model.manufacturer)
                Text(headband.model.model)
                if showRSSI {
                    CGXRSSISignalView(rssi: $rssi)
                }
            }
        }
        .onReceive(NotificationCenter.default
                    .publisher(for: CGXHeadband.RSSIChangeNotification, object: headband)) { (output) in
            rssi = headband.rssi
        }
        .onAppear {
            rssi = headband.rssi
        }
    }
    
}

struct ConnectToCGXHeadbandView <Content> : View where Content : View {

    let showRSSI: Bool
    let connect: ((_ headband: CGXHeadband) -> Void)?
    let content: (_ headband: CGXHeadband) -> Content
    
    @State private var headband = CGXHeadbandManager.shared.headbands.first
    @State private var headbandState = CGXHeadband.HeadbandState.disconnected
    @State private var assignHeadbandType = false

    private let devicesPublisher = NotificationCenter.default
                .publisher(for: CGXHeadbandManager.DevicesChangedNotification)

    func content(headband: CGXHeadband) -> some View {
        Group {
            switch headbandState {
            case .connecting:
                VStack {
                    ProgressView("Connecting…")
                        .padding()
                    CGXFilledButton(title: "Disconnect", imageName: "Disconnect") {
                        headband.disconnect()
                    }
                }

            case .connected:
                content(headband)
                
            default:
                VStack {
                    CGXHeadbandDescriptionView(showRSSI: showRSSI, headband: headband)
                    CGXFilledButton(title: "Connect", imageName: "Connect") {
                        if case .unknown = headband.model {
                            assignHeadbandType.toggle()
                        }
                        else if let connect = connect {
                            connect(headband)
                        }
                        else {
                            headband.connect()
                        }
                    }
                }
            }
        }
    }
    
    var body: some View {
        Group {
            if let headband = headband {
                #if os(macOS)
                content(headband: headband)
                    .onReceive(NotificationCenter.default
                                .publisher(for: CGXHeadband.StateChangeNotification, object: headband)) { (output) in
                        headbandState = headband.state
                    }
                #else
                content(headband: headband)
                    .onReceive(NotificationCenter.default
                                .publisher(for: CGXHeadband.StateChangeNotification, object: headband)) { (output) in
                        headbandState = headband.state
                    }
                    .actionSheet(isPresented: $assignHeadbandType) {
                        let buttons = CGXHeadbandPickerType.allCases.compactMap { (picker) -> Alert.Button? in
                            switch headband.model {
                            case .unknown:
                                return Alert.Button.default(Text(picker.headbandType.description)) {
                                    headband.model = picker.headbandType
                                    if let connect = connect {
                                        connect(headband)
                                    }
                                    else {
                                        headband.connect()
                                    }
                                }
                                
                            default: // we are only interested in the unknown model
                                return nil
                            }
                        }
                        
                        return ActionSheet(
                            title: Text("Choose the type of EEG headband you have"),
                            buttons: buttons + [.cancel()]
                        )
                    }
                #endif
            }
            else {
                Text("Please turn on your EEG headband.")
                    .font(titleFont)
                    .multilineTextAlignment(.center)
                    .padding()
            }
        }
        .onReceive(devicesPublisher) { (output) in
            print("devicesChanged: \(CGXHeadbandManager.shared.headbands)")
            
            headband = CGXHeadbandManager.shared.headbands.first
            headbandState = headband?.state ?? .disconnected
        }
        .transition(.opacity)
    }

}


struct CGXHeadbandView<Content: View> : View {
    //  Root App Shell view.  Start b ensuring we have access to Bluetooth
    
    let connect: ((_ headband: CGXHeadband) -> Void)?
    let content: (_ headband: CGXHeadband) -> Content

    @State private var bluetoothState = CGXHeadbandManager.shared.state

    private let bluetoothPublisher = NotificationCenter.default
                .publisher(for: CGXHeadbandManager.BluetoothStateChangedNotification)
        
    var body: some View {
        #if os(macOS)
        CGXBluetoothViewView(content: content, connect: connect)
            .environment(\.colorScheme, .dark)
            .preferredColorScheme(.dark)
            .frame(minWidth: 700, minHeight: 700)
        #else
        CGXBluetoothViewView(content: content, connect: connect)
            .environment(\.colorScheme, .dark)
            .preferredColorScheme(.dark)
        #endif
    }
}

//
//  CGXKitExtras.swift
//

import CGXKit


extension CGXHeadband {
    
    var batteryLevel: Double? {
        return model.mapBatteryVoltageToPercent(batteryv)
    }

}



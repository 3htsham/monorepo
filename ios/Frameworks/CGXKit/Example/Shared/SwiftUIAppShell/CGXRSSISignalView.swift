//
//  CGXRSSISignalView.swift
//  CGXKitDemo
//
//  Created by Mark Alldritt on 2021-06-27.
//

import SwiftUI
import LNSSignalStrength


struct CGXRSSISignalStrengthView: View {

    @Binding var rssi: Int

    private var rssiBars: Int {
        //  I'm grading the RSSI value (roughly) based on this page: https://www.metageek.com/training/resources/understanding-rssi.html
        
        switch rssi {
        case (-40)...0:
            return 5
            
        case (-67)...(-41):
            return 4
            
        case (-70)...(-68):
            return 3
            
        case (-80)...(-71):
            return 2
            
        case (-90)...(-81):
            return 1
            
        default:
            return 0
        }
    }

    var body: some View {
        LNSSignalStrength(bars: rssiBars, totalBars: 5)
    }
}


struct CGXRSSISignalView: View {
    @Binding var rssi: Int
    
    var body: some View {
        HStack(alignment: .center, spacing: 3) {
            Text("NNN").hidden().overlay(CGXRSSISignalStrengthView(rssi: _rssi))
            Text("\(rssi) dB")
        }
    }
}

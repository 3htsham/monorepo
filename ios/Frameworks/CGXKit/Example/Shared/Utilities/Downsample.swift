//
//  Downsample.swift
//

import Foundation
import Accelerate


extension Array where Element == Float {
    
    public func downsample(decimation decimationFactor: Int = 2) -> [Element] {
        guard decimationFactor >= 2 else { return self }

        let filterLength: vDSP_Length = 2
        let filter = [Float](repeating: 1 / Float(filterLength),
                             count: Int(filterLength))
        let inputLength = vDSP_Length(self.count)

        let n = vDSP_Length((inputLength - filterLength) / vDSP_Length(decimationFactor)) + 1

        var outputSignal = [Float](repeating: 0,
                                   count: Int(n))

        vDSP_desamp(self,
                    vDSP_Stride(decimationFactor),
                    filter,
                    &outputSignal,
                    n,
                    filterLength)

        return outputSignal
    }

}


extension Array where Element == Double {

    public func downsample(decimation decimationFactor: Int = 2) -> [Element] {
        guard decimationFactor >= 2 else { return self }
        
        let filterLength: vDSP_Length = 2
        let filter = [Double](repeating: 1 / Double(filterLength),
                              count: Int(filterLength))
        let inputLength = vDSP_Length(self.count)

        let n = vDSP_Length((inputLength - filterLength) / vDSP_Length(decimationFactor)) + 1

        var outputSignal = [Double](repeating: 0,
                                    count: Int(n))

        vDSP_desampD(self,
                     vDSP_Stride(decimationFactor),
                     filter,
                     &outputSignal,
                     n,
                     filterLength)

        return outputSignal
    }

}

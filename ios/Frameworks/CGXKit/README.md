# CGXKit

CGX SDK for connecting to CGX devices on iOS, iPadOS, tvOS, macOS and watchOS.

## Requirements

- Xcode 12.5 or later.
- an iPhone or iPad running iOS 14.0 or later
- an Apple Watch running watchOS 7.0 or later
- a Apple Macintosh running macOS Catalina or later

## CGX Device Support

- Dev Kit
- Quick-20r
- Quick-20r v2
- Quick-20m
- Quick-32r
- Quick-32m
- Patch (provisional)
- Optios (provisional)

## Installation

Add the CGXKit Swift Package Manager package to your Xcode project:

`https://github.com/BrainWave-Software/CGXKit.git`

## Platform Support

- iOS/iPadOS
- tvOS
- watchOS
- macOS

## Documentation

- [Reference Documentation](docs/index.html)

- [Example Application](Example)

# Usage

## Import CGXKit

Any code that uses `CGXKit` must import it:

```
import CGXKit
```

## Starting CGXKit

CGXKit must be _started_ in order to begin discovering CGX devices.  Execute the following line when your application launches:

```
CGXHeadbandManager.shared.start()
```

You can stop CGXKit using the `CGXHeadbandManager.shared.stop()` function.  You may choose to do this when your app is placed into the background to reduce energy consumption.  Should you do this, you must call `CGXHeadbandManager.shared.start()` to resume CGX device discovery when your app is activated.

## Monitor Bluetooth State

iOS and macOS require the user to authorize access to Bluetooth.  Additionally, the user may turn Bluetooth off or on.  Your application needs to be prepared for Bluetooth state to change spontaneously.  

The `CGXHeadbandManager` class reports the Bluetooth state through its `state` property.  Whenever the Bluetooth state changes, a `EEGHeadbandManager.BluetoothStateChangedNotification` notification is posted.

You can monitor Bluetooth state using the following code:

```
NotificationCenter.default.addObserver(forName: EEGHeadbandManager.BluetoothStateChangedNotification,
                                        object: EEGHeadbandManager.shared,
                                        queue: nil) { [weak self] (_) in
     switch EEGHeadbandManager.shared.state {
     case .poweredOff:
         // The user has turned off Bluetooth
         
     case .unauthorized:
         // The user has not authorized Bluetooth access
         
     case .poweredOn:
         // Bluetooth is available and operating, device acquisition begins
         
     default:
         // Bluetooth is not available
     }
}
```

## Device Acquisition

Once Bluetooth becomes available, CGXKit begins listening for CGX devices.  The `CGXHeadbandManager` class reports discovered CGX devices through its `headbands` property.  This property contains an array of `CGXHeadband` objects.  

The `headbands` property changes whenever devices appear or disappear. A `EEGHeadbandManager.DevicesChangedNotification ` notification is posted when the `headbands` property changes.

You can monitor discovered CGX devices using the following code:

```
NotificationCenter.default.addObserver(forName: EEGHeadbandManager.DevicesChangedNotification,
                                       object: EEGHeadbandManager.shared,
                                       queue: nil) { [weak self] (note) in
    self?.updateUIForHeadbands(EEGHeadbandManager.shared.headbands)
}
```

If you are interested in particular type of CGX device, you can filter the `headbands` property:

```
let devKitHeadbands = CGXHeadbandManager.shared.headbands.filter { (headband) in
    switch headband.model {
    case .cgxDevKit:
        return true
        
    default:
        return false
}
```

## Device Introspection

You can discover a device's type and capabilities by querying the device's `model` enumeration.  This enumeration indicates the device's type (e.g. `cgxDevKit`, `cgxQuick20r`, etc.) along with its EEG sample rate, EEG channel mapping and other information.

## Device Connection & Data Streaming

### Data Streaming

To receive streamed EEG and impedance data, you must provide a _listener_.  Listeners implement the `CGXSampleDelegate` protocol.  Multiple listeners may be registered using `EEGHeadband`'s `registerDataListener` function.  Listeners are removed using the `unregisterDataListener` function.

The listener receives samples as they arrive.  Samples are delivered on a background thread.  This thread must not be blocked.  Samples include EEG and impedance data (see the `CGXSample` class).

Here is an example `CGXSampleDelegate` implementation:

```
class MyListener: CGXSampleDelegate {
    ...

    func receive(sample: CGXSample) { // receive a CGX sample
        ...
    }

    func receive(dropped: Int, sample: CGXSample)  { // receive a count of dropped frames
        ...
    }
}
```

**NOTE**: CGXKit retains a weak reference to your listener.  You must maintain your own strong reference to your listener to avoid premature deallocation.

### Connecting

Call the `connect()` function to connect to a particular CGX device and begin streaming data:

```
if let headband = CGXHeadbandManager.shared.headbands.first {
    headband.connect()
}
```

### Disconnecting

Call the `disconnect()` function to stop streaming data and release the device.

### Connection Status

A headband's connection status changes when the `connect()` and `disconnect()` functions are called.  A headband can disconnect spontaneously if the device is turned off or when there is a Bluetooth connectivity issue.

The `CGXHeadband` class reports connection status through its `state` property.  A `EEGHeadband.StateChangeNotification` notification is posted whenever the `state` property changes:

```
NotificationCenter.default.addObserver(forName: EEGHeadband.StateChangeNotification,
                                       object: headband,
                                       queue: nil) { [weak self] (note) in
    switch headband.state {
    case .connected:
        // device is connected and streaming data
        
    case .connecting:
        // connect() has been called, the connection is pending
        
    case .disconnected:
        // device is disconnected
        
    case .disconnecting:
        // disconnect() has been called
    }
}
```

### RSSI

The device's Bluetooth signal strength is reported via a headband's `rssi` property.  Whenever this value changes, a `CGXHeadband.RSSIChangeNotification` notification is posted.

NOTE: `rssi` is available when the device is connected or disconnected.

### Battery Level

The device's battery voltage is reported via the headband's `batteryv` property.  Whenever this value changes, a `CGXHeadband.BatteryLevelChangeNotification` notification is posted.

**NOTE**: `batteryv` is only available when a device is connected.

You can convert a devices battery voltage to a % full using the device's `model.mapBatteryVoltageToPercent` function:

```
NotificationCenter.default.addObserver(forName: CGXHeadband.BatteryLevelChangeNotification,
                                       object: headband,
                                       queue: nil) { [weak self] (note) in
    let batteryLevel = headband.model.mapBatteryVoltageToPercent(headband.batteryv)
    
    self?.updateBatteryLevelUI(batteryLevel)
}
```

## IMPORTANT

- Your application **MUST** include the following `Info.plist` keys.  If these keys are missing, your application will not be permitted to use Bluetooth.

  - `NSBluetoothAlwaysUsageDescription`
  - `NSBluetoothPeripheralUsageDescription`

- You must test on Apple devices because the Xcode simulator cannot access Bluetooth.

- On iOS and watchOS, if you intend to stream EEG data while the application is in the background, you must enable the `Uses Bluetooth LE accessories` and `Background processing` Background Processing Modes.

- Some older CGX devices carry a generic Bluetooth name which prevents CGXKit from automatically determining the device's type.  When this happens, the discovered headband's `model` property will be `.unknown`.  When this happens, you must set the headband's `model` property to the correct device type:

  - `cgxDevKit(userSelected: true)`
  - `cgxQuick20r(userSelected: true)`
  - `cgxQuick32r(userSelected: true)`

  The sample application illustrates how to do this.

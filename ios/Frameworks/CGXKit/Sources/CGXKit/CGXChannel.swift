//
//  CGXChannel.swift
//  CGXKit
//
//
//  Created by Mark Alldritt on 2020-04-08.
//

#if os(iOS)
import UIKit
#endif
#if os(watchOS)
import WatchKit
#endif
#if os(macOS)
import Cocoa
#endif


extension CGXHeadband {
    
    /// An EEG channel.
    public enum Channel: String, Codable, Identifiable, CaseIterable {
        
        /// Identifiable conformity
        public var id: String { return self.rawValue }

        /// Channels mapped to a specific location on the head.
        case FP1,
             FPZ,
             FP2,
             Nz,
             AF9,
             AF7,
             AF3,
             AFZ,
             AF4,
             AF8,
             AF10,
             F9,
             F7,
             F5,
             F3,
             F1,
             FZ,
             F2,
             F4,
             F6,
             F8,
             F10,
             FT9,
             FT7,
             FC5,
             FC3,
             FC1,
             FCz,
             FC2,
             FC4,
             FC6,
             FT8,
             FT10,
             T9,
             T7,
             T3,
             C5,
             C3,
             C1,
             Cz,
             C2,
             C4,
             C6,
             T4,
             T8,
             T10,
             TP9,
             TP7,
             CP5,
             CP3,
             CP1,
             CPZ,
             CP2,
             CP4,
             CP6,
             TP8,
             TP10,
             P9,
             P7,
             P5,
             P3,
             P1,
             PZ,
             P2,
             P4,
             P6,
             P8,
             P10,
             PO9,
             PO7,
             PO3,
             POz,
             PO4,
             PO8,
             PO10,
             O1,
             Oz,
             O2,
             O9,
             O10,
             CB1,
             CB2,
             Iz
             
        /// CGX Dev Kit channels.
        case CH1, CH2,
             CH3,
             CH4,
             CH5,
             CH6,
             CH7,
             CH8

        /// Aux channels.
        case ExG1,
             ExG2,
             A1,
             A2

        /// Patch Channels.
        case ECG,
             respiration,
             PPG,
             SpO2,
             heartRate,
             GSR,
             temperature
             
        /// Unassigned channel.
        case none

        /// A channel's signal type.
        public enum SignalType { case EEG, ExG, ECG, bioimpedance, pulseWave, oxygenation, heartRate, skinImpedance, temperature, none }
        
        /// The channel's name.
        public var name: String {
            switch self {
            case .FP1:          return "FP1"
            case .FPZ:          return "FPZ"
            case .FP2:          return "FP2"
            case .Nz:           return "Nz"
            case .AF9:          return "AF9"
            case .AF7:          return "AF7"
            case .AF3:          return "AF3"
            case .AFZ:          return "AFZ"
            case .AF4:          return "AF4"
            case .AF8:          return "AF8"
            case .AF10:         return "AF10"
            case .F9:           return "F9"
            case .F7:           return "F7"
            case .F5:           return "F5"
            case .F3:           return "F3"
            case .F1:           return "F1"
            case .FZ:           return "FZ"
            case .F2:           return "F2"
            case .F4:           return "F4"
            case .F6:           return "F6"
            case .F8:           return "F8"
            case .F10:          return "F10"
            case .FT9:          return "FT9"
            case .FT7:          return "FT7"
            case .FC5:          return "FC5"
            case .FC3:          return "FC3"
            case .FC1:          return "FC1"
            case .FCz:          return "FCz"
            case .FC2:          return "FC2"
            case .FC4:          return "FC4"
            case .FC6:          return "FC6"
            case .FT8:          return "FT8"
            case .FT10:         return "FT10"
            case .T9:           return "T9"
            case .T7:           return "T7"
            case .T3:           return "T3"
            case .C5:           return "C5"
            case .C3:           return "C3"
            case .C1:           return "C1"
            case .Cz:           return "Cz"
            case .C2:           return "C2"
            case .C4:           return "C4"
            case .C6:           return "C6"
            case .T4:           return "T4"
            case .T8:           return "T8"
            case .T10:          return "T10"
            case .TP9:          return "TP9"
            case .TP7:          return "TP7"
            case .CP5:          return "CP5"
            case .CP3:          return "CP3"
            case .CP1:          return "CP1"
            case .CPZ:          return "CPZ"
            case .CP2:          return "CP2"
            case .CP4:          return "CP4"
            case .CP6:          return "CP6"
            case .TP8:          return "TP8"
            case .TP10:         return "TP10"
            case .P9:           return "P9"
            case .P7:           return "P7"
            case .P5:           return "P5"
            case .P3:           return "P3"
            case .P1:           return "P1"
            case .PZ:           return "PZ"
            case .P2:           return "P2"
            case .P4:           return "P4"
            case .P6:           return "P6"
            case .P8:           return "P8"
            case .P10:          return "P10"
            case .PO9:          return "PO9"
            case .PO7:          return "PO7"
            case .PO3:          return "PO3"
            case .POz:          return "POz"
            case .PO4:          return "PO4"
            case .PO8:          return "PO8"
            case .PO10:         return "PO10"
            case .O1:           return "O1"
            case .Oz:           return "Oz"
            case .O2:           return "O2"
            case .O9:           return "O9"
            case .O10:          return "O10"
            case .CB1:          return "CB1"
            case .CB2:          return "CB2"
            case .Iz:           return "Iz"

            case .ExG1:         return "ExG1"
            case .ExG2:         return "ExG2"
            case .A1:           return "A1"
            case .A2:           return "A2"

            case .CH1:          return "CH1"
            case .CH2:          return "CH2"
            case .CH3:          return "CH3"
            case .CH4:          return "CH4"
            case .CH5:          return "CH5"
            case .CH6:          return "CH6"
            case .CH7:          return "CH7"
            case .CH8:          return "CH8"

            case .ECG:          return "ECG"
            case .respiration:  return "Respiration"
            case .PPG:          return "PPG"
            case .SpO2:         return "SpO2"
            case .heartRate:    return "Heart Rate"
            case .GSR:          return "GSR"
            case .temperature:  return "Temperature"
                
            case .none:         return "none"
            }
        }

        /// All possible channels.
        public static var allCases: [Self] {
            return [.FP1,
                    .FPZ,
                    .FP2,
                    .Nz,
                    .AF9,
                    .AF7,
                    .AF3,
                    .AFZ,
                    .AF4,
                    .AF8,
                    .AF10,
                    .F9,
                    .F7,
                    .F5,
                    .F3,
                    .F1,
                    .FZ,
                    .F2,
                    .F4,
                    .F6,
                    .F8,
                    .F10,
                    .FT9,
                    .FT7,
                    .FC5,
                    .FC3,
                    .FC1,
                    .FCz,
                    .FC2,
                    .FC4,
                    .FC6,
                    .FT8,
                    .FT10,
                    .T9,
                    .T7,
                    .T3,
                    .C5,
                    .C3,
                    .C1,
                    .Cz,
                    .C2,
                    .C4,
                    .C6,
                    .T4,
                    .T8,
                    .T10,
                    .TP9,
                    .TP7,
                    .CP5,
                    .CP3,
                    .CP1,
                    .CPZ,
                    .CP2,
                    .CP4,
                    .CP6,
                    .TP8,
                    .TP10,
                    .P9,
                    .P7,
                    .P5,
                    .P3,
                    .P1,
                    .PZ,
                    .P2,
                    .P4,
                    .P6,
                    .P8,
                    .P10,
                    .PO9,
                    .PO7,
                    .PO3,
                    .POz,
                    .PO4,
                    .PO8,
                    .PO10,
                    .O1,
                    .Oz,
                    .O2,
                    .O9,
                    .O10,
                    .CB1,
                    .CB2,
                    .Iz]
        }

        /// A channel's signal type.
        public var signalType: SignalType {
            switch self {
            case .FP1,
                 .FPZ,
                 .FP2,
                 .Nz,
                 .AF9,
                 .AF7,
                 .AF3,
                 .AFZ,
                 .AF4,
                 .AF8,
                 .AF10,
                 .F9,
                 .F7,
                 .F5,
                 .F3,
                 .F1,
                 .FZ,
                 .F2,
                 .F4,
                 .F6,
                 .F8,
                 .F10,
                 .FT9,
                 .FT7,
                 .FC5,
                 .FC3,
                 .FC1,
                 .FCz,
                 .FC2,
                 .FC4,
                 .FC6,
                 .FT8,
                 .FT10,
                 .T9,
                 .T7,
                 .T3,
                 .C5,
                 .C3,
                 .C1,
                 .Cz,
                 .C2,
                 .C4,
                 .C6,
                 .T4,
                 .T8,
                 .T10,
                 .TP9,
                 .TP7,
                 .CP5,
                 .CP3,
                 .CP1,
                 .CPZ,
                 .CP2,
                 .CP4,
                 .CP6,
                 .TP8,
                 .TP10,
                 .P9,
                 .P7,
                 .P5,
                 .P3,
                 .P1,
                 .PZ,
                 .P2,
                 .P4,
                 .P6,
                 .P8,
                 .P10,
                 .PO9,
                 .PO7,
                 .PO3,
                 .POz,
                 .PO4,
                 .PO8,
                 .PO10,
                 .O1,
                 .Oz,
                 .O2,
                 .O9,
                 .O10,
                 .CB1,
                 .CB2,
                 .Iz:
                return .EEG

            case .ExG1,
                 .ExG2,
                 .CH1,
                 .CH2,
                 .CH3,
                 .CH4,
                 .CH5,
                 .CH6,
                 .CH7,
                 .CH8:
                return .ExG
                
            case .A1,
                 .A2:
                return .EEG

            case .ECG:
                return .ECG
                
            case .respiration:
                return .bioimpedance
                
            case .PPG:
                return .pulseWave
                
            case .SpO2:
                return .oxygenation
                
            case .heartRate:
                return .heartRate
                
            case .GSR:
                return .skinImpedance
                
            case .temperature:
                return .temperature
                
            case .none:
                return .none
            }
        }
        
    }

}

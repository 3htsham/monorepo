//
//  CGXHeadband.swift
//  PEER
//
//  Created by Mark Alldritt on 2020-03-02.
//  Copyright © 2020 Mark Alldritt. All rights reserved.
//

#if os(iOS)
import UIKit
#endif
#if os(watchOS)
import WatchKit
#endif
#if os(macOS)
import Cocoa
#endif
import CoreBluetooth


//  Bluetooth UUID constants

let SerialServiceUUIDString = "2456e1b9-26e2-8f83-e744-f34f01e9d701"
let SerialCharacteristicUUIDString = "2456e1b9-26e2-8f83-e744-f34f01e9d703"

//  CGX UUID constants

let CGXServiceUUIDString = SerialServiceUUIDString // Cognionics Devices, which are Bluetooth serial devices

let CGXImpedanceCheckOn = 0x11
let CGXImpedanceCheckOff = 0x12


extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}


fileprivate struct CGXWeakDelegate {
    weak var delegate: CGXSampleDelegate?
    
    init(_ delegate: CGXSampleDelegate) {
        self.delegate = delegate
    }
}


fileprivate struct CGXWeakDelegateArray {
    private var items: [CGXWeakDelegate] = []

    init() {}
    init(_ elements: [CGXSampleDelegate]) {
        items = elements.map { CGXWeakDelegate($0) }
    }
    
    public mutating func clean() {
        items = items.filter { (item) in
            return item.delegate != nil
        }
    }
    
    public mutating func append(_ listener: CGXSampleDelegate) {
        remove(listener) // make sure we don't have duplicates
        items.append(CGXWeakDelegate(listener))
    }
    
    public mutating func remove(_ listener: CGXSampleDelegate) {
        items = items.filter { (item) in
            return item.delegate !== listener
        }
    }
}


extension CGXWeakDelegateArray: Collection {
    public var startIndex: Int { return items.startIndex }
    public var endIndex: Int { return items.endIndex }

    public subscript(_ index: Int) -> CGXSampleDelegate? {
        return items[index].delegate
    }

    public func index(after idx: Int) -> Int {
        return items.index(after: idx)
    }
}



extension Date {
    
    static var usecTimestamp : UInt64 {
        return UInt64(Date().timeIntervalSince1970 * Double(USEC_PER_SEC) /* convert to Microseconds */)
    }

}

/// A discovered CGX device
public class CGXHeadband : NSObject, CBPeripheralDelegate /*, Hashable */ {
    /// The data type of EEG and Accelerometer samples.
    public typealias SampleType = Double

    #if os(macOS)
    /// A cross-platform Image type.
    public typealias Image = NSImage
    #else
    /// A cross-platform Image type.
    public typealias Image = UIImage
    #endif

    /// EEG sample voltage units
    public enum EEGUnits {
        /// return EEG samples in volts.
        case volts
        /// return EEG samples in microvolts.
        case microVolts
    }

    /// CGXHeadband connected state.
    public enum HeadbandState {
        
        /// The headband is connected and is streaming data
        case connected
        /// The headband is in the process of connecting, data is not yet streaming
        case connecting
        /// The headband is not connected
        case disconnected
        /// The headband is in the process of disconnecting, data is no longer streaming
        case disconnecting
        
    }

    /// This notification is broadcast when the headband's name property changes.
    public static let NameChangeNotification = Notification.Name("Headband.nameChanged")
    /// This notification is broadcast when the headband's `model` property changes.
    public static let TypeChangeNotification = Notification.Name("Headband.typeChanged")
    /// This notification is broadcast when the headband's connected state (`state` property) changes.
    public static let StateChangeNotification = Notification.Name("Headband.stateChanged")
    /// This notification is broadcast when the headband's `rssi` property changes
    public static let RSSIChangeNotification = Notification.Name("Headband.rssiChanged")
    /// This notification is broadcast when the headband's `batteryv` property changes.
    public static let BatteryLevelChangeNotification = Notification.Name("Headband.batteryChanged")
    /// This notification is broadcast when the headband's `droppedSampleCount` property  changes.
    public static let DroppedSamplesNotification = Notification.Name("Headband.droppedSamplesChanged")
    /// This notification is broadcast when the headband's `model.eegChannels` property  changes.
    public static let ChannelsChangeNotification = Notification.Name("Headband.channelsChanged")
    /// This notification is broadcast when the headband's `reportImpedance` property  changes.
    public static let ReportImpedanceChangeNotification = Notification.Name("Headband.reportImpedanceChanged")

    static let SerialServiceUUID = CBUUID(string: SerialServiceUUIDString)
    static let SerialCharacteristicUUID = CBUUID(string: SerialCharacteristicUUIDString)
    
    private static let DeviceLostInterval = TimeInterval(10)
    private static let BatteryChangeInterval = TimeInterval(120) // 2 minutes
    private static let DroppedSamplesChangeInterval = TimeInterval(1.0 / 2.0)
    private static let ConnectInterval = TimeInterval(10)
    private static let RSSIReadInterval = TimeInterval(5)

    /// Default EEG sample voltage units for newly discovered devices
    public static var defaultEEGUnits = EEGUnits.volts
    /// EEG sample voltage units.
    public var eegUnits = CGXHeadband.defaultEEGUnits

    private let centralManager: CBCentralManager
    private let peripheral: CBPeripheral
    private var cachedModel: CGXHeadbandType?
    private var cachedAssignedName: String?
    private var lastBatteryChange = Date.distantPast
    private var lastDroppedSamplesChange = Date.distantPast
    private var listeners = CGXWeakDelegateArray()
    var lastSeen: Date
    /// Current RSSI value (in db)
    public var rssi: Int {
        didSet {
            if rssi != oldValue {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Self.RSSIChangeNotification, object: self)
                }
            }
        }
    }
    
    /// The headband's current connected state.
    public var state: HeadbandState {
        #if os(iOS)
        guard deviceTypeController == nil else { return .connecting }
        #endif
        
        switch peripheral.state {
        case .connected:
            return .connected
            
        case .connecting:
            return .connecting
            
        case .disconnected:
            return .disconnected
            
        case .disconnecting:
            return .disconnecting
            
        @unknown default:
            fatalError()
        }
    }
    /// The device name.  If there is an assigned name,  that is returned, otherwise the `deviceName` is returned.
    public var name: String {
        return cachedAssignedName ?? deviceName
    }
    /// The CGX device name (e.g. "CGX Quick-20r v2").
    public var deviceName: String {
        return peripheral.name ?? "unknown"
    }
    /// A user assigned name for the device.  Note that this setting is persistent.
    public var assignedName: String? {
        //  The assigned name is a user specified.  Labs may want to attach an identifier to each device they own,
        //  and this setting allows that identifier to be reflected in the software.
        set {
            if newValue != cachedAssignedName {
                //  Record the device type for this headband
                if let newValue = newValue, newValue != deviceName {
                    UserDefaults.standard.set(newValue, forKey: "CGXHeadband.name.\(identifier.uuidString)")
                    cachedAssignedName = newValue
                }
                else {
                    UserDefaults.standard.removeObject(forKey: "CGXHeadband.name.\(identifier.uuidString)")
                    cachedAssignedName = nil
                }
                noteThisHeadband()
                UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: Self.TypeChangeNotification, object: self)
            }
        }
        get {
            return cachedAssignedName
        }
    }
    /// The [UUID](https://developer.apple.com/documentation/foundation/uuid) associated with this device.
    ///
    /// - Note: This identifier is stable and can be used to identify a device across app launches.
    public var identifier: UUID {
        return peripheral.identifier
    }
    /// The model of this CGX device.
    ///
    /// The `CGXHeadbandType` enumeration value returned allows you to introspect the headband's capabilities.
    ///
    /// - Note: If the value is `unknown`  the client app must set this value before attempting to connect.  See the
    /// `CGXHeadbandPickerType` for more information.
    ///
    /// - SeeAlso: CGXHeadbandPickerType
    public var model: CGXHeadbandType {
        get {
            return cachedModel ?? .unknown
        }
        set {
            guard newValue != model else { return }
            guard model.isChangable else { print("API MISUSE: CGXHeadband model cannot be changed"); return }

            //  Record the device type for this headband
            UserDefaults.standard.set(newValue.json, forKey: "CGXHeadband.type.\(identifier.uuidString)")
            noteThisHeadband()
            cachedModel = newValue
            if state == .connected {
                resetConnection()
            }
            NotificationCenter.default.post(name: Self.TypeChangeNotification, object: self)
        }
    }
    /// The device's report impedance state.
    ///
    /// Setting this value directs the device to change it's impedance reporting.  If the device is connected, the change is sent immediately.  If the device is disconnected, the change will be sent when the device connects.
    ///
    /// - Note: There may be a delay between changing this property and a corresponding change in the `impedanceCheck` value
    /// reported in streamed samples due to Bluetooth buffering.
    /// - Precondition: The headband's `canSetReportImpedance` property value must be `true` for changes to this property to have an effect.
    /// - Requires: The headband's model must be something other than `unknown`.  Setting this property when the headband's `model` property is `unknown` causes a fatal API misuse error.
    public var reportImpedance = true {
        didSet {
            if reportImpedance != oldValue && model.providesEEGQualityData {
                switch model {
                case .cgxQuick20r,
                     .cgxQuick20rv2,
                     .cgxQuick20m,
                     .cgxQuick32r,
                     .cgxQuick32m,
                     .cgxPatch,
                     .cgxOptios,
                     .cgxDevKit:
                    if serialPort != nil {
                        //  Convert the impedance check control value to a unicode character
                        let c = Character(Unicode.Scalar(reportImpedance ? CGXImpedanceCheckOn : CGXImpedanceCheckOff)!)
                    
                        write("\(c)")
                        
                        //  NOTE: I've found that it takes a while for the effect of sending this command to the headband
                        //  to be felt in the data returned from the headband.  I presume there is buffering going on and
                        //  older data must flow out of the headband before newer data representing this command begins to
                        //  appear.
                    }
                    NotificationCenter.default.post(name: Self.ReportImpedanceChangeNotification, object: self)
                    break

                case .unknown:
                    fatalError("API MISUSE - cannot set impedance reporting for \(model.description)")
                    break
                }
            }
        }
    }
    /// Can this headband's `reportImpedance` property be changed?
    ///
    /// When the value of this property is false, setting this headband's `reportImpedance` property has no effect.
    public var canSetReportImpedance: Bool {
        switch model {
        case .cgxQuick20r,
             .cgxQuick20rv2,
             .cgxQuick20m,
             .cgxQuick32r,
             .cgxQuick32m,
             .cgxPatch,
             .cgxOptios,
             .cgxDevKit:
            return true
              
        case .unknown:
            return false
        }
    }

    private let eegVScale = (5.0 / 3.0) / 4294967296.0 // Volts
    private let accVScale = 2.5 * (Double(1) / pow(2, 32))
    private let batteryVScale = Double(5) / Double(128)
    private let minRateSampleCount = 50

    private var lastState = HeadbandState.disconnected
    private var serialPort: CBCharacteristic?
    private var residue: Data?
    private var lastBlockCounter: UInt8?
    /// The time when the headband began streaming data.
    ///
    /// - Precondition: the headband must be connected in order for this property be non-nil
    public private (set) var started: Date?
    #if os(iOS)
    private var deviceTypeController: UIViewController?
    #endif
    private var connectDate = Date.distantPast
    private var rssiTimer: Timer?
    /// The total number of samples received since the device was connected.
    public private (set) var sampleCount = 0
    /// The total number of samples dropped since the device was connected.
    ///
    /// The `DroppedSamplesNotification` is posted at intervals whenever this property changes.
    public private (set) var droppedSampleCount = 0 {
        didSet {
            let now = Date()
            if droppedSampleCount != oldValue && now.timeIntervalSince(lastDroppedSamplesChange) >= Self.DroppedSamplesChangeInterval {
                lastDroppedSamplesChange = now
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Self.DroppedSamplesNotification, object: self)
                }
            }
        }
    }
    /// The observed sample range (Hz) since the device was connected.
    public private (set) var rate = 0 // hz
    /// The device's current battery voltage.  Use the headband's `model.mapBatteryVoltageToPercent` function to convert this value into a percentage full.
    ///
    /// Precondition: The headband must be connected in order to begin receiving battery voltages.  The value will be 0 when not connected.
    public private (set) var batteryv = Double(0) {
        didSet {
            let now = Date()
            if batteryv != oldValue && (oldValue == 0 || now.timeIntervalSince(lastBatteryChange) >= Self.BatteryChangeInterval) {
                lastBatteryChange = now
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Self.BatteryLevelChangeNotification, object: self)
                }
            }
        }
    }
    public private (set) var observedChannels: Int? {
        didSet {
            if observedChannels != oldValue {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Self.ChannelsChangeNotification, object: self)
                }
            }
        }
    }
    public private (set) var sample: CGXSample?
    private var impedanceSamples = RingBuffer<[CGXHeadband.SampleType]?>(repeating: nil, count: 3)
    private var dataLock = NSLock()

    init?(centralManager: CBCentralManager, peripheral: CBPeripheral, rssi: Int) {
        self.centralManager = centralManager
        self.peripheral = peripheral
        self.lastSeen = Date()
        self.rssi = rssi
        
        //  If we've seen this device before, restore the device type (in case there are channel assignments, etc.)
        let typePrefsKey = "CGXHeadband.type.\(peripheral.identifier.uuidString)"
        if let rawType = UserDefaults.standard.string(forKey: typePrefsKey) {
            self.cachedModel = CGXHeadbandType(json: rawType)
        }
        else {
            //  Figure out the model of CGX device we have based on the device name.  If there is no match, device
            //  model defaults to .unknown and the is asked to choose a device type when we connect.
            let deviceName = (peripheral.name ?? "unknown").lowercased()
            if let deviceModel = CGXHeadbandType.bluetoothPrefixes.first(where: { (prefix: String, model: CGXHeadbandType) in
                return deviceName.hasPrefix(prefix.lowercased())
            })?.model {
                self.cachedModel = deviceModel
            }
            else {
                return nil
            }
        }

        //  Recover the user-assigned device name if there is one...
        self.cachedAssignedName = UserDefaults.standard.string(forKey: "CGXHeadband.name.\(peripheral.identifier.uuidString)")
    }
    
    deinit {
        disconnect()
        #if os(iOS)
        deviceTypeController?.dismiss(animated: true, completion: nil)
        deviceTypeController = nil
        #endif
    }
    
    /// Connect the CGX device and begin streaming EEG data.
    ///
    /// Once you call this function, listen for the `StateChangeNotification`and quierty the headband's `state` property to learn if
    /// the device was successfully connected.
    ///
    /// - Note: Devices may spontaniously disconnect if they are turned off or there are Bluetooth connectivity issues.  listen for the
    /// `StateChangeNotification` and quierty the headband's `state` property to learn if the device has disconnected.
    public func connect() {
        guard state == .disconnected || state == .disconnecting else { return }
        
        if model == .unknown {
            //  This is a device where we need the user's help to figure out what model of device we are
            //  dealing with.  This is currently older CGX devices with a generic BlueTooth device name.
            #if os(iOS)
            guard let visibleViewController = UIApplication.shared.visibleViewController else {
                print("CGXKit API MISUSE: cannot get visibleViewController")
                self.lastState = .connecting
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.broadcastStateChange()
                }
                return
            }

            deviceTypeController = establishDeviceType(presentingViewController: visibleViewController,
                                                       completion: { (success) in
                                                        guard self.deviceTypeController != nil else { return } // Skip if this alert has been dismissed (see disconnect())
                                                        self.deviceTypeController = nil
                                                        
                                                        if success {
                                                            self.connectPart2()
                                                        }
                                                        else {
                                                            self.broadcastStateChange()
                                                        }
            })
            broadcastStateChange() // setting deviceTypeController has the side-effect of making the state == .connecting
            #elseif os(macOS)
            establishDeviceType(completion: { (success) in
                if success {
                    self.connectPart2()
                }
                else {
                    self.broadcastStateChange()
                }
            })
            #else
            //  NOTE: SwiftUI clients are responsible for estalishing a device type **before** calling connect()
            fatalError("CGXKit API MISUSE: device type unknown")
            #endif
        }
        else {
            connectPart2()
        }
    }

    /// Disconnect the CGX device.
    ///
    /// Once you call this function, listen for the `StateChangeNotification`and quierty the headband's `state` property to learn when the device is connected.
    public func disconnect() {
        rssiTimer?.invalidate()
        rssiTimer = nil
        guard state == .connected || state == .connecting else { return }
        
        #if os(iOS)
        deviceTypeController?.dismiss(animated: true, completion: nil)
        deviceTypeController = nil
        #endif

        centralManager.cancelPeripheralConnection(peripheral)
        lastSeen = Date()
        broadcastStateChange()
    }
        
    func broadcastStateChange() {
        assert(Thread.isMainThread)
        
        if state != lastState {
            if state == .connected {
                peripheral.discoverServices([Self.SerialServiceUUID])
            }
            //print("broadcastStateChange: \(peripheral)")
            lastState = state
            NotificationCenter.default.post(name: Self.StateChangeNotification, object: self)
        }
    }
    
    func isLost(_ now: Date) -> Bool {
        if peripheral.state == .connecting && Date().timeIntervalSince(connectDate) >= Self.ConnectInterval {
            disconnect()
        }
        return peripheral.state == .disconnected && lastSeen.addingTimeInterval(Self.DeviceLostInterval) < now
    }
    
    /// Register a EEG data listener.
    ///
    /// Once registered, the listener will begin receiving EEG samples when the headband is connected.
    ///
    /// - Parameter listener: A listener implementing the `CGXSampleDelegate` protocol.
    ///
    /// - Note: Samples are delivered on a background thread.
    /// - Important: The headband retains a **weak** reference to the listener.  Make sure you maintain your own strong reference to the lister to avoid premature deallocation.
    ///
    /// A listener is implemented as follows:
    ///
    /// ```
    /// class MyListener: CGXSampleDelegate {
    ///     ...
    ///
    ///     func receive(sample: CGXSample) { // receive a CGX sample
    ///         ...
    ///     }
    ///
    ///     func receive(dropped: Int, sample: CGXSample)  { // receive a count of dropped frames
    ///         ...
    ///     }
    /// }
    /// ```
    public func registerDataListener(_ listener: CGXSampleDelegate) {
        listeners.clean()
        listeners.append(listener)
    }
    
    /// Remove a registered EEG data listener.
    ///
    /// - Parameter listener: A previously registered listener implementing the `CGXSampleDelegate` protocol.
    public func unregisterDataListener(_ listener: CGXSampleDelegate) {
        listeners.remove(listener)
    }

    private func write(_ message: String) {
        guard let data = message.data(using: .utf8) else { fatalError("CGXKit cannot convert '\(message)' to data") }

        write(data)
    }
    
    private func write(_ data: Data) {
        guard let serialPort = serialPort else { fatalError("CGXKit no serial port available") }
        
        #if true
        peripheral.writeValue(data, for: serialPort, type: .withoutResponse)
        #else
        peripheral.writeValue(data, for: serialPort, type: .withResponse)
        #endif
    }
    
    private func resetConnection() {
        self.dataLock.lock()
        defer {
            self.dataLock.unlock()
        }

        lastSeen = Date()
        observedChannels = nil
        started = nil
        lastBlockCounter = nil // nil means we are starting
        sampleCount = 0
        droppedSampleCount = 0
        lastDroppedSamplesChange = Date.distantPast
        batteryv = 0
        lastBatteryChange = Date.distantPast
        sample = nil
        impedanceSamples.clear(repeating: nil)
    }
    
    private func connectPart2() {
        rssiTimer = Timer.scheduledTimer(withTimeInterval: Self.RSSIReadInterval,
                                         repeats: true,
                                         block: { [weak self] (_) in
                                            self?.peripheral.readRSSI()
        })
        peripheral.delegate = self
        resetConnection()
        connectDate = Date()
        residue = nil
        centralManager.connect(peripheral, options: [CBConnectPeripheralOptionNotifyOnDisconnectionKey:1])
        broadcastStateChange()
    }
    
    private func noteThisHeadband() {
        //  Update the list of device UUIDs for ehich we've added a type
        if let knownDeviceUUIDs = UserDefaults.standard.object(forKey: "CGXHeadband.uuids") as? [String] {
            if !knownDeviceUUIDs.contains(identifier.uuidString) {
                var deviceUUIDs = Array<String>(knownDeviceUUIDs)
                
                deviceUUIDs.append(identifier.uuidString)
                UserDefaults.standard.set(deviceUUIDs, forKey: "CGXHeadband.uuids")
            }
        }
        else {
            let deviceUUIDs = [identifier.uuidString]
            
            UserDefaults.standard.set(deviceUUIDs, forKey: "CGXHeadband.uuids")
        }
        UserDefaults.standard.synchronize()
    }

    //  MARK: - CBPeripheralDelegate
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        #if DEBUG
        print("CGXKit didDiscoverServices: \(String(describing: peripheral.services))")
        #endif
        
        if let service = peripheral.services?.first(where: { (service) -> Bool in
            return service.uuid == Self.SerialServiceUUID
        }) {
            peripheral.discoverCharacteristics([Self.SerialCharacteristicUUID], for: service)
        }
    }
        
    public func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
        #if DEBUG
        print("CGXKit peripheralDidUpdateName: \(String(describing: peripheral.name))")
        #endif

        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Self.NameChangeNotification, object: self)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        #if DEBUG
        print("CGXKit didReadRSSI: \(RSSI)")
        #endif
        rssi = RSSI.intValue
    }
        
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        #if DEBUG
        print("CGXKit didDiscoverCharacteristicsFor: \(service), \(String(describing: service.characteristics))")
        #endif

        if let serialPort = service.characteristics?.first(where: { (characteristic) -> Bool in
            return characteristic.uuid == Self.SerialCharacteristicUUID
        }) {
            self.serialPort = serialPort
            peripheral.setNotifyValue(true, for: serialPort)
            
            //  Set impedance reporting as per reportImpedance variable
            let c = Character(Unicode.Scalar(reportImpedance ? CGXImpedanceCheckOn : CGXImpedanceCheckOff)!)
            
            write("\(c)")
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
        #if DEBUG
        print("CGXKit didDiscoverIncludedServicesFor: \(service)")
        #endif
    }
    
    private func processBlock(_ block: Data, start: Int) -> (Int /* bytes processed */, Bool /* consumed */) {
        var end = start + 1
        var blockConsumed = true
        let expectedBlockSize: Int

        if let channels = observedChannels {
            switch model {
            case .cgxPatch:
                //  Patch device reports fewer channels than the data block provides.
                expectedBlockSize = (8 * 3) + 4 + 2 // 2x EEG + 6 biometric channels

            default:
                expectedBlockSize = (channels + 3) * 3 + 4 + 2
            }
        }
        else {
            //  First sample...
            started = Date()
            expectedBlockSize = 0
        }
        
        //  Deal with the posibility that the impedance value has "railed" because the reference and/or
        //  gound connections have been lost.  In this case, the impedance drops to near 0.
        //
        //  TODO - as it turns out, I misread Mike's instructions.  Its the EEG voltages that get railed and so I
        //  need to approach this in a different way.  I've set the minimum impedance to 10 to get things going for
        //  the tie being.
        let minimumImpedance = Double(10) // just a guess for now
        let maximumImpedance = Double(CGXHeadbandType.maxOhm + CGXHeadbandType.headroomOhm) // 2Mohms + headroom

        block.withUnsafeBytes { (p) in
            var ffFound = false
            while (end < block.count) {
                if p[end] == 0xff {
                    ffFound = true
                    break
                }
                end += 1
            }
            
            if (expectedBlockSize > 0 && (end - start) != expectedBlockSize) {
                #if false
                if (end - start) < expectedBlockSize {
                    print("SMALL - ffFound: \(ffFound), start: \(start), end - start: \(end - start)")
                }
                else {
                    print("LARGE - ffFound: \(ffFound), start: \(start), end - start: \(end - start)")
                }
                #endif
                blockConsumed = ffFound
                return
            }
            
            let channels = (end - start - (2 + 4)) / 3
            let blockCounter = UInt8(p[start + 1])
            let impStatus = UInt8(p[end - 4])
            let batteryByte = UInt8(p[end - 3])
            let trigger = Int(p[end - 2] << 8 + p[end - 1])
            let numACCChannels = 3
            let numEEGChannels: Int
                
            switch model {
            case .cgxPatch:
                numEEGChannels = CGXHeadbandType.cgxPatchChannels.count
                
            case .cgxOptios:
                numEEGChannels = CGXHeadbandType.cgxOptiosChannels.count
                
            default:
                numEEGChannels = channels - numACCChannels
            }
            
            if numEEGChannels <= 0 {
                return
            }
            
            self.sampleCount += 1
            if self.observedChannels == nil {
                self.observedChannels = numEEGChannels
            }

            if let lastBlockCounter = self.lastBlockCounter, model != .cgxPatch {
                //  See if we've missed any blocks
                let nextBlockCounter = UInt8(lastBlockCounter >= 0x7f ? 0 : lastBlockCounter + 1)
                
                if blockCounter != nextBlockCounter {
                    let droppedSamples: Int
                    if blockCounter > nextBlockCounter {
                        droppedSamples = Int(blockCounter - nextBlockCounter)
                    }
                    else {
                        droppedSamples = Int(blockCounter + (0x7f - nextBlockCounter))
                    }

                    self.droppedSampleCount += droppedSamples
                    self.sampleCount += droppedSamples
                    
                    //  Callbacks for dropped sample data...
                    let nanData = Array<Double>(repeating: Double.nan, count: numEEGChannels)
                    let sample = CGXSample(headband: self,
                                           timestamp: Date.usecTimestamp,
                                           eegV: nanData,
                                           impedanceCheck: false,
                                           impedance: nanData,
                                           accelX: Double.nan,
                                           accelY: Double.nan,
                                           accelZ: Double.nan,
                                           trigger: 0)

                    for listener in listeners {
                        listener?.receive(dropped: droppedSamples, sample: sample)
                    }
                }
            }
            self.lastBlockCounter = blockCounter

            if self.sampleCount > minRateSampleCount {
                //  Observed sample rate
                self.rate = Int(Double(self.sampleCount) / Date().timeIntervalSince(self.started!))
            }

            let q3 = self.impedanceSamples.count >= 3 ? self.impedanceSamples[2] : nil
            let q2 = self.impedanceSamples.count >= 2 ? self.impedanceSamples[1] : nil
            let q1 = self.impedanceSamples.count >= 1 ? self.impedanceSamples[0] : nil
            var eegvData = Array<Double>(repeating: Double.nan, count: numEEGChannels)
            var impData = Array<Double>(repeating: Double.nan, count: numEEGChannels)
            for i in 0 ..< min(numEEGChannels, model.eegChannelCount) {
                let msb = UInt32(p[start + 2 + i * 3])
                let lsb2 = UInt32(p[start + 3 + i * 3])
                let lsb1 = UInt32(p[start + 4 + i * 3])
                
                let adu = (msb << 24) | (lsb2 << 17) | (lsb1 << 10) // recover 24bit int
                let eegv = Double(Int32(bitPattern: adu)) * eegVScale // convert to Volts
                
                eegvData[i] = eegv
                                     
                //  TODO: Add EEG Filtering

                if impStatus == CGXImpedanceCheckOn /* device is reporting impedance */ &&
                    q3 != nil /* we have at least 4 samples to work with */ &&
                    q3!.count > i /* in case device type has changed and we don't have anough channels */ {
                    let q3v = q3![i]
                    let q2v = q2![i]
                    let q1v = q1![i]
                    // NOTE: eegv would be sample 0: q0v
                    
                    #if false
                    //  From: http://cognionics.com/wiki/pmwiki.php/Main/CognionicsRawDataSpec
                    //  Abs( sample[0] – sample[2] )/2 Abs( sample[1] – sample[3] )/2
                    let impedance = max(abs(eegv - q2v) / 2, abs(q1v - q3v) / 2)
                            * self.type.impedanceMagnitudeScale
                    #else
                    //  Mikes new document also talks about impedances being incrrect when the reference is lost.  I'll have to make
                    //  this better, but for now I've observed that under these conditions that the impedance drops to 0.
                    //
                    //  I'm not using this for now as the values returned are different (~2x) and don't play well with the the
                    let impedance = sqrt(pow(eegv - q2v, 2) + pow(q1v - q3v, 2)) // From Mike's new document, page 12
                            * self.model.impedanceMagnitudeScale
                    #endif
                                        
                    impData[i] = min(impedance >= minimumImpedance ? impedance : maximumImpedance, maximumImpedance)
                    
                    #if false
                    if i == 0 {
                        print("ch\(i) eeg: \(eegv), \(q1v), \(q2v), \(q3v), impedance: \(impedance), avg: \(self.impData[i].average)")
                    }
                    #endif
                }
            }

            var accvData = Array<Double>(repeating: Double.nan, count: numACCChannels)
            for i in 0 ..< numACCChannels {
                let msb = Int(p[start + 2 + (i + numEEGChannels) * 3])
                let lsb2 = Int(p[start + 3 + (i + numEEGChannels) * 3])
                let lsb1 = Int(p[start + 4 + (i + numEEGChannels) * 3])
                
                let adc = (msb << 24) | (lsb2 << 17) | (lsb1 << 10)
                let accv = Double(adc) * accVScale
                
                accvData[i] = accv
            }
            
            #if DEBUG
            //print("eegvData: \(eegvData), impeadance: \(averageImpedances), accvData: \(accvData)")
            #endif

            let sample: CGXSample
            if eegUnits == .microVolts {
                sample = CGXSample(headband: self,
                                   timestamp: Date.usecTimestamp,
                                   eegV: eegvData.map { (eegV) in
                                        return eegV * 1000000 // convert volts to microVolts
                                   },
                                   impedanceCheck: impStatus == CGXImpedanceCheckOn,
                                   impedance: impData,
                                   accelX: accvData[0],
                                   accelY: accvData[1],
                                   accelZ: accvData[2],
                                   trigger: trigger)
            }
            else {
                sample = CGXSample(headband: self,
                                   timestamp: Date.usecTimestamp,
                                   eegV: eegvData,
                                   impedanceCheck: impStatus == CGXImpedanceCheckOn,
                                   impedance: impData,
                                   accelX: accvData[0],
                                   accelY: accvData[1],
                                   accelZ: accvData[2],
                                   trigger: trigger)
            }
            
            self.sample = sample
            if impStatus == CGXImpedanceCheckOn {
                self.impedanceSamples.append(eegvData)
            }
            self.batteryv = Double(batteryByte) * batteryVScale

            //  Callbacks for sample data...
            for listener in listeners {
                listener?.receive(sample: sample)
            }
        }
        
        return (end - start, blockConsumed)
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        #if DEBUG
        //print("didUpdateValueFor: \(characteristic)")
        #endif
        
        self.dataLock.lock()
        defer {
            self.dataLock.unlock()
        }

        if characteristic == serialPort, var data = characteristic.value {
            #if DEBUG
            //print(data.hexEncodedString())
            #endif
            
            var start = 0
            
            if let residue = residue {
                data.replaceSubrange(Range(NSMakeRange(0, 0))!, with: residue)
                self.residue = nil
            }
            
            while start < data.count {
                let (count, consumed) = processBlock(data, start: start)
                
                if consumed {
                    start += count
                }
                else {
                    residue = data.subdata(in: Range(NSMakeRange(start, data.count - start))!)
                    break
                }
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        #if DEBUG
        print("CGXKit didWriteValueFor: \(characteristic), error: \(String(describing: error))")
        #endif
    }
    
    //  Mark: - Equitable

    public static func == (lhs: CGXHeadband, rhs: CGXHeadband) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}


extension CGXHeadband: Identifiable {
}

//
//  CGXHeadbandManager.swift
//  CGXKit
//
//  Created by Mark Alldritt on 2020-03-17.
//

#if os(iOS)
import UIKit
#endif
import CoreBluetooth

/// A singleton class which manages Bluetooth access and CGX device acquisition.
///
/// - Note: Access the singleton `CGXHeadbandManager` instance via its `shared` property.
///
/// - Attention: The `CGXHeadbandManager` must be started and Bluetooth must be powered on and authorized before CGX device acquisition beings:
///
/// ```
/// CGXHeadbandManager.shared.start()
/// ```
///
/// - Attention: The following `Info.plist` keys must be defined in order for Bluetooth access to be permitted:
///
///   - `NSBluetoothAlwaysUsageDescription`
///   - `NSBluetoothPeripheralUsageDescription`
public class CGXHeadbandManager: NSObject {
    
    /// A notification posted when the value of the `devices`, `uuids` and `headbands` properties change.
    ///
    /// Listening for this notification allows applications to update their display of discovered CGX devices.
    ///
    /// UIKit Example:
    /// ```
    /// NotificationCenter.default.addObserver(forName: EEGHeadbandManager.DevicesChangedNotification,
    ///                                        object: EEGHeadbandManager.shared,
    ///                                        queue: nil) { [weak self] (note) in
    ///     self?.updateUIForHeadbands(EEGHeadbandManager.shared.headbands)
    /// }
    /// ```
    ///
    /// SwiftUI Example:
    ///
    /// ```
    /// struct BluetoothViewView<Content: View> : View {
    ///     //  This view monitors Bluetooth state.  Once Bluetooth becomes available, CGX device acquisition begins.
    ///
    ///     @State private var headbands = CGXHeadbandManager.shared.headbands
    ///
    ///     private let headbandsPublisher = NotificationCenter.default.publisher(for: CGXHeadbandManager.DevicesChangedNotification)
    ///
    ///     var body: some View {
    ///         ScrollView {
    ///             List(headbands) { (headband) in
    ///                 HeadbandView(headband) // display headband details
    ///             }
    ///         }
    ///         .onReceive(headbandsPublisher) { (output) in
    ///             bluetoothState = CGXHeadbandManager.shared.headbands
    ///         }
    ///     }
    /// }
    ///
    /// ```
    ///
    /// Precondition:
    /// The headband manager must be started in order for these notifications to be delivered.
    public static let DevicesChangedNotification = Notification.Name("CGXHeadbandManager.DevicesChangedNotification")
    /// A notification posted when the value of the `state` property changes.
    ///
    /// Listening for this notification allows applications to update their user interface in response to Bluetooth state changes.
    ///
    /// UIKit Example:
    /// ```
    /// NotificationCenter.default.addObserver(forName: EEGHeadbandManager.BluetoothStateChangedNotification,
    ///                                        object: EEGHeadbandManager.shared,
    ///                                        queue: nil) { [weak self] (_) in
    ///     self?.updateUIForBluetoothChange()
    /// }
    /// ```
    ///
    /// SwiftUI Example:
    ///
    /// ```
    /// struct BluetoothViewView<Content: View> : View {
    ///     //  This view monitors Bluetooth state.  Once Bluetooth becomes available, CGX device acquisition begins.
    ///
    ///     @State private var bluetoothState = CGXHeadbandManager.shared.state
    ///
    ///     private let bluetoothPublisher = NotificationCenter.default.publisher(for: CGXHeadbandManager.BluetoothStateChangedNotification)
    ///
    ///     var body: some View {
    ///         Group() {
    ///             switch bluetoothState {
    ///             case .poweredOff:
    ///                 BluetoothPoweredOffView()
    ///
    ///             case .unauthorized:
    ///                 BluetoothUnauthorizedView()
    ///
    ///             case .poweredOn:
    ///                 BluetoothPoweredOnView() // CGX device acquisition begins...
    ///
    ///             default:
    ///                 NoBluetoothView()
    ///             }
    ///        }
    ///        .onReceive(bluetoothPublisher) { (_) in
    ///            bluetoothState = CGXHeadbandManager.shared.state
    ///        }
    ///     }
    /// }
    ///
    /// ```
    ///
    /// Precondition:
    /// The headband manager must be started in order for these notifications to be delivered.
    public static let BluetoothStateChangedNotification = Notification.Name("CGXHeadbandManager.BluetoothStateChangedNotification")
    /// Indicates if the `CGXHeadbandManager` has been started and is scanning for CGX devices.
    public private (set) var isRunning = false
    /// A dictionary, keyed by UUID, of discovered CGX headbands.
    public private (set) var devices: [UUID:CGXHeadband] = [:] {
        didSet {
            if devices != oldValue {
                NotificationCenter.default.post(name: Self.DevicesChangedNotification, object: self)
            }
        }
    }
    /// An array of UUIDs associated with discovered CGX headbands.
    public var uuids: [UUID] {
        return devices.keys.sorted { (v1, v2) -> Bool in
            return v1.uuidString < v2.uuidString
        }
    }
    /// An array of discovered CGX headbands.
    public var headbands: [CGXHeadband] {
        return uuids.map { (uuid) in return self.devices[uuid]! }
    }
    /// The current Bluetooth state.
    ///
    /// The Bluetooth state must be `poweredOn` in order for CGS device discovery to begin.
    public var state: CBManagerState {
        return centralManager?.state ?? .unknown
    }
    
    private let queue = DispatchQueue(label: "CGX")
    private var centralManager: CBCentralManager!
    private var timer: Timer?
    
    /// The singleton `CGXHeadbandManager` instance.
    public static let shared = CGXHeadbandManager()
    
    override private init() {}
    
    deinit {
        stop()
    }
    
    /// Start listening for CGX devices.
    public func start() {
        guard !isRunning else { return }
        
        isRunning = true
        devices.removeAll()

        if centralManager == nil {
            centralManager = CBCentralManager(delegate: self, queue: queue)
        }
        else {
            centralManagerDidUpdateState(centralManager)
        }
    }
    
    /// Strop listening for CGX devices.
    public func stop() {
        guard isRunning else { return }
        
        isRunning = false
        devices.removeAll()

        timer?.invalidate()
        timer = nil
        centralManager.stopScan()
    }
    
    private func timerFired(_ timer: Timer) {
        assert(Thread.isMainThread)
        
        let now = Date()
        self.devices = self.devices.filter({ (arg0) -> Bool in
            let (_, headband) = arg0
            
            return !headband.isLost(now)
        })
    }
}


extension CGXHeadbandManager: CBCentralManagerDelegate {
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("CGXKit central.state is .unknown")
            break

        case .resetting:
            print("CGXKit central.state is .resetting")
            break

        case .unsupported:
            print("CGXKit central.state is .unsupported") // probably running in the iOS simulator
            break
        
        case .unauthorized:
            print("CGXKit central.state is .unauthorized") // user has declined to permit access to Bluetooth
            break

        case .poweredOff:
            print("CGXKit central.state is .poweredOff")
            if isRunning {
                print("  - no longer listening for devices")
                centralManager.stopScan()
                DispatchQueue.main.async {
                    self.timer?.invalidate()
                    self.timer = nil
                    self.devices.removeAll()
                }
            }
            break

        case .poweredOn:
            print("CGXKit central.state is .poweredOn")
            if isRunning {
                print("  - listening for devices...")
                centralManager.scanForPeripherals(withServices: [CBUUID(string: CGXServiceUUIDString)],
                                                  options: [CBCentralManagerScanOptionAllowDuplicatesKey:1])
                DispatchQueue.main.async {
                    self.timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: self.timerFired)
                }
            }
            break

        @unknown default:
            fatalError()
        }
        
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Self.BluetoothStateChangedNotification, object: self)
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        DispatchQueue.main.async {
            if let headband = self.devices[peripheral.identifier] {
                headband.lastSeen = Date()
                headband.rssi = RSSI.intValue
                headband.broadcastStateChange()
            }
            else {
                #if DEBUG
                print("CGXKit didDiscover: \(peripheral), advertisementData: \(advertisementData), rssi: \(RSSI), when: \(Date())")
                #endif
                
                if let device = CGXHeadband(centralManager: self.centralManager, peripheral: peripheral, rssi: RSSI.intValue) {
                    self.devices[peripheral.identifier] = device
                }
            }
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("CGXKit didConnect: \(peripheral)")
        
        DispatchQueue.main.async {
            guard let headband = self.devices[peripheral.identifier] else { return }
            
            headband.lastSeen = Date()
            headband.broadcastStateChange()
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        #if DEBUG
        print("CGXKit didFailToConnect: \(peripheral), error: \(String(describing: error))")
        #endif

        DispatchQueue.main.async {
            guard let headband = self.devices[peripheral.identifier] else { return }
            
            headband.lastSeen = Date()
            headband.broadcastStateChange()
        }
    }

    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        #if DEBUG
        print("CGXKit didDisconnectPeripheral: \(peripheral), error: \(String(describing: error))")
        #endif

        DispatchQueue.main.async {
            guard let headband = self.devices[peripheral.identifier] else { return }
            
            headband.lastSeen = Date()
            headband.broadcastStateChange()
        }
    }
}

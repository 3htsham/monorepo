//
//  CGXHeadbandType.swift
//  CGXKit
//
//  Created by Mark Alldritt on 2020-04-01.
//

#if os(iOS)
import UIKit
#endif
#if os(watchOS)
import WatchKit
#endif
#if os(macOS)
import Cocoa
#endif


//  The values (1st: 1478787878, then: 1443296969 ** this is the one I've gone with) are calculated by
//  comparing impedance values returned by the CGX Acquisition software on windows.  I tried my best to
//  maintain a common impedance - focused on channels where the observed impedance values were not
//  moving.  I calculated a scale value based on the difference between what I observed with the
//  3030303030 magnitude scale in PEER and the values reported by the CGX windows software.
//
//  For the Dev Kit I want through the same process.  The constant used (372463735) represents the
//  closest I could get to the values displayed in the CGX Acquisition software on windows.

let CGXQuick32rImpedanceMagnitudeScale = Double(1443296969) // Double(3030303030) // Double(265000000) //
let CGXQuick20rImpedanceMagnitudeScale = Double(1443296969) // Double(3030303030) // Double(265000000) //
let CGXPatchImpedanceMagnitudeScale = Double(1443296969) // Double(3030303030)
let CGXDevKitImpedanceMagnitudeScale = Double(372463735) // Double(432989091) // Double(1443296969) // Double(3030303030) //   TODO - make sure this is correct

/// A user selectable CGX headband model.
///
/// Certain older CGX devices carry a generic Bluetooth device name making it impossible for CGSKit to determine the type of CGX
/// device.  In this circumstance, the CGSKit client  must set the `CGXHeadband`'s `model` property appropriately.
///
/// This enumeration lists possible types of devices the user can choose from, and the `headbandType` property provides the value
/// that must be assigned to the `CGXHeadband`'s `model` property.
///
public enum CGXHeadbandPickerType: CaseIterable {
    case pickUnknown, pickCGXDevKit, pickCGXQuick20r, pickCGXQuick32r, pickCGXOptios
    
    public static let allCases: [CGXHeadbandPickerType] = [Self.pickCGXQuick32r, Self.pickCGXQuick20r, Self.pickCGXDevKit, Self.pickCGXOptios]
    
    public var headbandType: CGXHeadbandType {
        switch self {
        case .pickCGXDevKit:    return .cgxDevKit(userSelected: true)
        case .pickCGXQuick20r:  return .cgxQuick20r(userSelected: true)
        case .pickCGXQuick32r:  return .cgxQuick32r(userSelected: true)
        case .pickCGXOptios:    return .cgxOptios(userSelected: true)
        case .pickUnknown:      return .unknown
        }
    }
}

/// A CGX headband model.
public enum CGXHeadbandType: CaseIterable {

    public static var allCases: [CGXHeadbandType] {
        return [.cgxDevKit(userSelected: false),
                .cgxQuick20r(userSelected: false),
                .cgxQuick20rv2,
                .cgxQuick20m,
                .cgxQuick32r(userSelected: false),
                .cgxQuick32m,
                .cgxPatch,
                .cgxOptios(userSelected: false)]
    }

    case unknown,
         cgxDevKit(userSelected: Bool),
         cgxQuick20r(userSelected: Bool),
         cgxQuick20rv2,
         cgxQuick20m,
         cgxQuick32r(userSelected: Bool),
         cgxQuick32m,
         cgxPatch,
         cgxOptios(userSelected: Bool)
    
    //  Newer CGX devices indicate the device model in the bluetooth device name.  This array maps device name
    //  prefixes to CGX device types.  Older CGX devices use a generic Bluetooth device name and require the
    //  client to indicate the type of device in use (userSelected: true).
    static var bluetoothPrefixes: [(prefix: String, model: CGXHeadbandType)] =
        [(prefix: "CGX Dev Kit",     model: .cgxDevKit(userSelected: false)),
         (prefix: "CGX Quick-20rv2", model: .cgxQuick20rv2),
         (prefix: "CGX Quick-20m",   model: .cgxQuick20m),
         (prefix: "CGX Quick-20r",   model: .cgxQuick20r(userSelected: false)),
         (prefix: "CGX Quick-32r",   model: .cgxQuick32r(userSelected: false)),
         (prefix: "Patch Device",    model: .cgxPatch),
         (prefix: "Optios Headband", model: .cgxOptios(userSelected: false)),
         (prefix: "CGX Puick-Series",model: .cgxPatch),
         (prefix: "CGX Quick-Series",model: .unknown)]

    static var cgxDevKitChannels: [CGXHeadband.Channel] = [.CH1, // EEG Channels
                                                           .CH2,
                                                           .CH3,
                                                           .CH4,
                                                           .CH5,
                                                           .CH6,
                                                           .CH7,
                                                           .CH8]
    static var cgxQuick20rChannels: [CGXHeadband.Channel] = [.F7, // EEG Channels
                                                             .FP1,
                                                             .FP2,
                                                             .F8,
                                                             .F3,
                                                             .FZ,
                                                             .F4,
                                                             .C3,
                                                             .Cz,
                                                             .P8,
                                                             .P7,
                                                             .PZ,
                                                             .P4,
                                                             .T3,
                                                             .P3,
                                                             .O1,
                                                             .O2,
                                                             .C4,
                                                             .T4,
                                                             .A2, // Accessory channels
                                                             .ExG1]
    static var cgxQuick32rChannels: [CGXHeadband.Channel] = [.F7, // EEG Channels
                                                             .FPZ,
                                                             .F7,
                                                             .FZ,
                                                             .T7,
                                                             .FC6,
                                                             .FP1,
                                                             .F4,
                                                             .C4,
                                                             .Oz,
                                                             .CP6,
                                                             .Cz,
                                                             .PO8,
                                                             .CP5,
                                                             .O2,
                                                             .O1,
                                                             .P3,
                                                             .P4,
                                                             .P7,
                                                             .P8,
                                                             .PZ,
                                                             .PO7,
                                                             .T8,
                                                             .C3,
                                                             .FP2,
                                                             .F3,
                                                             .F8,
                                                             .FC5,
                                                             .AF8,
                                                             .A2, // Accessory channels
                                                             .ExG1,
                                                             .ExG2]
    static var cgxPatchChannels: [CGXHeadband.Channel] = [.FP1,
                                                          .FP2] // Provisional Patch device support
        
    static var cgxOptiosChannels: [CGXHeadband.Channel] = [.T3,
                                                           .FP1,
                                                           .FP2,
                                                           .T4] // Optios device is based on the Quick-20 and so delivers 21 channels, but
                                                                // only these EEG channels are meaningful and reported.
    
    /// The device manufacturer, returns "CGX".
    public var manufacturer: String {
        return "CGX"
    }

    /// The name of the CGX device model.
    public var model: String {
        switch self {
        case .cgxDevKit:
            return "Dev Kit"

        case .cgxQuick20r:
            return "Quick-20r"

        case .cgxQuick20rv2:
            return "Quick-20r v2"

        case .cgxQuick20m:
            return "Quick-20m"

        case .cgxQuick32r:
            return "Quick-32r"
            
        case .cgxQuick32m:
            return "Quick-32m"

        case .cgxPatch:
            return "Patch"
            
        case .cgxOptios:
            return "Optios"
            
        case .unknown:
            return "unknown CGX model"
        }
    }

    public var identifier: String {
        switch self {
        case .cgxDevKit:
            return "cgxDevKit"

        case .cgxQuick20r:
            return "cgxQuick20r"

        case .cgxQuick20rv2:
            return "cgxQuick20rv2"

        case .cgxQuick20m:
            return "cgxQuick20m"

        case .cgxQuick32r:
            return "cgxQuick32r"

        case .cgxQuick32m:
            return "cgxQuick32m"

        case .cgxPatch:
            return "cgxPatch"
            
        case .cgxOptios:
            return "cgxOptios"
            
        case .unknown:
            return "unknown"
        }
    }
    
    /// Certain older CGX devices carry generic Bluetooth names making it impossible for CGXKit to detect the model of these
    /// devices.  When the SDK encounters one of these devices, its model is set to `unknown`.
    ///
    /// It is up to the CGXKit client to set the headband's `model` property with the correct `CGXHeadbandType` enumeration
    /// before attempting to connect to one of these devices (note that the headband's `model` property value is persistent and
    /// only needs to be set once.  The assigned value must be one of: `.unknown`, `.cgxDevKit(userSelected: true)`,
    /// `.cgxQuick20r(userSelected: true)` or `.cgxQuick32r(userSelected: true)`.  The
    /// `CGXHeadbandTypePicker` enumeration represents all the user selectable device models.
    ///
    /// The `CGXHeadbandTypePicker` enumeration can be enumerated to generate UIs.
    public var isChangable: Bool {
        //  Only device types that are use selected are changeable.  User selected device types are those where the device's
        //  Bluetooth name does not provide information identifying the type of device.  All modern CGX devices do this,
        //  so this is for older version CGX products.
        switch self {
        case .cgxDevKit(userSelected: let userSelected),
             .cgxQuick20r(userSelected: let userSelected),
             .cgxQuick32r(userSelected: let userSelected):
            return userSelected
            
        case .unknown:
            return true
            
        case .cgxOptios(userSelected: let userSelected):
            return userSelected
            
        default:
            return false
        }
    }
    
    /// Device description, a combination of manufacturer and model.
    public var description: String {
        return self == .unknown ? "unknown" : "\(manufacturer) \(model)"
    }
    
    /// The `CGXHeadbandTypePicker` enumeration corresponding to this device model.
    public var pickerType: CGXHeadbandPickerType {
        //  Early CGX devices had a generic bluetooth identify: "CGX Quick-Series Headset".  When we encounter this, we must
        //  ask the user which model of CGX device they have.  Later devices have Bluetooth identities that indicate the model
        //  of device and we do not need to query the user.
        switch self {
        case .cgxDevKit(true):
            return .pickCGXDevKit

        case .cgxQuick20r(true):
            return .pickCGXQuick20r

        case .cgxQuick32r(true):
            return .pickCGXQuick32r
              
        case .cgxOptios: // Test Optios devices may appear as generic Quick-20 devices
            return .pickCGXOptios
            
        default:
            return .pickUnknown
        }
    }

    /// Indicates if the device provides impedance data corresponding to signal quality.
    public var providesEEGQualityData: Bool {
        switch self {
        case .cgxDevKit,
             .cgxQuick20r,
             .cgxQuick20rv2,
             .cgxQuick20m,
             .cgxQuick32r,
             .cgxQuick32m,
             .cgxPatch,
             .cgxOptios:
            return true

        case .unknown:
            return false
        }
    }

    /// Indicates if the device provides battery voltage data.
    public var providesBatteryData: Bool {
        switch self {
        case .cgxDevKit,
             .cgxQuick20r,
             .cgxQuick20rv2,
             .cgxQuick20m,
             .cgxQuick32r,
             .cgxQuick32m,
             .cgxPatch,
             .cgxOptios:
            return true

        case .unknown:
            return false
        }
    }
    
    var impedanceMagnitudeScale: Double {
        switch self {
        case .cgxDevKit:
            return CGXDevKitImpedanceMagnitudeScale
            
        case .cgxQuick20r,
             .cgxQuick20rv2,
             .cgxQuick20m,
             .cgxOptios:
            return CGXQuick20rImpedanceMagnitudeScale
            
        case .cgxQuick32r,
             .cgxQuick32m:
            return CGXQuick32rImpedanceMagnitudeScale
            
        case .cgxPatch:
            return CGXPatchImpedanceMagnitudeScale
            
        case .unknown:
            return 0
        }
    }

    /// Indicates if the device provides accelerometer data.
    public var providesAccelerometerData: Bool { return true }

    /// An array of EEG channels provided by the device.  The order of channels corresponds with the order of values returned in the CGSSample's eegv property.
    public var eegChannels: [CGXHeadband.Channel] { // assigned (potentially) channel specification
        switch self {
        case .cgxDevKit:
            return Self.cgxDevKitChannels

        case .cgxQuick20r,
             .cgxQuick20rv2,
             .cgxQuick20m:
            return Self.cgxQuick20rChannels

        case .cgxQuick32r,
             .cgxQuick32m:
            return Self.cgxQuick32rChannels
            
        case .cgxPatch:
            return Self.cgxPatchChannels
            
        case .cgxOptios:
            return Self.cgxOptiosChannels
            
        case .unknown:
            return []
        }
    }
        
    /// The number of EEG, Aux and Ex channels provided by the device.
    public var eegChannelCount: Int {
        return eegChannels.count
    }
    
    /// The devices EEG sampling frequency (Hz).
    public var eegSampleFrequency: Int {
        switch self {
        case .cgxDevKit,
             .cgxQuick20r,
             .cgxQuick20rv2,
             .cgxQuick20m,
             .cgxQuick32r,
             .cgxQuick32m,
             .cgxPatch,
             .cgxOptios:
            return 500
            
        case .unknown:
            return 0
        }
    }
    
    /// The type of device, returns "deviceType".
    public var deviceType: String {
        switch self {
        default:
            return "EEG Headband"
        }
    }
    
    /// A 40x20 image provided in 1x, 2x and 3x resolutions representing the model of device.  The type of image will be `UIImage` on iOS and watchOS and `NSImage` on macOS.
    public var image: CGXHeadband.Image? {
        switch self {
        case .cgxDevKit:
            #if os(watchOS)
            return CGXHeadband.Image(named: "CGXDevKit", in: Bundle.module, with: nil)
            #elseif os(macOS)
            return Bundle.module.image(forResource: "CGXDevKit")
            #else
            return CGXHeadband.Image(named: "CGXDevKit", in: Bundle.module, compatibleWith: nil)
            #endif
            
        case .cgxQuick20r, .cgxQuick20m:
            #if os(watchOS)
            return CGXHeadband.Image(named: "CGXQuick20r", in: Bundle.module, with: nil)
            #elseif os(macOS)
            return Bundle.module.image(forResource: "CGXQuick20r")
            #else
            return CGXHeadband.Image(named: "CGXQuick20r", in: Bundle.module, compatibleWith: nil)
            #endif

        case .cgxQuick20rv2:
            #if os(watchOS)
            return CGXHeadband.Image(named: "CGXQuick20rv2", in: Bundle.module, with: nil)
            #elseif os(macOS)
            return Bundle.module.image(forResource: "CGXQuick20rv2")
            #else
            return CGXHeadband.Image(named: "CGXQuick20rv2", in: Bundle.module, compatibleWith: nil)
            #endif

        case .cgxQuick32r, .cgxQuick32m:
            #if os(watchOS)
            return CGXHeadband.Image(named: "CGXQuick32r", in: Bundle.module, with: nil)
            #elseif os(macOS)
            return Bundle.module.image(forResource: "CGXQuick32r")
            #else
            return CGXHeadband.Image(named: "CGXQuick32r", in: Bundle.module, compatibleWith: nil)
            #endif

        case .cgxPatch:
            #if os(watchOS)
            return CGXHeadband.Image(named: "CGXPatch", in: Bundle.module, with: nil)
            #elseif os(macOS)
            return Bundle.module.image(forResource: "CGXPatch")
            #else
            return CGXHeadband.Image(named: "CGXPatch", in: Bundle.module, compatibleWith: nil)
            #endif

        case .cgxOptios,
             .unknown:
            #if os(watchOS)
            return CGXHeadband.Image(named: "EEGUnknownDevice", in: Bundle.module, with: nil)
            #elseif os(macOS)
            return Bundle.module.image(forResource: "EEGUnknownDevice")
            #else
            return CGXHeadband.Image(named: "EEGUnknownDevice", in: Bundle.module, compatibleWith: nil)
            #endif
        }
    }
   
    /// Map the headband's battery voltage to a percentage (0..1) full.
    public func mapBatteryVoltageToPercent(_ voltage: Double) -> Double {
        switch self {
        case .cgxDevKit: // 5v - 3.5v
            return max(0.0, (voltage - 3.5) / (5.0 - 3.5))

        case .cgxOptios: // 4.3 to 3.2
            return max(0.0, (voltage - 3.2) / (4.3 - 3.2))

        case .cgxQuick20r, .cgxQuick20rv2, .cgxQuick20m, .cgxQuick32r, .cgxQuick32m, .cgxPatch: // Mike says: 3v - 2.0v, I observe: 2.6v - 2.2v
            return min(max(0.0, (voltage - 2.2) / (2.6 - 2.2)), 1.0)

        case .unknown:
            return 0
        }
    }
        
}


extension CGXHeadbandType: Codable {
    
    public enum CodingKeys: CodingKey {
        case type, userSelected
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let type = try container.decode(String.self, forKey: .type)
        let userSelected = try container.decodeIfPresent(Bool.self, forKey: .userSelected) ?? true

        switch type {
        case "cgxDevKit":
            self = .cgxDevKit(userSelected: userSelected)
            
        case "cgxQuick20r":
            self = .cgxQuick20r(userSelected: userSelected)
        
        case "cgxQuick20rv2":
            self = .cgxQuick20rv2

        case "cgxQuick20m", "Quick20m":
            self = .cgxQuick20m

        case "cgxQuick32r":
            self = .cgxQuick32r(userSelected: userSelected)
            
        case "cgxQuick32m":
            self = .cgxQuick32m
            
        case "cgxPatch":
            self = .cgxPatch
            
        case "cgxOptios":
            self = .cgxOptios(userSelected: userSelected)
            
        case "unknown":
            self = .unknown
            
        default:
            fatalError("CGXKit Unknown headband type: \(type)")
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.identifier, forKey: .type)

        switch self {
        case .cgxDevKit(userSelected: let userSelected),
             .cgxQuick20r(userSelected: let userSelected),
             .cgxQuick32r(userSelected: let userSelected),
             .cgxOptios(userSelected: let userSelected):
            try container.encode(userSelected, forKey: .userSelected)

        default:
            break
        }
    }
}


extension CGXHeadbandType {
    /// Maximum possible impedance value (Ohms)
    public static let maxOhm = 3000000
    public static let headroomOhm = 20000
}


extension CGXHeadbandType {
    /// Construct a `CGXHeadbandType` instance from a JSON string generated by the `CGXHeadbandType.json` property.
    ///
    /// - Parameter json: A JSON string representing a `CGXHeadbandType` returned by the `json` property.
    ///
    public init?(json: String) {
        guard let data = json.data(using: .utf8) else { fatalError("CGXKit Cannot convert \(json) to data") }
        let decoder = JSONDecoder()
        if let json = try? decoder.decode(Self.self, from: data) {
            self = json
        }
        else {
            return nil
        }
    }
    
    /// Returns a JSON string representing the device model.
    public var json: String {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        guard let data = try? encoder.encode(self) else { fatalError("CGXKit Cannot encode CGXHeadbandType(\(self))") }
        guard let s = String(data: data, encoding: .utf8) else { fatalError("CGXKit Cannot convert data to string") }

        return s
    }
}


extension CGXHeadbandType: Equatable {
}

public func ==(lhs: CGXHeadbandType, rhs: CGXHeadbandType) -> Bool {
    switch (lhs, rhs) {
    case (.cgxDevKit, .cgxDevKit):
        return true

    case (.cgxQuick20r, .cgxQuick20r):
        return true

    case (.cgxQuick20rv2, .cgxQuick20rv2):
        return true
        
    case (.cgxQuick20m, .cgxQuick20m):
        return true

    case (.cgxQuick32r, .cgxQuick32r):
        return true

    case (.cgxQuick32m, .cgxQuick32m):
        return true

    case (.cgxPatch, .cgxPatch):
        return true
        
    case (.cgxOptios, .cgxOptios):
        return true
        
    case (.unknown, .unknown):
        return true
        
    default:
        return false
    }
}


//
//  CGXSample.swift
//  CGXKit
//
//  Created by Mark Alldritt on 2020-04-01.
//


///  A single data sample received from a CGX Headband
public class CGXSample {
    
    /// The headband providing this sample
    public let headband: CGXHeadband
    /// The timestamp of the arrival of the sample on the host device (iPhone, iPad, mac, etc.) (in microseconds since epoc).
    ///
    /// This is **not** the time the sample was actually captured by the CGX device.
    public let timestamp: UInt64
    /// An array of EEG samples, corresponding to the channels specified in headband's `model.eegChannels` property.  The units will be microvolts or volts depending on the headband's `eegUnits` property.
    public let eegV: [CGXHeadband.SampleType]
    /// Is impedance data available in this sample?
    public let impedanceCheck: Bool
    /// An array of impedance samples (in Ohms)  corresponding to the channels specified in headband's `model.eegChannels`.
    public let impedance: [CGXHeadband.SampleType]
    /// Accelerometer X sample.
    public let accelX: CGXHeadband.SampleType
    /// Accelerometer Y sample.
    public let accelY: CGXHeadband.SampleType
    /// Accelerometer X sample.
    public let accelZ: CGXHeadband.SampleType
    /// Trigger value (will always be 0 unless an external CGX trigger device is used).
    public let trigger: Int
    
    init(headband: CGXHeadband,
         timestamp: UInt64,
         eegV: [CGXHeadband.SampleType],
         impedanceCheck: Bool,
         impedance: [CGXHeadband.SampleType],
         accelX: CGXHeadband.SampleType,
         accelY: CGXHeadband.SampleType,
         accelZ: CGXHeadband.SampleType,
         trigger: Int) {
        self.headband = headband
        self.timestamp = timestamp
        self.eegV = eegV
        self.impedanceCheck = impedanceCheck
        self.impedance = impedance
        self.accelX = accelX
        self.accelY = accelY
        self.accelZ = accelZ
        self.trigger = trigger
    }
}

/// EEG data stream listener
public protocol CGXSampleDelegate: AnyObject {
    
    /// Receive an EEG sample
    ///
    /// - Parameter sample: An EEG sample
    func receive(sample: CGXSample) // receive a CGX sample
    /// Receive a series of dropped EEG samples
    ///
    /// - Parameter dropped: The number of dropped samples
    /// - Parameter sample: a dropped sample
    func receive(dropped: Int, sample: CGXSample) // receive a count of dropped frames, sample contains nan data
    
}

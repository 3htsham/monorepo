
//
//  RingBuffer.swift
//  CGXKit
//
//  Created by Mark Alldritt on 2020-04-11.
//

import Foundation


/// A class representing a fixed sized circular array of values.  This class is used internally by CGXKit and may be of use to client applications for buffering
/// EEG samples.
public class RingBuffer<T> {
    private var array: [T]
    
    /// Number of values in the ring buffer.
    public var count: Int {
        return min(writeCount, slots)
    }
    /// Max number of values the ring buffer can hold
    public let slots: Int
    /// The total number of values written to the ring buffer
    public private (set) var writeCount = 0;
    
    public init(slots: Int) {
        self.slots = slots
        self.array = [T]()
        self.array.reserveCapacity(slots)
    }
    public init(repeating: T, count: Int) {
        self.slots = count
        self.writeCount = count
        self.array = [T](repeating: repeating, count: slots)
    }
    
    /// Add a value to the end of the ring buffer
    @inline(__always) public func append(_ element: T) {
        let index = writeCount % slots
        if array.count <= index {
            array.append(element)
        }
        else {
            array[index] = element
        }
        writeCount += 1
    }
    
    /// Remove all values from the ring buffer.
    public func clear() {
        writeCount = 0
    }
    /// Fill the ring buffer with a single value
    public func clear(repeating: T) {
        writeCount = slots
        for i in 0..<slots {
            array[i] = repeating
        }
    }
    
    /// Access a ring buffer value relative to the end of the ring buffer.
    @inline(__always) public subscript(index: Int) -> T {
        get {
            guard writeCount > 0 else { fatalError("index \(index) out of bounds") }
            guard index >= 0 && index < count else { fatalError("index \(index) out of bounds") }
            
            return array[(writeCount - 1 - index) % slots]
        }
        set(newValue) {
            guard writeCount > 0 else { fatalError("index \(index) out of bounds") }
            guard index >= 0 && index < count else { fatalError("index \(index) out of bounds") }
            
            array[(writeCount - 1 - index) % slots] = newValue
        }
    }
    
    /// Convert the ring buffer into an array.
    ///
    /// - Parameter count: the number of elements (relative to the end of the ring buffer) to return.  The number of values returned will be the minimum of `count` parameter and the ring buffer's `count` property.
    @inline(__always) public func asArray(count n: Int = Int.max) -> [T] {
        //  Return an array containing the last n items in the ring buffer.  If n exceeds the ring buffer's
        //  capacity or content, the entire ring buffer is returned.
        
        let start = writeCount % slots
        
        if start == 0 && writeCount >= slots {
            if n >= slots {
                return array
            }
            else {
                return Array<T>(array[(slots - n)...])
            }
        }
        else if writeCount >= slots {
            if n > slots {
                return Array<T>(array[start...]) + Array<T>(array[0..<start])
            }
            else {
                if n <= start {
                    return Array<T>(array[(start - n)..<start])
                }
                else {
                    return Array<T>(array[(slots - (n - start))...]) + Array<T>(array[0..<start])
                }
            }
        }
        else if writeCount >= n {
            return array
        }
        else {
            return Array<T>(array[max(0, (writeCount - n))...])
        }
    }
}


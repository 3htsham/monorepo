//
//  UIKitExtras.swift
//  CGXKit
//
//
//  Created by Mark Alldritt on 2021-03-31.
//
//
//  iOS specific UI code.
//


#if os(iOS)
import UIKit
import Eureka


extension UIApplication {
    var visibleViewController: UIViewController? {
        return delegate?.window??.visibleViewController
    }
}


extension UIWindow {
    var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
    }
    
    static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            if let topViewController = nc.topViewController {
                return UIWindow.getVisibleViewControllerFrom(topViewController)
            }
            else {
                return nc
            }
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
        //} else if let sc = vc as? UISplitViewController {
        //    //  is one of the split views displaying a modal view controller.  Just not sure how to accomplish this.
        //
        } else {
            //  Is vc displaying a modal view controller
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}


extension CGXHeadband {

    /// Establish type of unknown CGX devices.
    ///
    /// - Parameter presentingViewController: the UIViewController presenting this modal view controller.
    /// - Parameter completion: a completion handler called when the modal view controller is dismissed.
    ///
    /// **UIKit Only**
    ///
    /// When a headband's model is `unknown` this routine presents a modal alert allowing the user to identify the type of CGX device they have.  The
    /// CGX device models offered is controlled by the `CGXHeadbandPickerType` enumeration.
    @discardableResult
    public func establishDeviceType(presentingViewController: UIViewController,
                                    completion: ((_ success: Bool) -> Void)?) -> UIViewController {
        let formController = FormViewController()
        let section = SelectableSection<CGXImageCheckRow<CGXHeadbandPickerType>>(nil, selectionType: .singleSelection(enableDeselection: false))
        let continueAction = UIAlertAction(title: "Continue",
                                                style: .default,
                                                handler: { (action) in
                                                    guard let newValue = section.selectedRow()?.value else { return }
                                                    self.model = newValue.headbandType
                                                    completion?(true)
        })

        for headbandType in CGXHeadbandPickerType.allCases {
            section <<< CGXImageCheckRow<CGXHeadbandPickerType>() { (row) in
                row.title = headbandType.headbandType.model
                row.selectableValue = headbandType
                row.value = self.model.pickerType == headbandType ? headbandType : nil
                row.cell.accessoryType = .checkmark
            }
            .cellUpdate { (cell, row) in
                cell.imageView?.image = row.selectableValue?.headbandType.image
            }
            .onChange({ (row) in
                if let _ = row.selectableValue?.headbandType {
                    continueAction.isEnabled = true
                }
                else {
                    continueAction.isEnabled = false
                }
            })
        }
        
        formController.form +++ section
        formController.edgesForExtendedLayout = []
        formController.preferredContentSize = CGSize(width: 300, height: 252)

        let alertController = UIAlertController(title: "Device Type",
                                                message: "Select the type of CGX headband you have.",
                                                preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Cancel",
                                                style: .cancel,
                                                handler: { (action) in
                                                    completion?(false)
        }))
        alertController.addAction(continueAction)
        continueAction.isEnabled = model != .unknown // will be enabled when a device type is selected
        
        //  This call is potentially problematic since it exploits an undocumented API but I cannot
        //  find another way to make this work.  So far, Apple has not flagged this issue with PEER.
        alertController.setValue(formController, forKey: "contentViewController")

        presentingViewController.present(alertController, animated: true, completion: nil)
        return alertController
    }
}


#endif

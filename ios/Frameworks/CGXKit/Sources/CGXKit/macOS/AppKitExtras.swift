//
//  AppKitExtras.swift
//  CGXKit
//
//
//  Created by Mark Alldritt on 2021-04-16.
//
//
//  macOS specific UI code.
//

#if os(macOS)
import Cocoa

extension CGXHeadband {
    
    func establishDeviceType(completion: ((_ success: Bool) -> Void)?) {
        //  Todo - create a macOS modal alert to prompt for the device type...
        
        fatalError("CGXKit not implemented yet")
    }

}

#endif

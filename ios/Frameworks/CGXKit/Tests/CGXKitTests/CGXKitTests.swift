import XCTest
@testable import CGXKit

final class CGXKitTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(CGXKit().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}

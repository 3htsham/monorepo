import XCTest

import CGXKitTests

var tests = [XCTestCaseEntry]()
tests += CGXKitTests.allTests()
XCTMain(tests)

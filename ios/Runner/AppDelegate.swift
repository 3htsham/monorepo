import UIKit
import Flutter
import CGXKit

var cgxKitService : CGXKitService = CGXKitService()

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    private let bluetoothEventChannel = "cgxkitevent/bluetooth"
    private let devicesEventChannel = "cgxkitevent/devices"
    private let deviceStateEventChannel = "cgxkitevent/deviceState"
    private let eegEventChannel = "cgxkitevent/eeg"
    private let impedanceEventChannel = "cgxkitevent/impedance"
    private let methodChannel = "com.optios.golf/cgxkit"
    
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        var flutter_native_splash = 1
        UIApplication.shared.isStatusBarHidden = false
        let controller = window?.rootViewController as! FlutterViewController
        
        //listen bluetooth states
        FlutterEventChannel(name: bluetoothEventChannel, binaryMessenger: controller.binaryMessenger)
            .setStreamHandler(cgxKitService.bluetoothStream)
        //listen devices
        FlutterEventChannel(name: devicesEventChannel, binaryMessenger: controller.binaryMessenger)
            .setStreamHandler(cgxKitService.deviceStream)
        //listen device state
        FlutterEventChannel(name: deviceStateEventChannel, binaryMessenger: controller.binaryMessenger)
            .setStreamHandler(cgxKitService.deviceStateStream)
        //eeg event channel
        FlutterEventChannel(name: eegEventChannel, binaryMessenger: controller.binaryMessenger)
            .setStreamHandler(cgxKitService.eegDataStream)
        
        FlutterMethodChannel(name: methodChannel, binaryMessenger: controller.binaryMessenger).setMethodCallHandler { [weak self] call, result in
            let arguments = call.arguments as? Dictionary<String, Any>
            if call.method == "connect" {
                guard self != nil else { return }
                if let uuid = arguments?["uuid"] as? String {
                    let canConnect = cgxKitService.connectDevice(deviceUUID: uuid)
                    if canConnect == true {
                        result(JsonHelper.responseToJson(response: FLResponse<Bool>(error: false, message: "success", data: true)!))
                    } else {
                        result(JsonHelper.parseError(error: "Device not found"))
                    }
                }
            } else if call.method == "getBatteryLevel" {
                result(cgxKitService.getBatteryLevel() * 100)
            } else if call.method == "disconnect" {
                guard self != nil else { return }
                let canDisconnect = cgxKitService.disconnectDevice()
                if canDisconnect == true {
                    result(JsonHelper.responseToJson(response: FLResponse<Bool>(error: false, message: "success", data: true)!))
                } else {
                    result(JsonHelper.parseError(error: "Device not found"))
                }
            } else if call.method == "startEEGAcquisition" {
                cgxKitService.listenEEGData()
                result(cgxKitService.headband != nil)
            } else if call.method == "stopEEGAcquisition" {
                cgxKitService.stopEEGData()
                result(true)
            }
            else {
                result(FlutterMethodNotImplemented)
            }
        }
        
        cgxKitService.start()
        
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
}

//
//  Biquad.swift
//
//
//  This is a Swift implementation of this C implementation of a BiQuad filter:
//
//  https://github.com/wooters/miniDSP/blob/master/biquad.c
//
//  which is based on this:
//
//  https://webaudio.github.io/Audio-EQ-Cookbook/audio-eq-cookbook.html
//
//  At some point I need to improve to VDSP/Accelerate's implementation (especially its multichannel
//  implementation).
//
//  There is also this tool for designing BiQuad filters:
//
//  https://arachnoid.com/BiQuadDesigner/
//


import Foundation
import CGXKit


class BiQuad {
    public typealias T = CGXHeadband.SampleType

    public struct Coefficients {
        let a0: T
        let a1: T
        let a2: T
        let a3: T
        let a4: T
        
        public init(a0: T, a1: T, a2: T, b0: T, b1: T, b2: T) {
            self.a0 = b0 / a0
            self.a1 = b1 / a0
            self.a2 = b2 / a0
            self.a3 = a1 / a0
            self.a4 = a2 / a0
        }

    }
    
    public enum Filter {
        case LowPass(frequency: T, sampleRate: T, bandwidth: T),
             HighPass(frequency: T, sampleRate: T, bandwidth: T),
             BandPass(frequency: T, sampleRate: T, bandwidth: T),
             Notch(frequency: T, sampleRate: T, bandwidth: T),
             PeakingBandEQ(dbGain: T, frequency: T, sampleRate: T, bandwidth: T),
             LowShelf(dbGain: T, frequency: T, sampleRate: T),
             HighShelf(dbGain: T, frequency: T, sampleRate: T)
        
        public var coefficients: Coefficients {
            
            let b0: T;
            let b1: T;
            let b2: T;
            let a0: T;
            let a1: T;
            let a2: T;

            switch self {
            case .LowPass(let frequency, let sampleRate, let bandwidth):
                let omega = 2 * T.pi * frequency / sampleRate
                let sn = sin(omega);
                let cs = cos(omega);
                let alpha = sn * sinh(T(M_LN2) / 2 * bandwidth * omega / sn);

                b0 = (1 - cs) / 2;
                b1 = 1 - cs;
                b2 = (1 - cs) / 2;
                a0 = 1 + alpha;
                a1 = -2 * cs;
                a2 = 1 - alpha;
                break

            case .HighPass(let frequency, let sampleRate, let bandwidth):
                let omega = 2 * T.pi * frequency / sampleRate
                let sn = sin(omega);
                let cs = cos(omega);
                let alpha = sn * sinh(T(M_LN2) / 2 * bandwidth * omega / sn);

                b0 = (1 + cs) / 2;
                b1 = -(1 + cs);
                b2 = (1 + cs) / 2;
                a0 = 1 + alpha;
                a1 = -2 * cs;
                a2 = 1 - alpha;
                break

            case .BandPass(let frequency, let sampleRate, let bandwidth):
                let omega = 2 * T.pi * frequency / sampleRate
                let sn = sin(omega);
                let cs = cos(omega);
                let alpha = sn * sinh(T(M_LN2) / 2 * bandwidth * omega / sn);

                b0 = alpha;
                b1 = 0;
                b2 = -alpha;
                a0 = 1 + alpha;
                a1 = -2 * cs;
                a2 = 1 - alpha;
                break

            case .Notch(let frequency, let sampleRate, let bandwidth):
                let omega = 2 * T.pi * frequency / sampleRate
                let sn = sin(omega);
                let cs = cos(omega);
                let alpha = sn * sinh(T(M_LN2) / 2 * bandwidth * omega / sn);

                b0 = 1;
                b1 = -2 * cs;
                b2 = 1;
                a0 = 1 + alpha;
                a1 = -2 * cs;
                a2 = 1 - alpha;
                break
                
            case .PeakingBandEQ(let dbGain, let frequency, let sampleRate, let bandwidth):
                let A = pow(10, dbGain / 40);
                let omega = 2 * T.pi * frequency / sampleRate
                let sn = sin(omega);
                let cs = cos(omega);
                let alpha = sn * sinh(T(M_LN2) / 2 * bandwidth * omega / sn);

                b0 = 1 + (alpha * A);
                b1 = -2 * cs;
                b2 = 1 - (alpha * A);
                a0 = 1 + (alpha / A);
                a1 = -2 * cs;
                a2 = 1 - (alpha / A);
                break

            case .LowShelf(let dbGain, let frequency, let sampleRate):
                let A = pow(10, dbGain / 40);
                let omega = 2 * T.pi * frequency / sampleRate
                let sn = sin(omega);
                let cs = cos(omega);
                let beta = sqrt(A + A);

                b0 = A * ((A + 1) - (A - 1) * cs + beta * sn);
                b1 = 2 * A * ((A - 1) - (A + 1) * cs);
                b2 = A * ((A + 1) - (A - 1) * cs - beta * sn);
                a0 = (A + 1) + (A - 1) * cs + beta * sn;
                a1 = -2 * ((A - 1) + (A + 1) * cs);
                a2 = (A + 1) + (A - 1) * cs - beta * sn;
                break
                
            case .HighShelf(let dbGain, let frequency, let sampleRate):
                let A = pow(10, dbGain / 40);
                let omega = 2 * T.pi * frequency / sampleRate
                let sn = sin(omega);
                let cs = cos(omega);
                let beta = sqrt(A + A);
                
                b0 = A * ((A + 1) + (A - 1) * cs + beta * sn);
                b1 = -2 * A * ((A - 1) + (A + 1) * cs);
                b2 = A * ((A + 1) + (A - 1) * cs - beta * sn);
                a0 = (A + 1) - (A - 1) * cs + beta * sn;
                a1 = 2 * ((A - 1) - (A + 1) * cs);
                a2 = (A + 1) - (A - 1) * cs - beta * sn;
                break
            }
            
            return Coefficients(a0: a0, a1: a1, a2: a2, b0: b0, b1: b1, b2: b2)
        }
    }
    
    private let coefficients: Coefficients
    private var x1 = T(0)
    private var x2 = T(0)
    private var y1 = T(0)
    private var y2 = T(0)

    public init(_ filter: Filter) {
        self.coefficients = filter.coefficients
    }
    
    public init(_ coefficients: Coefficients) {
        self.coefficients = coefficients
    }
    
    @inline(__always) public func filter(_ sample: T) -> T {
        guard !sample.isNaN else { return 0 }
        /* compute result */
        let result = coefficients.a0 * sample + coefficients.a1 * x1 + coefficients.a2 * x2 - coefficients.a3 * y1 - coefficients.a4 * y2;

        /* shift x1 to x2, sample to x1 */
        x2 = x1;
        x1 = sample;

        /* shift y1 to y2, result to y1 */
        y2 = y1;
        y1 = result;

        return result;
    }
}

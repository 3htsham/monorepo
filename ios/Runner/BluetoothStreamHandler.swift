//
//  DevicesStreamHandler.swift
//  Runner
//
//  Created by abrian on 13/07/2021.
//
import CGXKit


class BluetoothStreamHandler: NSObject, FlutterStreamHandler {
    private var eventSink: FlutterEventSink? = nil
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        eventSink = events
        eventSink?(getBluetoothStateString())
        listenBluetoothState()
        return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        NotificationCenter.default.removeObserver(self, name: CGXHeadbandManager.BluetoothStateChangedNotification, object: CGXHeadbandManager.shared)
        eventSink = nil
        return nil
    }
    
    func listenBluetoothState() {
        NotificationCenter.default.addObserver(forName: CGXHeadbandManager.BluetoothStateChangedNotification,
                                               object: CGXHeadbandManager.shared,
                                               queue: nil) { [weak self] (_) in
            guard self != nil else { return }
            self?.eventSink?(self?.getBluetoothStateString())
        }
    }
    
    func getBluetoothStateString() -> String {
        switch CGXHeadbandManager.shared.state {
        case .poweredOff:
            return "powered off"
        // The user has turned off Bluetooth
        
        case .unauthorized:
            return "unauthorized"
        // The user has not authorized Bluetooth access
        
        case .poweredOn:
            return "powered on"
        // Bluetooth is available and operating, device acquisition begins
        default:
            return "not available"
        // Bluetooth is not available
        }
    }
    
}

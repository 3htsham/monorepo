//
//  CGXKitService.swift
//  Runner
//
//  Created by abrian on 13/07/2021.
//

import Foundation
import CGXKit

class CGXKitService {
    let headBandManager = CGXHeadbandManager.shared
    let bluetoothStream : BluetoothStreamHandler = BluetoothStreamHandler()
    let deviceStream : DeviceStreamHandler = DeviceStreamHandler()
    let deviceStateStream : DeviceStateStreamHandler = DeviceStateStreamHandler()
    let eegDataStream : EEGStreamHandler = EEGStreamHandler()
    public var deviceUUID : String?
    public var headband: CGXHeadband?
    private var eegListener: EEGListener?
    
    init() {
        start()
    }
    
    func start() {
        self.headBandManager.start();
    }
    
    func connectDevice(deviceUUID: String) -> Bool {
        if let device = CGXHeadbandManager.shared.devices[UUID(uuidString: deviceUUID) ?? UUID()] {
            device.connect()
            self.headband = device
            
            // Will over here where he does not belong.
            self.headband?.eegUnits = CGXHeadband.EEGUnits.microVolts;
            
            self.deviceUUID = deviceUUID
            //listen to device state
            deviceStateStream.listenToDeviceState()
            //set the connected headband
            self.eegDataStream.headband = headband!
            //            listenEEGData()
            return true
        } else {
            return false
        }
    }
    
    func disconnectDevice() -> Bool {
        if self.headband != nil {
            //stop eeg stream
            
            stopEEGData()
            //then do disconnect
            self.headband?.disconnect()
            //remove preferences
            eegDataStream.headband = nil
            headband = nil
            return true
        }
        return false
    }
    
    func listenEEGData() {
        eegDataStream.registerDataListener()
    }
    
    func stopEEGData() {
        eegDataStream.unRegisterDataListener()
    }
    
    func getBatteryLevel() -> Double {
        return headband?.model.mapBatteryVoltageToPercent(headband?.batteryv ?? 0) ?? 0
    }
    
}

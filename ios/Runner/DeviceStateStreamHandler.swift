//
//  DeviceStateStreamHandler.swift
//  Runner
//
//  Created by abrian on 20/07/2021.
//
import CGXKit

class DeviceStateStreamHandler: NSObject, FlutterStreamHandler {
    private var eventSink: FlutterEventSink? = nil
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        print("DEVICE STATE ON LISTEN CALLED")
        eventSink = events
        listenToDeviceState()
        return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        print("device state ON CANCEL CALLED")
        NotificationCenter.default.removeObserver(self, name: CGXHeadband.StateChangeNotification, object: cgxKitService.headband)
        eventSink = nil
        return nil
    }
    
    func listenToDeviceState() {
        if cgxKitService.headband == nil { return }
        NotificationCenter.default.addObserver(forName: CGXHeadband.StateChangeNotification,
                                               object: cgxKitService.headband,
                                               queue: nil) { [weak self] (note) in
            guard self != nil, cgxKitService.headband != nil else { return }
            self?.eventSink?(FLCGXHeadband.getHeadbandStateString(state: cgxKitService.headband!.state))
        }
    }
    
}

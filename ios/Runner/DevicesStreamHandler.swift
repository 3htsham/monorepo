//
//  DevicesStreamHandler.swift
//  Runner
//
//  Created by abrian on 13/07/2021.
//
import CGXKit


class DeviceStreamHandler: NSObject, FlutterStreamHandler {
    private var eventSink: FlutterEventSink? = nil
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        print("DEVICES STREAM ON LISTEN CALLED")
        eventSink = events
        self.eventSink?(self.getListDeviceJsonString())
        listenDevices();
        return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        print("DEVICES STREAM ON CANCEL CALLED")
        NotificationCenter.default.removeObserver(self, name: CGXHeadbandManager.DevicesChangedNotification, object: CGXHeadbandManager.shared)
        eventSink = nil
        return nil
    }
    
    func listenDevices() {
        NotificationCenter.default.addObserver(forName: CGXHeadbandManager.DevicesChangedNotification,
                                               object: CGXHeadbandManager.shared,
                                               queue: nil) { [weak self] (note) in
            guard self != nil else { return }
            
            self?.eventSink?(self?.getListDeviceJsonString())
        }
    }
    
    func getListDeviceJsonString() -> String {
        let devices = CGXHeadbandManager.shared.devices
        var flDevices = [FLCGXHeadband]()
        for device in devices {
            flDevices.append(FLCGXHeadband(deviceName: device.value.deviceName, assignedName: device.value.assignedName ?? "", identifier: device.key.uuidString, lastSeen: Date(), batteryPercent: Int(device.value.model.mapBatteryVoltageToPercent(device.value.batteryv) * 100), state: FLCGXHeadband.getHeadbandStateString(state: device.value.state)))
        }
        
        return JsonHelper.responseToJson(response: FLResponse<[FLCGXHeadband]>(error: false, message: "success", data: flDevices)!)
        
    }
    
}

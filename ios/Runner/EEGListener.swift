//
//  EEGListener.swift
//
//  EEGListener receives EEG samples from CGXKit, applies some filtering and buffers them for display by EEGView.
//

import Foundation
import CGXKit


class EEGListener: CGXSampleDelegate {
    let eegHandler : EEGStreamHandler?
    private let period = 3 /* seconds */
    private let ringImpedanceLength: Int
    private var eeg: [RingBuffer<CGXHeadband.SampleType>] = []
    private var impedance: [RingAverage<CGXHeadband.SampleType>] = []
    private var eegFilter1: [BiQuad] // 0.5Hz High Pass for channel
    private var eegFilter2: [BiQuad] // Impedance 1 filter for channel
    private var mutex = NSLock()

    let headband: CGXHeadband

    init(_ headband: CGXHeadband, eegHandler: EEGStreamHandler) {
        self.headband = headband
        self.eegHandler = eegHandler
        ringImpedanceLength = headband.model.eegSampleFrequency // 1 second
        
        let eegSampleFrequency = headband.model.eegSampleFrequency
        let samples = eegSampleFrequency * self.period
        let ringImpedanceLength = self.ringImpedanceLength

        //  Create buffers to receive `period` seconds of EEG data
        eeg = (0 ..< headband.model.eegChannelCount).map { _ in // do it this way as Array(repeating:count:) does not copy classes
            RingBuffer<CGXHeadband.SampleType>(repeating: CGXHeadband.SampleType.nan,
                                               count: samples)
        }

        //  Create buffers to track average impedance/channel
        impedance = (0 ..< headband.model.eegChannelCount).map { _ in // do it this way as Array(repeating:count:) does not copy classes
            RingAverage<CGXHeadband.SampleType>(repeating: 0,
                                                count: ringImpedanceLength)

        }

        //  Create a 0.5Hz High Pass filter to remove any DC offset in the data.
        eegFilter1 = (0 ..< headband.model.eegChannelCount).map { _ in // do it this way as Array(repeating:count:) does not copy classes
            BiQuad(.HighPass(frequency: 0.5,
                             sampleRate: CGXHeadband.SampleType(eegSampleFrequency),
                             bandwidth: 0.707))
        }

        //  Create impedance filter
        eegFilter2 = (0 ..< headband.model.eegChannelCount).map { _ in // do it this way as Array(repeating:count:) does not copy classes
            BiQuad(.LowPass(frequency: 30,
                            sampleRate: CGXHeadband.SampleType(eegSampleFrequency),
                            bandwidth: 0.707))
        }

        //  Listen for EEG data from the headband
        headband.registerDataListener(self)
    }
    
    deinit {
        //  Stop listening for EEG data
        headband.unregisterDataListener(self)
    }
    
    func reset() {
        //  Prevent another thread from altering the EEG data
        mutex.lock()
        defer {
            mutex.unlock()
        }

        eeg.forEach { b in
            b.clear()
        }
        //impedance.forEach { i in
        //    i.clear(value: 0)
        //}
    }
        
    func eeg(width: Int) -> (eeg: [[CGXHeadband.SampleType]], impedance: [CGXHeadband.SampleType]) {
        //  Called to ask for a series of EEG samples sclaed to the range 0..1, downsampled to width.
        
        //  Prevent another thread from altering the EEG data
        mutex.lock()
        defer {
            mutex.unlock()
        }
        
        let signalMin = CGXHeadband.SampleType(-1000)
        let signalMax = CGXHeadband.SampleType(1000)
        let impedance = self.impedance.map { a in
            return a.average
        }

        return (eeg: eeg.map({ (channelEEG) -> [CGXHeadband.SampleType] in
            let signal = channelEEG.asArray()
            
            return signal.downsample(decimation: Int(max(ceil(Float(signal.count) / Float(max(1, width))), 1.0))).map { (v) -> CGXHeadband.SampleType in
                guard !v.isNaN else { return 0.5 }
                
                return (v + abs(signalMin)) / (abs(signalMin) + signalMax) // convert -1000...1000 to 0...1
            }
        }),
        impedance: impedance)
    }

    func receive(sample: CGXSample) {
        //  Receive an EEG sample from the headband

        //  Prevent another thread from altering or viewing the EEG data
        mutex.lock()
        defer {
            mutex.unlock()
        }
        
        print(sample.eegV)
        print(sample.impedance)
        
        guard eegHandler != nil else {
            return
        }
        
        
        //stream the eeg and impedance data
        
    }
    
    func receive(dropped: Int, sample: CGXSample) {
        //  Received a series of dropped samples from the headband

        //  Prevent another thread from altering or viewing the EEG data
        mutex.lock()
        defer {
            mutex.unlock()
        }

        //  Add NaN values to the ring buffer's for each channel
        let channelCount = headband.model.eegChannelCount
        
        for _ in 0..<dropped {
            for c in 0..<channelCount {
                eeg[c].append(CGXHeadband.SampleType.nan)
            }
        }
    }
}

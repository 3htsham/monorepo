//
//  EEGStreamHandler.swift
//  Runner
//
//  Created by abrian on 22/07/2021.
//

import CGXKit

public let EEGReceiveNotification = Notification.Name("EEGReceiveNotification")

class EEGStreamHandler: NSObject, FlutterStreamHandler, CGXSampleDelegate {
    var headband: CGXHeadband?
    
    var eventSink: FlutterEventSink? = nil
    
    deinit {
        self.headband?.unregisterDataListener(self)
    }
    
    func registerDataListener() {
        headband?.registerDataListener(self)
    }
    
    func unRegisterDataListener() {
        headband?.unregisterDataListener(self)
    }
    
    func receive(sample: CGXSample) {
        let eegData = FLEEGData(timestamp: sample.timestamp, eeg: sample.eegV, impedance: sample.impedance)
        eventSink?(JsonHelper.toJson(response: eegData))
    }
    
    func receive(dropped: Int, sample: CGXSample) {
        print(sample)
    }
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        print("EEG ON LISTEN CALLED")
        eventSink = events
        registerDataListener()
        return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        print("EEG ON CANCEL CALLED")
        unRegisterDataListener()
        eventSink = nil
        return nil
    }
    
}


//
//  FLCGXHeadband.swift
//  Runner
//
//  Created by abrian on 16/07/2021.
//

import Foundation
import CGXKit

class FLCGXHeadband: Codable {
    var deviceName : String
    var assignedName: String
    var identifier: String
    var lastSeen: Date
    var batteryPercent: Int
    var state: String
    
    init(deviceName: String, assignedName: String, identifier: String, lastSeen: Date, batteryPercent: Int, state: String) {
        self.deviceName = deviceName
        self.assignedName = assignedName
        self.identifier = identifier
        self.lastSeen = lastSeen
        self.batteryPercent = batteryPercent
        self.state = state
    }
    
    static func getHeadbandStateString(state: CGXHeadband.HeadbandState) -> String {
        switch state {
        case .connected:
            return "connected"
            
        case .connecting:
            return "connecting"
            
        case .disconnected:
            return "disconnected"
            
        case .disconnecting:
            return "disconnecting"
        }
    }
}

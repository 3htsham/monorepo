//
//  FLEEGData.swift
//  Runner
//
//  Created by abrian on 23/07/2021.
//

import Foundation

class FLEEGData: Codable {
    var timestamp : UInt64
    var eeg: [Double]
    var impedance: [Double]
    
    init(timestamp: UInt64, eeg: [Double], impedance: [Double]) {
        self.timestamp = timestamp
        self.eeg = eeg
        self.impedance = impedance
    }
}

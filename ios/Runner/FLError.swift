//
//  FLError.swift
//  Runner
//
//  Created by abrian on 16/07/2021.
//

import Foundation

class FLError: Codable {
    var error: String
    var code: Int
    
    init(error: String, code: Int) {
        self.error = error
        self.code = code
    }
}

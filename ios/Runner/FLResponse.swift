//
//  FLResponse.swift
//  Runner
//
//  Created by abrian on 20/07/2021.
//

import Foundation

class FLResponse<T: Codable>: Codable {

    var error: Bool = false
    var message: String?
    var data : T?
    
    public init?(error: Bool, message: String?, data: T) {
        self.error = error
        self.message = message
        self.data = data
    }
}

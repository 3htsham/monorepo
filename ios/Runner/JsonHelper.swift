//
//  JsonHelper.swift
//  Runner
//
//  Created by abrian on 16/07/2021.
//

import Foundation

class JsonHelper {
    
    static let jsonEncoder = JSONEncoder()
    
    static func parseError(error: String) -> String {
        let flResponse = FLResponse<String>(error: true, message: error, data: error)!
        return responseToJson(response: flResponse)
    }
    
    static func responseToJson<T: Codable>(response: FLResponse<T>) -> String {
        let jsonEncoder = JSONEncoder()
        do {
            let data = try jsonEncoder.encode(response)
            if let jsonString = String(data: data, encoding: .utf8) {
                return jsonString
            }
        } catch let error {
            return error.localizedDescription
        }
        return parseError(error: "data parsing error")
    }
    
    static func toJson<T: Codable>(response: T) -> String {
        do {
            let data = try jsonEncoder.encode(response)
            if let jsonString = String(data: data, encoding: .utf8) {
                return jsonString
            }
        } catch let error {
            return error.localizedDescription
        }
        return parseError(error: "data parsing error")
    }
}


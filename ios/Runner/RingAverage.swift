//
//  RingAverage.swift
//


public class RingAverage<T : FloatingPoint> {
    
    private var array: [T]
    private var writeIndex = 0;
    private (set) var total: T
    public let count: Int
    public var average: T {
        guard writeIndex > 0 else { return T.zero }
        return total / T(min(writeIndex, count))
    }

    public init(value: T, count: Int) {
        assert(!value.isNaN)
        self.count = count
        self.array = [T](repeating: value, count: count)
        self.total = value
        self.writeIndex = 1
    }

    public init(count: Int) {
        self.count = count
        self.array = [T](repeating: 0, count: count)
        self.total = 0
        self.writeIndex = 0
    }
    
    public init(repeating: T, count: Int) {
        assert(!repeating.isNaN)
        self.count = count
        self.array = [T](repeating: repeating, count: count)
        self.total = repeating * T(count)
        self.writeIndex = count
    }

    @discardableResult @inline(__always) public func append(_ element: Int) -> RingAverage<T> {
        return append(T(element))
    }
    
    @discardableResult @inline(__always) public func append(_ element: T) -> RingAverage<T> {
        guard !element.isNaN else { return self }
        guard !element.isInfinite else { return self }
        
        if writeIndex >= count {
            let exitingValue = array[writeIndex % count]
            total -= exitingValue
        }
        array[writeIndex % count] = element
        total += element
        writeIndex += 1
        
        return self
    }

    public func clear() {
        writeIndex = 0
        total = 0
    }

    public func clear(value: T) {
        assert(!value.isNaN)
        writeIndex = 0
        total = 0
        append(value)
    }
}

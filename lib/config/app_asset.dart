class AppAssets {
  /// Icons
  static const String icRadioSelected = 'assets/icons/radio_selected.png';
  static const String icRadioUnSelected = 'assets/icons/radio_unselect.png';
  static const String batteryLowIcon = "assets/icons/low_battery_icon.png";
  static const String batteryNormalIcon =
      "assets/icons/normal_battery_icon.png";
  static const String arrowDownIcon = "assets/icons/arrow_down.png";
  static const String batteryFullIcon = "assets/icons/battery_full_icon.png";
  static const String checkIcon = "assets/icons/check.png";
  static const String checkCircleIcon = "assets/icons/check-circle.png";
  static const String xCircleIcon = "assets/icons/x-circle.png";
  static const String leftArrow = "assets/icons/arrow_left.png";
  static const String rightArrow = "assets/icons/arrow_right.png";

  static const String connectionSuccessIcon =
      "assets/icons/green_connection_icon.png";
  static const String connectionErrorIcon =
      "assets/icons/error_connection_icon.png";

  /// Images
  static const String greenSeekerImage = "assets/images/green_seeker_image.png";
  static const String universeMasterImage = "assets/images/universe_master.png";
  static const preShotRoutineImage = "assets/images/pre_shot_image.png";

  static const String greenSeekerGameBackground =
      'assets/images/game_greenseeker.png';
  static const String universeMasterGameBackground =
      'assets/images/universe_master_launch_img.png';

  static const String preShotRoutineGameBackground =
      'assets/images/pre_shot_routine_launch_img.png';

  static const String userProfileImageBackgroundPattern =
      "assets/images/profile_image_background_pattern.png";

  static const String editIcon = "assets/icons/edit_icon.png";
  static const String cameraIcon = "assets/icons/camera_icon.png";

  static const String resourcesFeaturedImage =
      "assets/images/resource_image.png";
  static const String headgearSettingUpImage =
      "assets/images/setting_up_headgrear_image.png";
  static const String supportFeatureImage = "assets/images/support_image.png";
  static const String settingUpHeadgearResource =
      "assets/images/setting_up_headgear_resource.jpg";

  static const String craftingPreShotRoutineFeature =
      "assets/images/crafting_preshot_routine_feature.png";
  static const String enteringTheExpertStateFeatureImage =
      "assets/images/entering_the_expert_state_image.png";
  static const String headsetModelImage =
      "assets/images/headset_model_image.png";
  static const String bluetoothSettingsScreenshot =
      "assets/images/bluetooth_settings_screenshot.png";

  static const String beepSound = "assets/audio/default_bing.wav";
  static const String feedbackTone = "assets/audio/audio_feedback_tone.wav";
  static const String fittingHead = "assets/images/fitting_head.png";

  static const String check = "assets/icons/check.png";
  static const String close = "assets/icons/close.png";
}

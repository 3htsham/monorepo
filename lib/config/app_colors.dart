import 'package:flutter/material.dart';

class AppColors {
  ///Headline 1 = 80 Regular Bebas => 60
  ///Headline2 = 48 Regular Bebas => 40
  ///HEadline 3 = 32 Bold Objektiv => 26
  ///Headline 4 = 20 Bold Objektiv => 18
  ///HEadline 5 =
  ///Headline 6 = 19 Bold Objektiv => 14
  ///Subtitle1 = 24 Bold Objectiv => 16
  ///Subtitle2 = 16 Bold Objektiv => 12
  ///BodyText1 = 16 Regular Objektiv => 13
  ///BodyText2 = 14 Regular Objektiv => 12
  ///Caption = 12 Regular Objektiv

  static Color scaffoldBackgroundColor = Color(0xFF012432);
  static Color mainColorDark = Color(0xFF00374E);

  static Color cyanDark = Color(0xFF008291);
  static Color cyan = Color(0xFF0198AA);

  static Color orange = Color(0xFFF15922);
  static Color orangeLight = Color(0xFFF06A21);
  static Color yellow = Color(0xFFFCAF17);
  static Color errorColor = Color(0xFFFA474B);
  static Color placeholderColor = Color(0xFFFF00EB);
  static Color greenColor = Color(0xFF4CAF50);
  static Color white = Color(0xFFFFFFFF);


  //For Gradient (whitish + greenish)
  static Color gradientLight1 = Color(0xFFE8E8E8);
  static Color gradientLight2 = Color(0xFF4CAF50);

  //For Gradient (yellowish + cyan)
  static Color gradientDark1 = Color(0xFFF1B243);
  static Color gradientDark2 = Color(0xFF4396A7);

  static Color shadowColor = Color(0xFF0B2331);

  //Headset Setup Sensors Indicators Colors
  static Color inactiveSensor = Color(0xFFC6C6C6);
  static Color activeSensor = Color(0xFF4CAF50);
  static Color errorSensor = Color(0xFFFA474B);
  static Color warningSensor = Color(0xFFFCAF17);

  MaterialColor scaffoldMaterialColor = MaterialColor(0xFF012432, {
    50:Color(0xFF012432),
    100:Color(0xFF012432),
    200:Color(0xFF012432),
    300:Color(0xFF012432),
    400:Color(0xFF012432),
    500:Color(0xFF012432),
    600:Color(0xFF012432),
    700:Color(0xFF012432),
    800:Color(0xFF012432),
    900:Color(0xFF012432),
  });

  static LinearGradient yellowGreenHorizontalGradient() {
    return LinearGradient(
        colors: [gradientDark1, gradientDark2],
        begin: Alignment.centerLeft,
        end: Alignment.centerRight);
  }
}

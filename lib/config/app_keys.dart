import 'package:brain_trainer/views/main_page/main_page.dart';
import 'package:flutter/material.dart';

class AppKeys {
  static GlobalKey<MainTabsState> mainTabsKey = GlobalKey<MainTabsState>();

  static GlobalKey<NavigatorState> hardwareNavigatorKey =
      GlobalKey<NavigatorState>();
  static GlobalKey<NavigatorState> trainingNavigatorKey =
      GlobalKey<NavigatorState>();
  static GlobalKey<NavigatorState> reportsNavigatorKey =
      GlobalKey<NavigatorState>();
  static GlobalKey<NavigatorState> supportNavigatorKey =
      GlobalKey<NavigatorState>();
  static GlobalKey<NavigatorState> resourcesNavigatorKey =
      GlobalKey<NavigatorState>();
}

import 'package:flutter/material.dart';

class AppStyle {
  static TextTheme getAppTextTheme(BuildContext context) => TextTheme(
        headline1: mainAppTextStyle().copyWith(
          fontSize: 80.0,
        ),
        headline2: mainAppTextStyle().copyWith(
          fontSize: 48.0,
        ),
        headline3: mainAppTextStyleObjektive()
            .copyWith(fontSize: 32.0, fontWeight: FontWeight.w800),
        headline4: mainAppTextStyleObjektive()
            .copyWith(fontSize: 20.0, fontWeight: FontWeight.w800),
        headline5: mainAppTextStyleObjektive()
            .copyWith(fontSize: 16.0, fontWeight: FontWeight.w800),
        headline6: mainAppTextStyleObjektive()
            .copyWith(fontSize: 19.0, fontWeight: FontWeight.w800),
        subtitle1: mainAppTextStyleObjektive()
            .copyWith(fontSize: 24.0, fontWeight: FontWeight.w800),
        subtitle2: mainAppTextStyleObjektive()
            .copyWith(fontSize: 16.0, fontWeight: FontWeight.w600),
        bodyText1: mainAppTextStyleObjektive()
            .copyWith(fontSize: 16.0, fontWeight: FontWeight.w400),
        bodyText2: mainAppTextStyleObjektive()
            .copyWith(fontSize: 14.0, fontWeight: FontWeight.w400),
        caption: mainAppTextStyleObjektive()
            .copyWith(fontSize: 12.0, fontWeight: FontWeight.w400),
      );

  static TextStyle mainAppTextStyle() => TextStyle(
      fontFamily: 'BabesNeue',
      fontWeight: FontWeight.w500,
      color: Colors.white);

  static TextStyle mainAppTextStyleObjektive() => TextStyle(
      fontFamily: 'ObjektivMk2',
      fontWeight: FontWeight.w800,
      color: Colors.white);
}

const kDefaultBadImpedanceThreshold = 127.0;
const kDefaultMediocreImpedanceThreshold = 40.0;

enum ImpedanceMeasurementLevel {
  bad,
  mediocre,
  good,
  unknown,
}

ImpedanceMeasurementLevel getImpedanceMeasurementLevel(measurement,
    {bad = kDefaultBadImpedanceThreshold,
    mediocre = kDefaultMediocreImpedanceThreshold}) {
  if (measurement > bad) {
    return ImpedanceMeasurementLevel.bad;
  } else if (measurement > mediocre) {
    return ImpedanceMeasurementLevel.mediocre;
  } else {
    return ImpedanceMeasurementLevel.good;
  }
}

ImpedanceMeasurementLevel getWorstImpedanceMeasurementLevel(measurements,
    {bad = kDefaultBadImpedanceThreshold,
    mediocre = kDefaultMediocreImpedanceThreshold}) {
  if (measurements.any((e) =>
      getImpedanceMeasurementLevel(e, bad: bad, mediocre: mediocre) ==
      ImpedanceMeasurementLevel.bad)) {
    return ImpedanceMeasurementLevel.bad;
  } else if (measurements.any((e) =>
      getImpedanceMeasurementLevel(e, bad: bad, mediocre: mediocre) ==
      ImpedanceMeasurementLevel.mediocre)) {
    return ImpedanceMeasurementLevel.mediocre;
  } else {
    return ImpedanceMeasurementLevel.good;
  }
}

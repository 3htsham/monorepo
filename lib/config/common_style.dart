import 'package:flutter/material.dart';

// Generates "Material Design" color swatch
MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  Map<int, Color> swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}

const optiosCorporateDarkBlue = const Color(0xff05313b);
const optiosCorporateLightBlue = const Color(0xff008899);
const optiosCorporateAccent = const Color(0xfff15922);
const optiosCorporateBlack = const Color(0xff000000);
const optiosCorporateWhite = const Color(0xffffffff);

const optiosAdditionalGold = const Color(0xfffcaf17);
const optiosAdditionalBlue = const Color(0xff00b9c8);
const optiosAdditionalDarkGrey = const Color(0xff4a5960);
const optiosAdditionalMediumGrey = const Color(0xff737e85);
const optiosAdditionalLightGrey = const Color(0xffa5acb1);

const optiosFunctionalGreen = const Color(0xff4caf50);
const optiosFunctionalRed = const Color(0xffe53935);
const optiosFunctionalOrange = Colors.orange;

final optiosMaterialDarkBlue = createMaterialColor(optiosCorporateDarkBlue);
final optiosMaterialLightBlue = createMaterialColor(optiosCorporateLightBlue);
final optiosMaterialAccent = createMaterialColor(optiosCorporateAccent);

final optiosMaterialAdditionalGold = createMaterialColor(optiosAdditionalGold);
final optiosMaterialAdditionalBlue = createMaterialColor(optiosAdditionalBlue);
final optiosMaterialAdditionalDarkGrey =
    createMaterialColor(optiosAdditionalDarkGrey);
final optiosMaterialAdditionalMediumGrey =
    createMaterialColor(optiosAdditionalMediumGrey);
final optiosMaterialAdditionalLightGrey =
    createMaterialColor(optiosAdditionalLightGrey);

final optiosMaterialFunctionalGreen =
    createMaterialColor(optiosFunctionalGreen);
final optiosMaterialFunctionalRed = createMaterialColor(optiosFunctionalRed);
final optiosMaterialFunctionalOrange =
    createMaterialColor(optiosFunctionalOrange);

const primaryColor = optiosCorporateDarkBlue;
const accentColor = optiosCorporateAccent;
const backgroundColor = optiosCorporateDarkBlue;
const linkColor = optiosCorporateLightBlue;

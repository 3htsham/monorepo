class S {
  static const String welcome = "Welcome!";
  static const String pleaseLoginToContinue = "Please log in to continue";

  static const String emailAddress = "Email Address";
  static const String email = "Email";
  static const String emailAddressCantBeEmpty = "Email Address can't be empty";
  static const String password = "Password";
  static const String confirmPassword = "Confirm password";
  static const String passwordDidNotMatch = "Password didn't match";
  static const String passwordCantBeEmpty = "Password can't be empty";
  static const String logIn = "Log in";
  static const String logOut = "Log out";
  static const String forgetPassword = "Forget Password?";
  static const String doNotHaveAnAccount = "Don't have an account?";
  static const String createNewAccount = "Create new account";
  static const String pleaseEnterYourPassword = "Please enter your password.";
  static const String pleaseEnterAValidPassword =
      "Please enter a valid password.";

  static const String createAnAccount = "Create an account";
  static const String required = "Required";
  static const String firstName = "First name";
  static const String lastName = "Last name";
  static const String birthday = "Birthday";
  static const String enterValidBirthday = "Enter a valid birthday";
  static const String optional = "Optional";
  static const String next = "Next";

  static const String cross = "×";
  static const String bullet = "•";
  static const String check = "✔";
  static const String atLeastTenChars = "At least 10 characters.";
  static const String atLeastOneUppercase = "At least one uppercase.";
  static const String atLeastOneLowercase = "At least one lowercase.";
  static const String atLeastOneNumber = "At least one number.";

  static const String tellUsAboutYourGolfGame = "Tell us about your golf game.";
  static const String handicapIndex = "Handicap index";
  static const String fieldCantBeEmpty = "Field can't be empty";
  static const String handicapGoal = "Handicap goal";
  static const String primaryGolfCourse = "Primary golf course";
  static String leftHand = "Left handed";
  static String rightHand = "Right handed";
  static String submit = "Submit";
  static String verifyEmailSentTitle =
      "Email verification";
  static String verifyEmailSentBody =
      "Click on the link that has just been sent to\nyour email account to verify your email.";
  static const String hardware = "Hardware";
  static const String training = "Training";
  static const String reports = "Reports";
  static const String support = "Support";
  static const String resources = "Resources";

  static const String chargeBeforeProceeding = "Charge before proceeding";
  static const String batteryLevelNotAbleToComplete =
      "At this battery level you may not be able\nto complete a training session";

  static const String hardwareSetup = "Hardware Set-up";
  static const String headsetFitting = "Headset Fitting";
  static const String stepOne = "Step 1:";
  static const String stepOneInstructions =
      "• Ensure hair is dry \n\n• Remove any facial or hair products in the regions where the sensor makes contact\n\n• Make sure headset is aligned to back center of\n you head\n\n• Headset should be snug but comfortable";

  static const String stepTwo = "Step 2:";
  static const String stepTwoInstructions =
      "Place the headset firmly on your head ensuring that the adjustment strap and hardware is aligned to the back-center of your head. The fitting should be snug but comfortable.";

  static const String testConnection = "Test Connection";

  static const String learnMore = "Learn more";
  static const String hardwareConnectingIssue = "Hardware connecting issue";
  static const String hardwareConnectingIssueInstructions =
      "Having issue connecting? Watch this video for simple step-by-step instructions and common connection issues";

  static const String hardwareSupport = "Hardware Support";
  static const String hardwareSupportInstructions =
      "Need help troubleshooting your connection? Call us at ";

  static const String greenSeeker = "Green Seeker";
  static const String greenSeekerDescription =
      "Your training goal during this 90 second activity is to stay \nin the optimal green zone (above the target line) for as \nlong as possible.";
  static const String trainNow = "Train Now";
  static const String fitTestingInProgress = "Fit testing in progress...";

  static const String adjustmentRequired = "Adjustment Required";
  static const String adjustmentRequiredInstruction =
      "Locate any sensors not lit in green. Try lifting the band slightly, adjusting hair out of the way, and firmly pressing the sensor back down";
  static const String adjustmentRequiredFurtherMoreInstruction =
      "If the problem persists, try wiggling the sensor back and forth to ensure contact with your head.";

  static const String reTestConnection = "Re-test connection";
  static const String successfullyConnected = "Successfully Connected!";

  static const String startTraining = "Start training";
  static const String testVibration = "Test Vibration";
  static const String didYourHeadsetVibrate = "Did your headset vibrate?";
  static const String yes = "Yes";
  static const String no = "No";

  static const String greatYouAreAllSet = "Great, you are all set!";
  static const String letUsHelpYouToTroubleshootYourDevice =
      "Let us help you troubleshoot your device.";
  static const String talkToOurCustomerSupport =
      "Talk to our customer support.";
  static const String customerSupportNumber = "888.404.2778";
  static const String itSupportEmail = "itsupport@optios.com";

  static const String welcomeBack = "Welcome back";
  static const String welcomeBackDescription =
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.";

  static const String expertBrainStateTraining = "OG brain state training";

  static const String fundamentalTrainingToUnderstandHowToReachTheExpertState =
      "Foundational training to learn how to access\naccess the expert state based on simple\nvisual cues.";

  static const String universeMaster = "Universe Master";
  static const String gainControlOverEnteringTheExportStateInOrderToEasily =
      "Gain control over the OG brain state in conjunction with a simple task.";

  static const String routine = "Routine";
  static const String preShotRoutine = "Pre-shot Routine";
  static const String associateTheExpertBrainStateWithYourPreShot =
      "Associate the OG brain state with your pre-shot routine.";

  static const String preShotPause = "Pause";
  static const String preShotResume = "Resume";
  static const String preShotFinish = "Finish";

  static const String monoRepo = "Mono Repo";
  static const String gameSessionResult = "Session Result";
  static const String trainingTime = "Training Time";
  static const String timeInExpertState = "Time in OG State";
  static const String expertStateMagnitudeOverTime =
      "OG State Magnitude Over Time";

  static const String aboveTarget = "Above target";
  static const String belowTarget = "Below target";
  static const String expertStateTarget = "OG\nState\nTarget";

  static const String trainAgain = "Train Again";
  static const String finish = "Finish";
  static const String done = "Done";
  static const String percentTimeInExpertState = "% of time in OG state";
  static const String good = "Good";
  static const String excellent = "Excellent";
  static const String superior = "Superior";
  static const String pastReport = "Past Reports";

  static const String ogStateTarget='OG State Target';

  static String mostRecentAverageTime(String gameName) =>
      "$gameName: Most recent average time in OG state by session";
  static const String universeMasterDescription =
      "This exercise is a 90 second activity to help you gain \ncontrol over the expert state. Your goal is to keep your \nspaceship (orange and white orb) close to the path (light \ngrey line).  As you pass through the circle checkpoints, you \nwill get feedback on your performance. If the circle lights-\nup green, congratulations, you are in the expert state!";
  static const String preShotRoutineDescription =
      "This module is designed to train your brain to\nautomatically enter the expert state during your pre-shot\nroutine. Your goal is to practice your routine until it\nbecomes automatic and you build an association to \nthe expert state by the end. After each round, pay \nattention to how your breathing, body movement, and \nmuscle tension impact your brain state.";
  static const String endedInExpertState = 'Ended in Expert State';
  static const String recentSections = 'Recent Sections';
  static const String duration = 'Duration';

  static const String account = "Account";
  static const String enterValidEmail = "Enter a valid Email";

  static const String difficultyLevel = "Difficulty Level";
  static const String recommendedDifficultyLevel =
      "Recommended difficulty level: 5";
  static const String userHandicapIndex = "User handicap Index";
  static const String homeCourse = "Home Course";
  static const String handicapIndexCanNotBeEmpty =
      "Handicap index can't be empty";
  static const String homeCourseCanNotBeEmpty = "Home Course can't be empty";

  static const String resetHardware = "Reset Hardware";
  static const String excessiveMotionOrSweatDetected =
      "Excessive motion \nor sweat detected.";
  static const String resetYourHardware = "Reset your hardware.";
  static const String hardwareConnectionLost = "Hardware \nconnection lost.";

  static const String trainingTipsAndResources = "Training tips & Resources";
  static const String settingUpTheHeadgear = "Setting up the headgear";
  static const String settingUpHeadgearDescription =
      "Follow these tips if you are having trouble getting a good \nconnection with your headset";
  static const String usingTheSystemDoingPractice =
      "Using the system doing practice";
  static const String usingTheSystemDoingPracticeDescription =
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam \nnonumy eirmod tempor invidunt ut labore et dolore magna \naliquyam erat, sed diam voluptua. At vero eos et accusam et justo \nduo dolores et ea rebum. Stet clita kasd gubergren, no sea \ntakimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum \ndolor sit amet, consetetur sadipscing elitr, sed diam nonumy \neirmod tempor invidunt ut labore et dolore magna aliquyam erat, \nsed diam voluptua. At vero eos et accusam et justo duo dolores et \nea rebum. Stet clita kasd gubergren, no sea takimata sanctus est \nLorem ipsum dolor sit amet.";
  static const String socksMetFacilities = "Socks met facilities";
  static const String socksMetFacilitiesDescription =
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam \nnonumy eirmod tempor invidunt ut labore et dolore magna \naliquyam erat, sed diam voluptua. At vero eos et accusam et justo \nduo dolores et ea rebum. Stet clita kasd gubergren, no sea \ntakimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum \ndolor sit amet, consetetur sadipscing elitr, sed diam nonumy \neirmod tempor invidunt ut labore et dolore magna aliquyam erat, \nsed diam voluptua. At vero eos et accusam et justo duo dolores et \nea rebum. Stet clita kasd gubergren, no sea takimata sanctus est \nLorem ipsum dolor sit amet.";

  static const String serviceAndSupport = "Service & Support";
  static const String hardwareConnectionTips = "Hardware Connection Tips";
  static const String tipOne = "Tip #1";
  static const String tipOneDetails =
      "Try lifting the band slightly, adjusting hair out of the way, \nand firmly pressing the sensor back down.  If the problem \npersists, try wiggling the sensor back and forth to ensure \ncontact with your head.";
  static const String tipTwo = "Tip #2";
  static const String tipTwoDetails =
      "Take a damp napkin or a wet wipe across the areas where \nthe sensors sit (forehead and above ears). Dry the area \nthoroughly before putting the headset on again.";
  static const String tipThree = "Tip #3";
  static const String tipThreeDetails =
      "If you have difficulty getting one sensor to connect, try \nreplacing it with a new one. With some light pressure, \nsnap the existing sensor off and use a replacement one.";
  static const String supportNumber = "888.404.2778";

  static const String resetPassword = "Reset password";
  static const String emailMeARecoveryLink = "Email me a recovery link";
  static const String returnToLogin = "Return to login";
  static const String recoverPassword = "Recover Password";
  static const String ifAccountExistsAnEmailWillBeSentWithFurtherInstructions =
      "If account exists, an email will be sent with \nfurther instructions";
  static const String resendTheEmail = "Resend the email";
  static const String emailSent = "Email Sent";

  static const changePassword = "Change password";
  static const updatePassword = "Update password";
  static const newPassword = "New password";
  static const confirmNewPassword = "Confirm new password";
  static const newPasswordSaved = "New password saved.";

  static const String uploadPhoto = "Upload photo";
  static const String howDoYouWantToUploadPhoto =
      "Choose how do you want to upload new photo?";
  static const String gallery = "Gallery";
  static const String camera = "Camera";

  static const String yourPGACoach = "Your PGA coach";
  static const String yourNeuroScientist = "Your neuroscientist";

  static const headsetCalibration = "Headset Calibration";
  static const headsetCalibrationInstruction =
      "Sit still with your eyes open and focused on a specific\nspot. Once you hear the beep, close your eyes. You hear\nanother beep to open your eyes. This process of\nalternating between eyes open and closed will continue\nfor a total of 60 sec.";
  static const startCalibration = "Start Calibration";
  static const eyesOpen = "Eyes Open";
  static const eyesClosed = "Eyes Closed";
  static const calibrationSuccessfully = "Calibration Successfully";
  static const forAnOptimalConnection = "For an optimal connection";
  static const String learnHowToAccessThe =
      "Learn to access the OG state based on simple visual cues.";
  static const String feedback = "Audio";

  static const settingUpHeadGearTipOne = "Tip #1";
  static const settingUpHeadGearTipTwo = "Tip #2";
  static const settingUpHeadGearTipThree = "Tip #3";
  static const settingUpHeadGearTipOneDetails =
      "Try lifting the band slightly, adjusting hair out of the way,\nand firmly pressing the sensor back down. If the problem\npersists try wiggling the sensor back and forth to ensure\ncontact with your head.";
  static const settingUpHeadGearTipTwoDetails =
      "Take a damp napkin or a wet wipe across the areas where\nthe sensors sit (forehead or above ears). Dry the area\nthoroughly before putting the headset on again";
  static const settingUpHeadGearTipThreeDetails =
      "If you have difficulty getting one sensor to connect, try\nreplacing it with a new one. With some light pressure,\nsnap the existing sensor off and use a replacement one.";

  static const String enteringTheExpertState = "Entering the expert state";
  static const String practiceTheseSkillsConsistentlyForBodyRelaxation =
      "Practice these skills consistently for body relaxation, a calm mind,\nand enhanced focus.";
  static const String craftingAPreShotRoutine = "Crafting a pre-shot routine";
  static const String consistentlyPracticeASimpleSeriesOfSteps =
      "Consistently practice a simple series of steps to give you\nconfidence in both your swing and mental game.";

  static const String tipFour = "Tip #4";
  static const String tipFive = "Tip #5";
  static const String yourOverallGoalIsToDevelopAConsistent =
      "Your overall goal is to develop a consistent, automatic routine associated in your brain to the expert state";
  static const String beginToDeepenYourBreath =
      "Begin to deepen your breath as soon as you start your\npre-shot routine";
  static const String whileWalkingAroundTheHole =
      "While walking around the hole, evaluate the green\nsystematically, and visualize the path your ball will follow";
  static const String setUpYourBallAndBody =
      "Set-up your ball and body so everything is aligned on the\ncorrect trajectory";
  static const String pickATargetLineAndStick =
      "Pick a target line and stick to it. Commitment is key";
  static const String practicingAConsistentRoutine =
      "Practicing a consistent routine in conjunction with the\nOG state will make the execution of your putt\nsubconscious and increase your confidence";

  static const String yourOverallGoalIsToSlowYourHeartRate =
      "Your overall goal is to slow your heart rate, deepen your breath, relax your muscles, and calm your mind while maintaining focus.";
  static const String tryInhalingThrough =
      "Try inhaling through your nose for four seconds, pausing\nfor seven, and exhaling through your mouth for eight.";
  static const String tryTensingAndRelaxingDifferent =
      "Try tensing and relaxing different muscle groups while\nbreathing slowly and deeply. (Inhale and tense, exhale\nand release. Repeat for areas where you continue to\ncarry tension.)";
  static const String tryVisualizingASuccessfulPuttUsing =
      "Try visualizing a successful putt using all five senses. Be\nspecific. Focus on your stance, feel of the grip, direction\nof the wind, sound of the putter making contact, path of\nthe golf ball, etc...";
  static const String bluetoothPermissionRequired =
      "Bluetooth permission\nrequired";
  static const String weNeedBluetoothPermissionBeforeWeCan =
      "We need Bluetooth permission before we can continue.\nGo to your device\'s settings menu and allow Bluetooth\nsharing so we can connect your products.";
  static const String goToSettings = "Go to Settings";
  static const String devices = "Devices";
  static const String bluetoothRequired = "Bluetooth Required";
  static const String turnOnBluetooth = "Turn ON Bluetooth";
  static const String disconnect = "Disconnect";
  static const String delete = "Delete";
  static const String connect = "Connect";
  static const String notConnected = "Not connected";
  static const String proceed = "Proceed";
  static const String trainingBeginsIn = "Training begins in";

  static const String deviceNotConnected = "Device not connected";
  static const String pleaseGoToTheHardwareTab =
      "Please go to the \“Hardware\” tap to connect your device.";
  static const String setupHardware = "Setup Hardware";

  static const String deviceNotConnectedTryAgain =
      "Device not connected, try again";
  static const String pleaseTurnOnYourOptiosDevice =
      "Please turn on your Optios device";
  static const String lastUsed = "Last used";
  static const String addNewDevice = "Add new device";
  static const String opps = 'Opps!';
  static const String wrongPasswordProvided = 'Wrong password provided.';
  static const String tryAgain = 'Try again';
  static const String wrongEmail = 'Wrong Email';

  static const String weakPasswordProvided = '';
  static const String emailAlreadyExists = 'The account already exists for that email.';
}

class ErrorMessage {
  static String emailNotVerified = 'Email is not verified';
}

class AppDateFormat {
  static const String normalDateFormat = r'M j, Y';
}

class CGXChannels {
  static const String bluetoothEventChannel = "cgxkitevent/bluetooth";
  static const String devicesEventChannel = "cgxkitevent/devices";
  static const String deviceStateEventChannel = "cgxkitevent/deviceState";
  static const String eegEventChannel = "cgxkitevent/eeg";
  static const String impedanceEventChannel = "cgxkitevent/impedance";
  static const String methodChannel = "com.optios.golf/cgxkit";
}

class CGXMethods {
  static const connect = "connect";
  static const getBatteryLevel = "getBatteryLevel";
  static const disconnect = "disconnect";
  static const startEEGAcquisition = "startEEGAcquisition";
  static const stopEEGAcquisition = "stopEEGAcquisition";
}

class Prefs {
  static const String cachedDevices = "CachedDevices";
  static const String lastUsedDevice = "LastUsedDevice";
}

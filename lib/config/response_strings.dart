class ResponseStrings  {

  //Bluetooth
  static const poweredOff = "powered off";
  static const unauthorized = "unauthorized";
  static const poweredOn = "powered on";
  static const notAvailable = "not available";

  //Device State
  static const connected = "connected";
  static const connecting = "connecting";
  static const disconnected = "disconnected";
  static const disconnecting = "disconnecting";

}
import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';
import 'package:brain_trainer/elements/headset/mini_sensor_detector.dart';
import 'package:brain_trainer/elements/others/mini_battery_indicator.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/models/headband_models/cgx_headband.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:catcher/catcher.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainAppbar extends StatelessWidget implements PreferredSizeWidget {
  final Key? appbarKey;
  final VoidCallback? onBack;
  final VoidCallback? onAccountTap;
  final VoidCallback? onGameNameTap;

  final String? batteryIndicatorTitle;
  final double? batteryPercentage;
  final bool showBatteryIndicator;

  final bool showDropDownICon;
  final String centerTitleText;

  Widget? dropDownGameButton;
  final bool connectionStateStable;

  MainAppbar(
      {this.appbarKey,
      this.onBack,
      this.onAccountTap,
      this.onGameNameTap,
      this.batteryIndicatorTitle,
      this.batteryPercentage,
      this.showBatteryIndicator = false,
      this.centerTitleText = "",
      this.showDropDownICon = false,
      this.connectionStateStable = true,
      this.dropDownGameButton});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    final headband =
        context.select<HardwareService, CGXHeadband?>((s) => s.headband);
    var isActive =
        headband != null && headband.state == HeadbandState.connected;
return Padding(
      padding: EdgeInsets.only(top: 23),
      child: AppBar(
        key: this.appbarKey ?? GlobalKey(),
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        leading: onBack != null
            ? IconButton(
                onPressed: () {
                  onBack!();
                  Navigator.of(context).pop();
                },
                highlightColor: Colors.white.withOpacity(0.08),
                icon: Icon(
                  CupertinoIcons.back,
                  color: Colors.white,
                ),
              )
            : SizedBox(),
        titleSpacing: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            this.showBatteryIndicator
                ? Container(
                    // width: (size.width / 100) * 19.66,
                    // height: 48.49,
                    margin: EdgeInsets.symmetric(vertical: 20),
                    padding: EdgeInsets.symmetric(vertical: 7, horizontal: 8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white.withOpacity(0.16),
                        border:
                            Border.all(color: Colors.white.withOpacity(0.24))),
                    child: Center(
                      child: FutureBuilder<double>(
                          initialData: 0,
                          future:
                              context.read<HardwareService>().getBatteryLevel(),
                          builder: (context, snapshot) {
                            return Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                MiniSensorsDetector(
                                  sensorOneStatus: isActive
                                      ? SensorStatus.active
                                      : SensorStatus.error,
                                  sensorTwoStatus: isActive
                                      ? SensorStatus.active
                                      : SensorStatus.error,
                                  sensorThreeStatus: isActive
                                      ? SensorStatus.active
                                      : SensorStatus.error,
                                  sensorFourStatus: isActive
                                      ? SensorStatus.active
                                      : SensorStatus.error,
                                ),
                                SizedBox(
                                  width: 7,
                                ),
                                MiniBatteryIndicator(
                                  batteryPercentage: this.batteryPercentage ??
                                      double.parse(snapshot.data.toString()),
                                ),
                                SizedBox(
                                  width: 7,
                                ),
                                Text(
                                  this.batteryIndicatorTitle ??
                                      '${double.parse(snapshot.data.toString()).toStringAsFixed(0)}%',
                                  style: textTheme.caption,
                                )
                              ],
                            );
                          }),
                    ),
                  )
                : SizedBox(),

            //To display current game name and DropDown
            dropDownGameButton ??
                InkWell(
                  onTap: this.onGameNameTap,
                  highlightColor: Colors.transparent,
                  child: Row(
                    children: <Widget>[
                      Text(
                        centerTitleText,
                        style: textTheme.bodyText2,
                      ),
                      showDropDownICon
                          ? Icon(
                              Icons.keyboard_arrow_down,
                              color: Colors.white,
                              size: 18,
                            )
                          : SizedBox()
                    ],
                  ),
                ),

            SizedBox(),
          ],
        ),
        actions: <Widget>[
          InkWell(
            onTap: this.onAccountTap ??
                () {
                  Catcher.navigatorKey?.currentState
                      ?.pushNamed(Routes.USER_ACCOUNT);
                },
            highlightColor: theme.focusColor,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  '${context.watch<TrainingService>().subject?.firstName ?? ""} ${context.watch<TrainingService>().subject?.lastName ?? ""}  ',
                  style: textTheme.bodyText2?.merge(
                      TextStyle(fontWeight: FontWeight.w800, fontSize: 14)),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(40),
                  child: context.watch<TrainingService>().subject?.imgUrl != null
                      ? Image.network(
                          context.watch<TrainingService>().subject?.imgUrl ?? "",
                          width: 32,
                          height: 32,
                        )
                      : Image.asset(
                          "assets/icons/user_icon_account.png",
                          color: Colors.white,
                          width: 32,
                          height: 32,
                        ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 15,
          ),
        ],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(72);
}

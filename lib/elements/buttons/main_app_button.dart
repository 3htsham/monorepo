import 'package:flutter/material.dart';

class MainAppButton extends StatelessWidget {
  const MainAppButton(
      {@required this.onTap,
      @required this.title,
      this.textStyle,
      this.height,
      this.width,
      this.backgroundColor = Colors.transparent});

  final Function? onTap;
  final TextStyle? textStyle;
  final String? title;
  final double? width;
  final double? height;
  final Color backgroundColor;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: ElevatedButton(
        onPressed: () {
          onTap?.call();
        },
        style: ElevatedButton.styleFrom(
          // primary: backgroundColor,
          shape: StadiumBorder(),
        ),
        child: Center(
          child: Text(title ?? "",
              style: textStyle ?? Theme.of(context).textTheme.headline6),
        ),
      ),
    );
  }
}

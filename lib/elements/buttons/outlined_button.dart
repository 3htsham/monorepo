import 'package:flutter/material.dart';

class AppOutLinedButton extends StatelessWidget {
  const AppOutLinedButton(
      {@required this.onTap,
      @required this.title,
      this.textStyle,
      this.height,
      this.width,
      this.backgroundColor = Colors.transparent,
      this.borderColor,
      this.borderRadius = 100});

  final Function? onTap;
  final TextStyle? textStyle;
  final String? title;
  final double? width;
  final double? height;
  final double borderRadius;
  final Color backgroundColor;
  final Color? borderColor;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? null,
      height: height ?? null,

      ///This was RaisedButton
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: backgroundColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(borderRadius),
                side: BorderSide(
                    color: borderColor ?? Colors.white.withOpacity(0.24)))),
        onPressed: () {
          onTap?.call();
        },
        child: Center(
          child: Text(title ?? "",
              style: textStyle ?? Theme.of(context).textTheme.bodyText2?.copyWith(fontWeight: FontWeight.w600)),
        ),
      ),
    );
  }
}

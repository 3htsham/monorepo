import 'package:flutter/material.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;
import 'package:flutter/services.dart';

class CustomTextFormField extends StatelessWidget {
  final double? width;
  final String? Function(String?)? validator;
  final TextEditingController? controller;
  final String? initialValue;
  final String? hint;
  final void Function(String?)? onSaved;
  final Function(String?)? onSubmitted;

  final Widget? prefixIcon;

  final Widget? suffixIcon;
  final bool obscureText;

  final TextInputType? inputType;

  final FocusNode? focusNode;

  final TextInputAction? inputAction;

  final Function(String?)? onChange;

  final List<TextInputFormatter>? inputFormatter;

  CustomTextFormField(
      { this.width = 334,
      this.validator,
      this.controller,
      this.initialValue,
      @required this.hint,
      this.prefixIcon,
      this.onSaved,
      this.suffixIcon,
      this.obscureText = false,
      this.inputType,
      this.focusNode,
      this.onSubmitted,
      this.inputAction,
      this.onChange,
      this.inputFormatter})
      : assert(initialValue == null || controller == null);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: BorderSide(width: 1, color: Colors.white));

    return Container(
      width: this.width,
      child: TextFormField(
        controller: this.controller,
        initialValue: this.initialValue,
        textInputAction: this.inputAction ?? TextInputAction.done,
        focusNode: this.focusNode ?? FocusNode(),
        obscureText: this.obscureText,
        validator: this.validator,
        style: textTheme.bodyText1,
        keyboardType: this.inputType ?? TextInputType.text,
        autocorrect: false,
        autofocus: false,
        onSaved: this.onSaved,
        onFieldSubmitted: this.onSubmitted,
        onChanged: this.onChange,
        // inputFormatters: this.inputFormatter,
        decoration: InputDecoration(
          filled: true,
          fillColor: config.AppColors.mainColorDark,
          focusColor: config.AppColors.mainColorDark,
          contentPadding: EdgeInsets.only(top: 15, right: 15, left: 15),
          enabledBorder: border,
          disabledBorder: border,
          focusedBorder: border,
          errorBorder: border,
          border: border,
          focusedErrorBorder: border,
          hintText: this.hint ?? "",
          errorStyle: textTheme.caption?.merge(
              TextStyle(color: theme.accentColor, fontSize: 12.0, height: 0.8)),
          hintStyle: textTheme.bodyText1?.merge(TextStyle(
            color: theme.hintColor,
          )),
          prefixIcon: this.prefixIcon,
          suffixIcon: this.suffixIcon,
        ),
      ),
    );
  }
}

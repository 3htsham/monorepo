import 'package:flutter/material.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;

// Green = impedance < 1500 => SensorStatus.active
// Yellow = impedance between 1501 and 3020  => SensorStatus.warning
// Red = impedance > 3020 => SensorStatus.error

enum SensorStatus {
  active, //Green
  inactive, //Grey
  error, //Red
  warning //Yellow
}

class HeadsetSensorPointIndicator extends StatelessWidget {
  final double size;
  final SensorStatus sensorStatus;

  HeadsetSensorPointIndicator(
      {this.size = 0, this.sensorStatus = SensorStatus.inactive});

  @override
  Widget build(BuildContext context) {
    var activeIcon = Icons.check; //Active sensor icon (check)
    var inactiveIcon = Icons.clear; //Error/Inactive Sensor Icon (Cross/Clear)

    var colorActive = config.AppColors.activeSensor; //Green
    var colorError = config.AppColors.errorSensor; //Red
    var colorWarning = config.AppColors.warningSensor; //Orange
    var colorInactive = config.AppColors.inactiveSensor; // Grey

    var _color = sensorStatus == SensorStatus.active
        ? colorActive
        : sensorStatus == SensorStatus.warning
            ? colorWarning
            : sensorStatus == SensorStatus.error
                ? colorError
                : colorInactive;

    var _icon = sensorStatus == SensorStatus.active
        ? activeIcon
        : (sensorStatus == SensorStatus.warning ||
                sensorStatus == SensorStatus.error)
            ? inactiveIcon
            : null;

    return Container(
      height: size,
      width: size,
      padding: EdgeInsets.all(1),
      decoration: BoxDecoration(
        color: _color,
        borderRadius: BorderRadius.circular(100),
      ),
      child: FittedBox(
        child: _icon != null
            ? Icon(
                _icon,
                color: Colors.black,
              )
            : Container(),
      ),
    );
  }
}

import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';
import 'package:brain_trainer/elements/headset/sensors_detector.dart';
import 'package:flutter/material.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;

class HardwareSuccessfulConnectionWidget extends StatelessWidget {
  final String batteryPercentage;
  final String batteryIcon;
  final VoidCallback? onStartTraining;
  final VoidCallback? onTestVibration;
  final bool isVibrationTestRequested;
  final bool? isVibrated;
  final VoidCallback? onVibrationSuccess;
  final VoidCallback? onVibrationFailed;

  HardwareSuccessfulConnectionWidget(
      {this.batteryPercentage = "100",
      this.batteryIcon = AppAssets.batteryNormalIcon,
      @required this.onStartTraining,
      @required this.onTestVibration,
      this.isVibrationTestRequested = true,
      this.isVibrated = false,
      @required this.onVibrationSuccess,
      @required this.onVibrationFailed});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Column(
      children: <Widget>[
        _buildSensorDetectorAndBatteryIndicator(context, textTheme),
        SizedBox(
          height: 31,
        ),
        Text(
          S.successfullyConnected,
          style: textTheme.headline5,
        ),
        SizedBox(
          height: 33,
        ),
        _buildStartTrainingButton(context, textTheme),
        SizedBox(
          height: 20,
        ),
        _buildTestVibrationButton(context, textTheme),
        SizedBox(
          height: 32,
        ),
        this.isVibrationTestRequested
            ? this.isVibrated == null
                ? _buildVibrationTestRequestedButtonsLayout(context, textTheme)
                : this.isVibrated != null && this.isVibrated == true
                    ? Padding(
                        padding: const EdgeInsets.only(top: 36.0),
                        child: Text(
                          S.greatYouAreAllSet,
                          style: textTheme.subtitle1,
                        ),
                      ) //Vibration Success Text
                    : _buildFailedToVibrateInstructions(
                        context, textTheme) //VibrationFailed Text
            : SizedBox(),
      ],
    );
  }

  Widget _buildSensorDetectorAndBatteryIndicator(
      BuildContext context, TextTheme textTheme) {
    var detectorPathSize = 299.3;

    var successCheckMarkSize = 39.64;

    return Container(
      height: detectorPathSize,
      child: Stack(
        children: <Widget>[
          Center(
              child: SensorsDetector(
            sensorOneStatus: SensorStatus.active,
            sensorTwoStatus: SensorStatus.active,
            sensorThreeStatus: SensorStatus.active,
            sensorFourStatus: SensorStatus.active,
          )),
          Center(
            child: Container(
                width: successCheckMarkSize,
                height: successCheckMarkSize,
                child: Center(
                    child: Image.asset(
                  "assets/images/success_check_mark.png",
                ))),
          ),
          Positioned(
            bottom: 5,
            left: 0,
            right: 0,
            child: Center(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(this.batteryIcon, width: 24),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "$batteryPercentage%",
                  style: textTheme.caption,
                ),
              ],
            )),
          ),
        ],
      ),
    );
  }

  Widget _buildStartTrainingButton(BuildContext context, TextTheme textTheme) {
    return SizedBox(
      height: 56,
      width: 334,
      child: ElevatedButton(
        onPressed: () {
          onStartTraining?.call();
        },
        style: ElevatedButton.styleFrom(shape: StadiumBorder()),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  "assets/icons/traning_icon.png",
                ),
              ),
              Text(
                S.startTraining.toUpperCase(),
                style: textTheme.headline6,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTestVibrationButton(BuildContext context, TextTheme textTheme) {
    return SizedBox(
      height: 56,
      width: 334,
      child: ElevatedButton(
        onPressed: () {
          onTestVibration?.call();
        },
        style: ElevatedButton.styleFrom(
            primary: Colors.white, shape: StadiumBorder()),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  "assets/icons/vibrate_icon.png",
                  color: config.AppColors.mainColorDark,
                ),
              ),
              Text(
                S.testVibration.toUpperCase(),
                style: textTheme.headline6
                    ?.merge(TextStyle(color: config.AppColors.mainColorDark)),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildVibrationTestRequestedButtonsLayout(
      BuildContext context, TextTheme textTheme) {
    return Column(
      children: <Widget>[
        Text(
          S.didYourHeadsetVibrate,
          style: textTheme.headline5,
        ),
        SizedBox(
          height: 18,
        ),
        SizedBox(
          height: 56,
          width: 334,
          child: Row(
            children: <Widget>[
              Expanded(
                child: ElevatedButton(
                  onPressed: () {
                    this.onVibrationSuccess?.call();
                  },
                  style: ElevatedButton.styleFrom(shape: StadiumBorder()),
                  child: Center(
                    child: Text(
                      S.yes.toUpperCase(),
                      style: textTheme.headline6,
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: ElevatedButton(
                  onPressed: () {
                    this.onVibrationFailed?.call();
                  },
                  style: ElevatedButton.styleFrom(shape: StadiumBorder()),
                  child: Center(
                    child: Text(
                      S.no.toUpperCase(),
                      style: textTheme.headline6,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildFailedToVibrateInstructions(
      BuildContext context, TextTheme textTheme) {
    return SizedBox(
      width: 334,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 36.0,
          ),
          Text(
            S.letUsHelpYouToTroubleshootYourDevice,
            style: textTheme.subtitle1,
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 17,
          ),
          Text(
            S.talkToOurCustomerSupport,
            style: textTheme.bodyText2,
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 32,
          ),
          Text(
            S.customerSupportNumber,
            style: textTheme.subtitle2,
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            S.itSupportEmail,
            style: textTheme.subtitle2,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

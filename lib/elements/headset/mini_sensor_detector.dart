import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

class MiniSensorsDetector extends StatelessWidget {
  final SensorStatus? sensorOneStatus;
  final SensorStatus? sensorTwoStatus;
  final SensorStatus? sensorThreeStatus;
  final SensorStatus? sensorFourStatus;

  MiniSensorsDetector(
      {this.sensorOneStatus,
      this.sensorTwoStatus,
      this.sensorThreeStatus,
      this.sensorFourStatus});

  @override
  Widget build(BuildContext context) {
    var detectorCircleSize = 30.0;

    var pointersSize = detectorCircleSize / 4;
    // var pointersSize = 10.0;
    // var paddingAll = detectorCircleSize / (pointersSize * 0.1);
    var paddingAll = 0.0;

    return Container(
      child: Stack(
        children: <Widget>[
          DottedBorder(
            strokeWidth: 1,
            borderType: BorderType.Circle,
            color: Colors.white,
            strokeCap: StrokeCap.butt,
            child: Container(
              height: detectorCircleSize,
              width: detectorCircleSize,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(paddingAll),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            //Sensor/Channel Two
                            ///Top left
                            HeadsetSensorPointIndicator(
                              size: pointersSize,
                              sensorStatus:
                                  sensorTwoStatus ?? SensorStatus.inactive,
                            ),

                            //Sensor/Channel Three
                            ///Top Right
                            HeadsetSensorPointIndicator(
                              size: pointersSize,
                              sensorStatus:
                                  sensorThreeStatus ?? SensorStatus.inactive,
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            //Sensor/Channel One
                            ///Bottom left
                            HeadsetSensorPointIndicator(
                              size: pointersSize,
                              sensorStatus:
                                  sensorOneStatus ?? SensorStatus.inactive,
                            ),

                            //Sensor/Channel Four
                            ///Bottom right
                            HeadsetSensorPointIndicator(
                              size: pointersSize,
                              sensorStatus:
                                  sensorFourStatus ?? SensorStatus.inactive,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Center(
                    child: sensorTwoStatus == SensorStatus.active &&
                            sensorThreeStatus == SensorStatus.active &&
                            sensorOneStatus == SensorStatus.active &&
                            sensorFourStatus == SensorStatus.active
                        ? Image.asset(
                            AppAssets.fittingHead,
                            color: Colors.green,
                          )
                        : Image.asset(
                            AppAssets.fittingHead,
                          ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

class SensorsDetector extends StatelessWidget {
  final SensorStatus? sensorOneStatus;
  final SensorStatus? sensorTwoStatus;
  final SensorStatus? sensorThreeStatus;
  final SensorStatus? sensorFourStatus;

  SensorsDetector(
      {this.sensorOneStatus,
      this.sensorTwoStatus,
      this.sensorThreeStatus,
      this.sensorFourStatus});

  @override
  Widget build(BuildContext context) {
    var detectorCircleSize = 222.78;

    var pointersSize = detectorCircleSize / 7;
    var paddingAll = detectorCircleSize / (pointersSize * 0.5);

    return Container(
      child: Stack(
        children: <Widget>[
          DottedBorder(
            strokeWidth: 1,
            borderType: BorderType.Circle,
            color: Colors.white,
            strokeCap: StrokeCap.butt,
            child: Container(
              height: detectorCircleSize,
              width: detectorCircleSize,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(paddingAll),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            //Sensor/Channel Two
                            ///Top left
                            HeadsetSensorPointIndicator(
                              size: pointersSize,
                              sensorStatus:
                              sensorTwoStatus ?? SensorStatus.inactive,
                            ),

                            Spacer(),

                            //Sensor/Channel Three
                            ///Top Right
                            HeadsetSensorPointIndicator(
                              size: pointersSize,
                              sensorStatus:
                              sensorThreeStatus ?? SensorStatus.inactive,
                            )
                          ],
                        ),
                        Spacer(),
                        Row(
                          children: <Widget>[

                            //Sensor/Channel One
                            ///Bottom left
                            HeadsetSensorPointIndicator(
                              size: pointersSize,
                              sensorStatus: sensorOneStatus ?? SensorStatus.inactive,
                            ),

                            Spacer(),

                            //Sensor/Channel Four
                            ///Bottom right
                            HeadsetSensorPointIndicator(
                              size: pointersSize,
                              sensorStatus:
                                  sensorFourStatus ?? SensorStatus.inactive,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Center(
                    child: Image.asset("assets/images/fitting_head.png"),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

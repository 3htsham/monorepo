import 'package:flutter/material.dart';

class StepInstructions extends StatelessWidget {
  final String? headline;
  final String? instructions;

  StepInstructions({@required this.headline, @required this.instructions});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var _width = 334.0;

    return Container(
      width: _width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            headline ?? "",
            style: textTheme.headline4,
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            instructions ?? "",
            style: textTheme.bodyText1,
          ),
        ],
      ),
    );
  }
}

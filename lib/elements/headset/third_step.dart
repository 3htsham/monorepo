import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';
import 'package:brain_trainer/elements/headset/sensors_detector.dart';
import 'package:brain_trainer/elements/headset/step_instructions.dart';
import 'package:flutter/material.dart';

class HardwareSetupThirdStep extends StatelessWidget {
  final bool isTestRunning;
  final bool isTestFailed;
  final String adjustmentErrorInstructions;

  final SensorStatus? sensorOneStatus,
      sensorTwoStatus,
      sensorThreeStatus,
      sensorFourStatus;

  HardwareSetupThirdStep({
    this.isTestRunning = false,
    this.isTestFailed = false,
    this.adjustmentErrorInstructions = "",
    this.sensorOneStatus,
    this.sensorTwoStatus,
    this.sensorThreeStatus,
    this.sensorFourStatus,
  });

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var detectorPathSize = 299.3;

    return Column(
      children: <Widget>[
        Container(
          height: detectorPathSize,
          decoration: BoxDecoration(
              image: this.isTestRunning
                  ? DecorationImage(
                      image:
                          AssetImage("assets/images/animated_headset_path.png"))
                  : null),
          child: Center(
              child: SensorsDetector(
            sensorOneStatus: this.sensorOneStatus,
            sensorTwoStatus: this.sensorTwoStatus,
            sensorThreeStatus: this.sensorThreeStatus,
            sensorFourStatus: this.sensorFourStatus,
          )),
        ),
        isTestRunning
            ? Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Text(
                  S.fitTestingInProgress,
                  style: textTheme.headline4,
                ),
              )
            : isTestFailed
                ? StepInstructions(
                    headline: S.adjustmentRequired,
                    instructions: this.adjustmentErrorInstructions,
                  )
                : SizedBox(),
      ],
    );
  }
}

import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:flutter/material.dart';

class KeyboardDismiss extends StatelessWidget {
  final Widget? child;

  KeyboardDismiss({this.child});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        UIHelper.hideKeyboard(context);
      },
      child: child ?? Container(),
    );
  }
}

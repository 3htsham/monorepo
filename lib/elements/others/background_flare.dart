import 'package:brain_trainer/elements/animations/flare_fader.dart';
import 'package:flutter/material.dart';

class BackgroundFlare extends StatelessWidget {
  final Offset? offset;
  final Offset? endOffset;
  final Color color;
  final double top;
  final double left;
  final double right;
  final double bottom;
  final double height;
  final double width;
  final Duration? flareDuration;

  BackgroundFlare(
      {this.bottom = 0,
      this.height = 100,
      this.width = 100,
      this.top = 0,
      this.flareDuration,
      this.color = Colors.white,
      this.left = 0,
      this.right = 0,
      this.offset,
      this.endOffset});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      left: left,
      bottom: bottom,
      right: right,
      child: FlareFadeAnimation(
        offset: offset ?? Offset(0.0, 32.0),
        endOffset: endOffset ?? Offset(0.0, 0.0),
        duration: flareDuration ?? Duration(seconds: 1),
        delay: Duration(milliseconds: 100),
        child: Container(
          height: height,
          width: width,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient: RadialGradient(colors: [
                color,
                color.withAlpha(150),
                color.withAlpha(100),
                color.withAlpha(50),
                color.withAlpha(15)
              ])),
        ),
      ),
    );
  }
}

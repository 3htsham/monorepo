import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/buttons/outlined_button.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateRangePicker extends StatefulWidget {
  DateTime? currentDate;
  Function(DateTime, DateTime)? onDateRangeSelected;

  DateRangePicker(
      {@required this.currentDate, @required this.onDateRangeSelected});

  @override
  State<StatefulWidget> createState() {
    return DateRangePickerState();
  }
}

class DateRangePickerState extends State<DateRangePicker> {
  final leftRightSpace = 16;
  final borderColor = Colors.white.withOpacity(0.24);
  final widthBorder = 2.0;
  final backgroundColor = Colors.white.withOpacity(0.16);
  final colorSelected = Colors.white.withOpacity(0.24);
  final styleHeader =
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white);
  final styleHeaderDayInWeek =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.white);
  final styleNormal =
      TextStyle(fontSize: 19, fontWeight: FontWeight.w700, color: Colors.white);
  final heightHeader = 45.0;

  List<List<DateTime?>> listDate = <List<DateTime>>[];
  DateTime? startTime;
  DateTime? endTime;

  // LIFE CYCLE
  @override
  void initState() {
    super.initState();
    refreshDates();
  }

  // LOGIC
  List<DateTime?>? checkShowDateTime(DateTime date) {
    List<DateTime?> listDateTime = [];
    var beginningNextMonth = (date.month < 12)
        ? new DateTime(date.year, date.month + 1, 1)
        : new DateTime(date.year + 1, 1, 1);
    var lastDay = beginningNextMonth.subtract(new Duration(days: 1)).day;

    for (var i = 1; i <= lastDay; i++) {
      listDateTime.add(DateTime(date.year, date.month, i));
    }
    List<DateTime?> listFake = [];
    if (listDateTime.first?.weekday != 7) {
      for (var i = 0; i < (listDateTime.first?.weekday ?? 0); i++) {
        listFake.add(null);
      }
    }
    listDateTime.insertAll(0, listFake);
    return listDateTime;
  }

  bool checkSameDate(DateTime date1, DateTime date2) {
    return date1.year == date2.year &&
        date1.month == date2.month &&
        date1.day == date2.day;
  }

  bool checkSameMonth(DateTime? date1, DateTime date2) {
    return date1?.year == date2.year && date1?.month == date2.month;
  }

  //WIDGET
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        height: MediaQuery.of(context).size.height * 696 / 1024,
        width: MediaQuery.of(context).size.width * 688 / 768,
        child: Align(
          alignment: Alignment.center,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _buildListDate(context),
                Container(height: 1),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// NEXT PREVIOUS
  Widget _buildNextPrivousWidget(
      BuildContext context, int index, DateTime dateInMonth) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 26),
      height: 40,
      child: Row(
        children: [
          Container(
            child: Text(
              DateFormat("MMMM yyyy").format(dateInMonth),
              style: styleHeader,
            ),
          ),
          Expanded(child: Container()),
          index == 0
              ? Row(children: [
                  _buildDoneButton(),
                  SizedBox(
                    width: 20,
                  ),
                  Container(
                    child: InkWell(
                      child: Image.asset(AppAssets.leftArrow,
                          height: 40, width: 40),
                      onTap: () {
                        widget.currentDate =
                            widget.currentDate?.add(Duration(days: -60));
                        refreshDates();
                      },
                    ),
                  ),
                  SizedBox(
                    width: 12,
                  ),
                  Container(
                    width: 25,
                    child: InkWell(
                      child: Image.asset(AppAssets.rightArrow,
                          height: 40, width: 40),
                      onTap: () {
                        if (checkSameMonth(
                            widget.currentDate, DateTime.now())) {
                          return;
                        }
                        widget.currentDate =
                            widget.currentDate?.add(Duration(days: 60));
                        refreshDates();
                      },
                    ),
                  )
                ])
              : SizedBox()
        ],
      ),
    );
  }

  Widget _buildDoneButton() {
    return AppOutLinedButton(
        backgroundColor: Colors.transparent,
        onTap: () {
          if (startTime != null && endTime != null) {
            if (validateDateRange(startTime!, endTime!)) {
              widget.onDateRangeSelected!(startTime!, endTime!);
              Navigator.of(context).pop();
            }
          }
        },
        title: S.done);
  }

  void refreshDates() {
    var currentDate = widget.currentDate ?? DateTime.now();
    var listStart = checkShowDateTime(
        DateTime(currentDate.year, currentDate.month - 1, currentDate.day));
    var listEnd = checkShowDateTime(currentDate);
    listDate = [listStart!, listEnd!];
    setState(() {});
  }

  /// LIST DATE IN WEEK
  Widget _buildListDateInWeek(context) {
    final List<String> list = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

    return LayoutBuilder(
      builder: (context, constraints) {
        final double width = (constraints.maxWidth) / 7;
        return Container(
          height: heightHeader,
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.zero,
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  width: width,
                  height: heightHeader,
                  alignment: Alignment.center,
                  child: Text(
                    list[index],
                    textAlign: TextAlign.center,
                    style: styleHeaderDayInWeek,
                  ),
                );
              }),
        );
      },
    );
  }

  /// LIST DATE ITEM
  Widget _buildListDate(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: backgroundColor,
          border: Border.all(width: widthBorder, color: borderColor),
          borderRadius: BorderRadius.circular(16)),
      padding: EdgeInsets.only(bottom: 20),
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.zero,
          itemCount: listDate.length,
          itemBuilder: (BuildContext context, int index) {
            var listDateItem = listDate[index];
            return Container(
              margin: EdgeInsets.only(top: 15),
              child: Column(
                children: [
                  /// NEXT PREVIOUS
                  _buildNextPrivousWidget(
                      context, index, listDateItem[10] ?? DateTime.now()),

                  /// LIST DATE IN WEEK
                  _buildListDateInWeek(context),

                  /// LIST DATE IN MONTH
                  _buildListDateInMonth(context, listDateItem),
                ],
              ),
            );
          }),
    );
  }

  /// DATE IN MOTH
  Widget _buildListDateInMonth(
      BuildContext context, List<DateTime?>? dateTimes) {
    final double width = (MediaQuery.of(context).size.width - 16 * 2) / 7;
    final double height = 40;
    return Container(
      child: GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.zero,
        itemCount: dateTimes?.length ?? 0,
        itemBuilder: (BuildContext context, int index) {
          var item = dateTimes?[index];
          return _buildItemDate(context, item);
        },
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 7,
          crossAxisSpacing: 0.0,
          mainAxisSpacing: 0.0,
          childAspectRatio: width / height,
        ),
      ),
    );
  }

  /// ITEM DATE
  Widget _buildItemDate(BuildContext context, DateTime? item) {
    bool isFirst = false;
    bool isLast = false;
    bool isSelected = false;
    bool isToday = false;
    if (item != null) {
      isToday = checkSameDate(item, DateTime.now());
    }
    if (startTime != null && item != null && checkSameDate(item, startTime!)) {
      isFirst = true;
    }
    if (endTime != null && item != null && checkSameDate(item, endTime!)) {
      isLast = true;
    }
    if (startTime != null && endTime != null && item != null) {
      if (item.isAfter(startTime!) && item.isBefore(endTime!)) {
        isSelected = true;
      }
    }

    return InkWell(
      onTap: () {
        if (item != null) {
          if (startTime == null) {
            // START  === null

            // if (item != null && endTime != null) {
            //   var value = validateDateRange(item, endTime);
            //   if (value == false) {
            //     return;
            //   }
            // }
            startTime = item;
          } else {
            // Start != null
            if (checkSameDate(startTime!, item)) {
              // SAME START
              // startTime = null;
            } else {
              if (endTime == null) {
                if (startTime != null) {
                  var value = validateDateRange(startTime!, item);
                  if (value == false) {
                    return;
                  }
                }
                endTime = item;
              } else {
                //reset
                endTime = null;
                startTime = item;
              }
            }
          }
          setState(() {});
        }
      },
      child: Container(
        alignment: Alignment.center,
        child: Container(
          // margin: EdgeInsets.symmetric(vertical: 7),
          decoration: BoxDecoration(
            color: isFirst || isLast || isSelected
                ? colorSelected
                : Colors.transparent,
            borderRadius: (isFirst == true && endTime == null) ||
                    (isLast == true && startTime == null)
                ? BorderRadius.all(Radius.circular(16))
                : BorderRadius.only(
                    topLeft: Radius.circular(isFirst ? 12 : 0),
                    bottomLeft: Radius.circular(isFirst ? 12 : 0),
                    topRight: Radius.circular(isLast ? 12 : 0),
                    bottomRight: Radius.circular(isLast ? 12 : 0),
                  ),
          ),
          alignment: Alignment.center,
          child: Text(
            item != null ? item.day.toString() : '',
            style: isToday == true
                ? styleNormal.copyWith(color: Theme.of(context).accentColor)
                : styleNormal,
          ),
        ),
      ),
    );
  }

  bool validateDateRange(DateTime? start, DateTime? end) {
    if (start == null || end == null) return false;
    int diff = end.difference(start).inDays;
    if (diff <= 0) {
      print("invalid date range (Value ${diff.toString()})");
      setState(() {
        startTime = end;
      });

      return false;
    }
    return true;
  }
}

import 'package:flutter/material.dart';

class LargeBatteryIndicator extends StatelessWidget {
  final double indicatorBoxWidth = 133;
  final double indicatorHeight = 88;
  final double batteryPercentage;

  LargeBatteryIndicator({this.batteryPercentage = 50});

  @override
  Widget build(BuildContext context) {
    var fillWidth = (indicatorBoxWidth - 14) * (batteryPercentage / 100);

    var batteryColor = batteryPercentage >= 50 ? Colors.green : Colors.red;

    return Container(
      width: 163,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: indicatorBoxWidth,
            height: indicatorHeight,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 7),
                borderRadius: BorderRadius.circular(2)),
            child: Center(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  width: fillWidth,
                  height: indicatorHeight,
                  color: batteryColor,
                ),
              ),
            ),
          ),
          Container(
            height: 15,
            width: 7,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.white, width: 7),
                borderRadius: BorderRadius.circular(2)),
          )
        ],
      ),
    );
  }
}

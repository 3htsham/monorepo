import 'dart:ui';

import 'package:brain_trainer/elements/others/background_flare.dart';
import 'package:flutter/material.dart';

class LightingScaffold extends StatelessWidget {
  final Widget? child;
  final GlobalKey? scaffoldKey;
  final bool resizeToAvoidBottomPadding;
  final PreferredSizeWidget? appBar;

  LightingScaffold({
    this.child,
    this.scaffoldKey,
    this.resizeToAvoidBottomPadding = true,
    this.appBar,
  });

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: this.scaffoldKey ?? GlobalKey(),
      resizeToAvoidBottomInset: resizeToAvoidBottomPadding,
      appBar: this.appBar,
      body: Stack(
        children: <Widget>[

          Stack(
            children: <Widget>[
              BackgroundFlare(
                color: Colors.white,
                offset: Offset(size.width, size.height),
                endOffset: Offset(0, 0),
                flareDuration: Duration(seconds: 20),
                height: size.height / 2,
                width: size.width,
                // bottom: size.height / 2.8,
                top: -(size.height / 2),
                right: 0,
                left: -(size.height / 2),
              ),
              BackgroundFlare(
                color: Colors.white,
                offset: Offset(size.width, -size.height),
                endOffset: Offset(0, 0),
                flareDuration: Duration(seconds: 15),
                height: size.height / 2.5,
                width: size.width,
                bottom: -(size.height / 2.5),
                // top: size.height/3,
                right: (size.height / 2.5),
                // left: 0,
              ),
            ],
          ),

          BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 15, sigmaY: 15),
            // filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
            child: Material(
              color: theme.primaryColor.withOpacity(0.5),
              child: this.child ?? Container(),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

class MiniBatteryIndicator extends StatelessWidget {
  final double indicatorBoxWidth = 20;
  final double indicatorHeight = 14;
  final double batteryPercentage;
  Color? batteryColor;

  MiniBatteryIndicator({this.batteryPercentage = 50, this.batteryColor});

  @override
  Widget build(BuildContext context) {
    var fillWidth = (indicatorBoxWidth - 4) * (batteryPercentage / 100);

    if (batteryColor == null) {
      batteryColor = batteryPercentage >= 50 ? Colors.white : Colors.red;
    }

    return Container(
      width: 24,
      height: 14,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: indicatorBoxWidth,
            height: indicatorHeight,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.circular(2)),
            child: Center(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  width: fillWidth,
                  height: indicatorHeight,
                  color: batteryColor,
                ),
              ),
            ),
          ),
          Container(
            height: 5,
            width: 2,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.white, width: 7),
                borderRadius: BorderRadius.circular(2)),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:brain_trainer/config/common_style.dart';

/// A mobile stepper step state.
class MobileStep {
  const MobileStep({
    @required this.title,
    @required this.content,
    this.isActive = false,
  })  : assert(title != null),
        assert(content != null);

  /// The title of the step.
  final String? title;

  /// The content of the step that appears below the [title].
  ///
  /// Below the content, every step has a 'Next' and 'Back' button.
  final Widget? content;

  /// Whether or not the step is active. The flag only influences styling.
  final bool isActive;
}

/// A material-like mobile stepper that displays a progress through a sequence of steps.
class MobileStepper extends StatefulWidget {
  const MobileStepper({
    Key? key,
    @required this.steps,
    this.currentStep = 0,
    this.title = "",
    this.content,
    this.onStepTapped,
    this.onStepNext,
    this.onStepBack,
  }) : super(key: key);

  final List<MobileStep>? steps;
  final int currentStep;

  final ValueChanged<int>? onStepTapped;
  final VoidCallback? onStepNext;
  final VoidCallback? onStepBack;

  final String title;
  final Widget? content;

  @override
  _MobileStepperState createState() => _MobileStepperState();
}

class _MobileStepperState extends State<MobileStepper>
    with TickerProviderStateMixin {
  bool _isFirst(int index) => index == 0;

  bool _isLast(int index) => widget.steps!.length - 1 == index;

  bool _isCurrent(int index) => widget.currentStep == index;

  Widget _buildCircle(int index) {
    if (_isCurrent(index)) {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 4.0,
        ),
        width: 8.0,
        height: 8.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: optiosCorporateAccent,
        ),
      );
    } else {
      return Container(
        width: 8.0,
        height: 8.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Theme.of(context).dividerColor,
        ),
      );
    }
  }

  // Widget _buildTitle(int index) {
  //   return Container(
  //     child: Text(
  //       widget.steps![index].title ?? "",
  //       textAlign: TextAlign.center,
  //       style: Theme.of(context).textTheme.headline6,
  //     ),
  //     width: double.infinity,
  //   );
  // }

  // TODO: Investigate reimplementing this with PageView and TabPageSelector
  Widget _buildNavigator(int index) {
    final List<Widget> children = <Widget>[
      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: !_isFirst(index),
        child: TextButton(
          child: Text('Back'),
          onPressed: widget.onStepBack,
        ),
      ),
      Expanded(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            for (int i = 0; i < widget.steps!.length; i += 1) _buildCircle(i),
          ],
        ),
      ),
      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: !_isLast(index),
        child: TextButton(
          child: Text(
            'Next',
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          onPressed: widget.onStepNext,
        ),
      ),
    ];

    return Container(
        height: 48.0,
        padding: EdgeInsets.zero,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: children,
        ));
  }

  Widget _buildStepper(int index) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Column(
        children: <Widget>[
//          _buildTitle(widget.currentStep),
//          Divider(height: 8.0,),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16.0,
              ),
              child: Center(child: widget.steps![index].content),
            ),
          ),
          Divider(
            height: 8.0,
          ),
          _buildNavigator(widget.currentStep),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    assert(() {
      if (context.findAncestorWidgetOfExactType<MobileStepper>() != null)
        throw FlutterError('Steppers must not be nested.\n'
            'The material specification advises that one should avoid embedding '
            'steppers within steppers. '
            'https://material.io/archive/guidelines/components/steppers.html#steppers-usage');
      return true;
    }());

    return _buildStepper(widget.currentStep);
  }
}

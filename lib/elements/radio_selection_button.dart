import 'package:flutter/material.dart';

class RadioSelectionButton extends StatelessWidget {
  final String? text;
  final Function? onTap;
  final Image? image;

  RadioSelectionButton({@required this.text, this.onTap, this.image});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.only(top: 2, bottom: 2),
      alignment: Alignment.center,
      child: RawMaterialButton(
        constraints: BoxConstraints(minHeight: 50),
        padding: EdgeInsets.all(4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            image ?? SizedBox(),
            SizedBox(
              width: 13,
            ),
            Text(text ?? "", style: Theme.of(context).textTheme.bodyText1,),
          ],
        ),
        onPressed: () {
          onTap?.call();
        },
      ),
    );
  }
}

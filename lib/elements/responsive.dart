import 'package:flutter/material.dart';

class Responsive extends StatelessWidget {
  final Widget? mobile;
  final Widget? tablet;
  final Widget? desktop;

  const Responsive(
      {Key? key,
      @required this.mobile,
      @required this.tablet,
      @required this.desktop})
      : super(key: key);

  static bool isMobile(BuildContext context) =>
      MediaQuery.of(context).size.width < 650;

  static bool isTablet(BuildContext context) =>
      MediaQuery.of(context).size.width < 1100 &&
      MediaQuery.of(context).size.width >= 650;

  static bool isDesktop(BuildContext context) =>
      MediaQuery.of(context).size.width >= 1100;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        ///If our width is more than 1100 then we will consider it as desktop
        if (constraints.maxWidth >= 1100) {
          return this.desktop ?? Container();
        }

        ///If our width is less than 1100 & more than 650 then we will consider it as tablet
        else if (constraints.maxWidth >= 650) {
          return this.tablet ?? Container();
        }

        ///or width is less than 650 then we will consider it as mobile
        else {
          return this.mobile ?? Container();
        }
      },
    );
  }
}

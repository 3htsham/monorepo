enum BluetoothState {
  on,
  off,
  unAuthorized,
  notAvailable,
  unknown
}

extension BluetoothServiceExtension on BluetoothState {
  String get message {
    switch (this) {
      case BluetoothState.on:
        return 'Bluetooth turned ON';
      case BluetoothState.off:
        return 'Bluetooth turned OFF';
      case BluetoothState.unAuthorized:
        return 'Bluetooth access not authorized';
      case BluetoothState.notAvailable:
        return 'Bluetooth not available';
      default:
        return this.toString();
    }
  }
}
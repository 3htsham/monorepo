enum EEGUnits {
  volts,
  microVolts
}

extension EEGUnitsExtension on EEGUnits {
  String get message {
    switch (this) {
      case EEGUnits.volts:
        return 'EEGUnits set to volts';
      case EEGUnits.microVolts:
        return 'EEGUnits set to micro volts';
      default:
        return this.toString();
    }
  }
}

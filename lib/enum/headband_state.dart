enum HeadbandState {
  connected,
  disconnected,
  connecting,
  disconnecting,
  connectionFailed,
  testing,
  testSuccess,
  testFailed,
}

extension HeadbandStateExtension on HeadbandState {
  String get message {
    switch (this) {
      case HeadbandState.connecting:
        return 'Hardware Headset Connecting';
      case HeadbandState.disconnected:
        return 'Hardware Headset Disconnected';
      case HeadbandState.connecting:
        return 'Hardware Headset Connecting';
      case HeadbandState.connectionFailed:
        return 'Hardware Headset Connection Failed';
      case HeadbandState.testFailed:
        return 'Hardware Headset Test Failed';
      case HeadbandState.testing:
        return 'Hardware Headset Testing';
      case HeadbandState.testSuccess:
        return 'Hardware Headset Test Success';
      default:
        return this.toString();
    }
  }
}
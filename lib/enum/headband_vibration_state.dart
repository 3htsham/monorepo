
enum VibrationState {
  enabled,
  disabled,
  vibrated,
}

extension VibrationStateExtension on VibrationState {
  String get message {
    switch (this) {
      case VibrationState.enabled:
        return 'Vibration Service Enabled';
      case VibrationState.disabled:
        return 'Vibration Service Disabled';
      case VibrationState.vibrated:
        return 'Headset vibrated';
      default:
        return this.toString();
    }
  }
}
import 'dart:typed_data';

class Float32CircularBuffer {
  Float32List? _buf;
  int _start = 0;
  int _end = -1;
  int _count = 0;

  Float32CircularBuffer(int n) {
    _buf = new Float32List(n);
    reset();
  }

  reset() {
    _start = 0;
    _end = -1;
    _count = 0;
  }

  insert(double el) async {
    // Inserting the next value
    _end++;
    if (_end == _buf?.length) {
      _end = 0;
    }
    _buf?[_end] = el;

    // updating the start
    if (_count < (_buf?.length ?? 0)) {
      _count++;
      return;
    }

    _start++;
    if (_start == _buf?.length) {
      _start = 0;
    }
  }

  Float32List? get buffer => _buf;

  /// Element at the start of the [CircularBuffer]
  double? get first => _buf?[_start];

  /// Element at the end of the [CircularBuffer]
  double? get last => _buf?[_end];

  /// Number of elements of [CircularBuffer]
  int get length => _count;

  /// Maximun number of elements of [CircularBuffer]
  int? get capacity => _buf?.length;

  @Deprecated("Use `isFilled` instead")
  bool get filled => (_count == _buf?.length);

  @Deprecated("Use `isUnfilled` instead")
  bool get unfilled => (_count < (_buf?.length ?? 0));

  bool get isFilled => (_count == _buf?.length);

  bool get isUnfilled => (_count < (_buf?.length ?? 0));

  /// Allows you to iterate over the contents of the buffer
  /// The [action] callback is called for each item in the
  /// buffer.
  void forEach(void Function(double) action) {
    for (var i = _start; i < _start + _count; i++) {
      var val = _buf?[i % length] ?? 0;
      action(val);
    }
  }

  double get mean {
    double sum = 0.0;
    this.forEach((el) {
      sum += el;
    });
    return sum / (_buf?.length ?? 0);
  }
}

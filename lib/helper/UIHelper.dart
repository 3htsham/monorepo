import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'dart:ui';
import 'package:catcher/core/catcher.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UIHelper {
  static void showErrorDialog(String error, BuildContext context,
      {bool isRootNavigator = false}) {
    showAlertDialog(context, 'Error', error, () {
      isRootNavigator
          ? Navigator.of(context, rootNavigator: true).pop()
          : Navigator.of(context).pop();
    });
  }

  static Future<void> showFullScreenErrorDialog(BuildContext context, String title, String subtitle,
      {VoidCallback? onClose}) {
    return showDialog(
      context: context,
      builder: (context) {
        return Stack(
          children: <Widget>[
            Positioned(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              top: 0,
              // left: 0,
              child: Material(
                color: Colors.transparent,
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                  child: Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.white.withOpacity(0.16),
                      child: Stack(
                        children: <Widget>[
                          Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  title,
                                  style: Theme.of(context).textTheme.subtitle1?.copyWith(fontSize: 32),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: 16,
                                ),
                                Text(
                                  subtitle,
                                  style: Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 16),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            left: 20,
                            top: 20,
                            child: IconButton(
                              onPressed: (){
                                onClose?.call();
                              },
                              icon: Icon(
                                CupertinoIcons.clear,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      )),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  static void wrongPasswordDialog(BuildContext context, String title, String subtitle,
      {VoidCallback? onClose, VoidCallback? onTryAgain, VoidCallback? onForgotPassword}) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    showDialog(
      context: context,
      builder: (context) {
        return Stack(
          children: <Widget>[
            Positioned(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              top: 0,
              // left: 0,
              child: Material(
                color: Colors.transparent,
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                  child: Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.white.withOpacity(0.16),
                      child: Stack(
                        children: <Widget>[
                          Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  title,
                                  style: Theme.of(context).textTheme.subtitle1?.copyWith(fontSize: 32),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: 16,
                                ),
                                Text(
                                  subtitle,
                                  style: Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 16),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: 28,
                                ),
                                MainAppButton(
                                  onTap: () {
                                    onTryAgain?.call();
                                  },
                                  title: S.tryAgain.toUpperCase(),
                                  width: 180,
                                  height: 56,
                                ),
                                SizedBox(
                                  height: 28,
                                ),
                                InkWell(
                                  onTap: () {
                                    onForgotPassword?.call();
                                  },
                                  splashColor: Colors.white.withOpacity(0.3),
                                  highlightColor: Colors.white.withOpacity(0.3),
                                  child: Text(
                                    S.forgetPassword,
                                    style: textTheme.bodyText1?.merge(TextStyle(color: theme.accentColor, fontWeight: FontWeight.bold)),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Positioned(
                            left: 20,
                            top: 20,
                            child: IconButton(
                              onPressed: (){
                                onClose?.call();
                              },
                              icon: Icon(
                                CupertinoIcons.clear,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      )),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  static Future<void> showAlertDialog(
      BuildContext context, String title, String content, Function? onOk) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: AppColors.cyanDark,
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(content),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                onOk?.call();
              },
            )
          ],
        );
      },
    );
  }

  static void showImagePickerDialog(BuildContext context,
      {Function? onGallery, Function? onCamera}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: AppColors.cyanDark,
          title: Text(S.uploadPhoto),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(S.howDoYouWantToUploadPhoto),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(S.gallery),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
                onGallery?.call();
              },
            ),
            TextButton(
              child: Text(S.camera),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
                onCamera?.call();
              },
            ),
          ],
        );
      },
    );
  }

  static Widget getCircularProgress() {
    return SizedBox(
      width: 30,
      height: 30,
      child: CircularProgressIndicator(),
    );
  }

  static Widget getHorizontalDivider({double height = 1, Color? color}) {
    return Container(
      height: height,
      color: color ?? AppColors.inactiveSensor,
    );
  }

  static void hideKeyboard(context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  static Offset getWidgetPosition(GlobalKey key) {
    final RenderBox containerRenderBox =
        key.currentContext?.findRenderObject() as RenderBox;
    final containerPosition = containerRenderBox.localToGlobal(Offset.zero);

    return containerPosition;
  }

  static void showDialogWithPositionAndBlur(
      GlobalKey key, BuildContext context, Widget child) {
    RenderBox renderBox = key.currentContext?.findRenderObject() as RenderBox;
    var height = renderBox.size.height;
    var dx = UIHelper.getWidgetPosition(key).dx;
    var dy = UIHelper.getWidgetPosition(key).dy + height + 10;
    showGeneralDialog(
      pageBuilder: (context, animation, secondaryAnimation) => SafeArea(
        child: Stack(
          children: <Widget>[
            BackdropFilter(filter: ImageFilter.blur(sigmaX: 25, sigmaY: 25)),
            Positioned(left: dx, top: dy, child: child)
          ],
        ),
      ),
      transitionBuilder: (ctx, anim1, anim2, child) => BackdropFilter(
        filter:
            ImageFilter.blur(sigmaX: 4 * anim1.value, sigmaY: 4 * anim1.value),
        child: FadeTransition(
          child: child,
          opacity: anim1,
        ),
      ),
      context: context,
      barrierColor: Colors.white.withOpacity(0.1),
      transitionDuration: Duration(milliseconds: 300),
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
    );
  }

  static void showDialogBlur(BuildContext context, Widget child,
      {bool canDismiss = true}) {
    showGeneralDialog(
      pageBuilder: (context, animation, secondaryAnimation) => SafeArea(
        child: Stack(
          children: <Widget>[
            GestureDetector(
                onTap: () {
                  if (canDismiss) {
                    Navigator.of(context).pop();
                  }
                },
                child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 25, sigmaY: 25))),
            Center(
              child: child,
            )
          ],
        ),
      ),
      transitionBuilder: (ctx, anim1, anim2, child) => BackdropFilter(
        filter:
            ImageFilter.blur(sigmaX: 4 * anim1.value, sigmaY: 4 * anim1.value),
        child: FadeTransition(
          child: child,
          opacity: anim1,
        ),
      ),
      context: context,
      barrierColor: Colors.white.withOpacity(0.01),
      transitionDuration: Duration(milliseconds: 300),
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
    );
  }

  ///pop the current general dialog with main app navigator key (current is catcher key)
  static void hideGeneralDialog() {
    if (Catcher.navigatorKey?.currentState?.canPop() ?? false)
      Catcher.navigatorKey?.currentState?.pop();
  }
}

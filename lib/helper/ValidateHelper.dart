import 'package:brain_trainer/config/constants.dart';

class ValidateHelper {
  static String? validateEmail(String? email, {bool isButtonPressed = true}) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (email == null || email.isEmpty || email == '') {
      return 'Email is required.';
    } else if (isButtonPressed && !regex.hasMatch(email)) {
      return 'Your email format is invalid. Please check again';
    }
    return null;
  }

  static String? validatePassword(String? value,{bool isButtonPressed = true}) {
    // Pattern pattern =
    //     r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{10,}$';
    RegExp regex = new RegExp(pattern);
    if(isButtonPressed) {
      if (value == null || value.isEmpty) {
        return S.pleaseEnterYourPassword;
      } else {
        if (isButtonPressed && !regex.hasMatch(value))
          return S.pleaseEnterAValidPassword;
        else
          return null;
      }
    } else {
      return null;
    }
  }

  static String? validateConfirmPassword(String? pass, String? confirmPass,{bool isButtonPressed = true}) {
    if (pass != null) {
      if (isButtonPressed && (confirmPass != pass)) {
        return S.passwordDidNotMatch;
      }
      return null;
    }
    return null;
  }

  static bool hasMatch(String? value, String pattern) {
    return (value == null) ? false : RegExp(pattern).hasMatch(value);
  }
}

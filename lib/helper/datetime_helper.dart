import 'package:brain_trainer/config/constants.dart';
import 'package:intl/intl.dart';
import 'package:date_time_format/date_time_format.dart';

class DateTimeHelper {
  static String getNameDate(int day) {
    List<String> weekStartModel = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];
    return weekStartModel[day];
  }

  static String formatDuration(int hour, int minute) {
    if (hour == 0) {
      return minute.toString() + ' mins';
    }
    return hour.toString() + 'h' + minute.toString() + ' mins';
  }

  static List<dynamic> getWorkingHourByStartDay(
      int? startDayIndex, List<dynamic> listWeekAfterMapStartDay) {
    var toDay = startDayIndex ?? 0;
    List<dynamic> result = [];
    for (int i = toDay; i < 7; i++) {
      result.add(listWeekAfterMapStartDay.elementAt(i));
    }
    for (int i = 0; i < toDay; i++) {
      result.add(listWeekAfterMapStartDay.elementAt(i));
    }
    return result;
  }

  static List<String> getDaysOnWeek(int startDate) {
    final now = DateTime.now();
    var firstDayOfWeek;
    if (now.weekday < startDate) {
      firstDayOfWeek = now.add(Duration(days: (now.weekday - startDate) - 1));
    } else if (now.weekday > startDate) {
      firstDayOfWeek = now.subtract(Duration(days: now.weekday - startDate));
    } else {
      firstDayOfWeek = now.add(Duration(days: now.weekday - startDate));
    }
    return List.generate(7, (index) => index).map((value) {
      return DateFormat('dd')
          .format(firstDayOfWeek.add(Duration(days: value)))
          .toString();
    }).toList();
  }

  static List<String> getDaysOfWeek([String? locale]) {
    final now = DateTime.now();
    final firstDayOfWeek = now.subtract(Duration(days: now.weekday - 1));
    return List.generate(7, (index) => index)
        .map((value) => DateFormat(DateFormat.WEEKDAY, locale)
            .format(firstDayOfWeek.add(Duration(days: value))))
        .toList();
  }

  static final now = DateTime.now();

  static String formatNormalDate(DateTime date) {
    return DateFormat("MM-dd-yyyy").format(date);
  }

  static String formatMMMdd(DateTime date) {
    return DateFormat("MMM dd").format(date);
  }

  static String formatkkmm(DateTime date) {
    return DateFormat("kk:mm").format(date);
  }

  static String formatMMdd(DateTime date) {
    return DateFormat("MM/dd").format(date);
  }

  static String formathhmm(DateTime date) {
    return DateFormat("MM/dd").format(date);
  }

  static String formatSimpleMMddyy(DateTime date) {
    return date.format(AppDateFormat.normalDateFormat);
  }

  static String formatDate(firstDayOfWeek, int value) {
    return DateFormat('dd').format(firstDayOfWeek.add(Duration(days: value)));
  }

  static String formatDateName(firstDayOfWeek, int value) {
    return DateFormat(DateFormat.ABBR_WEEKDAY)
        .format(firstDayOfWeek.add(Duration(days: value)));
  }

  static String getFormatForHourAndMinute({int hour = 1, int minute = 1}) {
    String formatHour = _formatTime(hour);
    String formatMinute = _formatTime(minute);

    return "$formatHour:$formatMinute";
  }

  static String _formatTime(int element) {
    String _element;
    if (element == 0) {
      _element = "00";
    } else if (element < 10) {
      _element = "0${element.toString()}";
    } else {
      _element = element.toString();
    }
    return _element.toString();
  }

  static DateTime getStartDayOfWeek(DateTime source) {
    return getDate(source.subtract(Duration(days: source.weekday - 1)));
  }

  static DateTime getDate(DateTime d) => DateTime(d.year, d.month, d.day);

  static bool checkSameDate(DateTime date1, DateTime date2) {
    return date1.year == date2.year &&
        date1.month == date2.month &&
        date1.day == date2.day;
  }
}

import 'dart:ui';
import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'package:brain_trainer/elements/others/large_battery_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Helpers {
  static Widget overlayChargeBattery(context, double batteryPercentage,
      {VoidCallback? onClose}) {
    var size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Positioned(
          height: size.height,
          width: size.width,
          top: 0,
          // left: 0,
          child: Material(
            color: Colors.transparent,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
              child: Container(
                  height: size.height,
                  width: size.width,
                  color: AppColors.mainColorDark.withOpacity(0.16),
                  child: Stack(
                    children: <Widget>[
                      Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            LargeBatteryIndicator(batteryPercentage: batteryPercentage,),
                            SizedBox(
                              height: 32,
                            ),

                            Text(
                              "${batteryPercentage.toStringAsFixed(0)}%",
                              style: Theme.of(context).textTheme.subtitle1?.copyWith(fontSize: 41),
                            ),
                            SizedBox(
                              height: 32,
                            ),
                            Text(
                              S.chargeBeforeProceeding,
                              style: Theme.of(context).textTheme.subtitle1?.copyWith(fontSize: 24),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              S.batteryLevelNotAbleToComplete,
                              style: Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 16),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                      Positioned(
                        left: 20,
                        top: 20,
                        child: IconButton(
                          onPressed: onClose,
                          icon: Icon(
                            CupertinoIcons.clear,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      ],
    );
  }

  static Widget overlayWelcomeUserToTraining(context, String userName,
      {VoidCallback? onClose}) {
    return Stack(
      children: [
        Positioned(
          top: 0,
          // left: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(16),
                bottomRight: Radius.circular(16)),
            child: Material(
              color: Colors.transparent,
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: Container(
                  height: 268,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      Center(
                        child: Container(
                          constraints:
                          BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.58854),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "${S.welcomeBack}, $userName",
                                style: Theme.of(context).textTheme.headline3,
                              ),
                              Text(
                                S.welcomeBackDescription,
                                style: Theme.of(context).textTheme.bodyText1,
                              )
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        left: 20,
                        top: 20,
                        child: IconButton(
                          onPressed: () {
                            onClose?.call();
                          },
                          highlightColor: Colors.white.withOpacity(0.16),
                          icon: Icon(
                            CupertinoIcons.clear,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  static Widget hardwareDisconnected(context,
      {VoidCallback? onClose, VoidCallback? onReset}) {
    return Stack(
      children: [
        Positioned(
          top: 0,
          // left: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(16),
                bottomRight: Radius.circular(16)),
            child: Material(
              color: Colors.transparent,
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: Container(
                  height: 301,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: EdgeInsets.only(left: 158),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                S.deviceNotConnected,
                                style: Theme.of(context).textTheme.headline3,
                              ),
                              SizedBox(height: 24),
                              Text(
                                S.pleaseGoToTheHardwareTab,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              SizedBox(height: 24),
                              MainAppButton(onTap: (){
                                onReset?.call();
                              },
                                  height: 56,
                                  width: 246,
                                  title: S.setupHardware
                              )
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        left: 20,
                        top: 20,
                        child: IconButton(
                          onPressed: () {
                            onClose?.call();
                          },
                          highlightColor: Colors.white.withOpacity(0.16),
                          icon: Icon(
                            CupertinoIcons.clear,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  static Widget overlayHardwareReset(context, String title, String subtitle,
      {VoidCallback? onClose, VoidCallback? onReset}) {
    return Stack(
      children: <Widget>[
        Positioned(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          top: 0,
          // left: 0,
          child: Material(
            color: Colors.transparent,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
              child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white.withOpacity(0.16),
                  child: Stack(
                    children: <Widget>[
                      Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              title,
                              style: Theme.of(context).textTheme.subtitle1?.copyWith(fontSize: 32),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              subtitle,
                              style: Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 16),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 35,
                            ),
                            MainAppButton(
                              onTap: () {
                                onReset?.call();
                              },
                              title: S.resetHardware.toUpperCase(),
                              width: 262,
                              height: 56,
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        left: 20,
                        top: 20,
                        child: IconButton(
                          onPressed: (){
                            onClose?.call();
                          },
                          icon: Icon(
                            CupertinoIcons.clear,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      ],
    );
  }

  static Widget overlayPasswordSaved(context, String text,
      {VoidCallback? onClose}) {
    var size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Positioned(
          top: 0,
          // left: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(16),
                bottomRight: Radius.circular(16)),
            child: Material(
              color: Colors.transparent,
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: Container(
                  height: 104,
                  width: size.width,
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.16),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(16),
                          bottomRight: Radius.circular(16)),
                      border: Border.all(
                          color: Colors.white.withOpacity(0.24), width: 1)),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 20,
                        ),
                        IconButton(
                          onPressed: () {
                            onClose?.call();
                          },
                          highlightColor: Colors.white.withOpacity(0.16),
                          iconSize: 24,
                          icon: Icon(
                            CupertinoIcons.clear,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(width: 90),
                        Text(
                          text,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              ?.copyWith(fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

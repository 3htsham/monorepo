import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';

class ImpHelper {
  static SensorStatus getSensorStatusFromImpedance(double? _channelImpedance) {
    //_channelImpedance is in Ohms, divide it by 1000 to change it into kiloOhms
    // Green = impedance < 1500 => SensorStatus.active
    // Yellow = impedance between 1501 and 3020  => SensorStatus.warning
    // Red = impedance > 3020 => SensorStatus.error
    var impedance = _channelImpedance;
    if (impedance != null) {
      impedance = impedance / 1000;
      return impedance < 1500
          ? SensorStatus.active
          : impedance < 3020
          ? SensorStatus.warning
          : SensorStatus.error;
    }
    return SensorStatus.error;
  }
}
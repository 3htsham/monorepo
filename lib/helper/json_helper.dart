import 'package:brain_trainer/config/response_strings.dart';
import 'package:brain_trainer/enum/bluetooth_state.dart';
import 'package:brain_trainer/enum/headband_state.dart';

class JSONHelper {

  static BluetoothState convertStringToBluetoothState(String? stateString) {
    switch (stateString) {
      case ResponseStrings.poweredOn:
        return BluetoothState.on;
      case ResponseStrings.poweredOff:
        return BluetoothState.off;
      case ResponseStrings.unauthorized:
        return BluetoothState.unAuthorized;
      case ResponseStrings.notAvailable:
        return BluetoothState.notAvailable;
      default:
        return BluetoothState.unknown;
    }
  }

  static HeadbandState convertStringToHeadbandState(String? stateString) {
    switch (stateString) {
      case ResponseStrings.connecting:
        return HeadbandState.connecting;
      case ResponseStrings.connected:
        return HeadbandState.connected;
      case ResponseStrings.disconnected:
        return HeadbandState.disconnected;
      case ResponseStrings.disconnecting:
        return HeadbandState.disconnecting;
      default:
        return HeadbandState.disconnected;
    }
  }

}
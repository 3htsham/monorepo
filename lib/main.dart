import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/config/app_style.dart';
import 'package:brain_trainer/services/firebase/crashlytics_handler.dart';
import 'package:brain_trainer/services/firebase/firebase_analytics_service.dart';
import 'package:brain_trainer/services/firebase/firestore_db_service.dart';
import 'package:brain_trainer/routes/route_generator.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:brain_trainer/services/hardware/hardware_service_impl.dart';
import 'package:brain_trainer/services/hardware/mock_hardware_service.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:brain_trainer/config/common_style.dart';
import 'package:flutter/services.dart';
import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
import 'package:brain_trainer/models/auth_user.dart';
import 'package:catcher/catcher.dart';
import 'package:brain_trainer/services/headset/hardware_headset_service.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;
import 'package:provider/provider.dart';

void main() async {
  // Enable integration testing with the Flutter Driver extension.
  // See https://flutter.dev/testing/ for more info.
  //enableFlutterDriverExtension();

  // setup Catcher - our error/exception catching helper
  // debug goes stack trace page and the console
  CatcherOptions debugOptions =
      CatcherOptions(SilentReportMode(), [ConsoleHandler()]);

  /// release does a dialog, and sends to Crashlytics
  CatcherOptions releaseOptions = CatcherOptions(DialogReportMode(), [
    CrashlyticsHandler(
        enableDeviceParameters: true,
        enableApplicationParameters: true,
        enableCustomParameters: true,
        printLogs: true)
  ]);

  // Need to wait for the Flutter framework to bind before using SystemChrome.
  WidgetsFlutterBinding.ensureInitialized();

  Provider.debugCheckInvalidValueType = null;

  //Initialize Default Firebase App
  await Firebase.initializeApp();
  // initialize Analytics
  FirebaseAnalyticsService().initialize();

  //check if user is authenticated
  var user = FirebaseAuth.instance.currentUser;
  bool? isAuthenticated = user != null && user.emailVerified;
  print(
      'user is authenticated?: $isAuthenticated, isEmailVerified? ${user?.emailVerified}');

  SystemChrome.setEnabledSystemUIOverlays([]);
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: backgroundColor));
  
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then((_) {
    Catcher(
        // debugConfig: debugOptions,
        // profileConfig: debugOptions,
        // releaseConfig: releaseOptions,
        rootWidget: MultiProvider(
      providers: [
        Provider(
          create: (_) => FirebaseAuthService(),
        ),
        Provider(
          create: (_) => FirestoreDBService(),
        ),
        StreamProvider<AuthUser?>(
          initialData: null,
          lazy: true,
          create: (context) =>
              context.read<FirebaseAuthService>().onAuthStateChanged,
        ),
        ChangeNotifierProvider<HardwareHeadsetService>(
          lazy: false,
          create: (_) => HardwareHeadsetService(),
        ),
        Provider<HardwareService>(
          lazy: false,
          create: (_) => HardwareServiceImpl(),
        ),
        ChangeNotifierProvider<TrainingService>(
          lazy: false,
          create: (ctx) => TrainingService(
              db: ctx.read<FirestoreDBService>(),
              authService: ctx.read<FirebaseAuthService>(),
              headsetService: ctx.read<HardwareService>()),
        ),
      ],
      child: BrainTrainingApp(isAuthenticated: isAuthenticated),
    ));
  });
}

class BrainTrainingApp extends StatelessWidget {
  final bool isAuthenticated;
  BrainTrainingApp({this.isAuthenticated = false});
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);

    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: backgroundColor));

    return Consumer<String?>(
        builder: (context, user, child) => MaterialApp(
              title: 'Brain Trainer',

              ///Added to WHEN UPDATING APP TO NEW UI
              builder: (context, child) {
                var query = MediaQuery.of(context);

                return MediaQuery(
                  ///This will make our app to have same UI & font size on all devices
                  data: query.copyWith(
                      textScaleFactor: 1.0, devicePixelRatio: 1.0),
                  child: Theme(
                    child: child ?? Container(),
                    data: ThemeData(
                      primarySwatch: createMaterialColor(primaryColor),
                      backgroundColor: config.AppColors.scaffoldBackgroundColor,
                      scaffoldBackgroundColor:
                          config.AppColors.scaffoldBackgroundColor,
                      accentColor: config.AppColors.orange,
                      visualDensity: VisualDensity.adaptivePlatformDensity,
                      fontFamily: 'ObjektivMk2',
                      hintColor: Colors.white.withOpacity(0.6),
                      splashColor: config.AppColors.mainColorDark,
                      textTheme: AppStyle.getAppTextTheme(context),
                      buttonTheme: ButtonThemeData(
                          buttonColor: config.AppColors.orange,
                          highlightColor: Colors.white.withOpacity(0.08)),
                      elevatedButtonTheme: ElevatedButtonThemeData(
                          style: ElevatedButton.styleFrom(
                              primary: AppColors.orange)),
                      highlightColor: accentColor,
                      appBarTheme: AppBarTheme(
                        color: config.AppColors.mainColorDark,
                      ),
                      bottomAppBarTheme: BottomAppBarTheme(
                        color: config.AppColors.mainColorDark,
                      ),
                      snackBarTheme: SnackBarThemeData(
                        backgroundColor: optiosAdditionalDarkGrey,
                      ),
                      pageTransitionsTheme: PageTransitionsTheme(builders: {
                        TargetPlatform.android:
                            CupertinoPageTransitionsBuilder(),
                      }),
                    ),
                  ),
                );
              },

              ///Updating routing to RouteGenerator
              onGenerateRoute: RouteGenerator.mainRoute,
              initialRoute: isAuthenticated ? Routes.MAIN : Routes.LOGIN,
              navigatorKey: Catcher.navigatorKey,
            ));
  }
}

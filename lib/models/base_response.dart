///Example Usage
//BaseResponse<CGXHeadBand>.fromJson(jsonData, parseJson: (json) => CGXHeadBand.fromJson(json));

class BaseResponse<T> {
  bool error = false;
  T? data;
  String? message;

  BaseResponse.fromJson(Map<String, dynamic> json,
      {T Function(dynamic json)? parseJson}) {
    this.error = json['error'] ?? false;
    this.message = json['message'] ?? '';
    this.data = json['data'] != null ? parseJson?.call(json['data']) : null;
  }
}
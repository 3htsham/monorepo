class CoachTechUser {
  final String? type;
  final String? imgUrl;
  final String? name;
  final String? email;

  const CoachTechUser(
      {
        this.type,
      this.imgUrl,
      this.name, this.email});

  factory CoachTechUser.fromMap(Map? data) {
    data = data ?? {};
    return CoachTechUser(
        type: data['type'],
        imgUrl: data['imgUrl'],
        name: data['name'],
        email: data['email']);
  }

  toJson() {
    return {
      'type': type,
      'imgUrl': imgUrl,
      'name': name,
      'email' : email
    };
  }
}

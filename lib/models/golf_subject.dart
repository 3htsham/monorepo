import 'package:brain_trainer/models/auth_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class GolfSubject {
  final String userAuthID;
  final String? groupID;
  final String firstName;
  final String lastName;
  final DateTime? birthDay;
  final String gender;
  final String? phone;
  final String email;
  final String? address1;
  final String? address2;
  final String? city;
  final String? state;
  final String? country;
  final String? zip;
  final String? imgUrl;

  final String? coachID;
  final String? neuroID;

  const GolfSubject(
      {this.userAuthID = "",
        this.groupID,
        this.gender = "",
        this.phone,
        this.email = "",
        this.address1,
        this.address2,
        this.city,
        this.state,
        this.country,
        this.zip,
        this.firstName = "",
        this.lastName = "",
        this.birthDay,
        this.imgUrl,
        this.coachID,
        this.neuroID});

  factory GolfSubject.fromMap(Map? data) {
    data = data ?? {};
    Timestamp birthDay = data['birthDay'];
    return GolfSubject(
        userAuthID: data['userAuthID'],
        groupID: data['groupID'],
        gender: data['gender'],
        phone: data['phone'],
        email: data['email'],
        address1: data['address1'],
        address2: data['address2'],
        city: data['city'],
        state: data['state'],
        country: data['country'],
        zip: data['zip'],
        firstName: data['firstName'] ?? "",
        lastName: data['lastName'] ?? "",
        birthDay: birthDay.toDate(),
        imgUrl: data['imgUrl'],
        coachID: data['coachID'],
        neuroID: data['neuroID']);
  }

  toJson() {
    return {
      'userAuthID': userAuthID,
      'groupID': groupID,
      'gender': gender,
      'phone': phone,
      'email': email,
      'address1': address1,
      'address2': address2,
      'city': city,
      'state': state,
      'country': country,
      'zip': zip,
      'firstName': firstName,
      'lastName': lastName,
      'birthDay': Timestamp.fromDate(birthDay!),
      'imgUrl': imgUrl,
      'coachID': coachID,
      'neuroID': neuroID
    };
  }

  static GolfSubject buildDefaultSubject(AuthUser? user,
      {String? firstName, String? lastName, DateTime? birthDay}) {
    return new GolfSubject(
        userAuthID: user?.uid ?? 'BUG BUG BUG',
        groupID: 'Default',
        gender: 'Male',
        phone: 'XXX-XXX-XXXX',
        email: user?.email ?? 'BUG BUG BUG',
        address1: 'NO ADDRESS',
        address2: '',
        city: 'NO CITY',
        state: 'NO STATE',
        country: 'United States',
        zip: 'NO ZIP',
        birthDay: birthDay ?? DateTime.now(),
        firstName: firstName ?? "",
        lastName: lastName ?? "",
        coachID: "0nHojT5BDWvqDkeJGxsy", // horrible hack for V 2.0
        neuroID: "FroX5Nb0RCgt59jfpsVs");
  }
}

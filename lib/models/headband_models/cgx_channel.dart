class CGXChannel {

  String? channelName;
  List<double>? eeg = [];

  CGXChannel({this.channelName, this.eeg});

  CGXChannel.fromJson(Map<String, dynamic> data) {
    this.channelName = data['name'];
    if(data['eeg'] != null && data['eeg'].isNotEmpty) {
      eeg = <double>[];
      data['eeg'].forEach((element) {
        eeg?.add(double.parse(element));
      });
    }
  }

}
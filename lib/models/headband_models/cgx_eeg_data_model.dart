import 'dart:ffi';

class EEGData {
  int? timestamp;
  List<double>? eeg = [];
  List<double>? impedance = [];

  EEGData({this.timestamp, this.eeg, this.impedance});

  EEGData.fromJson(Map<String, dynamic> data) {
    this.timestamp = data['timestamp'];
    if (data['eeg'] != null && data['eeg'].isNotEmpty) {
      eeg = <double>[];
      data['eeg'].forEach((element) {
        eeg?.add(double.parse(element.toString()));
      });
    }

    if (data['impedance'] != null && data['impedance'].isNotEmpty) {
      impedance = <double>[];
      data['impedance'].forEach((element) {
        impedance?.add(double.parse(element.toString()));
      });
    }
  }
}

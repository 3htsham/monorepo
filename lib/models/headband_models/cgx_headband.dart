import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/helper/json_helper.dart';

class CGXHeadband {
  String? deviceName; //Default Device name
  String? assignedName; // User assigned name
  String? identifier; // A Unique identifier or hash used to identify the device
  DateTime? lastSeen; // Last Seen Date
  bool connecting = false;
  HeadbandState? state = HeadbandState.disconnected;
  double batteryPercent = 0;
  String get deviceNameWithUID {
    var result = deviceName ?? "";
    if (identifier != null) {
      if (identifier!.length > 4) {
        result +=
            ' ${identifier!.substring(identifier!.length - 4, identifier!.length).toUpperCase()}';
      } else {
        result += ' ${identifier!.toUpperCase()}';
      }
    }
    return result;
  }

  CGXHeadband(
      {this.deviceName,
      this.assignedName,
      this.identifier,
      this.lastSeen,
      this.batteryPercent = 0,
      this.state = HeadbandState.disconnected});

  CGXHeadband.fromJson(Map<String, dynamic> data) {
    this.deviceName = data['deviceName'] ?? data['name'];
    this.assignedName = data['assignedName'];
    this.identifier = data['identifier'];
    this.lastSeen = DateTime.now();
    this.batteryPercent = double.parse(data['batteryPercent'].toString());
    this.state = JSONHelper.convertStringToHeadbandState(data['state']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map();
    data['deviceName'] = this.deviceName;
    data['assignedName'] = this.assignedName;
    data['identifier'] = this.identifier;
    data['lastSeen'] = this.lastSeen.toString();
    data['batteryPercent'] = this.batteryPercent;
    return data;
  }
}

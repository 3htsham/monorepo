import 'package:brain_trainer/models/headband_models/cgx_channel.dart';
import 'cgx_headband.dart';

class CGXSample {

  CGXHeadband? headband;
  var timestamp; //arrival time of sample on host device
  List<CGXChannel>? eegV; //of type Array of EEGSamples
  bool impedanceCheck = false; // Is impedance data available in sample?
  List<double>? impedance = []; // An array of ImpedanceSample/s (in Ohms)
  double? accelX; // Accelerometer X Sample
  double? accelY; // Accelerometer Y Sample
  double? accelZ; // Accelerometer Z Sample
  int trigger = 0; // Will be 0 until teh external CGX trigger device is used

  CGXSample({
    this.headband,
    this.timestamp,
    this.eegV,
    this.impedanceCheck = false,
    this.impedance,
    this.accelX,
    this.accelY,
    this.accelZ,
    this.trigger = 0,
  });

  CGXSample.fromJson(Map<String, dynamic> data) {
    this.headband = data['headband'] != null ? CGXHeadband.fromJson(data['headband']) : null;
    this.timestamp = data['timestamp'];
    if(data['eegV'] != null) {
      this.eegV = <CGXChannel>[];
      data['eegV'].forEach((element){
        this.eegV?.add(CGXChannel.fromJson(data['eegV']));
      });
    }
    this.impedanceCheck = data['impedanceCheck'] == 'true' || data['impedanceCheck'] == true;
    accelX = data['accelX'];
    accelY = data['accelY'];
    accelZ = data['accelZ'];
    trigger = data['trigger'] ?? 0;
  }

}


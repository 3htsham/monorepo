import 'package:brain_trainer/models/training_session.dart';

class RouteArgument {
  final String? gameName;
  final String? routeName;
  final TrainingType gameType;
  final Function? onFinishCallBack;


  const RouteArgument({
    this.gameName,
    this.routeName,
    this.gameType = TrainingType.trainingType_Graph,
    this.onFinishCallBack,
  });
}

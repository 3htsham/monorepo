import 'package:cloud_firestore/cloud_firestore.dart';

class SessionPutt {
  String docID;
  final String sessionID;
  final DateTime? timeStamp;
  final int zoneScore;
  final Blob? mechanicsData;
  final Blob? neuroData;
  final String? notes;

  SessionPutt({
    this.docID = "",
    this.sessionID = "",
    this.timeStamp,
    this.zoneScore = 0,
    this.mechanicsData,
    this.neuroData,
    this.notes,
  });

  factory SessionPutt.fromMap(String docID, Map? data) {
    data = data ?? {};

    Timestamp stamp = data['timeStamp'];

    return SessionPutt(
      docID: docID,
      sessionID: data['sessionID'],
      timeStamp: stamp.toDate(),
      zoneScore: data['zoneScore'],
      mechanicsData: data['mechanicsData'],
      neuroData: data['neuroData'],
      notes: data['notes'],
    );
  }

  toJson() {
    return {
      'sessionID': sessionID,
      'timeStamp': timeStamp,
      'zoneScore': zoneScore,
      'mechanicsData': mechanicsData,
      'neuroData': neuroData,
      'notes': notes,
    };
  }
}

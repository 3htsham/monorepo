import 'package:brain_trainer/models/training_session.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TrainingProgram {
  String docID;
  final String subjectID;
  final int? currentHandicap;
  double? handicap;
  final int? handicapGoal;
  final bool isLeftHand;
  String homeCourse;
  final DateTime? creationDate;
  DateTime? lastTraining;
  DateTime? firstTraining;
  int numberSessions;
  final int? sessionsRequired;
  Duration? totalTrainingTime;
  Duration? onlineTrainingTime;
  Duration? offlineTrainingTime;
  final int? baselineThreshold;
  int? currentThreshold;
  final int? goalThreshold;
  double? lastZoneAverage;
  DateTime? nextTrainingTime;

  TrainingProgram(
      {this.docID = "",
      this.subjectID = "",
      this.currentHandicap,
      this.creationDate,
      this.lastTraining,
      this.firstTraining,
      this.numberSessions = 0,
      this.sessionsRequired,
      this.totalTrainingTime,
      this.onlineTrainingTime,
      this.offlineTrainingTime,
      this.baselineThreshold,
      this.currentThreshold,
      this.goalThreshold,
      this.lastZoneAverage,
      this.nextTrainingTime,
      this.handicap,
      this.handicapGoal,
      this.homeCourse = "",
      this.isLeftHand = false});

  factory TrainingProgram.fromMap(String docID, Map data) {
    Timestamp _creationDate = data['creationDate'];
    Timestamp _lastTraining = data['lastTraining'];
    Timestamp _firstTraining = data['firstTraining'];
    Timestamp _nextTraining = data['nextTrainingTime'];
    int _totalTrainingTime = data['totalTrainingTime'];
    int _onlineTrainingTime = data['onlineTrainingTime'];
    int _offlineTrainingTime = data['offlineTrainingTime'];

    return TrainingProgram(
        docID: docID,
        subjectID: data['subjectID'],
        currentHandicap: data['currentHandicap'],
        creationDate: _creationDate.toDate(),
        lastTraining: _lastTraining.toDate(),
        firstTraining: _firstTraining.toDate(),
        numberSessions: data['numberSessions'],
        sessionsRequired: data['sessionsRequired'],
        totalTrainingTime: Duration(seconds: _totalTrainingTime),
        onlineTrainingTime: Duration(seconds: _onlineTrainingTime),
        offlineTrainingTime: Duration(seconds: _offlineTrainingTime),
        baselineThreshold: data['baselineThreshold'],
        currentThreshold: data['currentThreshold'],
        goalThreshold: data['goalThreshold'],
        lastZoneAverage: data['lastZoneAverage'],
        nextTrainingTime: _nextTraining.toDate(),
        handicap: data['handicap'] != null ? double.parse(data['handicap'].toString()) : null,
        handicapGoal: data['handicapGoal'],
        homeCourse: data['homeCourse'],
        isLeftHand: data['isLeftHand']);
  }

  toJson() {
    return {
      //docID;
      'subjectID': subjectID,
      'currentHandicap': currentHandicap,
      'creationDate': Timestamp.fromDate(creationDate ?? DateTime.now()),
      'lastTraining': Timestamp.fromDate(lastTraining ?? DateTime.now()),
      'firstTraining': Timestamp.fromDate(firstTraining ?? DateTime.now()),
      'numberSessions': numberSessions,
      'sessionsRequired': sessionsRequired,
      'totalTrainingTime': totalTrainingTime?.inSeconds ?? 0,
      'onlineTrainingTime': onlineTrainingTime?.inSeconds ?? 0,
      'offlineTrainingTime': offlineTrainingTime?.inSeconds ?? 0,
      'baselineThreshold': baselineThreshold,
      'currentThreshold': currentThreshold,
      'goalThreshold': goalThreshold,
      'lastZoneAverage': lastZoneAverage,
      'nextTrainingTime': Timestamp.fromDate(nextTrainingTime ?? DateTime.now()),
      'handicap': handicap,
      'handicapGoal': handicapGoal,
      'homeCourse': homeCourse,
      'isLeftHand': isLeftHand
    };
  }

  void updatePostSession(TrainingSession ts) {
    numberSessions++;

    // next training time, when they can play again
    nextTrainingTime = ts.endTime?.add(Duration(days: 1));

    // duration calcs
    Duration trainingTime = ts.endTime?.difference(ts.startTime ?? DateTime.now()) ?? Duration(seconds: 0);
    totalTrainingTime = totalTrainingTime ?? Duration(seconds: 0) + trainingTime;

    // putting is offline, games are online
    if (ts.trainingType ==
        TrainingSession.trainingTypesStrings[TrainingType.trainingType_PSR]) {
      offlineTrainingTime = (offlineTrainingTime ?? Duration(seconds: 0)) + trainingTime;
    } else {
      onlineTrainingTime = onlineTrainingTime ?? Duration(seconds: 0) + trainingTime;
    }

    lastTraining = ts.endTime;

    if (firstTraining == DateTime.fromMicrosecondsSinceEpoch(0)) {
      firstTraining = ts.endTime;
    }

    // TODO: last zone average
    lastZoneAverage = ts.averageZone.toDouble();
    currentThreshold = ts.zoneThreshold;
  }

  static TrainingProgram buildDefaultProgram(String subjectID,
      {double handicap = 0, int? handicapGoal, String? homeCourse, bool? isLeftHand}) {
    return TrainingProgram(
        docID: '0',
        subjectID: subjectID,
        currentHandicap: 10,
        creationDate: DateTime.now(),
        lastTraining: DateTime.fromMicrosecondsSinceEpoch(0),
        firstTraining: DateTime.fromMicrosecondsSinceEpoch(0),
        nextTrainingTime: DateTime.now(),
        numberSessions: 0,
        sessionsRequired: 12,
        totalTrainingTime: Duration.zero,
        onlineTrainingTime: Duration.zero,
        offlineTrainingTime: Duration.zero,
        baselineThreshold: 0,
        currentThreshold: 40,
        goalThreshold: 90,
        lastZoneAverage: 0.0,
        handicap: handicap,
        handicapGoal: handicapGoal ?? 90,
        homeCourse: homeCourse ?? "0",
        isLeftHand: isLeftHand ?? false);
  }
}

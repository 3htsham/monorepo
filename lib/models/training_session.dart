import 'dart:io';

import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/helper/enum_value.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:package_info/package_info.dart';
import 'package:uuid/uuid.dart';

enum TrainingType {
  trainingType_SpaceGame,
  trainingType_Graph,
  trainingType_PSR,
}

enum FeedbackType {
  feedbackType_Visual,
  feedbackType_Audio,
  feedbackType_Haptic,
}

class TrainingSession {
  String docID;
  final String subjectID;
  final String? deviceID;
  final String? appID;
  final String? trainingType;
  final String? feedbackType;
  String? fileKey;
  final DateTime? startTime;
  DateTime? endTime;
  final String location;
  final int zoneThreshold;
  String notes;
  int averageZone;
  Duration? timeInZone;

  List<double> summaryZScores = List<double>.empty(growable: true);

  static const trainingTypesStrings = {
    TrainingType.trainingType_SpaceGame: 'Space Game',
    TrainingType.trainingType_Graph: 'Graph',
    TrainingType.trainingType_PSR: 'PPR'
  };

  static const trainingTypesEnum = {
     'Space Game' : TrainingType.trainingType_SpaceGame,
     'Graph' : TrainingType.trainingType_Graph,
     'PPR' : TrainingType.trainingType_PSR
  };

  static var trainingTypeValue = EnumValues({
    S.universeMaster: TrainingType.trainingType_SpaceGame,
    S.greenSeeker: TrainingType.trainingType_Graph,
    S.preShotRoutine: TrainingType.trainingType_PSR
  });

  static const feedbackTypesStrings = {
    FeedbackType.feedbackType_Visual: 'Visual',
    FeedbackType.feedbackType_Audio: 'Audio',
    FeedbackType.feedbackType_Haptic: 'Haptic'
  };

  TrainingSession({this.docID = "",
    this.subjectID = "",
    this.deviceID,
    this.appID,
    this.trainingType,
    this.feedbackType,
    this.fileKey,
    this.startTime,
    this.endTime,
    this.location = "",
    this.zoneThreshold = 0,
    this.notes = "",
    this.averageZone = 0,
    this.timeInZone, required this.summaryZScores});

  factory TrainingSession.fromMap(String docID, Map? data) {
    data = data ?? {};

    Timestamp startstamp = data['startTime'];
    Timestamp endstamp = data['endTime'];

    Duration zoneDur = data.containsKey('timeInZone')
        ? Duration(seconds: data['timeInZone'])
        : Duration(seconds: 0);

    List<double> zScores = List<double>.empty(growable: true);
    if(data.containsKey('summaryZScores')) {
      var old_list = data['summaryZScores'] as List<dynamic>;

      old_list.forEach((element) {
        double item = double.parse(element.toString());
        zScores.add(item);
      });
    }

    return TrainingSession(
      docID: docID,
      subjectID: data['subjectID'],
      deviceID: data['deviceID'],
      appID: data['appID'],
      trainingType: data['trainingType'],
      feedbackType: data['feedbackType'],
      fileKey: data['fileKey'],
      startTime: startstamp.toDate(),
      endTime: endstamp.toDate(),
      location: data['location'],
      zoneThreshold: data['zoneThreshold'],
      notes: data['notes'],
      averageZone: data.containsKey('averageZone') ? data['averageZone'] : 0,
      timeInZone: zoneDur,
      summaryZScores: zScores,
    );
  }

  toJson() {
    return {
      // docID is not stored
      'subjectID': subjectID,
      'deviceID': deviceID,
      'appID': appID,
      'trainingType': trainingType,
      'feedbackType': feedbackType,
      'fileKey': fileKey,
      'startTime': Timestamp.fromDate(startTime ?? DateTime.now()),
      'endTime': Timestamp.fromDate(endTime ?? DateTime.now()),
      'location': location,
      'zoneThreshold': zoneThreshold,
      'notes': notes,
      'averageZone': averageZone,
      'timeInZone': timeInZone?.inSeconds ?? 0,
      'summaryZScores': summaryZScores.map((score) => score).toList(),
    };
  }

  static Future<TrainingSession> buildSession(String userAuthID,
      TrainingType _trainingType,
      FeedbackType _feedbackType,
      int _zoneThreshold) async {
    var startTime = DateTime.now();

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();

    String androidID = "";
    String iosID = "";
    String brand = "";
    String model = "";
    String sdk = "";
    String release = "";
    String securityPatch = "";
    String display = "";

    if (Platform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      androidID = androidDeviceInfo.androidId.toString();
      model = androidDeviceInfo.model;
      brand = androidDeviceInfo.brand;
      sdk = androidDeviceInfo.version.sdkInt.toString();
      release = androidDeviceInfo.version.release.toString();
      securityPatch = androidDeviceInfo.version.securityPatch.toString();
      display = androidDeviceInfo.display.toString();
    } else {
      final iosDeviceInfo = await deviceInfo.iosInfo;
      iosID = iosDeviceInfo.identifierForVendor;
      model = iosDeviceInfo.utsname.machine;
      brand = iosDeviceInfo.systemName;
      sdk = iosDeviceInfo.systemVersion.toString();
      release = iosDeviceInfo.utsname.release.toString();
      securityPatch = "";
      display = iosDeviceInfo.utsname.version.toString();
    }

    var deviceInfoString = {
      'androidID': androidID,
      'iosID': iosID,
      'brand': brand,
      'model': model,
      'sdk': sdk,
      'release': release,
      'securityPatch': securityPatch,
      'display': display,
    };

    var appInfoString = {
      'appName': packageInfo.appName,
      'version': packageInfo.version,
      'buildNumber': packageInfo.buildNumber,
    };

    TrainingSession ts = new TrainingSession(
      subjectID: userAuthID,
      appID: appInfoString.toString(),
      deviceID: deviceInfoString.toString(),
      trainingType: TrainingSession.trainingTypesStrings[_trainingType],
      feedbackType: TrainingSession.feedbackTypesStrings[_feedbackType],
      fileKey: Uuid().toString(),
      startTime: startTime,
      location: '0.0 0.0',
      zoneThreshold: _zoneThreshold,
      averageZone: 0,
      timeInZone: Duration(seconds: 0),
      summaryZScores: List<double>.empty(growable: true),
    );
    return ts;
  }

  endSession(String _fileKey, String _notes, int _average, Duration _time,
      List<double>? currentSessionZScores) {
    endTime = DateTime.now();
    notes = _notes;
    fileKey = _fileKey;
    averageZone = _average;
    timeInZone = _time;

    // get number of seconds
    if (currentSessionZScores != null) {
      var sessionDurSeconds = endTime!.difference(startTime!).inSeconds;
      int skip = 500; //currentSessionZScores.length / sessionDurSeconds;

      int index = 0;
      currentSessionZScores.forEach((element) {
        if (index % skip == 0)
          this.summaryZScores.add(element);
        index++;
      });

//      print(
//          'endSession: seconds: $sessionDurSeconds skip: $skip size: ${summaryZScores
//              .length}');
    }
  }
}

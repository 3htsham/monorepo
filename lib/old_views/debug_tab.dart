// import 'package:brain_trainer/elements/others/box_overlay_panel.dart';
// import 'package:brain_trainer/config/common_style.dart';
// import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
// import 'package:brain_trainer/services/firebase/firebase_storage_service.dart';
// import 'package:brain_trainer/services/firebase/firestore_db_service.dart';
// import 'package:brain_trainer/services/headset/hardware_headset_service.dart';
// import 'package:brain_trainer/models/session_putt.dart';
// import 'package:brain_trainer/services/training/training_service.dart';
// import 'package:brain_trainer/old_views/two_column_overlay_panel.dart';
// import 'package:catcher/core/catcher.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:pinput/pin_put/pin_put.dart';
// import 'package:provider/provider.dart';
// import 'package:brain_trainer/models/auth_user.dart';
// import 'package:intl/intl.dart';
// import 'package:firebase_storage/firebase_storage.dart';

// class DebugTab extends StatefulWidget {
//   static const tabName = 'System';
//   static const tabTitle = 'System Panel';

//   @override
//   State<StatefulWidget> createState() => _DebugTabState();
// }

// class _DebugTabState extends State<DebugTab> {
//   List<SessionPutt> putts;
//   StorageUploadTask task;

//   double _currentThreshold;
//   double _currentHapticTime;

//   static bool _unlocked = false;
//   static String _pin = '0611';

//   final TextEditingController _pinPutController = TextEditingController();
//   final FocusNode _pinPutFocusNode = FocusNode();

//   BoxDecoration get _pinPutDecoration {
//     return BoxDecoration(
//       border: Border.all(color: optiosMaterialAccent),
//       borderRadius: BorderRadius.circular(15),
//     );
//   }

//   _comparePin(BuildContext context, String pin) {
//     if (pin == _pin) {
//       SystemChrome.setEnabledSystemUIOverlays([]);
//       setState(() {
//         _unlocked = true;
//       });
//     }
//   }

//   @override
//   void dispose() {
//     super.dispose();

//     // in case the PIN pad is up
//     SystemChrome.setEnabledSystemUIOverlays([]);
//   }

//   @override
//   Widget build(BuildContext context) {
//     var user = Provider.of<AuthUser>(context, listen: false);

//     final db = context.read<FirestoreDBService>();

//     return Consumer<TrainingService>(
//       builder: (context, tm, child) {
//         var subject = tm?.subject;
//         var sessions = tm?.subjectTrainingSessions;
//         _currentThreshold = tm.zScoreThreshold.toDouble();
//         _currentHapticTime = tm.hapticTimeMilliseconds.toDouble();
//         if (sessions != null) {
//           sessions.sort((a, b) => a.startTime.compareTo(b.startTime));
//         }

//         return Builder(builder: (context) {
//           if (!_unlocked) {
//             return BoxOverlayPanel(
//               boxContents: Center(
//                 child: Column(
//                   mainAxisSize: MainAxisSize.min,
//                   children: <Widget>[
//                     Text('Enter PIN to access System Page'),
//                     Container(
//                       margin: EdgeInsets.all(20),
//                       padding: EdgeInsets.all(20),
//                       child: PinPut(
//                         fieldsCount: 4,
//                         onSubmit: (String pin) => _comparePin(context, pin),
//                         focusNode: _pinPutFocusNode,
//                         controller: _pinPutController,
//                         submittedFieldDecoration: _pinPutDecoration.copyWith(
//                             borderRadius: BorderRadius.circular(20)),
//                         selectedFieldDecoration: _pinPutDecoration,
//                         followingFieldDecoration: _pinPutDecoration.copyWith(
//                           borderRadius: BorderRadius.circular(5),
//                           border: Border.all(
//                             color: optiosMaterialAccent,
//                           ),
//                         ),
//                       ),
//                     ),
//                     SizedBox(height: 30),
//                     Divider(),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: <Widget>[
//                         FlatButton(
//                           child: Text('Focus'),
//                           onPressed: () => _pinPutFocusNode.requestFocus(),
//                         ),
//                         FlatButton(
//                           child: Text('Unfocus'),
//                           onPressed: () => _pinPutFocusNode.unfocus(),
//                         ),
//                         FlatButton(
//                           child: Text('Clear All'),
//                           onPressed: () => _pinPutController.text = '',
//                         ),
//                       ],
//                     ),
//                   ],
//                 ),
//               ),
//             );
//           } else {
//             return Column(children: [
//               Expanded(
//                 child: TwoColumnOverlayPanel(
//                   leftOverlay: IntrinsicWidth(
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.stretch,
//                       children: [
//                         RaisedButton(
//                             child: Text(
//                               'Create Test Session',
//                               style: TextStyle(color: Colors.white),
//                             ),
//                             onPressed: () async {
//                               if (subject == null || user == null) {
//                                 return;
//                               }
//                               Scaffold.of(context).showSnackBar(SnackBar(
//                                 content: Text('Creating Test Session...'),
//                               ));
//                               tm.buildDebugTrainingSession(subject.userAuthID);
//                             }),
//                         RaisedButton(
//                             child: Text(
//                               'Delete User Data & Logout',
//                               style: TextStyle(color: Colors.white),
//                             ),
//                             onPressed: () async {
//                               if (subject == null || user == null) {
//                                 return;
//                               }
//                               Scaffold.of(context).showSnackBar(SnackBar(
//                                 content: Text('Deleting Data...'),
//                               ));
//                               await tm.debugResetUserData();

//                               FirebaseAuthService _auth =
//                                   Provider.of<FirebaseAuthService>(context,
//                                       listen: false);
//                               // TODO - check that any data in the database streams up - it should
//                               await _auth.signOut();
//                             }),
//                         RaisedButton(
//                             child: Text(
//                               'Haptic Test',
//                               style: TextStyle(color: Colors.white),
//                             ),
//                             onPressed: () async {
//                               HardwareHeadsetService()
//                                   ?.headset
//                                   ?.playHaptic(Duration(milliseconds: 250));
//                             }),
//                         RaisedButton(
//                             child: Text(
//                               'Switch Game Times',
//                               style: TextStyle(color: Colors.white),
//                             ),
//                             onPressed: () async {
//                               (context
//                                           .read<TrainingService>()
//                                           .trainingDuration
//                                           .inMinutes ==
//                                       3)
//                                   ? context
//                                       .read<TrainingService>()
//                                       .setDebugGameTime(1)
//                                   : context
//                                       .read<TrainingService>()
//                                       .setDebugGameTime(3);
//                             }),
//                         RaisedButton(
//                             child: Text(
//                               'File Sync Test',
//                               style: TextStyle(color: Colors.white),
//                             ),
//                             onPressed: () async {
//                               FirebaseStorageService fs =
//                                   new FirebaseStorageService();
//                               int uploads =
//                                   await fs.syncDataFiles(subject.email);
//                               Scaffold.of(context).showSnackBar(SnackBar(
//                                 content: Text('Synced $uploads files'),
//                               ));
//                               print('Done - uploaded $uploads');
//                             }),
//                         Text(
//                           'ZScore Threshold',
//                           style: TextStyle(
//                             color: optiosCorporateWhite,
//                             fontSize: 24,
//                           ),
//                         ),
//                         Slider(
//                           activeColor: optiosMaterialAccent,
//                           inactiveColor: optiosMaterialAdditionalLightGrey,
//                           value: _currentThreshold,
//                           min: 40,
//                           max: 90,
//                           divisions: 5,
//                           label: _currentThreshold.toString(),
//                           onChanged: (double value) {
//                             context.read<TrainingService>().zScoreThreshold =
//                                 value.round();
//                             setState(() {
//                               _currentThreshold = value;
//                             });
//                           },
//                         ),
//                         Text(
//                           'Haptic Timer (MS)',
//                           style: TextStyle(
//                             color: optiosCorporateWhite,
//                             fontSize: 24,
//                           ),
//                         ),
//                         Slider(
//                           activeColor: optiosMaterialAccent,
//                           inactiveColor: optiosMaterialAdditionalLightGrey,
//                           value: _currentHapticTime,
//                           min: 200,
//                           max: 2000,
//                           divisions: 9,
//                           label: _currentHapticTime.toString(),
//                           onChanged: (double value) {
//                             context
//                                 .read<TrainingService>()
//                                 .hapticTimeMilliseconds = value.round();
//                             setState(() {
//                               _currentHapticTime = value;
//                             });
//                           },
//                         ),
//                         RaisedButton(
//                           color: Colors.red,
//                           child: Text(
//                             'CRASH TEST',
//                             style: TextStyle(color: Colors.white),
//                           ),
//                           onPressed: () async {
//                             Scaffold.of(context).showSnackBar(SnackBar(
//                               content: Text('Sending Crash!'),
//                             ));

//                             // have to do an explicit call here, since flutter sometimes
//                             // eats unhandled exceptions.
//                             // see https://github.com/jhomlala/catcher/issues/75
//                             try {
//                               throw ('Test Crash');
//                             } catch (error, stackTrace) {
//                               Catcher.reportCheckedError(error, stackTrace);
//                             }
//                           },
//                         ),
//                       ],
//                     ),
//                   ),
//                   rightOverlay: Column(
//                     children: <Widget>[
//                       Text('User UID: ' + (user?.uid?.toString() ?? 'No User')),
//                       Text('Email:' + subject?.email?.toString() ??
//                           'No Subject'),
//                       Text('Training Program: ' +
//                           (context
//                                       .watch<TrainingService>()
//                                       .subjectTrainingProgram
//                                       .numberSessions
//                                       .toString() +
//                                   ' sessions' ??
//                               'No Program')),
//                       Text('Training Time: ' +
//                               context
//                                   .watch<TrainingService>()
//                                   .trainingDuration
//                                   .inMinutes
//                                   .toString() +
//                               ' minutes' ??
//                           'No Program'),
//                       const Divider(
//                         color: Colors.black,
//                         height: 20,
//                         thickness: 1,
//                         indent: 20,
//                         endIndent: 20,
//                       ),
//                       Text(
//                         "Sessions: ${sessions?.length ?? 0}",
//                         style: Theme.of(context)
//                             .textTheme
//                             .headline5
//                             .copyWith(color: Colors.black),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(8.0),
//                         child: SizedBox(
//                           height: 200,
//                           child: ListView.separated(
//                               separatorBuilder: (context, index) => Divider(
//                                     color: Colors.black,
//                                   ),
//                               itemCount: sessions?.length ?? 0,
//                               itemBuilder: (BuildContext context, int index) {
//                                 return FlatButton(
//                                   textColor: Colors.black,
//                                   child: Text(
//                                       '${index.toString()} : ${sessions[index].trainingType} at ${DateFormat('MM-dd-yyyy hh:mm').format(sessions[index].startTime)}'),
//                                   onPressed: () async {
//                                     if (sessions != null)
//                                       db
//                                           .getPuttsForSession(sessions[index])
//                                           .then((value) {
//                                         if (value != null) {
//                                           setState(() {
//                                             putts = value;
//                                           });
//                                         }
//                                       });
//                                   },
//                                 );
//                               }),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               )
//             ]);
//           }
//         });
//       },
//     );
//   }
// }

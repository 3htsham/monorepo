// import 'package:flutter/material.dart';
// import 'package:brain_trainer/config/common_style.dart';
// import 'package:brain_trainer/elements/others/mobile_stepper.dart';
// import 'package:brain_trainer/old_views/two_column_overlay_panel.dart';

// class HardwareHeadsetAdjustmentsRequiredStepper extends StatefulWidget {
//   final VoidCallback onTestPressed;

//   const HardwareHeadsetAdjustmentsRequiredStepper(
//       {Key key, @required this.onTestPressed})
//       : super(key: key);

//   @override
//   State<StatefulWidget> createState() =>
//       _HardwareHeadsetAdjustmentsRequiredStepperState();
// }

// class _HardwareHeadsetAdjustmentsRequiredStepperState
//     extends State<HardwareHeadsetAdjustmentsRequiredStepper> {
//   final _step1 = const MobileStep(
//     isActive: true,
//     content:
//         Text('STEP 1: Locate any sensors not lit in green and firmly press it '
//             'to your head for 3 seconds.'),
//     title: 'Headset Fitting',
//   );

//   final _step2 = const MobileStep(
//     content: Text(
//         'STEP 2: If the problem persists, try lifting the band slightly and '
//         'adjust your hair while ensuring that your hair is dry and free of '
//         'product.'),
//     title: 'Headset Fitting',
//   );

//   List<MobileStep> _steps;
//   int _currentStep = 0;
//   bool _complete = false;

//   @override
//   void initState() {
//     super.initState();

//     _steps = [
//       _step1,
//       _step2,
//       MobileStep(
//         content: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: <Widget>[
//             const Text(
//                 'STEP 3: Press the button below to re-test the connection.'),
//             RaisedButton(
//               onPressed: () {
//                 widget.onTestPressed();
//               },
//               child: Container(
//                 //width: 100,
//                 //padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 48.0,),
//                 child: Center(
//                   child: Text(
//                     "Test".toUpperCase(),
//                     style: TextStyle(color: Colors.white),
//                   ),
//                 ),
//               ),
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(20.0),
//               ),
//             ),
//           ],
//         ),
//         title: 'Test Connection',
//       ),
//     ];
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   void _goTo(int step) {
//     setState(() {
//       _currentStep = step;
//     });
//   }

//   void _next() {
//     (_currentStep + 1 != _steps.length)
//         ? _goTo(_currentStep + 1)
//         : _complete = true;
//   }

//   void _back() {
//     (_currentStep - 1 >= 0) ? _goTo(_currentStep - 1) : _complete = false;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return OverlayBox(
//       title: 'Adjustment Required',
//       titleColor: optiosMaterialFunctionalRed[700],
//       content: MobileStepper(
//         steps: _steps,
//         currentStep: _currentStep,
//         onStepNext: _next,
//         onStepBack: _back,
//       ),
//     );
//   }
// }

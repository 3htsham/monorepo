// import 'package:flutter/material.dart';
// import 'package:brain_trainer/old_views/hardware_headset_fitting.dart';
// import 'package:url_launcher/url_launcher.dart';

// class HardwareHeadsetConfig extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: <Widget>[
//         Container(child: HardwareHeadsetFitting()),
//         Expanded(
//           child: _buildLearnMore(context),
//         ),
//       ],
//     );
//   }

//   // TODO: Replace the following with "LearnMore" widget
//   Widget _buildLearnMore(context) {
//     return Container(
//       //color: Colors.white.withOpacity(0.75),
//       padding: EdgeInsets.all(16.0),
//       child: Column(children: <Widget>[
//         Container(
//           child: Text(
//             'Learn More',
//             style: Theme.of(context).textTheme.headline6,
//           ),
//           width: double.infinity,
//         ),
//         Divider(),
//         Expanded(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             //padding: EdgeInsets.all(16.0),
//             children: <Widget>[
// //              Container(
// //                //padding: EdgeInsets.symmetric(vertical: 16.0),
// //                //height: 100.0,
// //                child: ListTile(
// //                  leading: Image.asset('assets/images/putting_video_1.png'),
// //                  title: Text('Headset Features'),
// //                  subtitle: Text(
// //                      'Watch this short video to learn how to get the most out of your Optios™ headset.'),
// //                  //trailing: Icon(Icons.play_circle_outline),
// //                ),
// //              ),
//               LearnMoreTile(
//                 leading: Stack(
//                   alignment: Alignment.center,
//                   children: <Widget>[
//                     Image.asset('assets/images/abm-hardwaresetup-youtube.png'),
//                     Container(
//                       padding: EdgeInsets.all(8.0),
//                       child: Image.asset('assets/images/play.png'),
//                     ),
//                   ],
//                 ),
//                 title: Text('Hardware Connection Issues'),
//                 subtitle: Text(
//                   "Having issues connecting? Watch this video for simple step-by-step instructions and common connection issues.",
//                 ),
//                 onTap: () async {
//                   final url = 'https://www.youtube.com/watch?v=BJWi1luGu1U';
//                   if (await canLaunch(url)) {
//                     await launch(url);
//                   } else {
//                     throw 'Could not launch $url';
//                   }
//                 },
//               ),
//               Divider(),
//               LearnMoreTile(
//                 leading: Image.asset('assets/images/hardware_support.png'),
//                 title: Text('Other Hardware Support'),
//                 subtitle: Text(
//                     "Need help troubleshooting your headset? Please call Natasha at (619) 273-3361."),
//                 //trailing: Icon(Icons.play_circle_outline),
//               ),
//             ],
//           ),
//         ),
//       ]),
//     );
//   }
// }

// class LearnMoreTile extends StatelessWidget {
//   LearnMoreTile({this.leading, this.title, this.subtitle, this.onTap});

//   final Widget leading;
//   final Widget title;
//   final Widget subtitle;
//   final GestureTapCallback onTap;

//   @override
//   Widget build(BuildContext context) {
//     final ThemeData theme = Theme.of(context);

//     final TextStyle titleStyle = _titleTextStyle(theme);
//     final Widget titleText = AnimatedDefaultTextStyle(
//       style: titleStyle,
//       duration: kThemeChangeDuration,
//       child: title ?? const SizedBox(),
//       textAlign: TextAlign.left,
//     );

//     Widget subtitleText;
//     TextStyle subtitleStyle;
//     if (subtitle != null) {
//       subtitleStyle = _subtitleTextStyle(theme);
//       subtitleText = AnimatedDefaultTextStyle(
//         style: subtitleStyle,
//         duration: kThemeChangeDuration,
//         child: subtitle,
//         textAlign: TextAlign.left,
//         maxLines: 3,
//       );
//     }

//     return InkWell(
//       onTap: onTap,
//       child: Container(
//         height: 100.0,
//         padding: EdgeInsets.all(0.0),
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           mainAxisSize: MainAxisSize.max,
//           children: <Widget>[
//             Flexible(
//               flex: 1,
//               child: leading,
//             ),
//             Flexible(
//               flex: 2,
//               child: Container(
//                   //color: Colors.black12,
//                   padding: EdgeInsets.all(8.0),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.spaceAround,
//                     mainAxisSize: MainAxisSize.max,
//                     crossAxisAlignment: CrossAxisAlignment.stretch,
//                     children: <Widget>[
//                       Flexible(
//                         flex: 1,
//                         child: titleText,
//                       ),
//                       Expanded(
//                         flex: 2,
//                         child: subtitleText,
//                       )
//                     ],
//                   )),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   TextStyle _titleTextStyle(ThemeData theme) {
//     TextStyle style;
//     style = theme.textTheme.subtitle1;
//     final Color color = style.color;
//     return style.copyWith(color: color);
//   }

//   TextStyle _subtitleTextStyle(ThemeData theme) {
//     final TextStyle style = theme.textTheme.bodyText2;
//     final Color color = theme.textTheme.caption.color;
//     return style.copyWith(color: color);
//   }
// }

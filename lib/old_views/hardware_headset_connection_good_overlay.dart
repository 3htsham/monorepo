// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:brain_trainer/services/headset/hardware_headset_service.dart';
// import 'package:brain_trainer/config/common_style.dart';
// import 'package:brain_trainer/old_views/two_column_overlay_panel.dart';
// import 'package:brain_trainer/keys.dart';

// class HardwareHeadsetConnectionGoodOverlay extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return OverlayBox(
//       title: 'Connection Good!',
//       titleColor: optiosMaterialFunctionalGreen[700],
//       content: Padding(
//         padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: <Widget>[
//             Text("Your headset is setup and functioning properly."),
// //                      Text("Begin training now or, for additional metrics, "
// //                        "connect Blast Motion."),
//             Text("If you would like to test the headset's vibration "
//                 "and sound settings, click here."),
//             RaisedButton(
//               onPressed: () {
//                 mainTabsKey.currentState.selectTabByName('training');
//               },
//               child: Container(
//                 child: Center(
//                   child: Text(
//                     "Start Training".toUpperCase(),
//                     style: TextStyle(color: Colors.white),
//                   ),
//                 ),
//               ),
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(20.0),
//               ),
//             ),
//             FlatButton(
//               //color: Colors.black12,
//               onPressed: () {
//                 Provider.of<HardwareHeadsetService>(context, listen: false)
//                     .resetLastImpedanceMeasurementLevel();
//               },
//               child: Text('Reset Fitting'),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

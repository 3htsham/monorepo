// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:m4/m4.dart';
// import 'package:brain_trainer/services/headset/hardware_headset_service.dart';
// import 'package:brain_trainer/config/common_style.dart';
// import 'package:brain_trainer/config/common_functions.dart';
// import 'package:brain_trainer/old_views/hardware_headset_select_dialog.dart';
// import 'package:brain_trainer/old_views/two_column_overlay_panel.dart';
// import 'package:brain_trainer/old_views/hardware_headset_adjustments_required_stepper.dart';
// import 'package:brain_trainer/old_views/hardware_headset_connection_good_overlay.dart';
// import 'package:brain_trainer/old_views/hardware_headset_fitting_stepper.dart';
// import 'package:brain_trainer/old_views/hardware_headset_head_indicator.dart';
// import 'package:brain_trainer/keys.dart';

// class HardwareHeadsetFitting extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => _HardwareHeadsetFittingState();
// }

// class _HardwareHeadsetFittingState extends State<HardwareHeadsetFitting> {
//   final _badImpedanceThreshold = 127.0;
//   var _gradientColors;

//   var _numBadMeasurements = 0;

//   StreamSubscription<dynamic> _headsetChangedSubscription;
//   StreamSubscription<dynamic>
//       _headsetImpedanceMeasurementLevelChangedSubscription;

//   @override
//   void initState() {
//     super.initState();
//     _gradientColors = [
//       optiosMaterialLightBlue[900],
//       optiosMaterialDarkBlue[700]
//     ];

//     _updateImpedanceMeasurementLevel(
//         HardwareHeadsetService().lastImpedanceMeasurementLevel);

//     _headsetImpedanceMeasurementLevelChangedSubscription =
//         HardwareHeadsetService()
//             .onImpedanceMeasurementLevelChanged
//             .listen(_updateImpedanceMeasurementLevel);
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     _headsetChangedSubscription?.cancel();
//     _headsetImpedanceMeasurementLevelChangedSubscription?.cancel();
//   }

//   void _updateImpedanceMeasurementLevel(level) {
//     switch (level) {
//       case ImpedanceMeasurementLevel.bad:
//         _gradientColors = [
//           optiosMaterialFunctionalRed[900],
//           optiosMaterialDarkBlue[700]
//         ];
//         _numBadMeasurements++;
//         break;
//       case ImpedanceMeasurementLevel.mediocre:
//         _gradientColors = [
//           optiosMaterialFunctionalOrange[900],
//           optiosMaterialDarkBlue[700]
//         ];
//         _numBadMeasurements++;
//         break;
//       case ImpedanceMeasurementLevel.good:
//         _gradientColors = [
//           optiosMaterialFunctionalGreen[900],
//           optiosMaterialDarkBlue[700]
//         ];
//         _numBadMeasurements = 0;
//         break;
//       default:
//         _gradientColors = [
//           optiosMaterialLightBlue[900],
//           optiosMaterialDarkBlue[700]
//         ];
//         _numBadMeasurements = 0;
//         break;
//     }

//     if (_numBadMeasurements >= 3) {
//       _numBadMeasurements = 0;
//       _showHelpDialog();
//     }

//     setState(() {});
//   }

//   void _showHelpDialog() {
//     showDialog<void>(
//       context: context,
//       builder: (BuildContext context) {
//         return AlertDialog(
//           title: Text('Need help?'),
//           content: SingleChildScrollView(
//             child: ListBody(
//               children: <Widget>[
//                 Text(
//                     "It looks like you might be having problems connecting the headset. Would you like assistance?"),
//               ],
//             ),
//           ),
//           actions: <Widget>[
//             FlatButton(
//               child: Text('Yes, please!'),
//               onPressed: () {
//                 mainTabsKey.currentState.selectTabByName('support');
//                 Navigator.of(context).pop();
//               },
//             ),
//             FlatButton(
//               child: Text('No, thank you.'),
//               onPressed: () {
//                 Navigator.of(context).pop();
//               },
//             ),
//           ],
//         );
//       },
//     );
//   }

//   Future<void> _testHeadset() async {
//     var headset =
//         Provider.of<HardwareHeadsetService>(context, listen: false).headset;
//     if (headset != null) {
//       setState(() {
//         _gradientColors = [
//           optiosMaterialLightBlue[900],
//           optiosMaterialDarkBlue[700]
//         ];
//       });

//       headset.measureImpedance().then((measurement) {
//         // TODO?
//       });
//     }
//   }

//   Future<void> _selectHeadset() async {
//     var device = await showDialog<M4DeviceProxy>(
//         context: context,
//         builder: (BuildContext context) {
//           return HardwareHeadsetSelectDialog();
//         });
//     Provider.of<HardwareHeadsetService>(context, listen: false).headset =
//         device;
//   }

//   Widget _buildAddHeadset() {
//     return OverlayBox(
//       title: 'Setup Headset',
//       content: Padding(
//         padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: <Widget>[
//             Text("It looks like you haven't added a headset yet..."),
//             RaisedButton(
//               onPressed: () {
//                 _selectHeadset();
//               },
//               child: Container(
//                 //width: 100,
//                 //padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 48.0,),
//                 child: Center(
//                   child: Text(
//                     "Connect".toUpperCase(),
//                     style: TextStyle(color: Colors.white),
//                   ),
//                 ),
//               ),
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(20.0),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   Widget _buildLeftOverlay() {
//     return HardwareHeadsetHeadIndicator(
//       badImpedanceThreshold: _badImpedanceThreshold,
//     );
//   }

//   Widget _buildRightOverlay() {
//     return Consumer<HardwareHeadsetService>(builder: (context, service, child) {
//       if (service.headset != null) {
//         if (service.headset.isOpen == true) {
//           if (service.lastImpedanceMeasurementLevel ==
//               ImpedanceMeasurementLevel.unknown) {
//             return HardwareHeadsetFittingStepper(
//               onTestPressed: _testHeadset,
//             );
//           } else if (service.lastImpedanceMeasurementLevel ==
//               ImpedanceMeasurementLevel.good) {
//             return HardwareHeadsetConnectionGoodOverlay();
//           } else {
//             return HardwareHeadsetAdjustmentsRequiredStepper(
//               onTestPressed: _testHeadset,
//             );
//           }
//         } else if (service.headset.state == M4DeviceState.failedOpen) {
//           return LayoutBuilder(builder: (context, constraints) {
//             return Container(
//               padding: EdgeInsets.all(16.0),
//               height: constraints.maxHeight,
//             );
//           });
//         } else if (service.headset.isMeasuringImpedance) {
//           return LayoutBuilder(builder: (context, constraints) {
//             return Container(
//                 padding: EdgeInsets.all(16.0),
//                 height: constraints.maxHeight,
//                 child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       Text('Testing headset...'),
//                       Container(
//                         height: 16.0,
//                       ),
//                       CircularProgressIndicator(),
//                     ]));
//           });
//         } else {
//           return LayoutBuilder(builder: (context, constraints) {
//             return Container(
//                 padding: EdgeInsets.all(16.0),
//                 height: constraints.maxHeight,
//                 child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       Text('Connecting to headset...'),
//                       Container(
//                         height: 16.0,
//                       ),
//                       CircularProgressIndicator(),
//                     ]));
//           });
//         }
//       } else {
//         return _buildAddHeadset();
//       }
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return TwoColumnOverlayPanel(
//       leftOverlay: _buildLeftOverlay(),
//       rightOverlay: _buildRightOverlay(),
//       gradientColors: _gradientColors,
//     );
//   }
// }

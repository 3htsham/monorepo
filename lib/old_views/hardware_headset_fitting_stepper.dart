// import 'package:flutter/material.dart';
// import 'package:brain_trainer/elements/others/mobile_stepper.dart';
// import 'package:brain_trainer/old_views/two_column_overlay_panel.dart';

// class HardwareHeadsetFittingStepper extends StatefulWidget {
//   final VoidCallback onTestPressed;

//   const HardwareHeadsetFittingStepper({Key key, @required this.onTestPressed})
//       : super(key: key);

//   @override
//   State<StatefulWidget> createState() => _HardwareHeadsetFittingStepperState();
// }

// class _HardwareHeadsetFittingStepperState
//     extends State<HardwareHeadsetFittingStepper> {
//   final _step1 = const MobileStep(
//     isActive: true,
//     content: Text(
//         'STEP 1: Ensure there is no hair product in the regions where the headset will make contact and that your hair is dry.'),
//     title: 'Headset Fitting',
//   );

//   final _step2 = const MobileStep(
//     content: Text(
//         'STEP 2: Place the headset firmly on your head, ensuring the front arrow '
//         'indicator is pointing forward and the adjustment strap is aligned to the '
//         'back-center of your head. The fitting should be snug, but comfortable.'),
//     title: 'Headset Fitting',
//   );

//   List<MobileStep> _steps;
//   int _currentStep = 0;
//   bool _complete = false;

//   @override
//   void initState() {
//     super.initState();

//     _steps = [
//       _step1,
//       _step2,
//       MobileStep(
//         content: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: <Widget>[
//             const Text(
//                 'STEP 3: Once the headset is in place, tap the button below to test the connection.'),
//             RaisedButton(
//               onPressed: () {
//                 widget.onTestPressed();
//               },
//               child: Container(
//                 //width: 100,
//                 //padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 48.0,),
//                 child: Center(
//                   child: Text(
//                     "Test".toUpperCase(),
//                     style: TextStyle(color: Colors.white),
//                   ),
//                 ),
//               ),
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(20.0),
//               ),
//             ),
//           ],
//         ),
//         title: 'Test Connection',
//       ),
//     ];
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   void _goTo(int step) {
//     setState(() {
//       _currentStep = step;
//     });
//   }

//   void _next() {
//     (_currentStep + 1 != _steps.length)
//         ? _goTo(_currentStep + 1)
//         : _complete = true;
//   }

//   void _back() {
//     (_currentStep - 1 >= 0) ? _goTo(_currentStep - 1) : _complete = false;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return OverlayBox(
//       title: 'Headset Fitting',
//       content: MobileStepper(
//         steps: _steps,
//         currentStep: _currentStep,
//         onStepNext: _next,
//         onStepBack: _back,
//       ),
//     );
//   }
// }

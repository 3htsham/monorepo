// import 'dart:async';
// import 'dart:math' as math;
// import 'package:brain_trainer/config/common_style.dart';
// import 'package:brain_trainer/config/common_functions.dart';
// import 'package:brain_trainer/services/headset/hardware_headset_service.dart';
// import 'package:flutter/material.dart';
// import 'package:m4/m4.dart';

// class HardwareHeadsetHeadIndicator extends StatefulWidget {
//   const HardwareHeadsetHeadIndicator({
//     Key key,
//     this.badImpedanceThreshold = kDefaultBadImpedanceThreshold,
//     this.mediocreImpedanceThreshold = kDefaultMediocreImpedanceThreshold,
//   }) : super(key: key);

//   final badImpedanceThreshold;
//   final mediocreImpedanceThreshold;

//   @override
//   State<StatefulWidget> createState() => _HardwareHeadsetHeadIndicatorState();
// }

// class _HardwareHeadsetHeadIndicatorState
//     extends State<HardwareHeadsetHeadIndicator> with TickerProviderStateMixin {
//   AnimationController controller;
//   Animation<double> animation;

//   StreamSubscription<dynamic> _headsetChangedSubscription;
//   StreamSubscription<dynamic> _headsetEventSubscription;
//   StreamSubscription<dynamic>
//       _headsetImpedanceMeasurementLevelChangedSubscription;
//   StreamSubscription<dynamic> _headsetImpedanceMeasureChangedSubscription;

//   Widget _headIndicator = Container();

//   Color _leftParietalColor = optiosAdditionalLightGrey;
//   Color _rightParietalColor = optiosAdditionalLightGrey;
//   Color _leftFrontalColor = optiosAdditionalLightGrey;
//   Color _rightFrontalColor = optiosAdditionalLightGrey;
//   Color _haloColor = optiosAdditionalLightGrey;

//   @override
//   void initState() {
//     super.initState();

//     controller = AnimationController(
//       vsync: this,
//       duration: Duration(seconds: 2),
//     );

//     Tween<double> _radius1Tween = Tween(begin: 0.2, end: 1.0);

//     final Animation curve =
//         CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

//     animation = _radius1Tween.animate(curve)
//       ..addListener(() {
//         setState(() {
//           // ...
//         });
//       })
//       ..addStatusListener((status) {
// //        if (status == AnimationStatus.completed) {
// //          controller.reverse();
// //        } else if (status == AnimationStatus.dismissed) {
// //          controller.forward();
// //        }
//       });

//     // Listen for headset changes and update subscription when they happen
//     _headsetChangedSubscription =
//         HardwareHeadsetService().onHeadsetChanged.listen((headset) {
//       _updateHeadset(headset);
//     });

//     // Try to subscribe to the current headset
//     _updateHeadset(HardwareHeadsetService().headset);

//     _headsetEventSubscription =
//         HardwareHeadsetService().onHeadsetEvent.listen((event) {
//       if (event == HardwareHeadsetEvent.testing) {
//         controller.reverse();
//         setState(() {
//           _headIndicator = Icon(Icons.more_horiz,
//               color: optiosAdditionalLightGrey,
//               size: 96.0,
//               key: ValueKey('testing'));
//         });
//       } else if (event == HardwareHeadsetEvent.testOk) {
//         controller.forward();
//       } else if (event == HardwareHeadsetEvent.testFailed) {
//         _leftParietalColor = _rightParietalColor = _leftFrontalColor =
//             _rightFrontalColor = _haloColor = optiosAdditionalLightGrey;

//         controller.forward();
//       } else if (event == HardwareHeadsetEvent.connecting) {
//         controller.reverse();
//         setState(() {
//           _headIndicator = Icon(Icons.more_horiz,
//               color: optiosAdditionalLightGrey,
//               size: 96.0,
//               key: ValueKey('connecting'));
//         });
//       } else if (event == HardwareHeadsetEvent.connected) {
//         controller.forward();
//         setState(() {
//           _headIndicator = Container();
//         });
//       } else if (event == HardwareHeadsetEvent.connectionFailed) {
//         controller.reverse();
//         setState(() {
//           _headIndicator = Icon(Icons.not_listed_location,
//               color: optiosAdditionalMediumGrey,
//               size: 96.0,
//               key: ValueKey('connectionFailed'));
//         });
//       }
//     });

//     _updateImpedanceMeasurementLevel(
//         HardwareHeadsetService().lastImpedanceMeasurementLevel);

//     _headsetImpedanceMeasurementLevelChangedSubscription =
//         HardwareHeadsetService()
//             .onImpedanceMeasurementLevelChanged
//             .listen(_updateImpedanceMeasurementLevel);
//   }

//   @override
//   void dispose() {
//     controller.dispose();
//     _headsetChangedSubscription?.cancel();
//     _headsetEventSubscription?.cancel();
//     _headsetImpedanceMeasureChangedSubscription?.cancel();
//     _headsetImpedanceMeasurementLevelChangedSubscription?.cancel();
//     super.dispose();
//   }

//   void _updateHeadset(M4DeviceProxy headset) {
//     if (headset != null) {
//       if (_headsetImpedanceMeasureChangedSubscription != null) {
//         _headsetImpedanceMeasureChangedSubscription.cancel();
//       }
//       _headsetImpedanceMeasureChangedSubscription =
//           headset.onImpedanceMeasureEvent.listen((event) {
// //        M4Impedance m4Impedance = M4Impedance()
// //        ..lP = 20.0
// //        ..rP = 20.0
// //        ..lF = 20.0
// //        ..rF = 20.0;
// //        _updateImpedanceMeasurement(m4Impedance);

//         _updateFromImpedanceMeasurement(event.deviceEvent.impedance);
//       });

//       if (headset.isOpen) {
//         controller.forward();
//         setState(() {
//           _headIndicator = Container();
//         });
//       }
//     } else {
//       controller.reverse();
//       setState(() {
//         _headIndicator = Icon(Icons.not_listed_location,
//             color: optiosAdditionalMediumGrey,
//             size: 96.0,
//             key: ValueKey('disconnected'));
//       });
//     }
//   }

//   Color _colorFromImpedance(impedance) {
//     switch (getImpedanceMeasurementLevel(impedance,
//         mediocre: widget.mediocreImpedanceThreshold,
//         bad: widget.badImpedanceThreshold)) {
//       case ImpedanceMeasurementLevel.bad:
//         return optiosFunctionalRed;
//       case ImpedanceMeasurementLevel.mediocre:
//         return optiosFunctionalOrange;
//       case ImpedanceMeasurementLevel.good:
//       default:
//         return optiosFunctionalGreen;
//     }
//   }

//   void _updateFromImpedanceMeasurement(M4Impedance measurement) {
//     _leftParietalColor = _colorFromImpedance(measurement.lP);
//     _rightParietalColor = _colorFromImpedance(measurement.rP);
//     _leftFrontalColor = _colorFromImpedance(measurement.lF);
//     _rightFrontalColor = _colorFromImpedance(measurement.rF);
//     setState(() {});
//   }

//   void _updateImpedanceMeasurementLevel(level) {
//     switch (level) {
//       case ImpedanceMeasurementLevel.bad:
//         _haloColor = optiosFunctionalRed;
//         _headIndicator = Icon(Icons.close,
//             color: optiosFunctionalRed, size: 96.0, key: ValueKey('bad'));
//         break;
//       case ImpedanceMeasurementLevel.mediocre:
//         _haloColor = optiosFunctionalOrange;
//         _headIndicator = Icon(Icons.close,
//             color: optiosFunctionalOrange,
//             size: 96.0,
//             key: ValueKey('mediocre'));
//         break;
//       case ImpedanceMeasurementLevel.good:
//         _haloColor = optiosFunctionalGreen;
//         _headIndicator = Icon(Icons.check,
//             color: optiosFunctionalGreen, size: 96.0, key: ValueKey('good'));
//         break;
//       case ImpedanceMeasurementLevel.unknown:
//       default:
//         _haloColor = optiosAdditionalLightGrey;
//         _headIndicator = Container();
//         _leftParietalColor = _rightParietalColor =
//             _leftFrontalColor = _rightFrontalColor = optiosAdditionalLightGrey;
//         break;
//     }
//     setState(() {});
//   }

//   @override
//   Widget build(BuildContext context) {
//     return LayoutBuilder(builder: (context, constraints) {
//       return Stack(children: <Widget>[
//         FittedBox(
//             child: SizedBox(
//           width: constraints.maxWidth,
//           height: constraints.maxHeight,
//           child: AnimatedBuilder(
//             animation: animation,
//             builder: (context, snapshot) {
//               return CustomPaint(
//                 painter: HardwareHeadsetHaloPainter(
//                   animation.value,
//                   _leftParietalColor,
//                   _rightParietalColor,
//                   _leftFrontalColor,
//                   _rightFrontalColor,
//                   _haloColor,
//                 ),
//               );
//             },
//           ),
//         )),
//         Center(child: Image.asset('assets/images/head.png')),
//         Container(
//           width: constraints.maxWidth,
//           height: constraints.maxHeight,
//           child: Align(
//             alignment: Alignment.center,
//             child: AnimatedSwitcher(
//               duration: Duration(milliseconds: 300),
//               switchOutCurve: Curves.easeOut,
//               switchInCurve: Curves.elasticIn,
//               transitionBuilder: (Widget child, Animation<double> animation) {
//                 return ScaleTransition(child: child, scale: animation);
//               },
//               child: _headIndicator,
//             ),
//           ),
//         ),
//       ]);
//     });
//   }
// }

// const defaultSensorSize = 20.0;

// class HardwareHeadsetHaloPainter extends CustomPainter {
//   HardwareHeadsetHaloPainter(
//     this.radius,
//     this.leftParietalSensorColor,
//     this.rightParietalSensorColor,
//     this.leftFrontalSensorColor,
//     this.rightFrontalSensorColor,
//     this.haloColor,
//   );

//   final double radius;
//   final Color leftParietalSensorColor;
//   final Color rightParietalSensorColor;
//   final Color leftFrontalSensorColor;
//   final Color rightFrontalSensorColor;
//   final Color haloColor;

//   static final _slice = 2 * math.pi / 4.0;

//   static final _rightParietal = 0;
//   static final _leftParietal = 1;
//   static final _rightFrontal = 3;
//   static final _leftFrontal = 2;

//   static final _outerCircleScale = 1.0;
//   static final _innerCircleScale = 0.72;
//   static final _bigCircleScale = 0.54;

//   Offset _pointOnCircumference(double radius, double angle) {
//     return Offset(radius * math.cos(angle), radius * math.sin(angle));
//   }

//   void _drawSensor(canvas, position, radius, index, color,
//       {size = defaultSensorSize}) {
//     Offset sensorOffset =
//         _pointOnCircumference(radius, (_slice * index) + _slice * 0.5);
//     canvas.drawCircle(
//         position + sensorOffset,
//         size * 1.2,
//         Paint()
//           ..color = Colors.black.withOpacity(0.5)
//           ..maskFilter = MaskFilter.blur(BlurStyle.normal, 4));
//     canvas.drawCircle(position + sensorOffset, size, Paint()..color = color);
//     canvas.drawCircle(
//         position + sensorOffset,
//         size,
//         Paint()
//           ..color = optiosAdditionalDarkGrey
//           ..style = PaintingStyle.stroke
//           ..strokeWidth = 4.0);
//   }

//   @override
//   void paint(Canvas canvas, Size size) {
//     final Offset center = Offset(size.width * 0.5, size.height * 0.5);

//     var r = 1.0 / radius;

//     final Paint outerCirclePaint = Paint()
//       ..color = haloColor.withOpacity(0.15)
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 32.0;
//     var outerRadius = size.width * radius * _outerCircleScale;
//     canvas.drawCircle(center, outerRadius, outerCirclePaint);

//     final Paint innerCirclePaint = Paint()
//       ..color = haloColor.withOpacity(0.33)
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 8.0;
//     var innerRadius = size.width * radius * _innerCircleScale;
//     canvas.drawCircle(center, innerRadius, innerCirclePaint);

//     final Paint bigCirclePaint = Paint()
//       ..color = haloColor.withOpacity(0.5)
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 48.0;
//     var bigRadius = size.width * radius * _bigCircleScale;
//     canvas.drawCircle(center, bigRadius, bigCirclePaint);

//     final Paint littleCirclePaint = Paint()
//       ..color = haloColor
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 4.0;
//     var littleRadius = size.width * radius * 0.5;
//     canvas.drawCircle(center, littleRadius, littleCirclePaint);

//     _drawSensor(
//         canvas, center, littleRadius, _rightParietal, rightParietalSensorColor);
//     _drawSensor(
//         canvas, center, littleRadius, _leftParietal, leftParietalSensorColor);
//     _drawSensor(
//         canvas, center, littleRadius, _rightFrontal, rightFrontalSensorColor);
//     _drawSensor(
//         canvas, center, littleRadius, _leftFrontal, leftFrontalSensorColor);
//   }

//   @override
//   bool shouldRepaint(CustomPainter oldDelegate) {
//     return false;
//   }
// }

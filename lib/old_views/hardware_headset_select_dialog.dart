// import 'package:flutter/material.dart';
// import 'package:m4/m4.dart';

// class HardwareHeadsetSelectDialog extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => _HardwareHeadsetSelectDialogState();
// }

// class _HardwareHeadsetSelectDialogState
//     extends State<HardwareHeadsetSelectDialog> {
//   Map _devices;
//   Future<Map> _devicesFuture;
//   M4DeviceProxy _device;

//   @override
//   void initState() {
//     super.initState();
//     _devicesFuture = _updateDevices();
//   }

//   Future<Map> _updateDevices() {
//     return M4().devices.then((value) => _devices = value);
//   }

//   Widget _buildHeadsetListView() {
//     return FutureBuilder(
//         future: _devicesFuture,
//         builder: (context, snapshot) {
//           if (snapshot.connectionState != ConnectionState.waiting) {
//             if (snapshot.hasData) {
//               return ListView.separated(
//                 padding: const EdgeInsets.all(10),
//                 itemCount: snapshot.data.length,
//                 itemBuilder: (BuildContext context, int index) {
//                   String key = snapshot.data.keys.elementAt(index);
//                   return AnimatedContainer(
//                       duration: const Duration(milliseconds: 400),
//                       color: (() {
//                         if (_device != null &&
//                             _device.hash == snapshot.data[key].hash) {
//                           return Colors.black12;
//                         }
//                         return null;
//                       }()),
//                       child: ListTile(
//                           leading: Builder(builder: (context) {
//                             switch (snapshot.data[key].type) {
//                               case 'BLUETOOTH':
//                                 return Icon(
//                                   Icons.bluetooth,
//                                   size: 42.0,
//                                   color: Colors.blue,
//                                 );
//                               case 'NETWORK':
//                                 return Icon(
//                                   Icons.network_wifi,
//                                   color: Colors.purpleAccent,
//                                   size: 42.0,
//                                 );
//                             }
//                             return Icon(
//                               Icons.device_unknown,
//                               color: Colors.blueGrey,
//                               size: 42,
//                             );
//                           }),
//                           title: Text(snapshot.data[key].name,
//                               style: Theme.of(context).textTheme.headline6),
//                           subtitle: Text(snapshot.data[key].address,
//                               style: Theme.of(context).textTheme.subtitle1),
//                           selected: _device != null
//                               ? _device.hash == snapshot.data[key].hash
//                               : false,
//                           onTap: () {
//                             Navigator.pop(context, snapshot.data[key]);
//                             if (_device == null) {
//                               setState(() {
//                                 _device = snapshot.data[key];
//                               });
//                             }
//                           }));
//                 },
//                 //separatorBuilder: (BuildContext context, int index) => const Divider(),
//                 separatorBuilder: (BuildContext context, int index) =>
//                     Container(
//                   width: 16.0,
//                   height: 16.0,
//                 ),
//               );
//             } else if (snapshot.hasError) {
//               return Container(
//                   child: Text(
//                       "Unable to get devices: " + snapshot.error.toString()));
//             } else {
//               return Center(
//                 child: Container(child: Text("Done, without data.")),
//               );
//             }
//           } else {
//             return Center(
//                 child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                   SizedBox(
//                     child: CircularProgressIndicator(),
//                     width: 384,
//                     height: 384,
//                   ),
//                   SizedBox(
//                     height: 40,
//                   ),
//                   Text("Getting devices..."),
//                 ]));
//           }
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Dialog(
//       child: Container(
//           padding: EdgeInsets.all(16.0),
//           child: Column(
//             mainAxisSize: MainAxisSize.min,
//             children: <Widget>[
//               Container(
//                 child: Text(
//                   'Select headset',
//                   style: Theme.of(context).textTheme.headline5,
//                   textAlign: TextAlign.center,
//                 ),
//               ),
//               Container(
//                 padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
//                 child: Text(
//                   "Ensure the headset is turned on and its green indicator light is flashing three times in a row.",
//                   style: Theme.of(context).textTheme.subtitle1,
//                   textAlign: TextAlign.center,
//                 ),
//               ),
//               Divider(),
//               //Container(child: Text(""),),
//               Container(
//                 height: MediaQuery.of(context).size.height / 2 * 1,
//                 child: _buildHeadsetListView(),
//               ),
//             ],
//           )),
//     );
//   }
// }

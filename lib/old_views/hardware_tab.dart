// import 'package:flutter/material.dart';
// //import 'package:brain_trainer/common_style.dart';
// //import 'package:brain_trainer/presentation/optios_icons_icons.dart';
// import 'package:brain_trainer/old_views/hardware_headset_config.dart';
// //import 'package:brain_trainer/hardware_blast_config.dart';

// class HardwareTab extends StatefulWidget {
//   static const tabName = 'Hardware';
//   static const tabTitle = 'Hardware Set Up';

//   @override
//   State<StatefulWidget> createState() => _HardwareTabState();
// }

// class _HardwareTabState extends State<HardwareTab> {
//   Widget _buildTabView() {
//     return HardwareHeadsetConfig();

// // TODO: DISABLED BLAST MOTION CONFIG
// //    return DefaultTabController(
// //      length: 2,
// //      child: Column(
// //        children: <Widget>[
// //          TabBar(
// //            labelColor: Colors.white,
// //            unselectedLabelColor: optiosAdditionalMediumGrey,
// //            indicator: BoxDecoration(
// //              color: optiosCorporateAccent,
// //            ),
// //            tabs: <Widget>[
// //              Tab(
// //                text: 'Headset'.toUpperCase(),
// //                icon: Icon(OptiosIcons.headset_connection),
// //              ),
// //              Tab(
// //                text: 'Blast Motion'.toUpperCase(),
// //                icon: Icon(OptiosIcons.blast_motion_connection),
// //              ),
// //            ],
// //          ),
// //          Divider(thickness: 1.0, height: 1.0, color: optiosCorporateAccent,),
// //          Expanded(
// //            child: TabBarView(
// //              physics: NeverScrollableScrollPhysics(),
// //              children: <Widget>[
// //                HardwareHeadsetConfig(),
// //                HardwareBlastMotionConfig(),
// //              ]
// //            ),
// //          ),
// //        ],
// //      ),
// //    );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return _buildTabView();

//     // TODO: DISABLED BLAST MOTION CONFIG
// //    return Consumer<M4DeviceProxy>(
// //      builder: (context, device, child) {
// //        if (device != null) {
// //          return _buildTabView();
// //        } else {
// //          return _buildM4NotConfigured();
// //        }
// //      },
// //    );
// //    return FutureBuilder(
// //      future: Provider.of<M4>(context).getBluetoothState(),
// //      builder: (context, snapshot) {
// //        if (snapshot.hasData) {
// //          if (snapshot.data) {
// //            return _buildTabView();
// //          } else {
// //            return _buildBluetoothDisabled();
// //          }
// //        } else if (snapshot.hasError) {
// //          return _buildFutureError();
// //        } else {
// //          return _buildFutureWait();
// //        }
// //      },
// //    );
//   }
// }

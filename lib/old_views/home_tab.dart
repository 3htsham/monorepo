// import 'package:brain_trainer/keys.dart';
// import 'package:brain_trainer/old_views/two_column_overlay_panel.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:brain_trainer/models/auth_user.dart';

// import '../config/common_style.dart';

// class HomeTab extends StatefulWidget {
//   static const tabName = 'Home';
//   static const tabTitle = 'Expert Brain State Training';

//   @override
//   State<StatefulWidget> createState() => _HomeTabState();
// }

// class _HomeTabState extends State<HomeTab> with TickerProviderStateMixin {
//   AnimationController controller;
//   Animation<double> animation;

//   @override
//   void initState() {
//     super.initState();
//     controller = new AnimationController(
//         duration: Duration(milliseconds: 1500), vsync: this)
//       ..addListener(() => setState(() {}));
//     final Animation curve =
//         CurvedAnimation(parent: controller, curve: Curves.elasticInOut);
//     animation = Tween(begin: 600.0, end: 0.0).animate(curve);
//     controller.forward();
//   }

//   @override
//   void dispose() {
//     controller.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: <Widget>[
//         Container(
//           decoration: BoxDecoration(
//             image: DecorationImage(
//               image: AssetImage('assets/images/golf_green_background.png'),
//               fit: BoxFit.fitWidth,
//               alignment: Alignment.topLeft,
//             ),
//             border: Border.all(
//               color: Colors.black,
//               width: 1.0,
//             ),
//           ),
//         ),
//         Center(
//           child: Transform.translate(
//             offset: Offset(0.0, animation.value),
//             child: Container(
//                 width: 400.0,
//                 height: 400.0,
//                 decoration: BoxDecoration(
//                     color: Colors.white.withOpacity(0.9),
//                     border: Border.all(
//                         color: optiosAdditionalMediumGrey, width: 2.0),
//                     boxShadow: [
//                       BoxShadow(
//                         color: Colors.black.withOpacity(0.2),
//                         spreadRadius: 8,
//                         blurRadius: 7,
//                         offset: Offset(0, 0),
//                       )
//                     ]),
//                 child: _buildWelcomeBox()),
//           ),
//         ),
//       ],
//     );
//   }

//   Widget _buildWelcomeBox() {
//     AuthUser _user = Provider.of<AuthUser>(context, listen: false);

//     return OverlayBox(
//       title: 'Welcome back,\n ${(_user?.displayName ?? ' ...')}',
//       content: Padding(
//         padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: <Widget>[
//             Text(
//               'Before your next in-person meeting, please complete 6 Pre-Shot Routine training sessions.\n\nReady to begin training? Tap the button below to connect your headset.',
//               style: Theme.of(context).textTheme.subtitle1,
//             ),
//             RaisedButton(
//               onPressed: () {
//                 mainTabsKey.currentState.selectTabByName('hardware');
//               },
//               child: Container(
//                 //width: 100,
//                 //padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 48.0,),
//                 child: Center(
//                   child: Text(
//                     "Setup Headset".toUpperCase(),
//                     style: TextStyle(color: Colors.white),
//                   ),
//                 ),
//               ),
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(20.0),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   Widget _buildImage() {
//     return Image.asset('assets/images/female_golfer_crouch.png');
//   }
// }

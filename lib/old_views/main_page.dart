// import 'dart:math' as math;
// import 'package:brain_trainer/old_views/more_tab.dart';
// import 'package:brain_trainer/old_views/space_game/space_game.dart';
// import 'package:brain_trainer/services/training/training_service.dart';
// import 'package:brain_trainer/views/graph_feedback/graph_feedback.dart';
// import 'package:flutter/material.dart';
// import 'package:brain_trainer/old_views/main_scaffold.dart';
// import 'package:brain_trainer/config/common_style.dart';
// import 'package:brain_trainer/old_views/home_tab.dart';
// import 'package:brain_trainer/old_views/hardware_tab.dart';
// import 'package:brain_trainer/old_views/training_tab.dart';
// import 'package:brain_trainer/old_views/debug_tab.dart';
// import 'package:brain_trainer/services/headset/hardware_headset_service.dart';
// import 'package:brain_trainer/keys.dart';
// import 'package:m4/m4.dart';
// import 'package:provider/provider.dart';

// class TabData {
//   final Widget tab;
//   final String title;
//   final String label;
//   final IconData icon;

//   TabData(this.tab, this.title, this.label, this.icon);
// }

// class MainPage extends StatefulWidget {
//   static const routeName = '/main';

//   @override
//   State<StatefulWidget> createState() => _MainPageState();
// }

// class _MainPageState extends State<MainPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Consumer<TrainingService>(builder: (context, service, child) {
//       int stackIndex = 0;
//       if (service.lastTrainingState == TrainingGameState.playingGraphGame) {
//         stackIndex = 1;
//       } else if (service.lastTrainingState ==
//           TrainingGameState.playingSpaceGame) {
//         stackIndex = 2;
//       }
//       return IndexedStack(
//         index: stackIndex,
//         children: <Widget>[
//           MainTabs(
//             key: mainTabsKey,
//           ),
//           Builder(builder: (context) {
//             if (context.watch<TrainingService>().lastTrainingState ==
//                 TrainingGameState.playingGraphGame) {
//               return GameWrapper(
//                 gameType: 'graph_game',
//               );
//             } else {
//               return Container();
//             }
//           }),
//           Builder(builder: (context) {
//             if (context.watch<TrainingService>().lastTrainingState ==
//                 TrainingGameState.playingSpaceGame) {
//               return GameWrapper(
//                 gameType: 'space_game',
//               );
//             } else {
//               return Container();
//             }
//           }),
//         ],
//       );
//     });
//   }
// }

// class GameWrapper extends StatefulWidget {
//   const GameWrapper({Key key, this.gameType}) : super(key: key);

//   final String gameType;

//   @override
//   State<StatefulWidget> createState() => _GameWrapperState();
// }

// class _GameWrapperState extends State<GameWrapper>
//     with TickerProviderStateMixin {
//   AnimationController controller;

//   String get timerString {
//     Duration duration = (controller.duration + Duration(seconds: 1)) -
//         controller.duration * controller.value;
//     return '${(duration.inSeconds % 60).toString()}';
//   }

//   bool _timerComplete = false;

//   @override
//   void initState() {
//     super.initState();

//     controller = AnimationController(
//       vsync: this,
//       duration: Duration(seconds: 10),
//     );
//     controller.addStatusListener((status) {
//       if (status == AnimationStatus.completed) {
//         setState(() {
//           _timerComplete = true;
//         });
//       }
//     });
//     controller.forward();
//   }

//   @override
//   void dispose() {
//     controller.dispose();

//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: <Widget>[
//         LayoutBuilder(builder: (context, constraints) {
//           if (_timerComplete) {
//             dynamic game;
//             switch (widget.gameType) {
//               case 'graph_game':
//                 game = GraphFeedbackGame(
//                         constraints.biggest,
//                         M4().onSampleEvent(),
//                         context.read<TrainingService>().gameDone,
//                         trainingService: context.read<TrainingService>())
//                     .widget;
//                 break;
//               case 'space_game':
//                 game = SpaceGame(constraints.biggest, M4().onSampleEvent(),
//                         context.read<TrainingService>().gameDone,
//                         trainingService: context.read<TrainingService>())
//                     .widget;
//                 break;
//             }
//             return game;
//           } else {
//             ThemeData themeData = Theme.of(context);
//             return AnimatedBuilder(
//                 animation: controller,
//                 builder: (context, child) {
//                   return Material(
//                       type: MaterialType.transparency,
//                       child: Container(
//                         padding: EdgeInsets.symmetric(horizontal: 16.0),
//                         child: Align(
//                           alignment: FractionalOffset.center,
//                           child: AspectRatio(
//                             aspectRatio: 1.0,
//                             child: Stack(
//                               children: <Widget>[
//                                 Positioned.fill(
//                                   child: CustomPaint(
//                                       painter: CustomTimerPainter(
//                                     animation: controller,
//                                     backgroundColor: Colors.transparent,
//                                     color: themeData.indicatorColor,
//                                   )),
//                                 ),
//                                 Align(
//                                   alignment: FractionalOffset.center,
//                                   child: Column(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceEvenly,
//                                       crossAxisAlignment:
//                                           CrossAxisAlignment.center,
//                                       children: <Widget>[
//                                         Text(
//                                           "Get Ready!",
//                                           style: TextStyle(
//                                               fontSize: 48.0,
//                                               color: Colors.white),
//                                         ),
//                                         Text(
//                                           timerString,
//                                           style: TextStyle(
//                                               fontSize: 112.0,
//                                               color: Colors.white),
//                                         ),
//                                       ]),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                       ));
//                 });
//           }
//         }),
//         Column(children: <Widget>[
//           Container(
//             padding: EdgeInsets.symmetric(vertical: 8.0),
//             width: 128.0,
//             child: RaisedButton(
//               onPressed: () => context.read<TrainingService>().gameDone(),
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(20.0),
//               ),
//               color: Colors.white10,
//               child: Text(
//                 "Cancel".toUpperCase(),
//                 style: TextStyle(color: optiosAdditionalMediumGrey),
//               ),
//             ),
//           ),
//           Expanded(child: Container()),
//         ]),
//       ],
//     );
//   }
// }

// class CustomTimerPainter extends CustomPainter {
//   CustomTimerPainter({
//     this.animation,
//     this.backgroundColor,
//     this.color,
//   }) : super(repaint: animation);

//   final Animation<double> animation;
//   final Color backgroundColor, color;

//   @override
//   void paint(Canvas canvas, Size size) {
//     Paint paint = Paint()
//       ..color = backgroundColor
//       ..strokeWidth = 10.0
//       ..strokeCap = StrokeCap.butt
//       ..style = PaintingStyle.stroke;

//     canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
//     paint.color = color;
//     double progress = (1.0 - animation.value) * 2 * math.pi;
//     canvas.drawArc(Offset.zero & size, math.pi * 1.5, -progress, false, paint);
//   }

//   @override
//   bool shouldRepaint(CustomTimerPainter old) {
//     return animation.value != old.animation.value ||
//         color != old.color ||
//         backgroundColor != old.backgroundColor;
//   }
// }

// class MainTabs extends StatefulWidget {
//   const MainTabs({Key key}) : super(key: key);

//   @override
//   State<StatefulWidget> createState() => MainTabsState();
// }

// class MainTabsState extends State<MainTabs> {
//   int _selectedIndex = 0;
//   Widget _currentWidget;

//   static List<TabData> _tabs = <TabData>[
//     TabData(HomeTab(), HomeTab.tabTitle, HomeTab.tabName, Icons.home),
//     TabData(HardwareTab(), HardwareTab.tabTitle, HardwareTab.tabName,
//         Icons.phonelink_setup),
//     TabData(TrainingTab(), TrainingTab.tabTitle, TrainingTab.tabName,
//         Icons.blur_circular),
//     TabData(MoreTab(), MoreTab.tabTitle, MoreTab.tabName, Icons.help),
//     TabData(DebugTab(), DebugTab.tabTitle, DebugTab.tabName, Icons.build),
//   ];

//   static Map<String, int> _tabIndexLookupByName = {
//     'home': 0,
//     'hardware': 1,
//     'training': 2,
//     'support': 3,
//     'debug': 4,
//   };

//   @override
//   void initState() {
//     super.initState();
//     _currentWidget = _tabs.elementAt(_selectedIndex).tab;
//     M4().getBluetoothState();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   selectTabByName(String name) {
//     if (_tabIndexLookupByName.containsKey(name)) {
//       onTabTapped(_tabIndexLookupByName[name]);
//     }
//   }

//   onTabTapped(int index) {
//     setState(() {
//       _selectedIndex = index;
//       _currentWidget = _tabs.elementAt(_selectedIndex).tab;
//     });
//   }

//   Widget _buildBluetoothDisabled() {
//     return Container(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: <Widget>[
//           Icon(
//             Icons.bluetooth_disabled,
//             size: 48,
//             color: Colors.red,
//           ),
//           Container(
//             height: 16.0,
//           ),
//           Text('Please enable Bluetooth.',
//               style: Theme.of(context).textTheme.headline4),
//         ],
//       ),
//     );
//   }

//   Widget _buildFutureError() {
//     return Container(
//       child: Icon(Icons.error),
//     );
//   }

//   bool _firstBuild = true;

//   @override
//   Widget build(BuildContext context) {
//     return MainScaffold(
//       title: _tabs.elementAt(_selectedIndex).title,
//       body: StreamBuilder(
//           stream: M4()
//               .onPluginEvent()
//               .map<M4PluginEvent>((M4Event event) => event.pluginEvent),
//           builder: (context, AsyncSnapshot<M4PluginEvent> snapshot) {
//             if (snapshot.hasData) {
//               if (snapshot.data.type ==
//                   M4PluginEvent_PluginEventType.PLUGIN_BLUETOOTH_OFF) {
//                 return Center(
//                   child: _buildBluetoothDisabled(),
//                 );
//               } else {
//                 // This "_firstBuild" business is here because you can't get a "context" in the
//                 // initState function -- where the following "forEach" should be called.
//                 // TODO: Is there a better way to do this?
//                 if (_firstBuild) {
//                   HardwareHeadsetService().onHeadsetEvent.forEach((event) {
//                     if (event == HardwareHeadsetEvent.connectionFailed) {
//                       showDialog<void>(
//                         context: context,
//                         builder: (BuildContext context) {
//                           return AlertDialog(
//                             title: Text('Oops!'),
//                             content: SingleChildScrollView(
//                               child: ListBody(
//                                 children: <Widget>[
//                                   Text('Unable to connect to headset.'),
//                                   Text(''),
//                                   Text(
//                                       "Ensure the headset is turned on and the headset's green indicator light is flashing three times in a row."),
//                                 ],
//                               ),
//                             ),
//                             actions: <Widget>[
//                               FlatButton(
//                                 child: Text('OK'),
//                                 onPressed: () {
//                                   Navigator.of(context).pop();
//                                 },
//                               ),
//                             ],
//                           );
//                         },
//                       );
//                     } else {
//                       IconData icon = Icons.check;
//                       if (event == HardwareHeadsetEvent.testing ||
//                           event == HardwareHeadsetEvent.connecting) {
//                         icon = Icons.access_time;
//                       } else if (event ==
//                               HardwareHeadsetEvent.connectionFailed ||
//                           event == HardwareHeadsetEvent.testFailed ||
//                           event == HardwareHeadsetEvent.disconnected) {
//                         icon = Icons.error_outline;
//                       }
//                       Scaffold.of(context).showSnackBar(
//                         SnackBar(
//                           //duration: Duration(seconds: 2,),
//                           content: Row(
//                             mainAxisSize: MainAxisSize.min,
//                             children: <Widget>[
//                               Icon(icon),
//                               Container(
//                                 height: 16.0,
//                                 width: 16.0,
//                               ),
//                               Text(event.message),
//                             ],
//                           ),
//                           behavior: SnackBarBehavior.floating,
//                         ),
//                       );
//                     }
//                   });

//                   _firstBuild = false;
//                 }
//                 return Center(
//                   child: AnimatedSwitcher(
//                     duration: Duration(milliseconds: 200),
//                     switchInCurve: Curves.easeInOut,
//                     child: _currentWidget,
//                   ),
//                 );
//               }
//             } else if (snapshot.hasError) {
//               return _buildFutureError();
//             }
//             return Center(child: CircularProgressIndicator());
//           }),
//       bottomNavigationBar: BottomNavigationBar(
//           backgroundColor: BottomAppBarTheme.of(context).color,
//           selectedItemColor: Theme.of(context).accentColor,
//           unselectedItemColor: optiosAdditionalMediumGrey,
//           unselectedFontSize: 16,
//           selectedFontSize: 18,
//           showUnselectedLabels: true,
//           currentIndex: _selectedIndex,
//           onTap: onTabTapped,
//           items: [
//             for (var item in _tabs)
//               BottomNavigationBarItem(
//                 backgroundColor: const Color(0xff041116),
//                 icon: Icon(item.icon),
//                 title: Text(item.label),
//               )
//           ]),
//     );
//   }
// }

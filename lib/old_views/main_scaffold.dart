// import 'dart:math' as math;
// import 'package:brain_trainer/routes/route_name.dart';
// import 'package:brain_trainer/views/presentation/optios_icons_icons.dart';
// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:package_info/package_info.dart';
// import 'package:provider/provider.dart';
// import 'package:brain_trainer/config/common_style.dart';
// import 'package:brain_trainer/services/headset/hardware_headset_service.dart';
// import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
// import 'package:brain_trainer/models/auth_user.dart';
// import 'package:brain_trainer/config/common_functions.dart';

// import '../config/constants.dart';
// import '../helper/UIHelper.dart';

// class MainScaffold extends StatelessWidget {
//   const MainScaffold(
//       {Key key,
//       @required this.title,
//       @required this.body,
//       this.bottomNavigationBar})
//       : super(key: key);

//   final String title;
//   final Widget body;
//   final BottomNavigationBar bottomNavigationBar;

//   final List<Color> _gradientColors = const [
//     const Color(0xFF05313B),
//     const Color(0xff041116)
//   ];
//   final List<double> _gradientStops = const [0.0, 0.8];

//   @override
//   Widget build(BuildContext context) {
//     var _user = Provider.of<AuthUser>(context, listen: false);

//     return Scaffold(
//       appBar: MainAppBar(
//         title: title,
//         username: _user?.displayName ?? '...',
//       ),
//       backgroundColor: optiosCorporateWhite,
//       body: Container(
//         child: body,
//       ),
//       endDrawer: Drawer(
//         child: Container(
//           //padding: EdgeInsets.zero,
//           child: Column(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: <Widget>[
//                 Expanded(
//                   child: ListView(
//                     padding: EdgeInsets.zero,
//                     children: <Widget>[
//                       MainDrawerHeader(
//                         decoration: BoxDecoration(
//                           color: optiosCorporateDarkBlue,
//                           gradient: LinearGradient(
//                             colors: _gradientColors,
//                             stops: _gradientStops,
//                             transform: GradientRotation(0.5),
//                           ),
//                         ),
//                         child: Text(
//                           _user?.displayName ?? '...',
//                           style: Theme.of(context)
//                               .textTheme
//                               .headline5
//                               .copyWith(color: Colors.white),
//                         ),
//                       ),
//                       Container(
//                         //color: Colors.white,
//                         child: ListTile(
//                           title: Text('Log Out'),
//                           trailing: Icon(Icons.exit_to_app),
//                           onTap: () async {
//                             FirebaseAuthService _auth =
//                                 Provider.of<FirebaseAuthService>(context,
//                                     listen: false);
//                             // TODO - check that any data in the database streams up - it should
//                             await _auth.signOut();
//                             Navigator.of(context).pushNamedAndRemoveUntil(
//                                 Routes.LOGIN, (route) => route.isFirst);
//                           },
//                         ),
//                       ),

//                       // settings to be added back later
// //                  Container(
// //                    //color: Colors.white,
// //                    child: ListTile(
// //                      title: Text('Settings'),
// //                      trailing: Icon(Icons.settings),
// //                      onTap: () {},
// //                    ),
// //                  ),
//                     ],
//                   ),
//                 ),
//                 Container(
//                   padding: EdgeInsets.only(
//                     bottom: 16.0,
//                     top: 16.0,
//                   ),
//                   width: double.infinity,
//                   decoration: BoxDecoration(
//                     color: Colors.black12,
//                   ),
//                   child: FutureBuilder(
//                       future: PackageInfo.fromPlatform(),
//                       builder: (context, snapshot) {
//                         if (snapshot.hasData) {
//                           return Text(
//                             'Version ${snapshot.data.version} (${snapshot.data.buildNumber})',
//                             textAlign: TextAlign.center,
//                           );
//                         } else {
//                           return Text('No Version Info');
//                         }
//                       }),
//                 )
//               ]),
//         ),
//       ),
//       bottomNavigationBar: bottomNavigationBar,
//     );
//   }
// }

// class MainDrawerHeader extends StatelessWidget {
//   const MainDrawerHeader(
//       {Key key,
//       this.decoration,
//       this.padding = const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
//       this.margin = const EdgeInsets.only(bottom: 8.0),
//       this.curve = Curves.fastOutSlowIn,
//       this.duration = const Duration(milliseconds: 250),
//       @required this.child})
//       : super(key: key);

//   final Widget child;
//   final Decoration decoration;
//   final EdgeInsetsGeometry padding;
//   final EdgeInsetsGeometry margin;
//   final Curve curve;
//   final Duration duration;

//   final double _kDrawerHeaderHeight = 160.0 + 1.0;

//   @override
//   Widget build(BuildContext context) {
//     assert(debugCheckHasMaterial(context));
//     assert(debugCheckHasMediaQuery(context));
//     final ThemeData theme = Theme.of(context);
//     final double statusBarHeight = MediaQuery.of(context).padding.top;
//     return Container(
//       height: statusBarHeight + _kDrawerHeaderHeight,
//       //height: _kDrawerHeaderHeight,
//       margin: margin,
//       decoration: BoxDecoration(
//         border: Border(
//           bottom: Divider.createBorderSide(context),
//         ),
//       ),
//       child: AnimatedContainer(
//         padding: padding.add(EdgeInsets.only(top: statusBarHeight)),
//         decoration: decoration,
//         duration: duration,
//         curve: curve,
//         child: child == null
//             ? null
//             : DefaultTextStyle(
//                 style: theme.textTheme.bodyText1,
//                 child: MediaQuery.removePadding(
//                   context: context,
//                   removeTop: true,
//                   child: child,
//                 ),
//               ),
//       ),
//     );
//   }
// }

// class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
//   static const appBarTopHeight = 78.0;
//   static const appBarBottomHeight = 64.0;

//   // TODO: Pull colors from theme
//   final List<Color> _gradientColors = const [
//     const Color(0xFF05313B),
//     const Color(0xff041116),
//     const Color(0xFF05313B)
//   ];
//   final List<double> _gradientStops = const [0.2, 0.5, 1.0];

//   final String title;
//   final String username;

//   MainAppBar({@required this.title, @required this.username});

//   @override
//   Size get preferredSize =>
//       Size.fromHeight(appBarTopHeight + appBarBottomHeight);

//   @override
//   Widget build(BuildContext context) {
//     const double horizontalPadding = 22.0;
//     return Container(
//       height: preferredSize.height + MediaQuery.of(context).viewPadding.top,
//       child: Column(
//         children: <Widget>[
//           Material(
//             elevation: 8.0,
//             child: Container(
//                 height:
//                     appBarTopHeight + MediaQuery.of(context).viewPadding.top,
//                 padding: EdgeInsets.symmetric(
//                     vertical: 18.0, horizontal: horizontalPadding),
//                 decoration: BoxDecoration(
//                   gradient: LinearGradient(
//                     colors: _gradientColors,
//                     stops: _gradientStops,
//                     transform: GradientRotation(0.5),
//                   ),
//                 ),
//                 child: Column(
//                   children: <Widget>[
//                     Container(
//                       height: MediaQuery.of(context).viewPadding.top,
//                     ),
//                     Expanded(
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Container(
//                             child: Image(
//                               image: AssetImage(
//                                   'assets/images/logo_h_color_reversed.png'),
//                             ),
//                           ),
//                           Container(
//                             child: FlatButton(
//                               padding: EdgeInsets.symmetric(
//                                 horizontal: 4.0,
//                               ),
//                               onPressed: () {
//                                 Scaffold.of(context).openEndDrawer();
//                               },
//                               child: Row(children: <Widget>[
//                                 Text(
//                                   username,
//                                   style: TextStyle(
//                                       color: Colors.white, fontSize: 16.0),
//                                 ),
//                                 Container(
//                                   width: 6.0,
//                                 ),
//                                 Icon(
//                                   Icons.account_circle,
//                                   color: Colors.white,
//                                 ),
//                               ]),
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ],
//                 )),
//           ),
//           Container(
//               height: appBarBottomHeight,
//               //padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
//               padding: EdgeInsets.only(left: horizontalPadding),
//               decoration: BoxDecoration(
//                 border: Border(
//                   bottom: BorderSide(
//                       width: 1.0, color: optiosMaterialLightBlue[700]),
//                 ),
//               ),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: <Widget>[
//                   Text(
//                     title,
//                     style: Theme.of(context)
//                         .textTheme
//                         .headline5
//                         .copyWith(color: optiosMaterialLightBlue[700]),
//                   ),
//                   Container(
//                     //color: Colors.black12,
//                     height: double.infinity,
//                     padding: EdgeInsets.only(right: 12.0),
//                     child: Row(children: <Widget>[
//                       VerticalDivider(
//                         width: 16.0,
//                         color: optiosAdditionalLightGrey,
//                       ),
//                       HeadsetStatusIndicator(),

// // TODO: DISABLED BLAST MOTION CONFIG
// //                      VerticalDivider(width: 16.0, color: optiosAdditionalLightGrey,),
// //                      BlastStatusIndicator(),
// //                      LayoutBuilder(builder: (context, constraint) {
// //                        return Icon(MyFlutterApp.BlastMotion, color: Colors.green, size: constraint.biggest.height,);
// //                      },),
//                     ]),
//                   ),
//                 ],
//               )),
//         ],
//       ),
//     );
//   }
// }

// class HeadsetStatusIndicator extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Consumer<HardwareHeadsetService>(builder: (context, service, child) {
//       dynamic batteryPercentageStream =
//           Provider.of<HardwareHeadsetService>(context)
//               ?.headset
//               ?.onBatteryPercentageChanged;
//       if (service.headset != null) {
//         return StreamBuilder(
//             stream: service.onImpedanceMeasurementLevelChanged,
//             builder: (context, snapshot) {
//               if (snapshot.hasError) {
//                 return Icon(Icons.error_outline);
//               } else {
//                 switch (snapshot.connectionState) {
//                   case ConnectionState.none:
//                     return Icon(Icons.more_horiz);
//                   case ConnectionState.waiting:
//                     return DeviceStatusIndicator(
//                       name: 'headset',
//                       icon: OptiosIcons.headset_connection,
//                       batteryPercentageStream: batteryPercentageStream,
//                       impedanceMeasurementLevel:
//                           ImpedanceMeasurementLevel.unknown,
//                     );
//                   case ConnectionState.active:
//                     return DeviceStatusIndicator(
//                       name: 'headset',
//                       icon: OptiosIcons.headset_connection,
//                       batteryPercentageStream: batteryPercentageStream,
//                       impedanceMeasurementLevel: snapshot.data,
//                     );
//                   case ConnectionState.done:
//                     return Icon(Icons.error_outline);
//                   default:
//                     return Icon(Icons.error_outline);
//                 }
//               }
//             });
//       } else {
//         return DeviceStatusIndicator(
//           name: 'headset',
//           icon: OptiosIcons.headset_connection,
//           batteryPercentageStream: batteryPercentageStream,
//           impedanceMeasurementLevel: ImpedanceMeasurementLevel.unknown,
//         );
//       }
//     });
//   }
// }

// //class BlastStatusIndicator extends StatelessWidget {
// //  @override
// //  Widget build(BuildContext context) {
// //    return DeviceStatusIndicator(
// //      name: 'blast motion',
// //      icon: OptiosIcons.blast_motion_connection,
// //      battery: -1.0,
// //      impedanceMeasurementLevel: ImpedanceMeasurementLevel.unknown,
// //    );
// //  }
// //}

// class DeviceStatusIndicator extends StatelessWidget {
//   const DeviceStatusIndicator(
//       {Key key,
//       this.name,
//       this.icon,
//       this.batteryPercentageStream,
//       this.impedanceMeasurementLevel = ImpedanceMeasurementLevel.unknown})
//       : super(key: key);

//   final String name;
//   final IconData icon;
//   final Stream<int> batteryPercentageStream;
//   //final double battery;
//   final ImpedanceMeasurementLevel impedanceMeasurementLevel;

//   Icon _getBatterIconData(int battery) {
//     if (battery >= 90)
//       return Icon(
//         OptiosIcons.battery_full,
//         color: Colors.green,
//       );
//     else if (battery >= 75)
//       return Icon(
//         OptiosIcons.battery_75,
//         color: Colors.green,
//       );
//     else if (battery >= 50)
//       return Icon(
//         OptiosIcons.battery_50,
//         color: Colors.green,
//       );
//     else if (battery >= 25)
//       return Icon(
//         OptiosIcons.battery_25,
//         color: Colors.green,
//       );
//     else if (battery >= 10)
//       return Icon(
//         OptiosIcons.battery_05,
//         color: Colors.orange,
//       );
//     else if (battery >= 0)
//       return Icon(
//         OptiosIcons.battery_dead,
//         color: Colors.red,
//       );
//     else
//       return Icon(Icons.battery_unknown, color: Colors.grey);
//   }

//   Widget _buildBatteryIndicator(int batteryPercentage) {
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//       children: <Widget>[
//         _getBatterIconData(batteryPercentage),
//         Text(
//           '${NumberFormat("#", "en_US").format(math.min(math.max(batteryPercentage, 0), 100))}%',
//           style: TextStyle(
//             fontSize: 10.0,
//           ),
//         ),
//       ],
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: <Widget>[
//         Expanded(
//           child: Container(
//             padding: EdgeInsets.symmetric(
//               vertical: 4.0,
//               horizontal: 4.0,
//             ),
//             //color: Colors.black12,
//             child: Row(
//               children: <Widget>[
//                 LayoutBuilder(
//                   builder: (context, constraint) {
//                     var color;
//                     switch (impedanceMeasurementLevel) {
//                       case ImpedanceMeasurementLevel.good:
//                         color = Colors.green;
//                         break;
//                       case ImpedanceMeasurementLevel.mediocre:
//                         color = Colors.orange;
//                         break;
//                       case ImpedanceMeasurementLevel.bad:
//                         color = Colors.red;
//                         break;
//                       default:
//                         color = Colors.grey;
//                         break;
//                     }
//                     return Icon(
//                       icon,
//                       color: color,
//                       size: constraint.biggest.height,
//                     );
//                   },
//                 ),
//                 Container(
//                   width: 8.0,
//                 ),
//                 StreamBuilder(
//                     stream: batteryPercentageStream,
//                     builder: (context, snapshot) {
//                       if (snapshot.hasError) {
//                         return Icon(Icons.error_outline);
//                       } else {
//                         switch (snapshot.connectionState) {
//                           case ConnectionState.none:
//                             return _buildBatteryIndicator(-1);
//                           case ConnectionState.waiting:
//                             return _buildBatteryIndicator(-1);
//                           case ConnectionState.active:
//                             return _buildBatteryIndicator(snapshot.data);
//                           case ConnectionState.done:
//                             return Icon(Icons.error_outline);
//                           default:
//                             return Icon(Icons.error_outline);
//                         }
//                       }
//                     }),
// //                Column(
// //                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
// //                  children: <Widget>[
// //                    _getBatterIconData(battery),
// //                    Text(
// //                      '${NumberFormat("###%", "en_US").format(math.min(math.max(battery, 0.0), 1.0))}',
// //                      style: TextStyle(
// //                        fontSize: 10.0,
// //                      ),
// //                    ),
// //                  ],
// //                ),
//               ],
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//             ),
//           ),
//         ),
//         Container(
//           height: 2.0,
//         ),
//         Container(
//           padding: EdgeInsets.zero,
//           //color: Colors.black38,
//           child: Text(
//             name.toUpperCase(),
//             style: TextStyle(
//               fontSize: 10.0,
//             ),
//           ),
//         ),
//       ],
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//     );
//   }
// }

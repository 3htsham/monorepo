// import 'package:brain_trainer/elements/others/box_overlay_panel.dart';
// import 'package:brain_trainer/config/common_style.dart';
// import 'package:brain_trainer/services/headset/hardware_headset_service.dart';
// import 'package:flutter/material.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:package_info/package_info.dart';

// class MoreTab extends StatefulWidget {
//   static const tabName = 'Support';
//   static const tabTitle = 'Support';

//   @override
//   State<StatefulWidget> createState() => _MoreTabState();
// }

// class _MoreTabState extends State<MoreTab> {
//   @override
//   Widget build(BuildContext context) {
//     var diagnosticTextStyle = Theme.of(context).textTheme.bodyText1;
//     return BoxOverlayPanel(
//       boxContents: Column(
//         mainAxisSize: MainAxisSize.min,
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           Text('Having an issue?',
//               style: Theme.of(context).textTheme.headline4),
//           Text('Call Natasha!', style: Theme.of(context).textTheme.headline4),
//           Text('(619) 273-3361',
//               style: Theme.of(context)
//                   .textTheme
//                   .headline4
//                   .copyWith(color: optiosAdditionalDarkGrey)),
//           Text('or', style: Theme.of(context).textTheme.headline4),
//           RaisedButton(
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(20.0),
//               ),
//               onPressed: () async {
//                 var packageInfo = await PackageInfo.fromPlatform();

//                 final uri =
//                     'mailto:support@optios.com?subject=Support Request&body=${packageInfo.appName} v${packageInfo.version} (${packageInfo.buildNumber})<br>Headset: ${(HardwareHeadsetService().headset == null) ? 'HEADSET NOT CONNECTED' : '${HardwareHeadsetService().headset.name}'}<br>Battery: ${(HardwareHeadsetService().headset == null) ? '---' : '${HardwareHeadsetService().headset.lastBatteryPercentage}%'}<br>${HardwareHeadsetService().lastImpedanceMeasurementToString()}<br><br>Please describe your problem: ';
//                 var encoded = Uri.encodeFull(uri);
//                 if (await canLaunch(encoded)) {
//                   await launch(encoded);
//                 } else {
//                   throw 'Could not launch $uri';
//                 }
//               },
//               child: Text('Email Support'.toUpperCase(),
//                   style: Theme.of(context)
//                       .textTheme
//                       .subtitle1
//                       .copyWith(color: Colors.white))),
//           Divider(
//             height: 32.0,
//           ),
//           Text('Diagnostic Information:',
//               style: Theme.of(context).textTheme.headline6),
//           FutureBuilder(
//               future: PackageInfo.fromPlatform(),
//               builder: (context, snapshot) {
//                 if (snapshot.hasData) {
//                   return Text(
//                     '${snapshot.data.appName} v${snapshot.data.version} (${snapshot.data.buildNumber})',
//                     textAlign: TextAlign.center,
//                     style: diagnosticTextStyle,
//                   );
//                 } else {
//                   return Text('No Version Info');
//                 }
//               }),
//           Text(
//             'Headset: ${(HardwareHeadsetService().headset == null) ? 'NOT CONNECTED' : '${HardwareHeadsetService().headset.name}'}',
//             textAlign: TextAlign.center,
//             style: diagnosticTextStyle,
//           ),
//           Text(
//             'Battery: ${(HardwareHeadsetService().headset == null) ? '---' : '${HardwareHeadsetService().headset.lastBatteryPercentage}%'}',
//             textAlign: TextAlign.center,
//             style: diagnosticTextStyle,
//           ),
//           Text(
//             '${HardwareHeadsetService().lastImpedanceMeasurementToString()}',
//             textAlign: TextAlign.center,
//             style: diagnosticTextStyle,
//           ),
//         ],
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;
    
    return Container(
      width: size.width,
      height: size.height,
      color: theme.scaffoldBackgroundColor,
      child: Center(
        // child: Image.asset(""),
      ),
    );
  }
}

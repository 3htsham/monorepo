// import 'package:brain_trainer/config/common_style.dart';
// import 'package:brain_trainer/old_views/pre_putt_training/pre_putt_training.dart';
// import 'package:brain_trainer/services/headset/hardware_headset_service.dart';
// import 'package:brain_trainer/models/training_session.dart';
// import 'package:brain_trainer/services/training/training_service.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
// import 'package:m4/m4.dart';
// import 'package:provider/provider.dart';

// class TrainingTab extends StatefulWidget {
//   static const tabName = 'Training';
//   static const tabTitle = 'Training';

//   @override
//   State<StatefulWidget> createState() => _TrainingTabState();
// }

// class _TrainingTabState extends State<TrainingTab> {
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();

//     print(HardwareHeadsetService().state.toString());
//     M4DeviceState _state = HardwareHeadsetService().state;
//     if (_state != M4DeviceState.opened && _state != M4DeviceState.acquiring) {
//       print('Headset not ready.');
//     } else {
//       context.read<TrainingService>().startAcquisition();
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Consumer<TrainingService>(
//         builder: (context, service, child) {
//           switch (service.lastTrainingState) {
//             case TrainingGameState.error:
//               if (context.watch<TrainingService>().isHeadsetAvailable &&
//                   !context.watch<TrainingService>().isHeadsetCalibrated) {
//                 return Center(
//                     child: Container(
//                   padding: EdgeInsets.symmetric(
//                     horizontal: 64.0,
//                   ),
//                   child: Text(
//                     "Please Test Headset Connection Before Training",
//                     style: Theme.of(context).textTheme.headline4,
//                     textAlign: TextAlign.center,
//                   ),
//                 ));
//               } else {
//                 return Center(
//                     child: Container(
//                   padding: EdgeInsets.symmetric(
//                     horizontal: 64.0,
//                   ),
//                   child: Text(
//                     "Please Configure Headset For Training",
//                     style: Theme.of(context).textTheme.headline4,
//                     textAlign: TextAlign.center,
//                   ),
//                 ));
//               }
//               break;
//             case TrainingGameState.choosing:
//               return buildTrainingChoiceWidget(context);
//               break;
//             case TrainingGameState.playingGraphGame:
//               return Container();
// //              return LayoutBuilder(builder: (context, constraints) =>
// //                GraphFeedbackGame(constraints.biggest, M4().onSampleEvent(), endTraining).widget);
//               break;
//             case TrainingGameState.playingSpaceGame:
//               return Container();
// //              return LayoutBuilder(builder: (context, constraints) =>
// //                SpaceGame(constraints.biggest, M4().onSampleEvent(), endTraining).widget);
//               break;
//             case TrainingGameState.playingPreShotRoutine:
//               return new PPRScreen(endTraining);
//               break;
//             case TrainingGameState.reporting:
//               return buildTrainingReport();
//               break;
//             default:
//               return Text("State: ${service.lastTrainingState.toString()}");
//               break;
//           }
//         },
//       ),
//     );
//   }

//   Widget buildTrainingReport() {
//     print('Build Training Report');

//     double average = context.watch<TrainingService>().sessionAverageZScore * 100.0;
//     int score = average.round();
//     Duration dur = context.watch<TrainingService>().sessionTime ?? Duration(seconds: 0);
//     Duration zoneDur =
//         context.watch<TrainingService>().sessionTimeInZone ?? Duration(seconds: 0);

//     double percentInZone = (dur.inMilliseconds != 0)
//         ? zoneDur.inMilliseconds / dur.inMilliseconds
//         : 0.0;

//     const newTrainingNames = {
//       'Space Game': 'Universe Master',
//       'Graph': 'Green Seeker',
//       'PPR': 'Pre Shot Routine',
//     };

//     String _trainingTimeString;
//     if (dur.inMinutes < 10) {
//       _trainingTimeString = dur.toString().substring(3, 7);
//     } else {
//       _trainingTimeString = dur.toString().substring(2, 7);
//     }

//     String _timeInZoneString;
//     if (zoneDur.inMinutes < 10) {
//       _timeInZoneString = zoneDur.toString().substring(3, 7);
//     } else {
//       _timeInZoneString = zoneDur.toString().substring(2, 7);
//     }

//     return Container(
//       padding: EdgeInsets.all(32.0),
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           Expanded(
//             flex: 3,
//             child: Column(
//               children: <Widget>[
//                 Text(
//                   newTrainingNames[
//                           context.watch<TrainingService>()?.inProgressSession?.trainingType] ??
//                       'No Training',
//                   style: Theme.of(context).textTheme.headline2,
//                 ),
//                 Text("Session Results",
//                     style: Theme.of(context).textTheme.headline3),
//               ],
//             ),
//           ),
//           Expanded(
//             flex: 4,
//             child: Column(
//               children: <Widget>[
//                 Divider(
//                   color: optiosAdditionalLightGrey.withOpacity(0.8),
//                   height: 20,
//                   thickness: 1,
//                   indent: 20,
//                   endIndent: 20,
//                 ),
//                 Text('Training Time: $_trainingTimeString minutes',
//                     style: Theme.of(context).textTheme.headline5),
//                 Text('Difficulty Level: ${context.watch<TrainingService>().zScoreThreshold}',
//                     style: Theme.of(context).textTheme.headline5),
//                 Text(
//                     'Time in Zone: $_timeInZoneString minutes (${(percentInZone * 100.0).round()}%)',
//                     style: Theme.of(context).textTheme.headline5),
//                 Text('Average Score: ' + score.toString(),
//                     style: Theme.of(context).textTheme.headline5),

//                 //Text(context.watch<TrainingService>().inProgressSession.startTime.toString()),
//                 //Text(context.watch<TrainingService>().inProgressSession.endTime.toString()),

//                 Divider(
//                   color: optiosAdditionalLightGrey.withOpacity(0.8),
//                   height: 20,
//                   thickness: 1,
//                   indent: 20,
//                   endIndent: 20,
//                 ),
//               ],
//             ),
//           ),
//           Expanded(
//             flex: 2,
//             child: Text(
//               'Great Job! You are one step closer to mastering the mental game of golf!',
//               style: Theme.of(context).textTheme.headline5,
//               textAlign: TextAlign.center,
//             ),
//           ),
//           Builder(builder: (context) {
//             List<Widget> kids = [];

//             // add the Play Again if needed
//             if (context.watch<TrainingService>().canPlayAgain) {
//               kids.add(Expanded(
//                 flex: 4,
//                 child: RaisedButton(
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(20.0),
//                   ),
//                   child: Text(
//                     "Play Again",
//                     style: TextStyle(color: Colors.white, fontSize: 24),
//                   ),
//                   onPressed: () => this.setState(() {
//                     if (context.watch<TrainingService>().inProgressSession.trainingType ==
//                         TrainingSession.trainingTypesStrings[
//                             TrainingType.trainingType_Graph]) {
//                       startGraph();
//                     } else {
//                       startSpace();
//                     }
//                   }),
//                 ),
//               ));
//               kids.add(Spacer(flex: 1));
//             }
//             // add the 'all done'
//             kids.add(
//               Expanded(
//                 flex: 4,
//                 child: RaisedButton(
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(20.0),
//                   ),
//                   child: Text(
//                     "All Done",
//                     style: TextStyle(color: Colors.white, fontSize: 24),
//                   ),
//                   onPressed: () => this.setState(() {
//                     context.read<TrainingService>().chooseTraining();
//                     //feedbackState = TrainingGameState.choose;
//                   }),
//                 ),
//               ),
//             );

//             Row row = Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: kids,
//             );

//             return row;
//           }),
//         ],
//       ),
//     );
//   }

//   void startSpace() {
//     context.read<TrainingService>().playSpaceGame();
//   }

//   void startGraph() {
//     context.read<TrainingService>().playGraphGame();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     context.read<TrainingService>().stopAcquisition();
//   }

//   Widget buildTrainingChoiceWidget(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Column(
//         children: [
//           Expanded(
//             flex: 1,
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: [
//                 Expanded(
//                   flex: 1,
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       Text(
//                         'Hardware Area',
//                         style: Theme.of(context).textTheme.headline6,
//                       ),
//                     ],
//                   ),
//                 ),
//                 VerticalDivider(
//                   thickness: 1.0,
//                   color: Colors.black12,
//                 ),
//                 Expanded(
//                   flex: 1,
//                   child: Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: Column(
//                       children: <Widget>[
//                         Column(
//                             crossAxisAlignment: CrossAxisAlignment.stretch,
//                             children: [
//                               Container(
//                                 color:
//                                     optiosAdditionalLightGrey.withOpacity(0.5),
//                                 child: Padding(
//                                   padding: const EdgeInsets.symmetric(
//                                       horizontal: 12, vertical: 7),
//                                   child: Text("Green Seeker",
//                                       style: TextStyle(
//                                           fontSize: 18,
//                                           color: optiosCorporateBlack)),
//                                 ),
//                               ),
//                             ]),
//                         Expanded(
//                           child: Center(
//                             child: Text(
//                               'Your goal here is to stay in the green zone (above the dividing line) for as long as possible. Above the dividing line means you are in the zone (green) and below the line means you are out of the zone (red).',
//                               style: Theme.of(context).textTheme.subtitle1,
//                             ),
//                           ),
//                         ),
//                         RaisedButton(
//                             shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.circular(20.0),
//                             ),
//                             child: Text(
//                               "TRAIN NOW",
//                               style: TextStyle(color: Colors.white),
//                             ),
//                             onPressed: () {
//                               context.read<TrainingService>().playGraphGame();
//                               setState(() {});
//                             }),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Divider(
//             thickness: 1.0,
//             indent: 10,
//             endIndent: 10,
//             color: Colors.black12,
//           ),
//           Expanded(
//             flex: 1,
//             child: Row(
//               children: [
//                 Expanded(
//                   flex: 1,
//                   child: Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: <Widget>[
//                         Column(
//                             crossAxisAlignment: CrossAxisAlignment.stretch,
//                             children: [
//                               Container(
//                                 color:
//                                     optiosAdditionalLightGrey.withOpacity(0.5),
//                                 child: Padding(
//                                   padding: const EdgeInsets.symmetric(
//                                       horizontal: 12, vertical: 7),
//                                   child: Text("Universe Master",
//                                       style: TextStyle(
//                                           fontSize: 18,
//                                           color: optiosCorporateBlack)),
//                                 ),
//                               ),
//                             ]),
//                         Expanded(
//                           child: Center(
//                             child: Text(
//                               'Your goal here is to keep your spaceship as close to the line as possible. The closer you are, the more you are in the zone! As you pass through the circles, we will give you feedback on your performance.',
//                               style: Theme.of(context).textTheme.subtitle1,
//                             ),
//                           ),
//                         ),
//                         RaisedButton(
//                             shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.circular(20.0),
//                             ),
//                             child: Text(
//                               "TRAIN NOW",
//                               style: TextStyle(
//                                 color: Colors.white,
//                               ),
//                             ),
//                             onPressed: () {
//                               context.read<TrainingService>().playSpaceGame();
//                               setState(() {});
//                             }),
//                       ],
//                     ),
//                   ),
//                 ),
//                 VerticalDivider(
//                   thickness: 1.0,
//                   color: Colors.black12,
//                 ),
//                 Expanded(
//                   flex: 1,
//                   child: Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: Column(
//                       children: <Widget>[
//                         Column(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             crossAxisAlignment: CrossAxisAlignment.stretch,
//                             children: [
//                               Container(
//                                 color:
//                                     optiosAdditionalLightGrey.withOpacity(0.5),
//                                 child: Padding(
//                                   padding: const EdgeInsets.symmetric(
//                                       horizontal: 12, vertical: 7),
//                                   child: Text("Pre Shot Routine",
//                                       style: TextStyle(
//                                           fontSize: 18,
//                                           color: optiosCorporateBlack)),
//                                 ),
//                               ),
//                             ]),
//                         Expanded(
//                           child: Center(
//                             child: Text(
//                               'This module trains your brain to associate the zone brain state with the physical movements of your pre-shot routine. Repetition of these activities in combination will teach your brain to AUTOMATICALLY enter the zone whenever you do your pre-shot routine.',
//                               style: Theme.of(context).textTheme.subtitle1,
//                             ),
//                           ),
//                         ),
//                         RaisedButton(
//                             shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.circular(20.0),
//                             ),
//                             child: Text(
//                               "TRAIN NOW",
//                               style: TextStyle(
//                                 color: Colors.white,
//                               ),
//                             ),
//                             onPressed: () {
//                               context.read<TrainingService>().playPreShotRoutine();
//                               setState(() {});
//                             }),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   void endTraining() {
//     context.read<TrainingService>().gameDone();
//   }
// }

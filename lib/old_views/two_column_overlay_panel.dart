// import 'dart:math' as math;
// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:brain_trainer/config/common_style.dart';

// class TwoColumnOverlayPanel extends StatefulWidget {
//   TwoColumnOverlayPanel({
//     Key key,
//     @required this.leftOverlay,
//     @required this.rightOverlay,
//     gradientColors,
//     gradientStops,
//   })  : this.gradientColors = gradientColors ??
//             [optiosMaterialLightBlue[900], optiosMaterialDarkBlue[700]],
//         this.gradientStops = gradientStops ?? const [0.3, 1.0],
//         super(key: key);

//   final leftOverlay;
//   final rightOverlay;
//   final gradientColors;
//   final gradientStops;

//   @override
//   State<StatefulWidget> createState() => _TwoColumnOverlayPanelState();
// }

// class _TwoColumnOverlayPanelState extends State<TwoColumnOverlayPanel> {
//   final _gradientRadius = 2.0;
//   Timer _gradientAlignmentStepTimer;
//   Alignment _gradientAlignment;

//   @override
//   void initState() {
//     super.initState();

//     _gradientAlignment = Alignment(1.0, -1.5);
//     _gradientAlignmentStepTimer =
//         Timer.periodic(Duration(seconds: 12), (timer) {
//       setState(() {
//         _gradientAlignment = _generateRandomAlignment(1.5, 2.5);
//       });
//     });
//   }

//   @override
//   void dispose() {
//     _gradientAlignmentStepTimer.cancel();

//     super.dispose();
//   }

//   Alignment _generateRandomAlignment(xScale, yScale) {
//     return Alignment(
//       math.cos(math.Random().nextDouble() * 2.0 * math.pi) * xScale,
//       math.sin(math.Random().nextDouble() * 2.0 * math.pi) * yScale,
//     );
//   }

//   Widget _buildBoxOverlays() {
//     return Positioned.fill(
//       child: Row(
//         children: <Widget>[
//           Expanded(
//               child: Container(
//             margin: EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
//             child: widget.leftOverlay,
//           )),
//           Expanded(
//             child: Container(
//               margin: EdgeInsets.fromLTRB(8.0, 16.0, 16.0, 16.0),
//               //padding: EdgeInsets.all(16.0),
//               decoration: BoxDecoration(
//                   color: Colors.white.withOpacity(0.9),
//                   border:
//                       Border.all(color: optiosAdditionalMediumGrey, width: 2.0),
//                   boxShadow: [
//                     BoxShadow(
//                       color: Colors.black.withOpacity(0.2),
//                       spreadRadius: 8,
//                       blurRadius: 7,
//                       offset: Offset(0, 0),
//                     )
//                   ]),
//               child: widget.rightOverlay,
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         height: MediaQuery.of(context).size.width / 3 * 2,
//         child: LayoutBuilder(builder: (context, constraints) {
//           return ClipRect(
//             child: Stack(children: <Widget>[
//               AnimatedContainer(
//                 duration: Duration(
//                   seconds: 10,
//                 ),
//                 curve: Curves.easeInOut,
//                 decoration: BoxDecoration(
//                   color: optiosCorporateDarkBlue,
//                   gradient: RadialGradient(
//                     center: _gradientAlignment,
//                     radius: _gradientRadius,
//                     colors: widget.gradientColors,
//                     stops: widget.gradientStops,
//                   ),
//                 ),
//                 //height: 332.0,
//               ),
//               _buildBoxOverlays(),
//             ]),
//           );
//         }));
//   }
// }

// class OverlayBox extends StatelessWidget {
//   const OverlayBox({Key key, this.title, this.titleColor, this.content})
//       : super(key: key);

//   final title;
//   final titleColor;
//   final content;

//   @override
//   Widget build(BuildContext context) {
//     Color titleTextColor = (titleColor != null)
//         ? titleColor.computeLuminance() > 0.5 ? Colors.black : Colors.white
//         : null;
//     return Container(
//       child: Column(
//         children: <Widget>[
//           Container(
//             padding: EdgeInsets.all(16.0),
//             color: titleColor,
//             child: Text(
//               title,
//               textAlign: TextAlign.center,
//               style: Theme.of(context)
//                   .textTheme
//                   .headline5
//                   .copyWith(color: titleTextColor),
//             ),
//             width: double.infinity,
//           ),
//           Divider(
//             height: 1.0,
//           ),
//           Expanded(
//             child: Container(
//               child: content,
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

import 'package:brain_trainer/models/route_argument.dart';
import 'package:brain_trainer/models/training_session.dart';
import 'package:brain_trainer/services/firebase/firestore_db_service.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:brain_trainer/views/forgot_password/forgot_password_page.dart';
import 'package:brain_trainer/views/forgot_password/forgot_password_provider.dart';
import 'package:brain_trainer/views/forgot_password/recover_password_page.dart';
import 'package:brain_trainer/views/login/login_page.dart';
import 'package:brain_trainer/views/login/login_provider.dart';
import 'package:brain_trainer/views/main_page/main_page.dart';
import 'package:brain_trainer/views/main_page/main_page_provider.dart';
import 'package:brain_trainer/views/main_tabs/hardware_tab/bluetooth_connection/devices_bluetooth_state_provider.dart';
import 'package:brain_trainer/views/main_tabs/hardware_tab/bluetooth_connection/devices_list_screen/bluetooth_devices_list.dart';
import 'package:brain_trainer/views/main_tabs/hardware_tab/bluetooth_connection/unauthorized/bluetooth_unauthorized.dart';
import 'package:brain_trainer/views/main_tabs/hardware_tab/hardware_tab_page.dart';
import 'package:brain_trainer/views/main_tabs/reports_tab/session_result/session_result.dart';
import 'package:brain_trainer/views/main_tabs/reports_tab/session_result/session_result_detail.dart';
import 'package:brain_trainer/views/main_tabs/reports_tab/session_result/session_result_provider.dart';
import 'package:brain_trainer/views/main_tabs/resources_tab/crafting_preshot_routine.dart';
import 'package:brain_trainer/views/main_tabs/resources_tab/entering_the_expert_state.dart';
import 'package:brain_trainer/views/main_tabs/resources_tab/resources_tab_page.dart';
import 'package:brain_trainer/views/main_tabs/resources_tab/setting_up_the_headgear.dart';
import 'package:brain_trainer/views/main_tabs/support_tab/support_tab_page.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/green_seeker/green_seeker_launchpage.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/green_seeker/green_seeker_train.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/pre_shot_routine/pre_shot_routine_launch.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/pre_shot_routine/pre_shot_routine_train.dart';

import 'package:brain_trainer/views/main_tabs/training_tab/training_tab_page.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/training_timer.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/universe_master/universe_master_launchpage.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/universe_master/universe_master_train.dart';
import 'package:brain_trainer/views/reset_password/reset_password_page.dart';
import 'package:brain_trainer/views/reset_password/reset_password_provider.dart';
import 'package:brain_trainer/views/signup/signup_page.dart';
import 'package:brain_trainer/views/signup/signup_provider.dart';
import 'package:brain_trainer/views/user_account/user_account_page.dart';
import 'package:brain_trainer/views/user_account/user_account_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../services/firebase/firebase_auth_service.dart';
import '../views/main_tabs/hardware_tab/setup_step_one/hardware_setup_step_one.dart';
import '../views/main_tabs/hardware_tab/setup_step_one/setup_step_one_provider.dart';
import '../views/main_tabs/hardware_tab/hardware_callibration/hardware_setup_calibration.dart';
import '../views/main_tabs/hardware_tab/hardware_callibration/hardware_calibration_provider.dart';
import '../views/main_tabs/hardware_tab/hardware_testing/hardware_setup_testing.dart';
import '../views/main_tabs/hardware_tab/hardware_testing/hardware_setup_testing_provider.dart';
import '../views/main_tabs/hardware_tab/connection_success/hardware_setup_connection_success.dart';
import '../views/main_tabs/hardware_tab/connection_success/hardware_connection_success_provider.dart';

class RouteGenerator {
  static Route<dynamic> mainRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.LOGIN:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider<LoginProvider>(
                  create: (ctx) => LoginProvider(
                      firebaseAuthService: ctx.read<FirebaseAuthService>()),
                  child: LoginPage(),
                ));
      case Routes.SIGNUP:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider<SignUpProvider>(
                  create: (ctx) => SignUpProvider(
                      firebaseAuthService: ctx.read<FirebaseAuthService>(),
                      firestoreDBService: ctx.read<FirestoreDBService>(),
                      trainingService: ctx.read<TrainingService>()),
                  child: SignUpPage(),
                ));
      case Routes.FORGOT_PASSWORD:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider<ForgotPasswordProvider>(
                  create: (ctx) => ForgotPasswordProvider(
                      firebaseAuthService: ctx.read<FirebaseAuthService>()),
                  child: ForgotPasswordPage(),
                ));
      case Routes.RECOVER_PASSWORD:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider<ForgotPasswordProvider>(
                  create: (ctx) => ForgotPasswordProvider(
                      firebaseAuthService: ctx.read<FirebaseAuthService>()),
                  child: RecoverPasswordPage(
                    userEmail: args as String,
                  ),
                ));
      case Routes.ACCOUNT_DRAWER:
        return CupertinoPageRoute(builder: (ctx) => UserAccountPage());
      case Routes.RESET_PASSWORD:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider<ResetPasswordProvider>(
                  create: (ctx) => ResetPasswordProvider(
                      firebaseAuthService: ctx.read<FirebaseAuthService>()),
                  child: ResetPasswordPage(),
                ));
      case Routes.MAIN:
        return CupertinoPageRoute(
            builder: (_) => ChangeNotifierProvider<MainPageProvider>(
                  create: (ctx) => MainPageProvider(
                      hardwareService: ctx.read<HardwareService>()),
                  child: MainPage(),
                ));
      case Routes.USER_ACCOUNT:
        return CupertinoPageRoute(
            builder: (context) => ChangeNotifierProvider<UserAccountProvider>(
                  create: (ctx) => UserAccountProvider(
                      firebaseAuthService: ctx.read<FirebaseAuthService>(),
                      firestoreDBService: ctx.read<FirestoreDBService>(),
                      trainingService: ctx.read<TrainingService>()),
                  child: UserAccountPage(),
                ));

      ///Games Routes
      case Routes.TRAINING_TIMER:
        return CupertinoPageRoute(
            builder: (ctx) => TrainingTimer(
                  arguments: args as RouteArgument,
                ));
      case Routes.GREEN_SEEKER_TRAIN:
        return CupertinoPageRoute(builder: (ctx) => GreenSeekerTrainPage());
      case Routes.PSR_TRAIN:
        return CupertinoPageRoute(builder: (ctx) => PreShotRoutineTrainPage());
      case Routes.UNIVERSE_TRAIN:
        return CupertinoPageRoute(builder: (ctx) => UniverseMasterTrainPage());
      //TODO: Add UNIVERSE_MASTER_TRAIN route as well
      case Routes.GAMESESSIONRESULT:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider(
                create: (BuildContext context) => SessionResultProvider(
                    firebaseAuthService: context.read<FirebaseAuthService>(),
                    db: context.read<FirestoreDBService>(),
                    trainingService: context.read<TrainingService>(),
                    gameType: (args as RouteArgument?)?.gameType),
                child: GameSessionResult(
                  arguments: args as RouteArgument?,
                )));
      case Routes.GAMESESSIONRESULT_DETAIL:
        return CupertinoPageRoute(
            builder: (ctx) =>
                SessionResultDetail(trainingSession: args as TrainingSession));
      default:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider<LoginProvider>(
                  create: (ctx) => LoginProvider(
                      firebaseAuthService: ctx.read<FirebaseAuthService>()),
                  child: LoginPage(),
                ));
    }
  }

  static Route<dynamic> hardwareRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.HARDWARE_SETUP_ONE:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider<StepOneProvider>(
                  create: (ctx) => StepOneProvider(),
                  child: HardwareSetupStepOne(),
                ));
      case Routes.HARDWARE_SETUP_CALIBRATION:
        return CupertinoPageRoute(
            builder: (ctx) =>
                ChangeNotifierProvider<HardwareCalibrationProvider>(
                  create: (ctx) => HardwareCalibrationProvider(),
                  child: HardwareSetupCalibration(),
                ));
      case Routes.HARDWARE_SETUP_TESTING:
        return CupertinoPageRoute(
            builder: (ctx) =>
                ChangeNotifierProvider<HardwareSetUpTestingProvider>(
                  create: (ctx) =>
                      HardwareSetUpTestingProvider(ctx.read<HardwareService>()),
                  child: HardwareSetupTesting(),
                ));
      case Routes.HARDWARE_SETUP_SUCCESS:
        return CupertinoPageRoute(
            builder: (ctx) =>
                ChangeNotifierProvider<HardwareConnectionSuccessProvider>(
                  create: (ctx) => HardwareConnectionSuccessProvider(),
                  child: HardwareSetupConnectionSuccess(),
                ));
      case Routes.BLUETOOTH_UNAUTHORIZED:
        return CupertinoPageRoute(
            builder: (ctx) => BluetoothUnAuthorizedWidget());
      case Routes.BLUETOOTH_DEVICES_LIST:
        return CupertinoPageRoute(
            builder: (ctx) =>
                ChangeNotifierProvider<DevicesBluetoothStateProvider>(
                  create: (ctx) => DevicesBluetoothStateProvider(
                      ctx.read<HardwareService>()),
                  child: BluetoothDevicesList(),
                ));
      case Routes.HARDWARE_SETUP_HOME:
        return CupertinoPageRoute(builder: (ctx) => HardwareTabPage());

      default:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider<StepOneProvider>(
                  create: (ctx) => StepOneProvider(),
                  child: HardwareSetupStepOne(),
                ));
    }
  }

  static Route<dynamic> trainingRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.TRAINING_HOME:
        return CupertinoPageRoute(builder: (ctx) => TrainingTabPage());
      case Routes.GREEN_SEEKER_LAUNCH:
        return CupertinoPageRoute(builder: (ctx) => GreenSeekerLaunchPage());
      // case Routes.GREEN_SEEKER_TRAIN:
      //   return CupertinoPageRoute(builder: (ctx) => GreenSeekerTrainPage());
      case Routes.PSR_LAUNCH:
        return CupertinoPageRoute(builder: (ctx) => PreShotRoutineLaunchPage());
      // case Routes.PSR_TRAIN:
      //   return CupertinoPageRoute(builder: (ctx) => PreShotRoutineTrainPage());
      case Routes.UNIVERSE_LAUNCH:
        return CupertinoPageRoute(builder: (ctx) => UniverseMasterLaunchPage());
      // case Routes.TRAINING_TIMER:
      //   return CupertinoPageRoute(builder: (ctx) => TrainingTimer(arguments: args as RouteArgument,));
      default:
        return CupertinoPageRoute(builder: (ctx) => TrainingTabPage());
    }
  }

  static Route<dynamic> reportRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.GAMESESSIONRESULT:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider(
                create: (BuildContext context) => SessionResultProvider(
                    firebaseAuthService: context.read<FirebaseAuthService>(),
                    db: context.read<FirestoreDBService>(),
                    trainingService: context.read<TrainingService>()),
                child: GameSessionResult()));
      case Routes.GAMESESSIONRESULT_DETAIL:
        return CupertinoPageRoute(
            builder: (ctx) =>
                SessionResultDetail(trainingSession: args as TrainingSession));
      default:
        return CupertinoPageRoute(
            builder: (ctx) => ChangeNotifierProvider(
                create: (BuildContext context) => SessionResultProvider(
                    firebaseAuthService: context.read<FirebaseAuthService>(),
                    db: context.read<FirestoreDBService>(),
                    trainingService: context.read<TrainingService>()),
                child: GameSessionResult()));
    }
  }

  static Route<dynamic> supportRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.SUPPORTHOME:
        return CupertinoPageRoute(builder: (ctx) => SupportTabPage());
      default:
        return CupertinoPageRoute(builder: (ctx) => SupportTabPage());
    }
  }

  static Route<dynamic> resourcesRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.RESOURCESHOME:
        return CupertinoPageRoute(builder: (ctx) => ResourcesTabPage());
      case Routes.RESOURCE_SETTINGUPHEADSET:
        return CupertinoPageRoute(builder: (ctx) => SettingUpHeadgear());
      case Routes.RESOURCE_CRAFTING_PRESHOT_ROUTINE:
        return CupertinoPageRoute(builder: (ctx) => CraftingPreShotRoutine());
      case Routes.RESOURCE_ENTERINGEXPERTSTATE:
        return CupertinoPageRoute(builder: (ctx) => EnteringTheExpertState());
      default:
        return CupertinoPageRoute(builder: (ctx) => ResourcesTabPage());
    }
  }
}

class Routes {
  static const String MAIN = '/Main';
  static const String LOGIN = '/Login';
  static const String SIGNUP = '/Signup';
  static const String FORGOT_PASSWORD =
      "/ForgotPassword"; //Used to navigate user to Forgot Password Page (unauthenticated user)
  static const String RECOVER_PASSWORD =
      "/RecoverPassword"; //Used to navigate user to ResetPassword Page after Forgot Password (unauthenticated user)
  static const String RESET_PASSWORD =
      "/ResetPassword"; //User to change the account password for authenticated user
  static const String ACCOUNT_DRAWER = "/AccountDrawer";
  static const String USER_ACCOUNT = '/UserAccount';

  //HARDWARE
  static const String HARDWARE_SETUP_ONE = "/HardWareSetupOne";
  static const String HARDWARE_SETUP_TWO = '/HardWareSetupTwo';
  static const String HARDWARE_SETUP_TESTING = "/HardwareSetupTesting";
  static const String HARDWARE_SETUP_SUCCESS =
      '/HardwareSetupConnectionSuccess';
  static const String HARDWARE_SETUP_CALIBRATION = "/HardwareSetupCalibration";
  static const String BLUETOOTH_UNAUTHORIZED = "/BluetoothUnAuthorized";
  static const String BLUETOOTH_DEVICES_LIST = "/BluetoothDevicesList";
  static const String HARDWARE_SETUP_HOME = "/";

  //TRAINING
  static const String TRAINING_HOME = "/";
  static const String GREEN_SEEKER_LAUNCH = "/GreenSeekerLaunch";
  static const String GREEN_SEEKER_TRAIN = '/GreenSeekerTrain';
  static const String PSR_LAUNCH = '/PreshotRoutineLaunch';
  static const String PSR_TRAIN = '/PreshotRoutineTrain';
  static const String UNIVERSE_LAUNCH = '/UniverseLaunch';
  static const String UNIVERSE_TRAIN = '/UniverseTrain;';
  static const String TRAINING_TIMER = '/TrainignTimer';

  //REPORTS
  static const String GAMESESSIONRESULT = "/";
  static const String GAMESESSIONRESULT_DETAIL = '/GameSessionResult_Detail';

  //SUPPORTS

  static const String SUPPORTHOME = "/";

  //RESOURCES
  static const String RESOURCESHOME = "/";
  static const String RESOURCE_SETTINGUPHEADSET = "/SettingUp_Headset";
  static const String RESOURCE_CRAFTING_PRESHOT_ROUTINE = "/CraftingAPreShotRoutine";
  static const String RESOURCE_ENTERINGEXPERTSTATE = "/EnteringTheExpertState";
}

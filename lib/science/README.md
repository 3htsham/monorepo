# Golf Signal Procesor

## Purpose

This library provides the classes necessary to process EEG signals into usable Z Scores for use in Dart/Flutter applications.  

This library was developed for the 4 channel Optios headset, with sensors at T3, T4, Fp1, and Fp2.  The signal is 500hz.

## Concepts

Import this into your application where you recieve the raw EEG signals.  Using this requires your application to operate in three stages.

Stage 1 is sending signal to the processesor with no expecations of any usable output.  This is usually for 30 seconds, and allows the software filters to prime.  This signal should sent after the user has achieved "fit" of the device, but before they are directed to baseline.

Stage 2 is baselining the algorithm, aka calibration.  This is the only stage that is commanded by the application.  This is a 30 second period where the user is to be directed to be calm and do nothing.  The classic application behavior at this time is called "Eyes Open / Eyes Closed", where the user is directed to sit calmly and open and close their eyes in 5 second intervals.

Stage 3 is acquisition.  This stage starts automatically after baselineing is commanded.  This stage computes usable Z-score results.

## Usage

Instantiate a single GolfSignalProcessor, and use that instance for the life of your application.

The main interface is the member function **AddSample**.  You call this for every sample that you recieve from the headset.  If the baseline has been set correctly, it will return a Z score value.

The other member function is **StartBaseline**.  You call this when you want to set or reset the baseline for the algorithm.  You need only call this, and continue to send new data through **AddSample**.

Basic flow in where samples come:
```Dart
import
var processor = GolfSignalProcessor();
var start_time = DateTime.now();
while (var sample = hardware.GetSample()) {
    
    var zScore = processor.AddSample(sample);
    // state 1 - let it fill for 30 seconds
    if(DateTime.now() < start_time + Duration(seconds: 30)) {
        continue;
    }

    // state 2 - a listener to start baseline
    if(start_baseline_event) {
        processor.StartBaseline();
    }

    // state 3: process zScore into app
}
```


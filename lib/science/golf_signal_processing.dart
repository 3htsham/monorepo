import 'dart:core';
import 'dart:math';

// passing out baseline info
class baselineInfo {
  double mean = 0.0;
  double sd = 0.0;
}

// complex numbers
class complex {
  double re = 0.0;
  double im = 0.0;
}

class GolfSignalProcessor {
  final double _pi = 3.1415926535897932384626434;
  final double _inverseLN10 = 0.43429448190325182765112891891660508;
  int _signalFrequency = 500;
  final int _medianFilterOrder = 250;
  final int _medianIndex = 125; // MEDIAN FILTER ORDER / 2
  final int _baselineDuration = 60; // in seconds
  final int _threshold = 1;

  late List<double>
      _filterList; // = List<double>.filled(MEDIAN_FILTER_ORDER, 0.0, growable: true);
  late Iterator<double> _filterListIterator; // = mFilterHKList.iterator;

  late List<double>
      _filterHKArray; //=   List<double>.generate(MEDIAN_FILTER_ORDER, (_) => 0.0);
  int _filterHKArrayIndex = 0;
  double _filterOldestS = 0;

  int _sampleCount = -1;

  bool _isBaselineing = false;
  bool _hasBaseline = false;

  double _postFilterSample = 0.0;

  bool get hasBaseline => _hasBaseline;

  int _baselineStart = 0, _baselineEnd = 0;
  late List<double> _baselineSamples;

  double _lastZScore = 0.0;
  double _lastAlpha = 0.0;

  double get lastAlphaT => _lastAlpha;

  double _zRange = 2.0;

  double get zRange => _zRange;

  get baselineEEGSamples => _baselineSamples;

  get postFilterEEG => _postFilterSample;

  set zRange(double zRange) {
    _zRange = zRange;
  }

  late List<double> _epoch;

  // baseline data
  double _baselineMean = 0.0;
  double get baselineMean => _baselineMean;

  double _baselineSD = 0.0;

  double get baselineSD => _baselineSD;

  bool _debugComputer = true;

  bool get debugComputer => _debugComputer;

  int _epochCount = 0;

  set debugComputer(bool debugComputer) {
    _debugComputer = debugComputer;
  }

  GolfSignalProcessor() {
    _filterList =
        List<double>.generate(_medianFilterOrder, (_) => 0.0, growable: true);
    _filterHKArray = List<double>.generate(_medianFilterOrder, (_) => 0.0);
    _epoch = List<double>.generate(_signalFrequency, (_) => 0.0);
    _baselineSamples = List<double>.empty(growable: true);
  }

// math functions
  double _log10(final double x) {
    return log(x) * _inverseLN10;
  }

// Complex Number opertaions
  complex _complexFromPolar(final double r, final double radians) {
    var result = complex();
    result.re = r * cos(radians);
    result.im = r * sin(radians);
    return result;
  }

  complex _addComplex(final complex left, final complex right) {
    var result = complex();
    result.re = left.re + right.re;
    result.im = left.im + right.im;
    return result;
  }

  complex _multiplyComplex(final complex left, final complex right) {
    var result = complex();
    result.re = left.re * right.re - left.im * right.im;
    result.im = left.re * right.im + left.im * right.re;
    return result;
  }

// Cooley-Tukey FFT Algorithm
  List<complex> _computeDFTNative(final List<complex> x, final int N) {
    var X = List.generate(N, (_) => complex());

    int k, n;
    for (k = 0; k < N; k++) {
      X[k].re = 0.0;
      X[k].im = 0.0;
      for (n = 0; n < N; n++) {
        X[k] = _addComplex(X[k],
            _multiplyComplex(x[n], _complexFromPolar(1, -2 * _pi * n * k / N)));
      }
    }
    return X;
  }

  List<complex> _computeFFT(
      final List<complex> input, final int N, final int N1, final int N2) {
    int k1, k2;

    // /* Allocate columnwise matrix */
    // complex** columns = (complex**) malloc(sizeof(struct complex_t*) * N1);
    // for(k1 = 0; k1 < N1; k1++) {
    //     columns[k1] = (complex*) malloc(sizeof(struct complex_t) * N2);
    // }

    //var columns = List.filled(N1, List.generate(N2, (_) => complex()));

    var columns = List.generate(N1, (_) => List.generate(N2, (_) => complex()));

    /* Allocate rowwise matrix */
    // complex** rows = (complex**) malloc(sizeof(struct complex_t*) * N2);
    // for(k2 = 0; k2 < N2; k2++) {
    //     rows[k2] = (complex*) malloc(sizeof(struct complex_t) * N1);
    // }

    var rows = List.generate(N2, (_) => List.generate(N1, (_) => complex()));

    /* Reshape input into N1 columns */
    for (k1 = 0; k1 < N1; k1++) {
      for (k2 = 0; k2 < N2; k2++) {
        columns[k1][k2] = input[N1 * k2 + k1];
      }
    }

    //print('DFT step');
    /* Compute N1 DFTs of length N2 using naive method */
    for (k1 = 0; k1 < N1; k1++) {
      columns[k1] = _computeDFTNative(columns[k1], N2);
      // for (k2 = 0; k2 < N2; ++k2) {
      //   print(' - $k1 $k2 is ${columns[k1][k2].im}, ${columns[k1][k2].re}');
      // }
    }

    /* Multiply by the twiddle factors  ( e^(-2*pi*j/N * k1*k2)) and transpose */
    for (k1 = 0; k1 < N1; k1++) {
      for (k2 = 0; k2 < N2; k2++) {
        rows[k2][k1] = _multiplyComplex(
            _complexFromPolar(1, -2.0 * _pi * k1 * k2 / N), columns[k1][k2]);
      }
    }

    /* Compute N2 DFTs of length N1 using naive method */
    for (k2 = 0; k2 < N2; k2++) {
      rows[k2] = _computeDFTNative(rows[k2], N1);
    }

    /* Flatten into single output */
    //complex* output = (complex*) malloc(sizeof(struct complex_t) * N);
    var output = List.generate(N, (_) => complex());
    for (k1 = 0; k1 < N1; k1++) {
      for (k2 = 0; k2 < N2; k2++) {
        output[N2 * k1 + k2] = rows[k2][k1];
      }
    }

    return output;
  }

// Bandwidth Algorithm
  void _computePSD(final List<complex> input, final int N, List<double> psdv) {
    /* PSD 0Hz to 250Hz, 249 Real and 251 Img components */
    for (int k = 0; k < N / 2; k++) {
      psdv[k] =
          2 * ((input[k].re * input[k].re) + (input[k].im * input[k].im)) / N;
    }
  }

  double _computeAlpha(final List<double> psdv) {
    return _log10((psdv[8] + psdv[9] + psdv[10] + psdv[11] + psdv[12]) / 5);
  }

  double _computeBeta(final List<double> psdv) {
    /* BETA = 13-30Hz */
    var betat = 0.0;
    for (var k = 13; k <= 30; k++) {
      betat += psdv[k];
    }
    var betav = _log10(betat / 18);
    return betav;
  }

  double _getAlpha(final List<complex> input1) {
    var psdA = List<double>.generate(250, (index) => 0.0);
    var alphaV = 0.0;
    var betaV = 0.0;

    /* Do FFT */
    var fftA = _computeFFT(input1, _signalFrequency, 125, 4);

    //Debug
    // for (var k = 0; k < 252; k++) {
    //   print('getAlpha - FFT - $k, ${fftA[k].re} , ${fftA[k].im}');
    // }

    /* Do PSD */
    _computePSD(fftA, _signalFrequency, psdA);

    //Debug
    //for (var k = 0; k <= 250; k++) {
    //fprintf(psdout,"%d,%d,%f\n",epochT,k,psdA[k]);
    //}

    /* Get ALPHA */
    alphaV = _computeAlpha(psdA);

    //debug
    //fprintf(alphaout,"%d,%f\n",epochT,alphaV);

    /* Get BETA */
    betaV = _computeBeta(psdA);

    return alphaV;
  }

//------------------------------Baseline Filter ---------------------------//
  double _medianFilter(double sample) {
    var binsert = false;
    _filterOldestS = _filterHKArray[_filterHKArrayIndex];
    _filterHKArray[_filterHKArrayIndex] = sample;
    _filterHKArrayIndex++;
    if (_filterHKArrayIndex >= _medianFilterOrder) {
      _filterHKArrayIndex = 0;
    }

    // //remove
    _filterList.remove(_filterOldestS);

    binsert = false;
    var index = _filterList.indexWhere((element) => sample >= element);
    if (index != -1) {
      _filterList.insert(index, sample);
      binsert = true;
    }

    if (!binsert) {
      _filterList.add(sample);
    }

    return (sample - _filterList[_medianIndex]);
  }

//-------------------------------Math Functions --------------------------------//
  double _calculateSD(final List<double> data, int N) {
    var sum = 0.0, SD = 0.0;

    //for (i = 0; i < N; ++i)
    data.forEach((element) {
      sum += element;
    });

    var mean = sum / N;

    data.forEach((element) {
      SD += pow(element - mean, 2);
    });
    return sqrt(SD / N);
  }

//-----------------------------------Baseline algorithm -----------------------------//
  baselineInfo _getBaseline(List<double> data, int N) {
    print('baseline:  input is ${data.length} long');

    //get alpha
    var alphaT = List<double>.generate(N, (_) => 0.0); //2min baseline

    var input1 = List<complex>.generate(_signalFrequency, (index) => complex());

    for (var k = 0; k < N; k++) {
      for (var i = 0; i < _signalFrequency; i++) {
        input1[i].re = data[k * _signalFrequency + i];
        input1[i].im = 0.0;
      }
      alphaT[k] = _getAlpha(input1);

      //print('getBaseline - alphaT[$k] = ${alphaT[k]}');
    }

    //compute baseline Mean and SD
    var baselineM = 0.0;
    for (var i = 0; i < N; i++) {
      baselineM = baselineM + alphaT[i];
    }
    var info = baselineInfo();
    info.mean = baselineM / N;
    info.sd = _calculateSD(alphaT, N);

    //print('GetBaseline - mean=$mean, sd=$sd');
    return info;
  }

//------------------------------ZScore algorithm ------------------------------------//
  double _computeZScore(final List<double> data, double M, double SD) {
    //get alpha

    var input1 = List<complex>.generate(_signalFrequency, (index) => complex());

    for (var i = 0; i < _signalFrequency; i++) {
      input1[i].re = data[i];
      input1[i].im = 0.0;
    }
    _lastAlpha = _getAlpha(input1);

    //compute zscore
    return ((_lastAlpha - M) / SD);
  }

  double getNormalizedZScore(double sample) {
    var zscore = _lastZScore;
    _epoch[_sampleCount % _signalFrequency] = sample;

    // only compute every second
    if ((_sampleCount % _signalFrequency) == 0) {
      zscore = _computeZScore(_epoch, baselineMean, baselineSD);

      // map the zScore to 0-100 based on the floor/top of zRange;
      zscore += _zRange;
      zscore = (zscore / (2.0 * _zRange)) * 100.0;
      _lastZScore = zscore;

      _epochCount++;
    }

    return zscore;
  }

  void addBaselineSample(double sample) {
    _baselineSamples.add(sample);
    if (_sampleCount >= _baselineEnd) {
      endBaseLine();
    }
  }

  // will not need to be called
  void endBaseLine() {
    var info = _getBaseline(_baselineSamples, _baselineDuration);
    _baselineMean = info.mean;
    _baselineSD = info.sd;
    _isBaselineing = false;
    _hasBaseline = true;
    if (debugComputer) {
      print(
          'GolfSignalComputer: Baseline complete: time: ${DateTime.now().toString()} mean $baselineMean, sd: $baselineSD');
      print('Start: $_baselineStart, end: $_baselineEnd');
    }
  }

  //------------------------------ public realtime interface

  void reset() {}

  void startBaseLine() {
    _baselineMean = 0.0;
    _baselineSD = 0.0;
    _isBaselineing = true;
    _baselineStart = _sampleCount;
    _baselineSamples.clear();
    _baselineEnd = _baselineStart + _signalFrequency * _baselineDuration;
    if (debugComputer) {
      print('GolfSignalComputer: Starting Baseline');
    }
  }

  double addSample(List<double> data, [bool skipFilter = false]) {
    _sampleCount++;

    // data is
    // 0 - t3
    // 1 - fp1
    // 2 - fp2
    // 3 - t4

    // right now this is using T4, but may change based on
    // scientist input
    var value = data[3];

    // median filter
    if (!skipFilter) {
      value = _medianFilter(value);
    }
    _postFilterSample = value;

    // if baseline
    if (_isBaselineing == true) {
      addBaselineSample(value);
      return 0.0;
    }
    // calculate

    if (hasBaseline || skipFilter) {
      return getNormalizedZScore(value);
    }

    // no baseline, no zscore!
    return 0.0;

    //
  }
}

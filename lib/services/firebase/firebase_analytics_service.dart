import 'dart:async';
import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
import 'package:brain_trainer/models/auth_user.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

class FirebaseAnalyticsService {
  static final FirebaseAnalyticsService _singleton =
      new FirebaseAnalyticsService._internal();

  factory FirebaseAnalyticsService() {
    return _singleton;
  }

  dispose() {
    authUserStream?.cancel();
  }

  FirebaseAnalytics? _firebaseAnalytics;
  StreamSubscription<AuthUser?>? authUserStream;

  FirebaseAnalyticsService._internal() {
    _firebaseAnalytics = FirebaseAnalytics();
    authUserStream =
        FirebaseAuthService().onAuthStateChanged?.listen(userChangeListener);
  }

  void initialize() {
    logAnalytics('App Startup');
  }

  void userChangeListener(AuthUser? _user) {
    if (_user != null) {
      print('FirebaseAnalyticsService: setting user to ${_user.email}');
      FirebaseCrashlytics.instance.setUserIdentifier(_user.email);
      _firebaseAnalytics?.setUserId(_user.email);
    }
  }

  void logAnalytics(String _message) {
    _firebaseAnalytics?.logEvent(
        name: 'golf_app_event',
        parameters: <String, dynamic>{'message': _message});
  }

  void crashTest() {
    FirebaseCrashlytics.instance.crash();
  }
}

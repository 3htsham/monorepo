import 'package:brain_trainer/config/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import '../../models/auth_user.dart';
class FirebaseAuthService {
  static AuthUser? user;
  final FirebaseAuth? _firebaseAuth;

  FirebaseAuthService({FirebaseAuth? firebaseAuth})
      : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance;

  AuthUser? _userFromFirebase(User? user) {
    if (user == null) {
      return null;
    }
    return AuthUser(
      uid: user.uid,
      email: user.email ?? "",
      displayName: user.displayName ?? "",
      photoUrl: user.photoURL ?? "",
    );
  }

  Stream<AuthUser?>? get onAuthStateChanged {
    print('Auth State Changed');
    return _firebaseAuth?.authStateChanges().map((_user) => _userFromFirebase(_user));
    // return _firebaseAuth?.onAuthStateChanged.map(_userFromFirebase);
  }

  Future<AuthUser?> signInAnonymously() async {
    final authResult = await _firebaseAuth?.signInAnonymously();
    return _userFromFirebase(authResult?.user);
  }

  //register with firebase email method
  Future<dynamic> registerWithEmailPassword(String email, String password,
      {String displayName = ''}) async {
    try {
      UserCredential authResult = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      //update user displayname
      authResult.user?.updateDisplayName(displayName);
      return _userFromFirebase(authResult.user);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return 'The password provided is too weak.';
      } else if (e.code == 'email-already-in-use') {
        return 'The account already exists for that email.';
      }
    } on PlatformException catch (e) {
      return e.message.toString();
    } catch (e) {
      print(e);
      return e.toString();
    }
    return null;
  }

  //sign in with firebase email method
  Future<dynamic> signInWithEmailPassword(String email, String password) async {
    try {
      UserCredential authResult = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      //check if user's email is verified:
      if (!(authResult.user?.emailVerified ?? false)) {
        return ErrorMessage.emailNotVerified;
      }
      // show popup for user to check email
      return _userFromFirebase(authResult.user);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        return 'Wrong password provided.';
      }
    } on PlatformException catch (e) {
      if (e.code == 'ERROR_USER_NOT_FOUND') {
        return 'No user found for that email.';
      }
      return e.message.toString();
    }
  }

  //send password reset link to email
  Future<dynamic> sendPasswordResetLink(String email) async {
    try {
      await FirebaseAuth.instance
          .sendPasswordResetEmail(email: email);
      return;
    }
    on PlatformException catch (e) {
      if (e.code == 'ERROR_USER_NOT_FOUND') {
        return 'No user found for that email.';
      }
      return e.message.toString();
    } on FirebaseAuthException catch (e) {
      if (e.code == "too-many-requests") {
        return "Too many requests, Please try again later.";
      } else  {
        return e.message;
      }
    }
    catch (e) {
      print(e);
      return e;
    }
  }

  Future<dynamic> updatePassword(String newPassword) async {
    try {
      final _currentUser = FirebaseAuth.instance.currentUser;
      if(_currentUser != null) {
        await _currentUser.updatePassword(newPassword);
        return;
      } else {
        return "This action requires recent login";
      }
    }
    on PlatformException catch (e) {
      return e.message;
    }
    catch (e) {
      return e.toString();
    }
  }

  Future<void> sendVerificationEmail() async {
    var user = FirebaseAuth.instance.currentUser;
    return await user?.sendEmailVerification();
  }

  Future<void> signOut() async {
    await _firebaseAuth?.signOut();
  }

  Future<AuthUser?> currentUser() async {
    final user = _firebaseAuth?.currentUser;
    return _userFromFirebase(user);
  }

  Future<bool> isAuthenticated() async {
    final user = _firebaseAuth?.currentUser;
    return (user != null && user.emailVerified);
  }

  Future<void> updateUserEmail(String email) async {
      final user = _firebaseAuth?.currentUser;
      return await user?.updateEmail(email);
  }
}

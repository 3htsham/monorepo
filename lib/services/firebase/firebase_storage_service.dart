import 'dart:async';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

class FirebaseStorageService {
  static String userPhotoRef = "users/";

  Future<UploadTask> syncFile(File file, Reference ref) async {
    return ref.putFile(file);
  }

  Future<int> fileSizeOnFirebase(Reference _ref) async {
    var meta = await _ref.getMetadata();
    return meta.size ?? 0;
  }

  Future<bool> _isSyncNeeded(File file, Reference ref) async {
    int localFileSize = file.lengthSync();

    bool upload = false;
    bool exists = await fileExistsOnFirebase(ref);

    if (exists == false) {
      upload = true;
    } else {
      if (localFileSize != await fileSizeOnFirebase(ref)) {
        upload = true;
      }
    }
    return upload;
  }

  Future<bool> fileExistsOnFirebase(Reference _ref) async {
    // this attempts to touch the online file, and catches the exception caused
    // by it not being there
    try {
      var value = await _ref.getDownloadURL();
      return true;
    } on FirebaseException catch (e) {
      print('Caught a firebase exception');
      return false;
    }
  }

  Future<int> syncDataFiles(String bucketName, String path) async {
    int uploads = 0;
    var files = await sortedLocalFiles(path);
    int num = files?.length ?? 0;
    if (files?.isNotEmpty ?? false) {
      for (var file in files!) {
        var stats = file.statSync();
        print('FirebaseStorageService: Look to Sync File: ' +
            file.path +
            ' is  ${stats.size.toString()}');
        String fileRoot = p.basename(file.path);

        File _file = File(file.path);

        if (_file.existsSync()) {
          Reference ref =
              FirebaseStorage.instance.ref().child(bucketName).child(fileRoot);

          bool _uploadThisFile = await _isSyncNeeded(_file, ref);

          UploadTask? task;

          try {
            if (_uploadThisFile) {
              uploads++;
              task = await syncFile(_file, ref).catchError(
                  (e) => print('Firestore Upload Error: ${e.toString()}'));
            } else {
              print(
                  'FirebaseStorageService: Done at $uploads files synced out of $num');
              checkForDeletableFiles(files, uploads);
              return uploads;
            }

            if (task != null) {
              await task.whenComplete(() {
                print(
                    'FirebaseStorageService: Synced file ${task?.snapshot.ref.name} size: ${task?.snapshot.bytesTransferred.toString()}');
              });
            }
          } on Exception catch (e) {
            print('Upload File Execption + ${e.toString()}');
          }
        }
      }
    }

    return uploads;
  }

  // generates file newest to oldest (b to a)
  Future<List<FileSystemEntity>?> sortedLocalFiles(String path) async {
    Directory? appDocDir = await getLibraryDirectory();
    appDocDir = Directory(appDocDir.path + path);

    var files = appDocDir.listSync();

    files
        .sort((a, b) => b.statSync().modified.compareTo(a.statSync().modified));

    return files;
  }

  Future<String> getLatestFileRoot(String path) async {
    var files = await sortedLocalFiles(path);
    if (files?.length == 0) return 'NONE';

    String name = files![0].path;

    return p.basenameWithoutExtension(name);
  }

  void checkForDeletableFiles(List<FileSystemEntity> files, int uploads) {

    // start at the index of the first known, safe file, and go back in time.

    int index = uploads;

    for (index = uploads; index < files.length; index++) {
      FileSystemEntity file = files[index];
      var stats = file.statSync();

      // delete safe files older than seven days.
      if (stats.modified.isBefore(DateTime.now().subtract(Duration(days: 7)))) {
        print('Delete ${file.path}');
        file.deleteSync();
      }
    }
  }
}

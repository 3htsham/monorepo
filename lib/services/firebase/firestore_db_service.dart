import 'dart:math';
import 'dart:typed_data';

import 'package:brain_trainer/models/coach_tech_user.dart';
import 'package:brain_trainer/models/session_putt.dart';
import 'package:brain_trainer/models/training_program.dart';
import 'package:brain_trainer/models/training_session.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'package:brain_trainer/models/golf_subject.dart';

class FirestoreDBService {
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  static const subjectCollection = 'golf_subjects';
  static const coachTechCollection = 'coach_tech';
  static const sessionsCollection = 'training_sessions';
  static const sessionPuttsCollection = 'session_putts';
  static const trainingProgramCollection = 'training_programs';

  /// Get a stream of a single document
  Stream<GolfSubject?> getGolfSubjectStream(String userAuthID) {
    return _db
        .collection(subjectCollection)
        .where('userAuthID', isEqualTo: userAuthID)
        .snapshots()
        .map((snap) {
      if (snap.docs.length > 1) {
        throw ("getGolfSubjectStream:  Subjects > 1");
      }
      if (snap.docs.isEmpty) {
        return null;
      }
      return GolfSubject.fromMap(snap.docs[0].data());
    });
  }

  Future<GolfSubject?> getGolfSubject(String? userAuthID) async {
    try {
      var docs = await _db
          .collection(subjectCollection)
          .where('userAuthID', isEqualTo: userAuthID)
          .get();

      if (docs.docs.isEmpty) {
        return null;
      }

      if (docs.docs.length > 1) {
        throw ("getGolfSubject:  Subjects > 1");
      }

      return GolfSubject.fromMap(docs.docs[0].data());
    } catch (e) {
      return null;
    }
  }

  /// Write data
  Future<void> createGolfSubject(GolfSubject _subject) async {
    var newSubject = await getGolfSubject(_subject.userAuthID);
    if (newSubject == null) {
      await _db.collection(subjectCollection).add(_subject.toJson());
      return;
    } else {
      return null;
    }
  }

  Future<void> updateGolfSubject(GolfSubject newSubject) async {
    // find the document
    QuerySnapshot q = await _db
        .collection(subjectCollection)
        .where('userAuthID', isEqualTo: newSubject.userAuthID)
        .limit(1)
        .get();

    String? docId = q.docs[0].id;
    if (docId != null) {
      await _db
          .collection(subjectCollection)
          .doc(docId)
          .update(newSubject.toJson());
      return;
    }
  }

  Future<void> updateUserProfilePhoto(String uid, String photoUrl) async {
    QuerySnapshot query = await _db
        .collection(subjectCollection)
        .where('userAuthID', isEqualTo: uid)
        .limit(1)
        .get();
    String? docId = query.docs[0].id;
    if (docId != null) {
      await _db
          .collection(subjectCollection)
          .doc(docId)
          .set({'imgUrl': photoUrl}, SetOptions(merge: true));
      return;
    }
  }

  Future<void> updateUserEmail(String uid, String email) async {
    QuerySnapshot query = await _db
        .collection(subjectCollection)
        .where('userAuthID', isEqualTo: uid)
        .limit(1)
        .get();
    String? docId = query.docs[0].id;
    if (docId != null) {
      await _db
          .collection(subjectCollection)
          .doc(docId)
          .set({'email': email}, SetOptions(merge: true));
      return;
    }
  }

  Future<void> deleteGolfSubject(GolfSubject _subject) async {
    // find the document
    QuerySnapshot q = await _db
        .collection(subjectCollection)
        .where('userAuthID', isEqualTo: _subject.userAuthID)
        .limit(1)
        .get();

    String? docId = q.docs[0].id;
    if (docId != null) {
      print("Deleting Golf Subject for ${_subject.userAuthID}");
      await _db.collection(subjectCollection).doc(docId).delete();
      return;
    }
  }

  Future createTrainingSession(TrainingSession ts) async {
    var doc = await _db.collection(sessionsCollection).add(ts.toJson());
    ts.docID = doc.id;
  }

  Future<void> deleteTrainingSession(TrainingSession ts) async {
    await deleteTrainingSessionPutts(ts);
    await deleteTrainingSessionDoc(ts.docID);
  }

  Future<void> deleteTrainingSessionDoc(String docID) async {
    return await _db.collection(sessionsCollection).doc(docID).delete();
  }

  Future<void> deleteTrainingSessionsForUser(String userAuthID) async {
    var sessions = await getSubjectSessions(userAuthID);
    sessions.forEach((session) async {
      await deleteTrainingSession(session);
    });
  }

  Future<List<TrainingSession>> getSubjectSessions(String? userAuthID) async {
    var docs = await _db
        .collection(sessionsCollection)
        .where('subjectID', isEqualTo: userAuthID)
        .get();

    List<TrainingSession> list = <TrainingSession>[];

    docs.docs.forEach((e) {
      list.add(TrainingSession.fromMap(e.id, e.data()));
    });

    return list;
  }

  Stream<List<TrainingSession>> getSubjectSessionsStream(String userAuthID) {
    return _db
        .collection(sessionsCollection)
        .where('subjectID', isEqualTo: userAuthID)
        //       .orderBy('endTime')
        .snapshots()
        .map((snap) {
      return snap.docs.map((doc) {
        return TrainingSession.fromMap(doc.id, doc.data());
      }).toList();
    });
  }

  Future<List<SessionPutt>> getPuttsForSession(TrainingSession ts) async {
    return await _db
        .collection(sessionPuttsCollection)
        .where('sessionID', isEqualTo: ts.docID)
        .get()
        .then((value) => value.docs
            .map((e) => SessionPutt.fromMap(e.id, e.data()))
            .toList());
  }

  Future<void> deleteTrainingSessionPutts(TrainingSession ts) async {
    var putts = await getPuttsForSession(ts);
    putts.forEach((element) async {
      await deleteSessionPutt(element.docID);
    });
  }

  Future<void> deleteSessionPutt(String docID) async {
    await _db.collection(sessionPuttsCollection).doc(docID).delete();
  }

  Future addSessionPutt(SessionPutt putt) async {
    var doc = await _db.collection(sessionPuttsCollection).add(putt.toJson());
    putt.docID = doc.id;
  }

  Future<void> createTestPutts(TrainingSession ts) async {
    DateTime puttTime = ts.startTime!;

    Random rand = new Random(DateTime.now().millisecond);

    for (int i = 0; i < 15; ++i) {
      puttTime = puttTime.add(Duration(seconds: rand.nextInt(60) + 60));

      var bytes = List<int>.generate(48, (index) => rand.nextInt(256));

      Blob mechData = new Blob(Uint8List.fromList(bytes));

      bytes = List<int>.generate(2048, (index) => rand.nextInt(256));
      Blob neuroData = new Blob(Uint8List.fromList(bytes));

      SessionPutt sessionPutt = new SessionPutt(
        sessionID: ts.docID,
        timeStamp: puttTime,
        zoneScore: rand.nextInt(100),
        mechanicsData: mechData,
        neuroData: neuroData,
        notes: 'Fake putt!',
      );

      await addSessionPutt(sessionPutt);
    }
  }

  Future<void> createTrainingProgram(TrainingProgram tp) async {
    // check to see if one exists
    var _subject = await getTrainingProgram(tp.subjectID);
    if (_subject == null) {
      DocumentReference ref =
          await _db.collection(trainingProgramCollection).add(tp.toJson());
      tp.docID = ref.id;
    } else {
      print("Training program for subject ${tp.subjectID} already exists");
      return null;
    }
  }

  Future<TrainingProgram?> getTrainingProgram(String? subjectID) async {
    var docs = await _db
        .collection(trainingProgramCollection)
        .where('subjectID', isEqualTo: subjectID)
        .get();

    if (docs.docs.isEmpty) {
      return null;
    }

    if (docs.docs.length > 1) {
      throw ("getTrainingProgram:  Programs > 1");
    }

    return TrainingProgram.fromMap(docs.docs[0].id, docs.docs[0].data());
  }

  Stream<TrainingProgram?> getTrainingProgramStream(String subjectID) {
    return _db
        .collection(trainingProgramCollection)
        .where('subjectID', isEqualTo: subjectID)
        .snapshots()
        .map((snap) {
      if (snap.docs.length > 1) {
        throw ("getTrainingProgramStream:  Subjects > 1");
      }
      if (snap.docs.isEmpty) {
        return null;
      }
      return TrainingProgram.fromMap(snap.docs[0].id, snap.docs[0].data());
    });
  }

  Future<void> updateTrainingProgram(TrainingProgram tp) async {
    print("Updating Training Program ${tp.docID}");

    DocumentReference? ref =
        _db.collection(trainingProgramCollection).doc(tp.docID);
    if (ref != null) {
      await ref.update(tp.toJson());
    } else {
      print(
          "FirestoreDBService - Could Not Update TrainingProgram - no such document!");
    }
  }

  Future<void> deleteTrainingProgram(TrainingProgram tp) async {
    print("Deleting Training Program ${tp.docID}");

    DocumentReference ref =
        _db.collection(trainingProgramCollection).doc(tp.docID);
    if (ref != null) {
      await ref.delete();
    } else {
      print(
          "FirestoreDBService - Could Not delete TrainingProgram - no such document!");
    }
  }

  // for coach / tech users
  Future<CoachTechUser?> getCoachTechUser(String? id) async {
    try {
      var doc = await _db.collection(coachTechCollection).doc(id).get();
      if (doc.data() == null) {
         return null;
      }
      return CoachTechUser.fromMap(doc.data());
    } catch (e) {
      return null;
    }
  }
}

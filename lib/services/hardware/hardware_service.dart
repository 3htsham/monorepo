import 'dart:async';
import 'package:brain_trainer/enum/bluetooth_state.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/models/base_response.dart';
import 'package:brain_trainer/models/headband_models/cgx_headband.dart';

abstract class HardwareService {
  //getBluetoothState: used to get the bluetooth state of user's device
  //Might need to create some enum for state indication
  Stream<BluetoothState> getBluetoothState();

  //to get current bluetooth state
  BluetoothState currentBluetoothState = BluetoothState.unknown;

  ///current connected headband, return null if no headband connected
  CGXHeadband? headband;

  //getAvailableDevices: To get the available devices for connection
  //Not sure it will be used or not
  Stream<List<CGXHeadband>> getAvailableDevices();

  //connectWithDevice: Used to connect to an available device by passing device id/hash as parameter
  Future<BaseResponse<bool>> connectWithDevice(CGXHeadband headband);

  //disconnect current connected device
  Future<BaseResponse<bool>> disconnectDevice();

  //getBattery: To get the battery state or battery level of the connected hardware
  Future<double> getBatteryLevel();

  ///Data Streams
  //listenHardwareConnectionState: To listen the changes of Hardware Connection (can connected or disconnected)
  Stream<HeadbandState> listenHardwareConnectionState();

  //listenHardwareDataChanges: To listen the changes into Hardware data
  Stream<dynamic> listenHardwareDataChanges();

  //listenBatteryLevel: To listen the changes in battery state e.g battery getting low continuously
  Stream<String> listenHardwareBatteryLevel();

  //EEG Stream: listen to EEG and impedance data stream from the connected device
  Stream<dynamic> eegDataStream();

  //start acquiring the eeg data stream
  Future<bool> startEEGAcquisition();

  //stop acquiring the eeg data stream
  Future<bool> stopEEGAcquisition();
}

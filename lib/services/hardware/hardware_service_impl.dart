import 'dart:async';
import 'dart:convert';

import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';
import 'package:brain_trainer/enum/bluetooth_state.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/helper/json_helper.dart';
import 'package:brain_trainer/models/base_response.dart';
import 'package:brain_trainer/models/headband_models/cgx_eeg_data_model.dart';
import 'package:brain_trainer/models/headband_models/cgx_headband.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HardwareServiceImpl extends ChangeNotifier implements HardwareService {
  EventChannel bluetoothEventChannel =
      EventChannel(CGXChannels.bluetoothEventChannel);
  EventChannel deviceStateEventChannel =
      EventChannel(CGXChannels.deviceStateEventChannel);
  EventChannel devicesEventChannel =
      EventChannel(CGXChannels.devicesEventChannel);
  EventChannel eegEventChannel = EventChannel(CGXChannels.eegEventChannel);
  EventChannel impedanceEventChannel =
      EventChannel(CGXChannels.impedanceEventChannel);
  MethodChannel methodChannel = MethodChannel(CGXChannels.methodChannel);

  Stream<dynamic>? eegStream;
  Stream<BluetoothState>? bluetoothStateStream;
  Stream<List<CGXHeadband>>? devicesListStream;
  Stream<HeadbandState>? headbandStateStream;
  Stream<dynamic>? headbandDataStream;

  //to save the latest bluetooth state
  BluetoothState currentBluetoothState = BluetoothState.unknown;

  //current headband
  CGXHeadband? headband;

  @override
  Stream<BluetoothState> getBluetoothState() {
    if (bluetoothStateStream != null) {
      return bluetoothStateStream!;
    }
    bluetoothStateStream =
        bluetoothEventChannel.receiveBroadcastStream().map((event) {
      var state = JSONHelper.convertStringToBluetoothState(event.toString());
      currentBluetoothState = state;
      return state;
    });
    return bluetoothStateStream!;
  }

  @override
  Stream<List<CGXHeadband>> getAvailableDevices() {
    if (devicesListStream != null) {
      return devicesListStream!;
    }
    try {
      devicesListStream =
          devicesEventChannel.receiveBroadcastStream().distinct().map((event) {
        print(event);
        var json = jsonDecode(event);
        final res = BaseResponse<List<CGXHeadband>>.fromJson(json,
            parseJson: (json) => List<CGXHeadband>.from(
                json.map((x) => CGXHeadband.fromJson(x))));
        //For emulator testing
        // final list  = [
        //   CGXHeadband(deviceName: "ABC", identifier: "123456", assignedName: "ABC"),
        //   CGXHeadband(deviceName: "DEF", identifier: "123567", assignedName: "DEF"),
        //   CGXHeadband(deviceName: "GHI", identifier: "123678", assignedName: "GHI"),
        //   CGXHeadband(deviceName: "JKL", identifier: "123789", assignedName: "JKL")
        // ];
        // return list;
        if (res.data != null) {
          return res.data ?? [];
        } else {
          print(res.message);
          return [];
        }
      });
      return devicesListStream!;
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<BaseResponse<bool>> connectWithDevice(CGXHeadband headband) async {
    final _connection = await methodChannel.invokeMethod(CGXMethods.connect,
        <String, String>{"uuid": headband.identifier.toString()});
    var result = BaseResponse<bool>.fromJson(jsonDecode(_connection),
        parseJson: (json) => json);
    if (!result.error) {
      //update current device
      this.headband = headband;
    }
    return result;
  }

  @override
  Stream<HeadbandState> listenHardwareConnectionState() {
    if (headbandStateStream != null) {
      return headbandStateStream!;
    }
    headbandStateStream = deviceStateEventChannel
        .receiveBroadcastStream()
        .distinct()
        .map((event) {
      //update current headband state
      var newState = JSONHelper.convertStringToHeadbandState(event.toString());
      if (this.headband != null) {
        this.headband = this.headband?..state = newState;
        notifyListeners();
      }
      return newState;
    });
    return headbandStateStream!;
  }

  @override
  Stream<dynamic> listenHardwareDataChanges() {
    /*
    return new Stream.fromFuture(rootBundle.loadString('assets/impedance.json'))
        .transform(json.decoder)
        .expand((jsonBody) => (jsonBody as Map)['data'])
        .map((jsonPlace) =>
    new EEGData.fromJson(jsonPlace));
    */

    if (headbandDataStream != null) {
      return headbandDataStream!;
    }
    headbandDataStream =
        eegEventChannel.receiveBroadcastStream().distinct().map((event) {
      var res;
      try {
        res = EEGData.fromJson(json.decode(event));
      } catch (e) {
        return 'Incorrect Data';
      }
      return res;
    });
    return headbandDataStream!;
  }

  @override
  Stream<String> listenHardwareBatteryLevel() {
    // ignore: close_sinks
    StreamController<String> _stream = StreamController.broadcast();
    return _stream.stream;
  }

  @override
  Stream eegDataStream() {
    if (eegStream != null) {
      return eegStream!;
    }
    eegStream = eegEventChannel.receiveBroadcastStream();
    return eegStream!;
  }

  @override
  Future<double> getBatteryLevel() async {
    dynamic batteryLevel =
        await methodChannel.invokeMethod(CGXMethods.getBatteryLevel);
    if (batteryLevel == null) {
      return 0;
    }
    return double.parse(batteryLevel.toString());
  }

  @override
  Future<BaseResponse<bool>> disconnectDevice() async {
    this.headband = null;
    final result = await methodChannel.invokeMethod(CGXMethods.disconnect);
    return BaseResponse<bool>.fromJson(jsonDecode(result), parseJson: (json) {
      if (json is bool) {
        return json;
      }
      return true;
    });
  }

  @override
  Future<bool> startEEGAcquisition() async {
    return await methodChannel.invokeMethod(CGXMethods.startEEGAcquisition);
  }

  @override
  Future<bool> stopEEGAcquisition() async {
    return await methodChannel.invokeMethod(CGXMethods.stopEEGAcquisition);
  }
}

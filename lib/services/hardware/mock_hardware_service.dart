import 'dart:async';
import 'package:brain_trainer/enum/bluetooth_state.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/models/base_response.dart';
import 'package:brain_trainer/models/headband_models/cgx_eeg_data_model.dart';
import 'package:brain_trainer/models/headband_models/cgx_headband.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import '../hardware/hardware_service.dart';
import '../../enum/headband_vibration_state.dart';

class MockHardwareService extends ChangeNotifier implements HardwareService {
  EventChannel eventChannel = EventChannel('dataStreamEvent');

  BluetoothState _bluetoothState = BluetoothState.unAuthorized;
  VibrationState _vibrationState = VibrationState.enabled;

  HeadbandState _headsetState = HeadbandState.disconnected;
  StreamController<HeadbandState> _headsetStateChangedController =
      StreamController<HeadbandState>.broadcast();
  StreamSubscription<dynamic>? calibrateSubscription;

  @override
  Stream<BluetoothState> getBluetoothState() {
    // ignore: close_sinks
    StreamController<BluetoothState> _headsetStateChangedController =
        StreamController<BluetoothState>.broadcast();
    return _headsetStateChangedController.stream;
  }

  @override
  Stream<List<CGXHeadband>> getAvailableDevices() {
    // ignore: close_sinks
    return StreamController<List<CGXHeadband>>.broadcast().stream;
    // return Stream.fromFuture(future)
  }

  @override
  Future<CGXHeadband?> getConnectedDevice() async {
    return CGXHeadband(
        deviceName: "ORIENT Connected", identifier: "1234orient");
  }

  @override
  Future<BaseResponse<bool>> connectWithDevice(CGXHeadband headband) {
    throw UnimplementedError();
  }

  @override
  void beginHardwareTest() {
    changeHardwareState(HeadbandState.testing);
  }

  @override
  StreamSubscription<dynamic> startHardwareCalibration() {
    calibrateSubscription =
        eventChannel.receiveBroadcastStream().listen((event) {
      return event;
    });
    return calibrateSubscription!;
  }

  @override
  void cancelHardwareCalibration() {
    calibrateSubscription?.cancel();
  }

  @override
  String changeVibrationState(String newState) {
    //Change state to newState
    return "Changed to newState";
  }

  @override
  Future<VibrationState> getVibrationState() async {
    return _vibrationState;
  }

  @override
  String vibrateHardware() {
    _vibrationState = VibrationState.vibrated;
    notifyListeners();
    return _vibrationState.message;
  }

  @override
  Stream<HeadbandState> listenHardwareConnectionState() {
    return _headsetStateChangedController.stream;
  }

  @override
  Stream<String> listenHardwareSensorsState() {
    // ignore: close_sinks
    StreamController<String> _stream = StreamController.broadcast();
    return _stream.stream;
  }

  @override
  Stream<EEGData?> listenHardwareDataChanges() {
    // ignore: close_sinks
    StreamController<EEGData?> _stream = StreamController.broadcast();
    return _stream.stream;
  }

  @override
  Stream<String> listenHardwareBatteryLevel() {
    // ignore: close_sinks
    StreamController<String> _stream = StreamController.broadcast();
    return _stream.stream;
  }

  @override
  Stream<String> getHardwareImpedance() {
    // ignore: close_sinks
    StreamController<String> _stream = StreamController.broadcast();
    return _stream.stream;
  }

  @override
  Future<HeadbandState> changeHardwareState(HeadbandState _newState) async {
    print('${this._headsetState} => $_newState');
    this._headsetState = _newState;
    _headsetStateChangedController.add(_newState);
    notifyListeners();
    return this._headsetState;
  }

  @override
  Stream eegDataStream() {
    // TODO: implement eegDataStream
    throw UnimplementedError();
  }

  @override
  void stopEEGDataStream() {
    // TODO: implement stopEEGDataStream
  }

  @override
  BluetoothState currentBluetoothState = BluetoothState.on;

  @override
  CGXHeadband? headband;

  @override
  Future<double> getBatteryLevel() {
    // TODO: implement getBatteryLevel
    throw UnimplementedError();
  }

  @override
  Future<BaseResponse<bool>> disconnectDevice() {
    // TODO: implement disconnectDevice
    throw UnimplementedError();
  }

  @override
  Future<bool> startEEGAcquisition() {
    // TODO: implement startEEGAcquisition
    throw UnimplementedError();
  }

  @override
  Future<bool> stopEEGAcquisition() {
    // TODO: implement stopEEGAcquisition
    throw UnimplementedError();
  }
}

import 'dart:async';
import 'package:intl/intl.dart';
import 'package:brain_trainer/config/common_functions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tuple/tuple.dart';

enum HardwareHeadsetEvent {
  connected,
  connecting,
  connectionFailed,
  disconnected,
  testing,
  testOk,
  testFailed,
}

extension HardwareHeadsetEventExtension on HardwareHeadsetEvent {
  String get message {
    switch (this) {
      case HardwareHeadsetEvent.connected:
        return 'Connected to headset.';
      case HardwareHeadsetEvent.connecting:
        return 'Connecting to headset...';
      case HardwareHeadsetEvent.connectionFailed:
        return 'Failed to connect to headset.';
      case HardwareHeadsetEvent.disconnected:
        return 'Headset disconnected.';
      case HardwareHeadsetEvent.testing:
        return 'Testing headset...';
      case HardwareHeadsetEvent.testOk:
        return 'Headset test complete.';
      case HardwareHeadsetEvent.testFailed:
        return 'Headset test failed.';
      default:
        return this.toString();
    }
  }
}

class HardwareHeadsetService extends ChangeNotifier {
  static final HardwareHeadsetService _singleton =
      new HardwareHeadsetService._internal();

  factory HardwareHeadsetService() {
    return _singleton;
  }

//  @visibleForTesting
  HardwareHeadsetService._internal() {
    // Map M4 state changes to transition events
    /*
    _transitionMap = {
      Tuple2(M4DeviceState.opening, M4DeviceState.opened): () =>
          _headsetEventStreamController.add(HardwareHeadsetEvent.connected),
      Tuple2(M4DeviceState.closed, M4DeviceState.opening): () =>
          _headsetEventStreamController.add(HardwareHeadsetEvent.connecting),
      Tuple2(M4DeviceState.opening, M4DeviceState.failedOpen): () =>
          _headsetEventStreamController
              .add(HardwareHeadsetEvent.connectionFailed),
      Tuple2(M4DeviceState.opened, M4DeviceState.disconnected): () =>
          _headsetEventStreamController.add(HardwareHeadsetEvent.disconnected),
      Tuple2(M4DeviceState.opened, M4DeviceState.measuringImpedance): () =>
          _headsetEventStreamController.add(HardwareHeadsetEvent.testing),
      Tuple2(M4DeviceState.measuringImpedance, M4DeviceState.opened): () =>
          _headsetEventStreamController.add(HardwareHeadsetEvent.testOk),
      Tuple2(
          M4DeviceState.measuringImpedance,
          M4DeviceState
              .failedImpedanceMeasure): () =>
          _headsetEventStreamController.add(HardwareHeadsetEvent.testFailed),
    };
      */

    /*
    _getPreferredHeadset().then((device) {
      headset = device;
    });

     */
  }

  // M4DeviceProxy? _headset;
  StreamSubscription<dynamic>? _headsetState;
  StreamSubscription<dynamic>? _headsetImpedance;
  // M4DeviceProxy? get headset => _headset;
  /*
  set headset(M4DeviceProxy? device) {
    if (device == _headset) return;

    _setPreferredHeadset(device).then((result) {
      if (result == true) {
        if (_headset != null) {
          if (_headset?.isOpen ?? false) {
            _headset?.close().then((value) {
              // Cancel subscriptions after close completes.
              _headsetState?.cancel();
              _headsetImpedance?.cancel();
            });
          } else {
            _headsetState?.cancel();
            _headsetImpedance?.cancel();
          }
        }

        resetLastImpedanceMeasurementLevel();

        _headset = device;

        if (_headset != null) {
          _headsetState = _headset?.onStateChanged.listen(_headsetStatusUpdate);
          _headsetImpedance =
              _headset?.onImpedanceMeasureEvent.listen(_impedanceMeasured);
          _headset?.open().catchError((e) {
            print('Error: $e');
            headset = null;
          });
        }
      } else {
        print('Unable to write headset hash to shared preferences.');

        if (_headset != null) {
          _headset?.close();
        }
        _headset = null;
      }

      // Notify stream listeners
      _headsetChangedStreamController.add(_headset);

      // Notify consumers
      notifyListeners();
    });
  }


  void _headsetStatusUpdate(M4DeviceState newState) {
    print('$_state -> $newState');

    if (_state != newState) {
      // Emit the transition event, if one exists
      _transitionMap?[Tuple2(_state, newState)]?.call();

      _state = newState;

      _headsetStateChangedStreamController.add(_state);

      // If the current headset disconnects, clear the current headset
      if (_state == M4DeviceState.disconnected) {
        headset = null;
      }
    }

    notifyListeners();
  }

  Map<Tuple2<M4DeviceState, M4DeviceState>, Function>? _transitionMap;

  StreamController<HardwareHeadsetEvent> _headsetEventStreamController =
      StreamController.broadcast();
  Stream<HardwareHeadsetEvent> get onHeadsetEvent =>
      _headsetEventStreamController.stream;
   */

  DateTime? _lastImpedanceMeasuredTime;
  DateTime? get lastImpedanceMeasuredTime => _lastImpedanceMeasuredTime;
  // M4Impedance? _lastImpedanceMeasured;
  // M4Impedance? get lastImpedanceMeasured => _lastImpedanceMeasured;

  /*
  String lastImpedanceMeasurementToString() {
    if (_lastImpedanceMeasured == null) {
      return 'Impedance has not been measured.';
    }
    var dateStr = _lastImpedanceMeasuredTime != null ? DateFormat('yyyy-MM-dd').format(_lastImpedanceMeasuredTime!) : "";
    var timeStr = _lastImpedanceMeasuredTime != null ? DateFormat.Hm().format(_lastImpedanceMeasuredTime!) : "";
    return 'Impedance last measured on $dateStr at $timeStr UTC - LF: ${_lastImpedanceMeasured?.lF}, RF: ${_lastImpedanceMeasured?.rF}, LP: ${_lastImpedanceMeasured?.lP}, RP: ${_lastImpedanceMeasured?.rP}';
  }

  void _impedanceMeasured(M4Event event) {
    var measurement = event.deviceEvent.impedance;
    _lastImpedanceMeasured = measurement;
    _lastImpedanceMeasuredTime = DateTime.now().toUtc();
    _lastImpedanceMeasurementLevel = getWorstImpedanceMeasurementLevel(
        [measurement.lP, measurement.rP, measurement.lF, measurement.rF]);
    _impedanceMeasureChangedStreamController
        .add(_lastImpedanceMeasurementLevel);
    notifyListeners();
  }

  void resetLastImpedanceMeasurementLevel() {
    _lastImpedanceMeasurementLevel = ImpedanceMeasurementLevel.unknown;
    _impedanceMeasureChangedStreamController
        .add(_lastImpedanceMeasurementLevel);
    notifyListeners();
  }

  ImpedanceMeasurementLevel _lastImpedanceMeasurementLevel =
      ImpedanceMeasurementLevel.unknown;
  ImpedanceMeasurementLevel get lastImpedanceMeasurementLevel =>
      _lastImpedanceMeasurementLevel;
  // ignore: close_sinks
  StreamController<ImpedanceMeasurementLevel>
      _impedanceMeasureChangedStreamController = StreamController.broadcast();
  Stream<ImpedanceMeasurementLevel> get onImpedanceMeasurementLevelChanged =>
      _impedanceMeasureChangedStreamController.stream;

  M4DeviceState _state = M4DeviceState.closed;
  M4DeviceState get state => _state;
  StreamController<M4DeviceState> _headsetStateChangedStreamController =
      StreamController.broadcast();
  Stream<M4DeviceState> get onHeadsetStateChanged =>
      _headsetStateChangedStreamController.stream;

  // TODO: How to close singleton sinks in Dart? (Likely not necessary.)
  // ignore: close_sinks
  StreamController<M4DeviceProxy?> _headsetChangedStreamController =
      StreamController.broadcast();
  Stream<M4DeviceProxy?> get onHeadsetChanged =>
      _headsetChangedStreamController.stream;


  static final _headsetHashKeyName = 'headset_hash';

  /// Sets a particular M4 device as the preferred M4 device
  Future<bool> _setPreferredHeadset(M4DeviceProxy? device) async {
    if (device == null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return await prefs.remove(_headsetHashKeyName);
    }

    var devices = await M4().devices;
    devices.removeWhere((key, value) {
      // TODO: Interrogate each device MAC address to filter all non-M4 devices
      return false;
    });

    if (devices.containsKey(device.hash)) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return await prefs.setString(_headsetHashKeyName, device.hash);
    }

    return false;
  }

  Future<M4DeviceProxy?> _getPreferredHeadset() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(_headsetHashKeyName)) {
      String? headsetHash = prefs.getString(_headsetHashKeyName);
      print('Read headset hash ($headsetHash) from shared preferences.');

      // Check that the preferred M4 headset is still bound to this machine
      var devices = (await M4().devices)
          .values
          .where((device) => (device.hash == headsetHash));
      if (devices.length != 0) {
        if (devices.length > 1) {
          throw ('Retrieved more than one headset with the same hash value!');
        }
        return devices.first;
      } else {
        print('The preferred headset no longer available.');
      }
    } else {
      print('No preferred headset configured.');
    }

    return null;
  }
   */
}

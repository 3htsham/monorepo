import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/models/headband_models/cgx_eeg_data_model.dart';
import 'package:brain_trainer/science/golf_signal_processing.dart';
import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
import 'package:brain_trainer/services/firebase/firebase_storage_service.dart';
import 'package:brain_trainer/services/firebase/firestore_db_service.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:brain_trainer/models/auth_user.dart';
import 'package:brain_trainer/models/golf_subject.dart';
import 'package:brain_trainer/models/training_program.dart';
import 'package:brain_trainer/models/training_session.dart';
import 'package:flutter/foundation.dart';
import 'package:just_audio/just_audio.dart';
import 'package:stats/stats.dart';
import 'package:path_provider/path_provider.dart';

enum TrainingGameState {
  error,
  choosing,
  playingSpaceGame,
  playingGraphGame,
  playingPreShotRoutine,
  paused,
  reporting
}

class TrainingService extends ChangeNotifier {
  static GolfSubject? currentSubject;
  FirestoreDBService? db;
  FirebaseAuthService? authService;
  HardwareService? headsetService;

  // static GolfSubject currentSubject; //Current user's information

  TrainingService(
      {@required this.db,
      @required this.authService,
      @required this.headsetService}) {
    initialize();
  }

  dispose() {
    super.dispose();

    player.stop();
    player.dispose();

    authUserStream?.cancel();
    headsetEEGSubscription?.cancel();

    _trainingStateStreamController.close();
    trainingSessionsStream.close();
    _zScoreStream.close();
  }

  int get zScoreThreshold => subjectTrainingProgram?.currentThreshold ?? 0;
  set zScoreThreshold(int value) =>
      subjectTrainingProgram?.currentThreshold = value;

  TrainingGameState _lastTrainingState = TrainingGameState.choosing;
  TrainingGameState get lastTrainingState => _lastTrainingState;

  StreamController<TrainingGameState> _trainingStateStreamController =
      StreamController.broadcast();
  Stream<TrainingGameState> get onTrainingStateChanged =>
      _trainingStateStreamController.stream;

  StreamController<List<TrainingSession>> trainingSessionsStream =
      StreamController.broadcast();

  StreamController<double> _zScoreStream = StreamController.broadcast();
  Stream<double> get onNewZScore => _zScoreStream.stream;

  var _eegDataLines = List<String>.empty(growable: true);
  final _csvHeaderEEG =
      'time, eeg1, eeg2, eeg3, eeg4, imp1, imp2, imp3, imp4, postFiltereeg';

  var _markerLines = List<String>.empty(growable: true);
  final _csvHeaderMarker = 'time, marker_text';
  final _csvHeaderBaseline = 'eeg';
  final _eegDataDir = '/EEGData';

  String trainingTypeString = "";
  TrainingType? _trainingType;
  FeedbackType? _feedbackType;
  DateTime? sessionStartTime, sessionEndTime;
  Duration? trainingDuration;
  StreamSubscription<AuthUser?>? authUserStream;
  List<TrainingSession>? subjectTrainingSessions;
  TrainingSession? inProgressSession;
  TrainingProgram? subjectTrainingProgram;
  AuthUser? user;
  GolfSubject? subject;
  StreamSubscription<dynamic>? headsetEEGSubscription;
  List<double>? currentSessionZScores;
  bool isAcquiring = false;

  bool recordingRound = false;

  GolfSignalProcessor processor = GolfSignalProcessor();

  bool get _isHeadsetReady => _isHeadsetAvailable && _isHeadsetCalibrated;
  bool _isHeadsetAvailable = false;
  bool get isHeadsetAvailable => _isHeadsetAvailable;
  bool _isHeadsetCalibrated = false;
  bool get isHeadsetCalibrated => _isHeadsetCalibrated;

  bool _feedbackActive = false;
  bool get feedbackOn => _feedbackActive;
  set feedbackOn(bool value) {
    print("TrainingService: Feedback is $value");
    _feedbackActive = value;
  }

  // audio stuff
  final player = AudioPlayer();

  void _gameStateTransitionTo(TrainingGameState state) {
    print(
        'training service: state change from ${_lastTrainingState.toString()} to ${state.toString()}');
    _lastTrainingState = state;
    _trainingStateStreamController.add(_lastTrainingState);
    notifyListeners();
  }

  void chooseTraining() {
    _gameStateTransitionTo(TrainingGameState.choosing);
  }

  void playGraphGame() {
    startTraining(
        TrainingType.trainingType_Graph, FeedbackType.feedbackType_Visual);
    _gameStateTransitionTo(TrainingGameState.playingGraphGame);
  }

  void playSpaceGame() {
    startTraining(
        TrainingType.trainingType_SpaceGame, FeedbackType.feedbackType_Visual);
    _gameStateTransitionTo(TrainingGameState.playingSpaceGame);
  }

  void playPreShotRoutine() {
    // this call is moved to when the user start PPR after instructions
    startTraining(
        TrainingType.trainingType_PSR, FeedbackType.feedbackType_Haptic);
    _gameStateTransitionTo(TrainingGameState.playingPreShotRoutine);
  }

  void donePlaying(String notes) {
    endTraining(notes)
        .then((value) => _gameStateTransitionTo(TrainingGameState.reporting));
  }

  void gameDone() {
    endTraining('NOTES')
        .then((value) => _gameStateTransitionTo(TrainingGameState.reporting));
  }

  bool get canPlayAgain {
    if (inProgressSession == null) {
      return false;
    }

    return (inProgressSession?.trainingType !=
        TrainingSession.trainingTypesStrings[TrainingType.trainingType_PSR]);
  }

  void _updateHeadsetStatus() {
    if (!_isHeadsetReady) {
      _gameStateTransitionTo(TrainingGameState.error);
    } else {
      _gameStateTransitionTo(TrainingGameState.choosing);
    }
  }

  initialize() {
    print('TrainingService - Training Singleton Created');
    trainingDuration = Duration(seconds: 90);
    authUserStream =
        authService?.onAuthStateChanged?.listen(userChangeListener);

    headsetEEGSubscription =
        headsetService!.eegDataStream().listen(eegStreamListener);
    stopAcquisition();
    currentSessionZScores = <double>[];

    recordingRound = false;

    _trainingStateStreamController.add(_lastTrainingState);

    player.setAsset(AppAssets.feedbackTone);

    _updateHeadsetStatus();
  }

  void init() {
    print('TrainingService - Training Init');
  }

  void userChangeListener(
    AuthUser? _user,
  ) {
    print('TrainingService - Training - User Changed!');

    if (_user != null) {
      user = _user;

      db?.getGolfSubject(user?.uid ?? "").then((value) async {
        if (value == null) {
          //await buildNewSubject(db!);
          // comment this because we will create when sign up, not when user change
          print("Training Service - NULL Subject!");
        } else {
          subject = value;
          await loadSubjectData(db!);
        }
        notifyListeners();
      });
    } else {
      print("TrainingManager: User is null");
    }
    notifyListeners();
  }

  Future loadSubjectData(FirestoreDBService db) async {
    print("TrainingManager: Existing Subject");
    subjectTrainingProgram = await db.getTrainingProgram(subject?.userAuthID);
    subjectTrainingSessions = await db.getSubjectSessions(subject?.userAuthID);
    trainingSessionsStream.add(subjectTrainingSessions!);
  }

  Future buildNewSubject(FirestoreDBService db,
      {String firstName = "",
      String lastName = "",
      DateTime? birthDay,
      double handicap = 0,
      int? handicapGoal,
      String? homeCourse,
      bool isLeftHand = false}) async {
    {
      print('TrainingService - Auto generating new subject');
      subject = GolfSubject.buildDefaultSubject(user,
          firstName: firstName, lastName: lastName, birthDay: birthDay);
      await db.createGolfSubject(subject!);

      print('TrainingService - Auto generating new training program');
      subjectTrainingProgram = TrainingProgram.buildDefaultProgram(
          subject?.userAuthID ?? "",
          handicap: handicap,
          handicapGoal: handicapGoal,
          homeCourse: homeCourse,
          isLeftHand: isLeftHand);
      await db.createTrainingProgram(subjectTrainingProgram!);
    }
  }

  Future<dynamic> getUserTrainingProgram(FirestoreDBService db) async {
    try {
      subject = await db.getGolfSubject(user?.uid ?? "");
      var tpDoc = await db.getTrainingProgram(subject?.userAuthID);
      subjectTrainingProgram = tpDoc;
      return subjectTrainingProgram;
    } catch (e) {
      throw ('message: $e');
    }
  }

  Future<dynamic> updateUserTrainingProgram(
      FirestoreDBService db, TrainingProgram _trainingProgram) async {
    try {
      subject = await db.getGolfSubject(user?.uid ?? "");
      return await db.updateTrainingProgram(_trainingProgram);
    } catch (e) {
      throw ('message: $e');
    }
  }

  void startTraining(TrainingType type, FeedbackType feedback) async {
    print("TrainingManager: Start Training");

    headsetService?.startEEGAcquisition();

    currentSessionZScores?.clear();

    _trainingType = type;

    if(feedbackOn)
      _feedbackType = FeedbackType.feedbackType_Audio;
    else
      _feedbackType = FeedbackType.feedbackType_Visual;

    //sessionStartTime = DateTime.now();

//    inProgressSession = await TrainingSession.buildSession(
//        user?.uid ?? "", _trainingType, _feedbackType, zScoreThreshold);
//
//    startAcquisition();

    notifyListeners();

    _eegDataLines.clear();
    _eegDataLines.add(_csvHeaderEEG);

    _markerLines.clear();
    _markerLines.add(_csvHeaderMarker);

    String? marker = 'Starting Training ' +
        TrainingSession.trainingTypesStrings[_trainingType]!;
    sendMarker(marker);
  }

  void startAcquisition() {
    isAcquiring = true;
  }

  Future<void> endTraining(String _notes) async {
    if (isAcquiring == false) {
      return;
    }

    headsetService?.stopEEGAcquisition();

    stopAcquisition();

    print(
        "TrainingManager: End Training with ${currentSessionZScores!.length} samples");

    sessionEndTime = DateTime.now();
    sendMarker('End Training ' + inProgressSession!.trainingType!);

    await _writeEEGDataFile(_eegDataDir, user!, sessionStartTime!,
        inProgressSession!.trainingType!);

    FirebaseStorageService fs = FirebaseStorageService();
    String _fileKey = await fs.getLatestFileRoot(_eegDataDir);

    inProgressSession!.endSession(_fileKey, _notes,
        sessionAverageZScore.round(), sessionTimeInZone, currentSessionZScores);

    await db!.createTrainingSession(inProgressSession!);

    subjectTrainingProgram!.updatePostSession(inProgressSession!);
    await db!.updateTrainingProgram(subjectTrainingProgram!);

    subjectTrainingSessions = await db?.getSubjectSessions(subject?.userAuthID);
    trainingSessionsStream.add(subjectTrainingSessions!);

    fs.syncDataFiles(subject!.email, _eegDataDir);

    notifyListeners();
  }

  Future<void> cancelTraining() async {
    print('Cancel Training');

    if (isAcquiring == false) {
      return;
    }

    headsetService?.stopEEGAcquisition();

    stopAcquisition();

    if(feedbackOn) {
      player.stop();
    }

    //_gameStateTransitionTo(TrainingGameState.choosing);
  }

  void stopAcquisition() {
    isAcquiring = false;
  }

  startSession() async {
    sendMarker('Start Session');
    currentSessionZScores?.clear();
    recordingRound = true;

    sessionStartTime = DateTime.now();

    inProgressSession = await TrainingSession.buildSession(
        user?.uid ?? "", _trainingType!, _feedbackType!, zScoreThreshold);

    if(feedbackOn) {
      player.play();
      player.setLoopMode(LoopMode.all);
    }


      startAcquisition();

  }

  endSession() {
    sendMarker('End Session');
    recordingRound = false;
    if(feedbackOn)    {    player.stop(); }
  }

  Future<void> buildDebugTrainingSession(String userAuthID) async {
    var program = subjectTrainingProgram;
    var rnd = Random();
    print('TrainingService - Build Debug Training Session');
    TrainingSession ts = await TrainingSession.buildSession(userAuthID,
        TrainingType.trainingType_Graph, FeedbackType.feedbackType_Visual, 50);
    ts.endTime = ts.startTime?.add(Duration(minutes: 10));
    ts.endSession("NO FILE", "DEBUG SESSION", rnd.nextInt(100),
        Duration(seconds: 10), null);

    await db?.createTrainingSession(ts);

    print('TrainingService - Updating the program');
    subjectTrainingProgram?.updatePostSession(ts);
    if (program != null) {
      await db?.updateTrainingProgram(program);
    }

    print('TrainingService - adding fake putts');
    await db?.createTestPutts(ts);

    subjectTrainingSessions = await db?.getSubjectSessions(subject?.userAuthID);

    notifyListeners();
  }

  Future<void> debugResetUserData() async {
    // await db?.deleteTrainingSessionsForUser(user.uid);
    // await db?.deleteTrainingProgram(subjectTrainingProgram);
    // await db?.deleteGolfSubject(subject);

    // subject = null;
    // subjectTrainingProgram = null;
    // inProgressSession = null;

    // // rebuild the stuff you deleted!
    // await buildNewSubject(db);

    notifyListeners();
  }

  double get sessionAverageZScore {
    if (currentSessionZScores == null || currentSessionZScores!.length == 0) {
      print('TrainingService - No Z session Scores?');
      return 0.0;
    }

    print('TrainingService - zScores length ' +
        currentSessionZScores!.length.toString());
    var stats = Stats.fromData(currentSessionZScores!);
    return stats.average as double;
  }

  Duration get sessionTime {
    return inProgressSession!.endTime!
        .difference(inProgressSession!.startTime!);
  }

  Duration get sessionTimeInZone {
    if (currentSessionZScores == null || currentSessionZScores?.length == 0) {
      print('TrainingService - No session Z Scores?');
      return Duration(seconds: 0);
    }

    int count = currentSessionZScores!
        .where((element) => (element) >= zScoreThreshold)
        .toList()
        .length;
    double dTime =
        sessionEndTime!.difference(sessionStartTime!).inMilliseconds.toDouble();
    dTime =
        dTime * (count.toDouble() / currentSessionZScores!.length.toDouble());
    return Duration(milliseconds: dTime.round());
  }

  void setDebugGameTime(int newTime) {
    /*
    trainingDuration = Duration(minutes: newTime);
    if (newTime == 3) {
      pprDuration = 3;
      numPPRRounds = 8;
    } else {
      pprDuration = 1;
      numPPRRounds = 2;
    }
    notifyListeners();

     */
  }

  void sendMarker(String marker) {
    _markerLines.add('${DateTime.now().toIso8601String()},$marker');
    print('TrainingService: Marker - $marker');
  }

  void pauseRound() {
    print('TrainingService - Pausing Round');
  }

  void resumeRound() {
    print('TrainingService - Resuming Round');
  }

  void startProcessorBaseline() {
    processor.startBaseLine();
    startAcquisition();
  }

  void endProcessorBaseline() {
    stopAcquisition();
  }

  void eegStreamListener(event) {
    if (isAcquiring == false) return;

    //print('Got one');
    double _zScore;
    var eegData = EEGData.fromJson(jsonDecode(event));

    _zScore = processor.addSample(eegData.eeg!);

    //print(eegData.eeg?[3]);

    if (_zScore.isNaN) {
      _zScore = 0.0;
    }

    if (_zScore.isInfinite) {
      _zScore = 0.0;
    }

    _zScore = _zScore.clamp(0.0, 100.0);

    var postFilter = processor.postFilterEEG;

    _storeEEGData(eegData, postFilter);

    double lastZScore =
        currentSessionZScores != null && currentSessionZScores!.isNotEmpty
            ? currentSessionZScores!.last
            : 0.0;

    currentSessionZScores?.add(_zScore);

    _zScoreStream.add(_zScore);

    if ((lastZScore).round() < zScoreThreshold &&
        (_zScore).round() >= zScoreThreshold) {
      sendMarker('Over Threshold');
    }

    if ((lastZScore).round() >= zScoreThreshold &&
        (_zScore).round() < zScoreThreshold) {
      sendMarker('Under Threshold');
    }

    // record meta data
    if (lastZScore != _zScore) {
      String threshold = (_zScore).round() < zScoreThreshold
          ? 'Under Threshold'
          : 'Over Threshold';
      sendMarker(
          'MetaData: SD: ${processor.baselineSD}  M: ${processor.baselineMean}  A: ${processor.lastAlphaT}  $threshold');
    }

    // feedback
    if(feedbackOn) {

      var volume = (_zScore.toDouble() / zScoreThreshold.toDouble());

      volume = volume.clamp(0.0, 1.0);

      // volume goes down as you get better
      // OG-139
      player.setVolume(1.0 - volume);
    }
  }

  void _storeEEGData(EEGData eegData, double postFilter) {
    String line = DateTime.now().toIso8601String() +
        ',' +
        eegData.eeg![0].toString() +
        ',' +
        eegData.eeg![1].toString() +
        ',' +
        eegData.eeg![2].toString() +
        ',' +
        eegData.eeg![3].toString() +
        ',' +
        eegData.impedance![0].toString() +
        ',' +
        eegData.impedance![1].toString() +
        ',' +
        eegData.impedance![2].toString() +
        ',' +
        eegData.impedance![3].toString() +
        ',' +
        postFilter.toString();

    _eegDataLines.add(line);
  }

  Future<void> _writeEEGDataFile(String path, AuthUser user,
      DateTime sessionStartTime, String trainingType) async {
    String fileName = user.displayName.replaceAll(" ", '_') +
        '_' +
        trainingType +
        '_' +
        sessionStartTime.toIso8601String() +
        '.csv';

    print('EEGData: writing ${_eegDataLines.length} lines to $fileName');

    Directory appDocDirectory = await getLibraryDirectory();

    // check for the subdirectory, create if necessary
    if (Directory(appDocDirectory.path + path).existsSync()) {
      appDocDirectory = Directory(appDocDirectory.path + path);
    } else {
      appDocDirectory = await Directory(appDocDirectory.path + path).create();
    }

    // write the EEG file
    final eegFile = File('${appDocDirectory.path}/$fileName').openWrite();

    _eegDataLines.forEach((element) {
      eegFile.writeln(element);
    });

    eegFile.close();

    // write the Marker file
    fileName = user.displayName.replaceAll(" ", '_') +
        '_' +
        trainingType +
        '_' +
        sessionStartTime.toIso8601String() +
        '_markers' +
        '.csv';

    final markerFile = File('${appDocDirectory.path}/$fileName').openWrite();
    _markerLines.forEach((element) {
      markerFile.writeln(element);
    });
    markerFile.close();

    // write the baseline file
    fileName = user.displayName.replaceAll(" ", '_') +
        '_' +
        trainingType +
        '_' +
        sessionStartTime.toIso8601String() +
        '_baseline' +
        '.csv';
    final baselineFile = File('${appDocDirectory.path}/$fileName').openWrite();

    baselineFile.writeln(this._csvHeaderBaseline);

    var baselineEEG = processor.baselineEEGSamples;
    baselineEEG.forEach((element) {
      String output = element.toString();
      baselineFile.writeln(output);
    });
    baselineFile.close();
  }
}

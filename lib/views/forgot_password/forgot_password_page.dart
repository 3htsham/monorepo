import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/custom_text_form_field.dart';
import 'package:brain_trainer/elements/keyboard_dismiss.dart';
import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:brain_trainer/helper/ValidateHelper.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'forgot_password_provider.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final FocusNode _emailFocus = FocusNode();

  @override
  void initState() {
    _formKey = GlobalKey<FormState>();
    super.initState();
  }

  bool isValid() => _formKey.currentState?.validate() ?? true;

  @override
  void dispose() {
    // _currentForm = null;
    _emailFocus.dispose();
    super.dispose();
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: KeyboardDismiss(
        child: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Center(
                  child: Form(
                    key: this._formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          S.resetPassword,
                          style: textTheme.headline1,
                        ),
                        SizedBox(height: 35,),
                        _buildEmailForm(context),
                        SizedBox(height: 28,),
                        context.watch<ForgotPasswordProvider>().isSending
                            ? SizedBox(
                          width: 452,
                            height: 56,
                            child: Center(child: CircularProgressIndicator()))
                            : _buildSendEmailButton(context, textTheme),
                        SizedBox(height: 35,),
                        _buildReturnToLogin(context, textTheme, theme)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildEmailForm(BuildContext context) {
    return CustomTextFormField(
      controller: context.watch<ForgotPasswordProvider>().emailController,
      focusNode: _emailFocus,
      width: 452,
      hint: S.emailAddress,
      inputType: TextInputType.emailAddress,
      validator: ValidateHelper.validateEmail,
      prefixIcon: IconButton(
        icon: new Image.asset(
          'assets/icons/person.png',
          width: 18.0,
          height: 18.0,
        ),
        onPressed: null,
      ),
      onSubmitted: (term) {
        _fieldFocusChange(context, _emailFocus, FocusNode());
      },
      inputAction: TextInputAction.next,
    );
  }

  Widget _buildReturnToLogin(
      BuildContext context, TextTheme textTheme, ThemeData theme) {
    return SizedBox(
      width: 452,
      child: Center(
        child: InkWell(
          onTap: () {
            UIHelper.hideKeyboard(context);
            Navigator.of(context).pop();
          },
          splashColor: Colors.white.withOpacity(0.3),
          highlightColor: Colors.white.withOpacity(0.3),
          child: Text(
            S.returnToLogin,
            style: textTheme.subtitle2?.merge(TextStyle(color: theme.accentColor)),
          ),
        ),
      ),
    );
  }

  Widget _buildSendEmailButton(BuildContext context, TextTheme textTheme) {
    return SizedBox(
      height: 56,
      width: 452,
      child: RaisedButton(
        onPressed: () async => sendEmailButtonAction(context),
        shape: StadiumBorder(),
        child: Center(
          child: Text(
            S.emailMeARecoveryLink.toUpperCase(),
            style: textTheme.headline6,
          ),
        ),
      ),
    );
  }

  Future<void> sendEmailButtonAction(BuildContext context) async {
    UIHelper.hideKeyboard(context);
    if (this.isValid()) {
      var result =
      await context.read<ForgotPasswordProvider>().sendLinkToEmail();
      if (result is String) {
        // got an error
        UIHelper.showErrorDialog(result, context);
      } else if (result == null) {
        // link sent to email
        Navigator.of(context).pushReplacementNamed(Routes.RECOVER_PASSWORD, arguments: context.read<ForgotPasswordProvider>().emailController.text);
      }
    }
  }

}

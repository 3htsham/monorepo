import 'package:flutter/material.dart';

import '../../services/firebase/firebase_auth_service.dart';

class ForgotPasswordProvider extends ChangeNotifier {
  final TextEditingController emailController = TextEditingController();
  FirebaseAuthService? firebaseAuthService;

  ForgotPasswordProvider({this.firebaseAuthService});
  bool isSending = false;
  @override
  void dispose() {
    emailController.dispose();
    super.dispose();
  }

  Future<dynamic> sendLinkToEmail() async {
    isSending = true;
    notifyListeners();
    var result = await firebaseAuthService?.sendPasswordResetLink(emailController.text);
    isSending = false;
    notifyListeners();
    return result;
  }
}

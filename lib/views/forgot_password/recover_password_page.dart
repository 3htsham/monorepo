import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'forgot_password_provider.dart';

class RecoverPasswordPage extends StatefulWidget {
  final String userEmail;

  RecoverPasswordPage({this.userEmail = ""});

  @override
  _RecoverPasswordPageState createState() => _RecoverPasswordPageState();
}

class _RecoverPasswordPageState extends State<RecoverPasswordPage> {

  @override
  void initState() {
    super.initState();
    context.read<ForgotPasswordProvider>().emailController.text = widget.userEmail;
  }
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Stack(
          children: <Widget>[

            Positioned(
              left: 20,
              top: 20,
              child: IconButton(
                onPressed: (){
                  Navigator.of(context).pop();
                },
                highlightColor: Colors.white.withOpacity(0.16),
                icon: Icon(
                  CupertinoIcons.clear,
                  color: Colors.white,
                ),
              ),
            ),

            Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      S.resetPassword,
                      style: textTheme.headline1,
                    ),
                    SizedBox(height: 35,),
                    Text(
                      widget.userEmail,
                      style: textTheme.bodyText1?.copyWith(fontSize: 16),
                    ),
                    SizedBox(height: 17,),
                    Text(
                      S.ifAccountExistsAnEmailWillBeSentWithFurtherInstructions,
                      style: textTheme.bodyText1?.copyWith(fontSize: 16),
                    ),
                    SizedBox(height: 17,),

                    context.watch<ForgotPasswordProvider>().isSending
                        ? SizedBox(
                        width: 334,
                        height: 56,
                        child: Center(child: CircularProgressIndicator()))
                        : _buildResendEmailButton(context, textTheme),

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildResendEmailButton(BuildContext context, TextTheme textTheme) {
    return SizedBox(
      height: 56,
      width: 334,
      child: ElevatedButton(
        onPressed: () async => resendEmailButtonAction(context),
        style: ElevatedButton.styleFrom(
          shape: StadiumBorder(),
        ),
        child: Center(
          child: Text(
            S.resendTheEmail.toUpperCase(),
            style: textTheme.headline6,
          ),
        ),
      ),
    );
  }

  Future<void> resendEmailButtonAction(BuildContext context) async {
    UIHelper.hideKeyboard(context);
    var result =
    await context.read<ForgotPasswordProvider>().sendLinkToEmail();
    if (result is String) {
      // got an error
      UIHelper.showErrorDialog(result, context);
    } else if (result == null) {
      // link sent to email
      UIHelper.showAlertDialog(context, S.emailSent, "Reset password link successfully sent\nto ${context.read<ForgotPasswordProvider>().emailController.text}", (){
        Navigator.of(context).pop();
      });
    }
  }
}

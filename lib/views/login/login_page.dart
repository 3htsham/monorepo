import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/custom_text_form_field.dart';
import 'package:brain_trainer/elements/keyboard_dismiss.dart';
import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:brain_trainer/helper/ValidateHelper.dart';
import 'package:brain_trainer/models/auth_user.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'login_provider.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  _LoginPageState();

  ///Additions in update
  bool obscureText = true;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  @override
  void initState() {
    _formKey = GlobalKey<FormState>();
    super.initState();
  }

  bool isValid() => _formKey.currentState?.validate() ?? false;

  @override
  void dispose() {
    // _currentForm = null;
    _emailFocus.dispose();
    _passwordFocus.dispose();
    super.dispose();
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: KeyboardDismiss(
        child: SafeArea(
            child: Center(
              child: SingleChildScrollView(
                child: Container(
                  child: Center(
                    child: Form(
                      key: this._formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            S.welcome.toLowerCase(),
                            style: textTheme.headline1,
                          ),
                          Text(
                            S.pleaseLoginToContinue,
                            style: textTheme.bodyText1,
                          ),
                          SizedBox(
                              height: 29),
                          _buildEmailForm(context),
                          SizedBox(
                              height: 10,
                          ),
                          _buildPasswordForm(context),
                          SizedBox(
                              height: 29),
                          context.watch<LoginProvider>().isLoggingIn
                              ? CircularProgressIndicator()
                              : _buildLoginButton(context, textTheme),
                          SizedBox(
                              height: 24),
                          _buildForgotPassword(context, textTheme, theme),
                          SizedBox(
                            height: 64,
                          ),
                          Text(
                            S.doNotHaveAnAccount,
                            style: textTheme.subtitle1,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          _buildSignUpButton(context, textTheme, theme),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )),
      ),
    );
  }

  InkWell _buildSignUpButton(
      BuildContext context, TextTheme textTheme, ThemeData theme) {
    return InkWell(
      onTap: () {
        UIHelper.hideKeyboard(context);
        Navigator.of(context).pushNamed(Routes.SIGNUP);
      },
      splashColor: Colors.white.withOpacity(0.3),
      highlightColor: Colors.white.withOpacity(0.3),
      child: Text(
        S.createNewAccount,
        style: textTheme.bodyText1?.merge(TextStyle(color: theme.accentColor, fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget _buildEmailForm(BuildContext context) {
    return CustomTextFormField(
      controller: context.watch<LoginProvider>().emailController,
      focusNode: _emailFocus,
      width: 334,
      hint: S.emailAddress,
      inputType: TextInputType.emailAddress,
      validator: ValidateHelper.validateEmail,
      prefixIcon: IconButton(
        icon: new Image.asset(
          'assets/icons/person.png',
          width: 18.0,
          height: 18.0,
        ),
        onPressed: null,
      ),
      onSubmitted: (term) {
        _fieldFocusChange(context, _emailFocus, _passwordFocus);
      },
      inputAction: TextInputAction.next,
    );
  }

  Widget _buildPasswordForm(BuildContext context) {
    return CustomTextFormField(
      controller: context.watch<LoginProvider>().passwordController,
      focusNode: _passwordFocus,
      width: 334,
      hint: S.password,
      obscureText: this.obscureText,
      inputType: TextInputType.text,
      validator: (value) => (value?.isEmpty ?? true) ? S.passwordCantBeEmpty : null,
      prefixIcon: IconButton(
        icon: new Image.asset(
          'assets/icons/key.png',
          width: 20.0,
          height: 20.0,
        ),
        onPressed: null,
      ),
      suffixIcon: IconButton(
        icon: Icon(
          this.obscureText ? Icons.visibility : Icons.visibility_off,
          color: Colors.white,
        ),
        onPressed: () {
          setState(() {
            this.obscureText = !this.obscureText;
          });
        },
      ),
      onSubmitted: (term) {
        _formKey.currentState?.validate();
      },
      inputAction: TextInputAction.done,
    );
  }

  Widget _buildForgotPassword(
      BuildContext context, TextTheme textTheme, ThemeData theme) {
    return InkWell(
      onTap: () {
        UIHelper.hideKeyboard(context);
        Navigator.of(context).pushNamed(Routes.FORGOT_PASSWORD);
      },
      splashColor: Colors.white.withOpacity(0.3),
      highlightColor: Colors.white.withOpacity(0.3),
      child: Text(
        S.forgetPassword,
        style: textTheme.bodyText1?.merge(TextStyle(color: theme.accentColor, fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget _buildLoginButton(BuildContext context, TextTheme textTheme) {
    return SizedBox(
      height: 56,
      width: 334,
      child: ElevatedButton(
        onPressed: () async => loginButtonAction(context),
        style: ElevatedButton.styleFrom(
          shape: StadiumBorder(),
        ),
        child: Center(
          child: Text(
            S.logIn.toUpperCase(),
            style: textTheme.headline6,
          ),
        ),
      ),
    );
  }

  Future<void> loginButtonAction(BuildContext context) async {
    UIHelper.hideKeyboard(context);
    if (this._formKey.currentState?.validate() ?? false) {
      var result =
          await context.read<LoginProvider>().signInWithEmailPassword();
      if (result is String) {
        if (result == ErrorMessage.emailNotVerified) {
          //send a verification email again
          context.read<FirebaseAuthService>().sendVerificationEmail();
          UIHelper.showFullScreenErrorDialog(context, S.verifyEmailSentTitle, S.verifyEmailSentBody, onClose: () {
            Navigator.of(context).pop();
          });
        } else {
          if(result.toLowerCase().contains('password')) {
            //Password Error
            UIHelper.wrongPasswordDialog(context, S.opps, S.wrongPasswordProvided, onClose: () {
              Navigator.of(context).pop();
            }, onForgotPassword: (){
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed(Routes.FORGOT_PASSWORD);
            }, onTryAgain: (){
              Navigator.of(context).pop();
            });
          } else if(result.toLowerCase().contains('email')) {
            //Email Error
            UIHelper.showFullScreenErrorDialog(context, S.wrongEmail, result, onClose: () {
              Navigator.of(context).pop();
            });
          } else {
            UIHelper.showFullScreenErrorDialog(context, '', result, onClose: () {
              Navigator.of(context).pop();
            });
          }
        }
      } else if (result is AuthUser) {
        // sign in success
        Navigator.of(context).pushNamed(Routes.MAIN);
      }
    }
  }
}

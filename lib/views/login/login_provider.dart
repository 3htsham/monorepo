import 'package:flutter/material.dart';

import '../../services/firebase/firebase_auth_service.dart';

class LoginProvider extends ChangeNotifier {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  FirebaseAuthService? firebaseAuthService;

  LoginProvider({this.firebaseAuthService});
  bool isLoggingIn = false;
  @override
  void dispose() {
    // TODO: implement dispose
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  Future<dynamic> signInWithEmailPassword() async {
    isLoggingIn = true;
    notifyListeners();
    var result = await firebaseAuthService?.signInWithEmailPassword(
        emailController.text, passwordController.text);
    isLoggingIn = false;
    notifyListeners();
    return result;
  }
}

import 'dart:async';
import 'dart:ui';
import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/enum/bluetooth_state.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/routes/route_generator.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/helper/helpers.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:brain_trainer/views/main_page/main_page_provider.dart';
import 'package:brain_trainer/views/main_tabs/hardware_tab/hardware_tab_page.dart';
import 'package:brain_trainer/views/main_tabs/reports_tab/reports_tab_page.dart';
import 'package:brain_trainer/views/main_tabs/resources_tab/resources_tab_page.dart';
import 'package:brain_trainer/views/main_tabs/support_tab/support_tab_page.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/training_tab_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:brain_trainer/elements/responsive.dart';
import 'package:provider/provider.dart';

class TabData {
  final String title;
  final String label;
  final String assetIcon;
  final GlobalKey<NavigatorState>? navigatorState;
  final Route<dynamic> Function(RouteSettings)? onGenerateRoute;
  final String? initialRoute;

  TabData(this.label, this.assetIcon,
      {this.title = "",
      this.navigatorState,
      this.onGenerateRoute,
      this.initialRoute});
}

GlobalKey<MainTabsState> mainTabsKey = GlobalKey<MainTabsState>();

class MainPage extends StatefulWidget {
  static const routeName = '/main_page';

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _getCurrentUser();
    super.initState();

    ///Show low battery alert
    /* TODO: showthis somewhere else */
    // _showBatteryLowOverlay();
  }

  Future<void> _getCurrentUser() async {
    FirebaseAuthService.user = await FirebaseAuthService().currentUser();
    print("user");
  }

  _showBatteryLowOverlay() async {
    Future.delayed(Duration(seconds: 1), () async {
      var battery = await context
          .read<MainPageProvider>()
          .hardwareService
          ?.getBatteryLevel();
      double _batteryLevel = double.parse(battery.toString());
      if (_batteryLevel <= 45) {
        showDialog(
          context: context,
          builder: (context) {
            return Helpers.overlayChargeBattery(context, _batteryLevel,
                onClose: () {
              Navigator.of(context).pop();
            });
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var isDeviceConnected = true;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: this._scaffoldKey,
      body: Stack(
        children: <Widget>[
          MainTabs(
            key: AppKeys.mainTabsKey,
          )
        ],
      ),
    );
  }
}

class MainTabs extends StatefulWidget {
  const MainTabs({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => MainTabsState();
}

class MainTabsState extends State<MainTabs> {
  int _selectedIndex = 0;
  StreamSubscription? headsetStateSubscription;
  StreamSubscription? bluetoothStateSubscription;

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  List<TabData> _tabs = <TabData>[
    TabData(HardwareTabPage.tabName, "assets/icons/hardware_icon.png",
        navigatorState: AppKeys.hardwareNavigatorKey,
        onGenerateRoute: RouteGenerator.hardwareRoute,
        initialRoute: Routes.HARDWARE_SETUP_HOME),
    TabData(TrainingTabPage.tabName, "assets/icons/traning_icon.png",
        navigatorState: AppKeys.trainingNavigatorKey,
        onGenerateRoute: RouteGenerator.trainingRoute,
        initialRoute: Routes.TRAINING_HOME),
    TabData(ReportsTabPage.tabName, "assets/icons/reports_icon.png",
        navigatorState: AppKeys.reportsNavigatorKey,
        onGenerateRoute: RouteGenerator.reportRoute,
        initialRoute: Routes.GAMESESSIONRESULT),
    TabData(SupportTabPage.tabName, "assets/icons/support_icon.png",
        navigatorState: AppKeys.supportNavigatorKey,
        onGenerateRoute: RouteGenerator.supportRoute,
        initialRoute: Routes.SUPPORTHOME),
    TabData(ResourcesTabPage.tabName, "assets/icons/resources_icon.png",
        navigatorState: AppKeys.resourcesNavigatorKey,
        onGenerateRoute: RouteGenerator.resourcesRoute,
        initialRoute: Routes.RESOURCESHOME),
  ];

  static Map<String, int> _tabIndexLookupByName = {
    'hardware': 0,
    'training': 1,
    'reports': 2,
    'support': 3,
    'resources': 4,
  };

  @override
  void initState() {
    super.initState();
    listenBluetoothState();

    ///Show Welcome Overlay
    // _showWelcomeOverlay();

    // M4().getBluetoothState();
  }

  HeadbandState? lastState;
  BluetoothState? lastBluetoothState;
  listenHardwareState() {
    headsetStateSubscription = context
        .read<HardwareService>()
        .listenHardwareConnectionState()
        .listen((event) {
      print('LAST STATE $event');
      if (this.lastState == HeadbandState.connected && event != lastState) {
        //Display Disconnect dialog
        _showHardwareConnectionResetOverlay();
      }
      this.lastState = event;
    });
  }

  listenBluetoothState() {
    bluetoothStateSubscription =
        context.read<HardwareService>().getBluetoothState().listen((event) {
      if (this.lastBluetoothState == BluetoothState.on &&
          event != BluetoothState.on &&
          context.read<HardwareService>().headband?.state ==
              HeadbandState.connected) {
        _showHardwareConnectionResetOverlay();
      }
      this.lastBluetoothState = event;
    });
  }

  @override
  void dispose() {
    headsetStateSubscription?.cancel();
    bluetoothStateSubscription?.cancel();
    super.dispose();
  }

  selectTabByName(String name) {
    if (_tabIndexLookupByName.containsKey(name)) {
      onTabTapped(_tabIndexLookupByName[name] ?? 0);
    }
  }

  onTabTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _onTap(int index) {
    if (_selectedIndex == index) {
      // if (_tabs[index].navigatorState?.currentState?.canPop() ?? false) {
      //   _tabs[index].navigatorState?.currentState?.pop();
      // }
    } else {
      if (mounted) {
        setState(() {
          _selectedIndex = index;
        });
      }
    }
  }

  _showWelcomeOverlay() {
    showDialog(
        context: context,
        builder: (context) {
          return Helpers.overlayWelcomeUserToTraining(context, "Dustin",
              onClose: () {
            Navigator.of(context).pop();
          });
        });
  }

  //TODO: Call this function when Excessive Motion or SWEAT detected, or Hardware Connection Lost
  //TODO: The title S.hardwareConnectionLost can be replaced by S.excessiveMotionOrSweatDetected
  _showHardwareConnectionResetOverlay() {
    showDialog(
      context: context,
      builder: (context) {
        return Helpers.overlayHardwareReset(
            context, S.hardwareConnectionLost, S.resetYourHardware,
            onClose: () {
          _navigateToHardwareRoot();
        }, onReset: () {
          _navigateToHardwareRoot();
        });
      },
    );
  }

  _navigateToHardwareRoot() {
    Navigator.of(context).pop();
    this._onTap(0);
    AppKeys.hardwareNavigatorKey.currentState
        ?.popUntil((route) => route.isFirst);
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return WillPopScope(
      onWillPop: () async {
        this._onTap(_selectedIndex);
        return false;
      },
      child: Scaffold(
        key: this.scaffoldKey,
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              IndexedStack(
                  index: _selectedIndex,
                  children: List.generate(
                      _tabs.length,
                      (index) => Navigator(
                            key: _tabs[index].navigatorState,
                            onGenerateRoute: _tabs[index].onGenerateRoute,
                            initialRoute: _tabs[index].initialRoute,
                          ))),

              ///Bottom Navigation
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: _buildBottomBar(context, textTheme),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBottomBar(BuildContext context, TextTheme textTheme) {
    return ClipRRect(
      child: Container(
        height: 72,
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 15, sigmaY: 15),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: Responsive.isMobile(context) ? 10 : 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.generate(_tabs.length, (index) {
                bool isCurrent = index == _selectedIndex;

                return Expanded(
                  child: InkWell(
                    onTap: () {
                      this._onTap(index);
                    },
                    highlightColor: Colors.white.withOpacity(0.16),
                    child: Container(
                      height: 72,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                              topRight: Radius.circular(8)),
                          border: isCurrent
                              ? Border.all(
                                  width: 1,
                                  color: Colors.white.withOpacity(0.24))
                              : Border.all(width: 0, color: Colors.transparent),
                          gradient: isCurrent
                              ? LinearGradient(
                                  colors: [
                                      Colors.white.withOpacity(0.16),
                                      Colors.white.withOpacity(0.08)
                                    ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight)
                              : null),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ImageIcon(
                            AssetImage(
                              _tabs[index].assetIcon,
                            ),
                            color: Colors.white,
                            size: 24,
                          ),
                          SizedBox(width: 8,),
                          Text(
                            _tabs[index].label,
                            style: textTheme.bodyText1,
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }),
            ),
          ),
        ),
      ),
    );
  }
}

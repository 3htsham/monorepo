import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:flutter/cupertino.dart';

class MainPageProvider extends ChangeNotifier {

  HardwareService? hardwareService;

  MainPageProvider({this.hardwareService});

}
import 'dart:async';
import 'dart:convert';
import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/models/base_response.dart';
import 'package:brain_trainer/models/headband_models/cgx_headband.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DevicesBluetoothStateProvider extends ChangeNotifier {
  List<CGXHeadband> cachedDevices = [];
  List<CGXHeadband> availableDevices = [];
  HardwareService hardwareService;
  bool isLoading = false;
  CGXHeadband? activeDevice;
  CGXHeadband? lastUsed;

  StreamSubscription<List<CGXHeadband>>? deviceListStream;
  StreamSubscription<HeadbandState>? deviceStateStream;

  Timer? timer;
  int _maxSecondsToWait = 5;
  int seconds = 0;

  DevicesBluetoothStateProvider(this.hardwareService) {
    getAvailableDevices();
    listenHeadbandState();
    // getCachedDevices();
    getLastUsedDevice();
  }

  @override
  void dispose() {
    deviceListStream?.cancel();
    deviceStateStream?.cancel();
    timer?.cancel();
    super.dispose();
  }

  Future<dynamic> getAvailableDevices() async {
    timer = Timer.periodic(Duration(seconds: 1), (t) {
      seconds = t.tick;
      notifyListeners();
      if(_maxSecondsToWait == t.tick) {
        timer?.cancel();
      }
    });
    try {
      // deviceListStream?.cancel();
      isLoading = true;
      deviceListStream = hardwareService.getAvailableDevices().listen((event) {
        isLoading = false;
        availableDevices = event;
        notifyListeners();
      }, onError: (e) {
        print(e);
      }, onDone: () {
        isLoading = false;
        notifyListeners();
      });
    } catch (e) {
      print(e);
    }
  }

  getCachedDevices() async {
    this.cachedDevices.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Get Cached Devices from SharedPreferences
    if (prefs.containsKey(Prefs.cachedDevices)) {
      Map<String, dynamic> _ccDevices =
          json.decode(prefs.getString(Prefs.cachedDevices) ?? "");
      print(_ccDevices.length);
      if (_ccDevices.length > 0) {
        _ccDevices[Prefs.cachedDevices].forEach((value) {
          final _device = CGXHeadband.fromJson(value);
          this.cachedDevices.add(_device);
        });
      }
      notifyListeners();
    }
  }

  Future<BaseResponse<bool>> connectWithDevice(CGXHeadband device) async {
    if (this.activeDevice != null) {
      await disconnectDevice(activeDevice!);
    }
    activeDevice = device;
    activeDevice?.state = HeadbandState.connecting;
    notifyListeners();
    var result = await hardwareService.connectWithDevice(device);
    if (!result.error) {
      cacheADevice(device);
      AppKeys.mainTabsKey.currentState?.lastState = HeadbandState.connected;
      this.listenHeadbandState();
      AppKeys.mainTabsKey.currentState?.listenHardwareState();
    } else {
      // this.activeDevice = null;
      activeDevice?.state = HeadbandState.disconnected;
    }
    notifyListeners();
    return result;
  }

  Future<BaseResponse<bool>> disconnectDevice(CGXHeadband _device) async {
    notifyListeners();
    var result = await hardwareService.disconnectDevice();
    //Add the connected device to Cached Devices
    if (!result.error) {
      // cacheADevice(device);
      this
          .availableDevices
          .firstWhere((element) => element.identifier == _device.identifier)
          .state = HeadbandState.disconnected;
    } else {
      // this.activeDevice = null;
    }
    notifyListeners();
    return result;
  }

  Future<void> listenHeadbandState() async {
    deviceStateStream?.cancel();
    deviceStateStream =
        hardwareService.listenHardwareConnectionState().listen((event) {
          activeDevice?.state = event;
          notifyListeners();
        });
  }


  //Devices Caching Stuff
  cacheADevice(CGXHeadband _device) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Prefs.lastUsedDevice, json.encode(_device.toJson()));
  }
  //Cached Device
  getLastUsedDevice() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.containsKey(Prefs.lastUsedDevice)) {
      try {
        final device = prefs.getString(Prefs.lastUsedDevice);
        if(device != null) {
          this.lastUsed = CGXHeadband.fromJson(json.decode(device));
          notifyListeners();
        }
      } catch (e) {
        print(e);
      }
    }
  }


  //====> Code below can be Used to cache multiple devices

  void deleteFromCachedDevices(CGXHeadband _device) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _isExists = updateOrRemoveADeviceFromCached(_device, false);
    //If a device is deleted from cached devices, then updated teh cachedDevices in preferences
    if (_isExists) {
      setCachedDevices(prefs);
      getCachedDevices();
    }
  }

  setCachedDevices(SharedPreferences prefs) {
    //CachedDevices
    Map<String, dynamic> ccDevices = Map();
    ccDevices[Prefs.cachedDevices] =
        this.cachedDevices.map((e) => e.toJson()).toList();
    //Update the prefs with cached Devices
    prefs.setString(Prefs.cachedDevices, json.encode(ccDevices));
  }

  //This function will find a device in cached list and either remove that device or update
  bool updateOrRemoveADeviceFromCached(CGXHeadband _device, bool shouldUpdate) {
    bool _isExists = false;
    //Check if device already Exists in Cached Devices
    if (this.cachedDevices.isNotEmpty) {
      //ForEach loop doesn't await
      for (int i = 0; i < this.cachedDevices.length; i++) {
        //If Exists, then remove or update the device
        if (this.cachedDevices[i].identifier == _device.identifier) {
          if (shouldUpdate) {
            this.cachedDevices[i] = _device;
          } else {
            this.cachedDevices.removeAt(i);
          }
          _isExists = true;
          break;
        }
      }
    }
    return _isExists;
  }
}

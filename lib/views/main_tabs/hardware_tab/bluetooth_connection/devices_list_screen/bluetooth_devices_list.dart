import 'package:app_settings/app_settings.dart';
import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'package:brain_trainer/elements/buttons/outlined_button.dart';
import 'package:brain_trainer/elements/others/mini_battery_indicator.dart';
import 'package:brain_trainer/enum/bluetooth_state.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:brain_trainer/models/headband_models/cgx_headband.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import '../devices_bluetooth_state_provider.dart';

class BluetoothDevicesList extends StatefulWidget {
  @override
  _BluetoothDevicesListState createState() => _BluetoothDevicesListState();
}

class _BluetoothDevicesListState extends State<BluetoothDevicesList> {
  @override
  void initState() {
    // getAvailableDevices();
    super.initState();
  }

  Future<void> connectADevice(CGXHeadband _device) async {
    //Available but not connected, connect now
    print("Connecting Device ID ${_device.identifier}");
    var result = await context
        .read<DevicesBluetoothStateProvider>()
        .connectWithDevice(_device);
    if (!result.error) {
      //Device Connected
      // context.read<DevicesBluetoothStateProvider>().deviceStateStream?.cancel();
    } else {
      //Device Not connected
      UIHelper.showErrorDialog(S.deviceNotConnectedTryAgain, context);
    }
  }

  Future<void> deleteDevice(CGXHeadband _device) async {
    //Not available, delete now
    print("Deleting Device ID from cache ${_device.identifier}");
    context
        .read<DevicesBluetoothStateProvider>()
        .deleteFromCachedDevices(_device);
  }

  Future<void> proceedWithADevice(CGXHeadband _device) async {
    //Already Connected, Proceed now
    print("Proceeding with a Device ID ${_device.identifier}");
    Navigator.of(context).pushNamed(Routes.HARDWARE_SETUP_ONE);
  }

  Future<void> disconnectADevice(CGXHeadband _device) async {
    //Not available, delete now
    print("Deleting Device ID from cache ${_device.identifier}");
    context.read<DevicesBluetoothStateProvider>().disconnectDevice(_device);
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    final availableDevices =
        context.watch<DevicesBluetoothStateProvider>().availableDevices;
    final cachedDevices =
        context.watch<DevicesBluetoothStateProvider>().cachedDevices;

    return Scaffold(
      appBar: MainAppbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildDevicesHeadline(theme),
              context.watch<DevicesBluetoothStateProvider>().seconds >= 5 &&
                      context
                              .watch<DevicesBluetoothStateProvider>()
                              .availableDevices
                              .length ==
                          0
                  ? Text(
                      S.pleaseTurnOnYourOptiosDevice,
                      style: theme.textTheme.bodyText1,
                    )
                  : SizedBox(
                      height: 0,
                    ),

              availableDevices.length > 0 ? Padding(
                  padding: EdgeInsets.only(top: 15, bottom: 30),
                child: Text(S.addNewDevice, style: theme.textTheme.headline3,),
              )
              : SizedBox(),
              StreamBuilder(
                stream: context.watch<HardwareService>().getBluetoothState(),
                initialData:
                    context.watch<HardwareService>().currentBluetoothState,
                builder: (context, AsyncSnapshot<BluetoothState> snapshot) {
                  return snapshot.data == BluetoothState.off
                      ? _buildTurnOnBluetoothWidget(theme)
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //Cached Devices
                            // _buildCachedDevicesList(
                            //     theme, cachedDevices, availableDevices),
                            // //Divider
                            // cachedDevices.length > 0
                            //     ? Divider(
                            //         thickness: 2,
                            //         color: Colors.white.withOpacity(0.5),
                            //       )
                            //     : SizedBox(
                            //         height: 0,
                            //       ),
                            //Available Devices
                            _buildNewAvailableDeviceList(
                                theme, availableDevices, cachedDevices)
                          ],
                        );
                },
              ),
              SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTurnOnBluetoothWidget(ThemeData theme) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          S.bluetoothRequired,
          style: theme.textTheme.headline3,
        ),
        SizedBox(
          height: 32,
        ),
        MainAppButton(
            onTap: () {
              //Prompt user to turn bluetooth ON
              AppSettings.openBluetoothSettings(asAnotherTask: true);
            },
            width: 303,
            height: 56,
            title: S.turnOnBluetooth.toUpperCase())
      ],
    );
  }

  Widget _buildDevicesHeadline(ThemeData theme) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          S.devices,
          style: theme.textTheme.headline1?.copyWith(
            fontSize: 80.0,
          ),
        ),
        context.watch<DevicesBluetoothStateProvider>().isLoading
            ? CupertinoTheme(
                data: CupertinoTheme.of(context)
                    .copyWith(brightness: Brightness.dark),
                child: CupertinoActivityIndicator(
                  radius: 12,
                ))
            : SizedBox()
      ],
    );
  }

  Widget _buildCachedDevicesList(ThemeData theme, List<CGXHeadband> _deviceList,
      List<CGXHeadband> _availableList) {
    return ListView.builder(
      itemCount: _deviceList.length,
      shrinkWrap: true,
      primary: false,
      itemBuilder: (context, i) {
        //Need to display the device button as 'connect' if it is currently available
        bool isAvailable = _availableList
            .where((element) => element.identifier == _deviceList[i].identifier)
            .isNotEmpty;

        //if device is connected, then get the batteryState
        // double? batteryPercentage = i == 0 ? 67 : null;

        return _buildDeviceItem(theme, _deviceList[i], () {
          if (_deviceList[i].state == HeadbandState.connected) {
            proceedWithADevice(_deviceList[i]);
          } else if (isAvailable &&
              _deviceList[i].state == HeadbandState.disconnected) {
            connectADevice(_deviceList[i]);
          } else {
            deleteDevice(_deviceList[i]);
          }
        }, () {
          ///Disconnect Device
        }, isAvailable: isAvailable);
      },
    );
  }

  Widget _buildNewAvailableDeviceList(ThemeData theme,
      List<CGXHeadband> _deviceList, List<CGXHeadband> _cachedList) {
    return _deviceList.isNotEmpty
        ? ListView.builder(
            itemCount: _deviceList.length,
            shrinkWrap: true,
            primary: false,
            itemBuilder: (context, i) {
              var _device = _deviceList[i];
              return _buildDeviceItem(theme, _device, () {
                if (_device.state == HeadbandState.connected) {
                  proceedWithADevice(_device);
                } else if (_device.state == HeadbandState.disconnected) {
                  connectADevice(_device);
                } else {
                  deleteDevice(_device);
                }
              }, () {
                ///Disconnect Device
                this.disconnectADevice(_device);
              }, isAvailable: true, isCached: false);
            },
          )
        : SizedBox();
  }

  //If _device.state == HeadbandState.connected, that means device is connected
  //If isAvailable == true and _device.state == HeadbandState.disconnected, that means Device is available but not connected
  //Function(String) onTap: is a function with parameter deviceHash/deviceId (headset's unique identifier) as a String
  Widget _buildDeviceItem(ThemeData theme, CGXHeadband _device,
      VoidCallback onTap, VoidCallback onDisconnect,
      {bool isAvailable = true, bool isCached = true}) {
    return Row(
      children: [
        Opacity(
            opacity:
                (_device.state == HeadbandState.disconnected && !isAvailable)
                    ? 0.5
                    : 1,
            child: Image.asset(AppAssets.headsetModelImage, width: 142)),
        SizedBox(width: 20),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                _device.deviceNameWithUID,
                style: theme.textTheme.subtitle1,
              ),
              SizedBox(
                height: 5,
              ),
              _device.state == HeadbandState.disconnected
                  ? isCached
                      ? Text(
                          S.notConnected,
                          style: theme.textTheme.bodyText1,
                        )
                      : SizedBox()
                  : Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox()
//                        MiniBatteryIndicator(
//                            batteryPercentage: _device.batteryPercent),
//                        SizedBox(
//                          width: 7,
//                        ),
//                        Text(
//                          '${_device.batteryPercent.toStringAsFixed(0)}%',
//                          style: theme.textTheme.bodyText1,
//                        )
                      ],
                    ),
              context.watch<DevicesBluetoothStateProvider>().lastUsed != null &&
                      context
                              .watch<DevicesBluetoothStateProvider>()
                              .lastUsed
                              ?.identifier ==
                          _device.identifier
                  ? Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Text(
                        S.lastUsed,
                        style: theme.textTheme.bodyText1,
                      ),
                    )
                  : SizedBox(height: 0)
            ],
          ),
        ),
        SizedBox(width: 20),
        //If device state is connecting, show progress indicator, otherwise display button
        _device.state == HeadbandState.connecting
            ? SizedBox(
                width: 156,
                child: Center(
                  child: CupertinoTheme(
                      data: CupertinoTheme.of(context)
                          .copyWith(brightness: Brightness.dark),
                      child: CupertinoActivityIndicator(
                        radius: 12,
                      )),
                ),
              )
            : _device.state == HeadbandState.connected
                ? Column(
                    children: [
                      AppOutLinedButton(
                        onTap: () {
                          onTap.call();
                        },
                        title: isAvailable
                            ? S.proceed.toUpperCase()
                            : S.delete.toUpperCase(),
                        backgroundColor: theme.accentColor,
                        borderColor: Colors.transparent,
                        width: 156,
                        height: 48,
                      ),
                      SizedBox(
                        height: isAvailable ? 10 : 0,
                      ),
                      isAvailable
                          ? AppOutLinedButton(
                              onTap: () {
                                onDisconnect.call();
                              },
                              title: S.disconnect.toUpperCase(),
                              backgroundColor: theme.scaffoldBackgroundColor,
                              width: 156,
                              height: 48,
                              borderColor: theme.scaffoldBackgroundColor,
                            )
                          : SizedBox()
                    ],
                  )
                : AppOutLinedButton(
                    onTap: () {
                      onTap.call();
                    },
                    title: isAvailable &&
                            _device.state == HeadbandState.disconnected
                        ? S.connect.toUpperCase()
                        : S.delete.toUpperCase(),
                    backgroundColor: theme.scaffoldBackgroundColor,
                    width: 156,
                    height: 48,
                  )
      ],
    );
  }
}

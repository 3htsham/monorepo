import 'dart:async';

import 'package:app_settings/app_settings.dart';
import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'package:brain_trainer/enum/bluetooth_state.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BluetoothUnAuthorizedWidget extends StatefulWidget {
  @override
  _BluetoothUnAuthorizedWidgetState createState() => _BluetoothUnAuthorizedWidgetState();
}

class _BluetoothUnAuthorizedWidgetState extends State<BluetoothUnAuthorizedWidget> {

  StreamSubscription<BluetoothState>? _bluetoothStreamSubscription;

  @override
  void initState() {
    super.initState();
  }

  void _handleButtonTap() async {
    await AppSettings.openAppSettings(asAnotherTask: true);
  }

  @override
  void dispose() {
    _bluetoothStreamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: MainAppbar(),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [

            Image.asset(AppAssets.bluetoothSettingsScreenshot, width: size.width,),
            Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 100),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  SizedBox(height: 64,),

                  Text(
                    S.bluetoothPermissionRequired.toUpperCase(),
                    style: Theme.of(context)
                        .textTheme
                        .headline2,
                  ),

                  SizedBox(height: 34,),

                  Text(
                    S.weNeedBluetoothPermissionBeforeWeCan,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1,
                  ),

                  SizedBox(height: 30,),

                  MainAppButton(
                    onTap: () async {
                      _handleButtonTap();
                    },
                    title: S.goToSettings.toUpperCase(),
                    width: 246,
                    height: 56,
                  ),

                  SizedBox(height: 150,),

                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}

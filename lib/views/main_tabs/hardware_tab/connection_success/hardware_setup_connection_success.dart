import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';
import 'package:brain_trainer/elements/headset/sensors_detector.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/elements/others/mini_battery_indicator.dart';
import 'package:brain_trainer/elements/responsive.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HardwareSetupConnectionSuccess extends StatefulWidget {
  @override
  _HardwareSetupConnectionSuccessState createState() =>
      _HardwareSetupConnectionSuccessState();
}

class _HardwareSetupConnectionSuccessState
    extends State<HardwareSetupConnectionSuccess> {
  void _handleStartTraining() {
    // fix OG-68 - go to the training tab
    AppKeys.mainTabsKey.currentState?.onTabTapped(1);
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    var bottomPadding = 172.0;

    return LightingScaffold(
      resizeToAvoidBottomPadding: false,
      child: Container(
        height: size.height,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              MainAppbar(
                onBack: () {},
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: Responsive.isMobile(context) ? 10 : 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      width: size.width,
                    ),
                    Text(
                      S.hardwareSetup,
                      style: textTheme.headline1,
                    ),
                    Text(
                      S.headsetFitting,
                      style: textTheme.headline3,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 32,
              ),
              _buildSensorDetectorAndBatteryIndicator(context, textTheme),
              SizedBox(
                height: 31,
              ),
              Text(
                S.calibrationSuccessfully,
                style: textTheme.headline4,
              ),
              SizedBox(
                height: 33,
              ),
              _buildStartTrainingButton(context, textTheme),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: bottomPadding,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSensorDetectorAndBatteryIndicator(
      BuildContext context, TextTheme textTheme) {
    var detectorPathSize = 299.3;
    var successCheckMarkSize = 39.64;

    return Container(
      height: detectorPathSize,
      child: Stack(
        children: <Widget>[
          Center(
              child: SensorsDetector(
            sensorOneStatus: SensorStatus.active,
            sensorTwoStatus: SensorStatus.active,
            sensorThreeStatus: SensorStatus.active,
            sensorFourStatus: SensorStatus.active,
          )),
          Center(
            child: Container(
                width: successCheckMarkSize,
                height: successCheckMarkSize,
                child: Center(
                    child: Image.asset(
                  "assets/images/success_check_mark.png",
                ))),
          ),
          Positioned(
            bottom: 5,
            left: 0,
            right: 0,
            child: Center(
                child: FutureBuilder<double>(
                    initialData: 0,
                    future: context.read<HardwareService>().getBatteryLevel(),
                    builder: (context, snapshot) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          MiniBatteryIndicator(
                            batteryPercentage:
                                double.parse(snapshot.data.toString()),
                            batteryColor: Colors.white,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "${double.parse(snapshot.data.toString()).toStringAsFixed(0)}%",
                            style: textTheme.caption,
                          ),
                        ],
                      );
                    })),
          ),
        ],
      ),
    );
  }

  Widget _buildStartTrainingButton(BuildContext context, TextTheme textTheme) {
    return SizedBox(
      height: 56,
      width: 334,
      child: ElevatedButton(
        onPressed: this._handleStartTraining,
        style: ElevatedButton.styleFrom(
          shape: StadiumBorder(),
        ),
        child: Center(
          child: Text(
            S.startTraining.toUpperCase(),
            style: textTheme.headline6,
          ),
        ),
      ),
    );
  }
}

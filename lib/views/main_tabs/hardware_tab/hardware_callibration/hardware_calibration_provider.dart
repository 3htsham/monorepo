import 'dart:async';

import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:just_audio/just_audio.dart';

import '../../../../services/hardware/hardware_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HardwareCalibrationProvider extends ChangeNotifier {
  SensorStatus? sensorOneStatus,
      sensorTwoStatus,
      sensorThreeStatus,
      sensorFourStatus = SensorStatus.active;

  bool isOpenEyes = false;
  final player = AudioPlayer();

  double batteryPercentage = 80;
  int seconds = 0;
  int totalSeconds =
      60; //Should be Calibrated for 60 seconds, setting up the test Future for just 10 seconds
  bool isCalibrating = false;

  // HardwareService? hardwareService;

  BuildContext? context;
  Timer? timer;

  StreamController<int> timerChangedController = StreamController<int>.broadcast();
  StreamController<bool> eyesOpenChangedController = StreamController<bool>.broadcast();

  HardwareCalibrationProvider(
      {this.sensorOneStatus = SensorStatus.active,
      this.sensorTwoStatus = SensorStatus.active,
      this.sensorThreeStatus = SensorStatus.active,
      this.sensorFourStatus = SensorStatus.active}) {
    player.setVolume(1);
    player.setAsset(AppAssets.beepSound);
  }

  @override
  void dispose() {
    timerChangedController.close();
    eyesOpenChangedController.close();
    try {
      player.stop();
      player.dispose();
    } catch (e) {}
    super.dispose();
  }

  void setFakeInteraction(BuildContext context) {
    this.context = context;
    //Calibrating Hardware
    sensorOneStatus = SensorStatus.active;
    sensorTwoStatus = SensorStatus.active;
    sensorThreeStatus = SensorStatus.active;
    sensorFourStatus = SensorStatus.active;
    seconds = 0;
    isCalibrating = true;
    notifyListeners();
    context.read<TrainingService>().startProcessorBaseline();

    //Should be tested for 60 seconds, setting up the test Future for just 10 seconds
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      this.seconds = t.tick;
      timerChangedController.add(t.tick);
      if (this.seconds % 10 == 0) {
        this.isOpenEyes = !this.isOpenEyes;
        eyesOpenChangedController.add(this.isOpenEyes);
        //TODO: Play beep
        playBeep();
      }
      // notifyListeners();
    });
    Future.delayed(Duration(seconds: totalSeconds), () {
      // context.read<HardwareService>().cancelHardwareCalibration();

      timer?.cancel();
      timerChangedController.close();
      eyesOpenChangedController.close();
      sensorOneStatus = SensorStatus.active;
      sensorTwoStatus = SensorStatus.active;
      sensorThreeStatus = SensorStatus.active;
      sensorFourStatus = SensorStatus.active;
      seconds = 0;
      isCalibrating = false;
      context.read<HardwareService>().stopEEGAcquisition();
      context.read<TrainingService>().stopAcquisition();

      _navigatorToSuccess(context);
    });
  }

  void _navigatorToSuccess(BuildContext context) {
    //Navigator to Hardware calibration page
    Navigator.of(context).pushReplacementNamed(Routes.HARDWARE_SETUP_SUCCESS);
  }

  void calibrateHardwareHeadset(BuildContext context) {
    // context.read<HardwareService>().startHardwareCalibration().onData((data) {
    //TODO: Change the UI state on data received
    // });

    context.read<HardwareService>().startEEGAcquisition();

    setFakeInteraction(context);
  }

  playBeep() async {
//    player.play().then((value) {
//      try {
//        player.stop();
//      } catch (e) {}
//    });

    // fix OG-67 - need a new asset every time
    await player.setAsset(AppAssets.beepSound);
    player.play();
  }
}

import 'dart:async';
import 'package:blobs/blobs.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/headset/sensors_detector.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/elements/others/mini_battery_indicator.dart';
import 'package:brain_trainer/elements/responsive.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;

import 'hardware_calibration_provider.dart';

class HardwareSetupCalibration extends StatefulWidget {
  @override
  _HardwareSetupCalibrationState createState() =>
      _HardwareSetupCalibrationState();
}

class _HardwareSetupCalibrationState extends State<HardwareSetupCalibration> {
  @override
  void initState() {
    super.initState();
  }

  void _calibrateNow() async {
    context
        .read<HardwareCalibrationProvider>()
        .calibrateHardwareHeadset(context);
  }

  var _sensorTransform = Matrix4.translationValues(0.0, -70.0, 0.0);
  var _transform = Matrix4.translationValues(0.0, -120.0, 0.0);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    var bottomPadding = 228.0;

    return LightingScaffold(
      resizeToAvoidBottomPadding: false,
      child: Container(
        height: size.height,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              MainAppbar(
                onBack: () {},
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      width: size.width,
                    ),
                    Text(
                      S.hardwareSetup,
                      style: textTheme.headline1,
                    ),
                    Text(
                      S.headsetFitting,
                      style: textTheme.headline3,
                    ),
                  ],
                ),
              ),
              _buildSensorDetectorAndBatteryIndicator(context, textTheme),
              Container(
                transform: _transform,
                child: Column(
                  children: [
                    SizedBox(
                      height: context
                              .watch<HardwareCalibrationProvider>()
                              .isCalibrating
                          ? 105
                          : 31,
                    ),
                    context.watch<HardwareCalibrationProvider>().isCalibrating
                        ? StreamBuilder(
                            stream: context
                                .watch<HardwareCalibrationProvider>()
                                .eyesOpenChangedController
                                .stream,
                            builder: (context, AsyncSnapshot<bool> isOpenEyes) {
                              return Text(
                                isOpenEyes.data ?? false
                                    ? S.eyesOpen
                                    : S.eyesClosed,
                                style: textTheme.headline3,
                              );
                            },
                          )
                        : _buildCalibrationInstructionsStuff(
                            context, textTheme),
                    SizedBox(
                      height: bottomPadding,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSensorDetectorAndBatteryIndicator(
      BuildContext context, TextTheme textTheme) {
    var detectorPathSize = 222.78;
    var successCheckMarkSize = 39.64;

    return Container(
      height: detectorPathSize + 200,
      child: Stack(
        children: <Widget>[
          Container(
            transform: _sensorTransform,
            child: Center(
                child: _wrapWithAnimation(
              size: detectorPathSize + 200,
              isWrapped:
                  context.watch<HardwareCalibrationProvider>().isCalibrating,
              // isWrapped: true,
              child: SensorsDetector(
                sensorOneStatus: context
                    .watch<HardwareCalibrationProvider>()
                    .sensorOneStatus,
                sensorTwoStatus: context
                    .watch<HardwareCalibrationProvider>()
                    .sensorTwoStatus,
                sensorThreeStatus: context
                    .watch<HardwareCalibrationProvider>()
                    .sensorThreeStatus,
                sensorFourStatus: context
                    .watch<HardwareCalibrationProvider>()
                    .sensorFourStatus,
              ),
            )),
          ),
          Container(
            transform: _sensorTransform,
            child: Center(
              child: context.watch<HardwareCalibrationProvider>().isCalibrating
                  ? StreamBuilder(
                      stream: context
                          .watch<HardwareCalibrationProvider>()
                          .timerChangedController
                          .stream,
                      builder: (context, AsyncSnapshot<int> seconds) {
                        return Text(
                          '${seconds.data ?? ""}',
                          style: textTheme.headline1,
                        );
                      },
                    )
                  : Container(
                      width: successCheckMarkSize,
                      height: successCheckMarkSize,
                      child: Center(
                          child: Image.asset(
                        "assets/images/success_check_mark.png",
                      ))),
            ),
          ),
          context.watch<HardwareCalibrationProvider>().isCalibrating
              ? SizedBox()
              : Positioned(
                  bottom: 5,
                  left: 0,
                  right: 0,
                  child: Container(
                    transform: _transform,
                    child: Center(
                        child: FutureBuilder<double>(
                            initialData: 0,
                            future: context
                                .read<HardwareService>()
                                .getBatteryLevel(),
                            builder: (context, snapshot) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  MiniBatteryIndicator(
                                    batteryPercentage:
                                        double.parse(snapshot.data.toString()),
                                    batteryColor: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    "${double.parse(snapshot.data.toString()).toStringAsFixed(0)}%",
                                    style: textTheme.caption,
                                  ),
                                ],
                              );
                            })),
                  ),
                ),
        ],
      ),
    );
  }

  Widget _wrapWithAnimation(
      {double size = 50, Widget? child, bool isWrapped = false}) {
    return isWrapped
        ? Blob.animatedRandom(
            size: size,
            // debug: true,
            loop: true,
            edgesCount: 6,
            minGrowth: 5,
            duration: Duration(seconds: 1),
            styles: BlobStyles(
                fillType: BlobFillType.stroke,
                color: config.AppColors.cyanDark,
                strokeWidth: 1),
            child: Center(
              child: child ?? Container(),
            ),
          )
        : child ?? Container();
  }

  Widget _buildCalibrationInstructionsStuff(
      BuildContext context, TextTheme textTheme) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          S.successfullyConnected,
          style: textTheme.headline4,
        ),
        SizedBox(
          height: 33,
        ),
        Text(
          S.headsetCalibration,
          style: textTheme.headline3,
        ),
        SizedBox(
          height: 17,
        ),
        Text(
          S.headsetCalibrationInstruction,
          style: textTheme.bodyText1,
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 32,
        ),
        _buildStartCalibrationButton(context, textTheme),
      ],
    );
  }

  Widget _buildStartCalibrationButton(
      BuildContext context, TextTheme textTheme) {
    return SizedBox(
      height: 56,
      width: 334,
      child: ElevatedButton(
        onPressed: _calibrateNow,
        style: ElevatedButton.styleFrom(
          shape: StadiumBorder(),
        ),
        child: Center(
          child: Text(
            S.startCalibration.toUpperCase(),
            style: textTheme.headline6,
          ),
        ),
      ),
    );
  }
}

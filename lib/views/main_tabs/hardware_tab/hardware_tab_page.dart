import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/enum/bluetooth_state.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'bluetooth_connection/devices_bluetooth_state_provider.dart';
import 'bluetooth_connection/devices_list_screen/bluetooth_devices_list.dart';
import 'bluetooth_connection/unauthorized/bluetooth_unauthorized.dart';

class HardwareTabPage extends StatefulWidget {
  static String tabName = S.hardware;

  @override
  _HardwareTabPageState createState() => _HardwareTabPageState();
}

class _HardwareTabPageState extends State<HardwareTabPage> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: context.read<HardwareService>().getBluetoothState(),
      initialData: BluetoothState.unknown,
      builder: (context, AsyncSnapshot<BluetoothState> state) {
        print(state.data);
        return state.data == BluetoothState.unknown
            ? _buildLoadingIndicator()
            : state.data == BluetoothState.unAuthorized
                ? BluetoothUnAuthorizedWidget()
                : ChangeNotifierProvider<DevicesBluetoothStateProvider>(
                    create: (ctx) => DevicesBluetoothStateProvider(
                        ctx.read<HardwareService>()),
                    child: BluetoothDevicesList(),
                  );
      },
    );
  }

  Widget _buildLoadingIndicator() {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: SizedBox(
            height: 40,
            width: 40,
            child: CircularProgressIndicator(
              color: Theme.of(context).accentColor,
            ),
          ),
        ),
      ),
    );
  }
}

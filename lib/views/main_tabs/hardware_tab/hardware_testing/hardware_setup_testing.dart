import 'dart:ui';
import 'package:blobs/blobs.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/headset/sensors_detector.dart';
import 'package:brain_trainer/elements/headset/step_instructions.dart';
import 'package:brain_trainer/elements/responsive.dart';
import 'package:flutter/material.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'hardware_setup_testing_provider.dart';

class HardwareSetupTesting extends StatefulWidget {
  @override
  _HardwareSetupTestingState createState() => _HardwareSetupTestingState();
}

class _HardwareSetupTestingState extends State<HardwareSetupTesting> {
  String adjustmentErrorInstructions = S.adjustmentRequiredInstruction;

  @override
  void initState() {
    super.initState();
    _testConnection();
  }

  _testConnection() {
    SchedulerBinding.instance?.addPostFrameCallback((_) {
      context
          .read<HardwareSetUpTestingProvider>()
          .listenToHeadsetDataState(context);
    });
  }

  var _sensorTransform = Matrix4.translationValues(0.0, -70.0, 0.0);
  var _transform = Matrix4.translationValues(0.0, -120.0, 0.0);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    var detectorPathSize = 222.78;

    var bottomPadding = 228.0;

    return Scaffold(
      body: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 25, sigmaY: 25),
        child: Material(
          color: theme.primaryColor.withOpacity(0.5),
          child: Container(
            height: size.height,
            width: size.width,
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  MainAppbar(onBack: () {
                    if (Navigator.of(context).canPop()) {
                      Navigator.of(context).pop();
                    }
                  }),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: 40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: size.width,
                        ),
                        Text(
                          S.hardwareSetup,
                          style: textTheme.headline1,
                        ),
                        Text(
                          S.headsetFitting,
                          style: textTheme.headline3,
                        ),
                      ],
                    ),
                  ),
                  // SizedBox(
                  //   height: 32,
                  // ),

                  Container(
                    transform: _sensorTransform,
                    child: Blob.animatedRandom(
                      size: detectorPathSize + 210,
                      // debug: true,
                      loop: true,
                      edgesCount: 6,
                      minGrowth: 5,
                      duration: Duration(seconds: 1),
                      styles: BlobStyles(
                          fillType: BlobFillType.stroke,
                          color: config.AppColors.cyanDark,
                          strokeWidth: 1),
                      child: Container(
                        // height: detectorPathSize,
                        child: Center(
                            child: SensorsDetector(
                          sensorOneStatus: context
                              .watch<HardwareSetUpTestingProvider>()
                              .sensorOneStatus,
                          sensorTwoStatus: context
                              .watch<HardwareSetUpTestingProvider>()
                              .sensorTwoStatus,
                          sensorThreeStatus: context
                              .watch<HardwareSetUpTestingProvider>()
                              .sensorThreeStatus,
                          sensorFourStatus: context
                              .watch<HardwareSetUpTestingProvider>()
                              .sensorFourStatus,
                        )),
                      ),
                    ),
                  ),

                  Container(
                    transform: _transform,
                    child: Column(
                      children: [
                        context
                                .watch<HardwareSetUpTestingProvider>()
                                .isTestRunning
                            ? Padding(
                                padding: const EdgeInsets.only(top: 0.0),
                                child: Text(
                                  S.fitTestingInProgress,
                                  style: textTheme.headline4,
                                ),
                              )
                            : context
                                    .watch<HardwareSetUpTestingProvider>()
                                    .isTestFailed
                                ? StepInstructions(
                                    headline: S.adjustmentRequired,
                                    instructions:
                                        this.adjustmentErrorInstructions,
                                  )
                                : SizedBox(),
                        SizedBox(
                          height: 32,
                        ),
                        context
                                .watch<HardwareSetUpTestingProvider>()
                                .isTestRunning
                            ? SizedBox()
                            : context
                                    .watch<HardwareSetUpTestingProvider>()
                                    .isTestFailed
                                ? _buildReTestButton(context, textTheme)
                                : SizedBox(),
                        (!context
                                    .watch<HardwareSetUpTestingProvider>()
                                    .isTestRunning &&
                                context
                                    .watch<HardwareSetUpTestingProvider>()
                                    .isTestFailed)
                            ? _buildLearnMoreInstructions(context, textTheme)
                            : SizedBox(),
                        SizedBox(
                          height: bottomPadding,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildReTestButton(BuildContext context, TextTheme textTheme) {
    return SizedBox(
      height: 56,
      width: 334,
      child: ElevatedButton(
        onPressed: () {
          context
              .read<HardwareSetUpTestingProvider>()
              .listenToHeadsetDataState(context);
        },
        style: ElevatedButton.styleFrom(
          shape: StadiumBorder(),
        ),
        child: Center(
          child: Text(
            S.reTestConnection.toUpperCase(),
            style: textTheme.headline6,
          ),
        ),
      ),
    );
  }

  Widget _buildLearnMoreInstructions(
      BuildContext context, TextTheme textTheme) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal:  40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 64,
          ),
          Text(
            S.learnMore,
            style: textTheme.headline3,
          ),
          SizedBox(
            height: 39,
          ),
          Container(
            child: IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  /*
                  ///Box For Video Link
                  Expanded(
                    child: GestureDetector(
                      onTap: () {},
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 15),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.16),
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                  color: Colors.white.withOpacity(0.24),
                                  width: 1)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                S.hardwareConnectingIssue,
                                style: textTheme.subtitle2?.merge(TextStyle(
                                    color: config.AppColors.placeholderColor)),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                S.hardwareConnectingIssueInstructions,
                                style: textTheme.bodyText1,
                              ),
                            ],
                          )),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
*/

                  Spacer(),

                  ///Box For Video Link
                  SizedBox(
                    width: (MediaQuery.of(context).size.width / 2) - 30,
                    child: GestureDetector(
                      onTap: () {},
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 15),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.16),
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                  color: Colors.white.withOpacity(0.24),
                                  width: 1)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                S.hardwareSupport,
                                style: textTheme.subtitle2,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                '${S.hardwareSupportInstructions}${S.supportNumber}',
                                style: textTheme.bodyText1,
                              ),
                            ],
                          )),
                    ),
                  ),

                  Spacer(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

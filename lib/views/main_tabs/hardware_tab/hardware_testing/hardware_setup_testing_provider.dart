import 'dart:async';
import 'dart:convert';

import 'package:brain_trainer/elements/headset/headset_sensor_point_indicator.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:brain_trainer/helper/impedance_helper.dart';
import 'package:brain_trainer/models/base_response.dart';
import 'package:brain_trainer/models/headband_models/cgx_eeg_data_model.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/hardware/mock_hardware_service.dart';

import '../../../../services/hardware/hardware_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HardwareSetUpTestingProvider extends ChangeNotifier {
  // Green = impedance < 1500 => SensorStatus.active
  // Yellow = impedance between 1501 and 3020  => SensorStatus.warning
  // Red = impedance > 3020 => SensorStatus.error

  SensorStatus sensorOneStatus,
      sensorTwoStatus,
      sensorThreeStatus,
      sensorFourStatus;
  bool isTestRunning = true;
  bool isTestFailed = false;

  BuildContext? context;

  var error;

  // StreamController<String> _headsetStateChangedController = StreamController<String>.broadcast();
  StreamSubscription<dynamic>? eegStreamSubscription;

  HardwareService hardwareService;

  HardwareSetUpTestingProvider(
    this.hardwareService, {
    this.sensorOneStatus = SensorStatus.inactive,
    this.sensorTwoStatus = SensorStatus.inactive,
    this.sensorThreeStatus = SensorStatus.inactive,
    this.sensorFourStatus = SensorStatus.inactive,
  });

  @override
  void dispose() {
    eegStreamSubscription?.cancel();
    super.dispose();
  }

  Future<dynamic> listenToHeadsetDataState(BuildContext context) async {
    this.context = context;

    error = null;
    this.isTestRunning = true;
    this.sensorOneStatus = SensorStatus.inactive;
    this.sensorTwoStatus = SensorStatus.inactive;
    this.sensorThreeStatus = SensorStatus.inactive;
    this.sensorFourStatus = SensorStatus.inactive;
    notifyListeners();

    hardwareService.startEEGAcquisition();


    eegStreamSubscription =
        hardwareService.eegDataStream().listen((value) {
          if(value != null && value is String) {
            final event = EEGData.fromJson(json.decode(value));
            if (event is EEGData) {
              //TestRunning to false
              //Set Sensors'statuses corresponds to values
              this.isTestRunning = false;

              //TestFailed only if impedance' values are > 1500
              if (event.impedance != null) {
                if (event.impedance!.isNotEmpty) {
                  //TODO: Find set Impedance' correct values corresponds to the correct Channels
                  //For now, taking values as
                  //Front Channel 1 = impedance[0]
                  //Front Channel 2 = impedance[1]
                  //Back Channel 3 = impedance[2]
                  //Back Channel 4 = impedance[3]

                  sensorOneStatus = event.impedance!.length >= 1
                      ? ImpHelper.getSensorStatusFromImpedance(event.impedance![0])
                      : SensorStatus.error;
                  sensorTwoStatus = event.impedance!.length >= 2
                      ? ImpHelper.getSensorStatusFromImpedance(event.impedance![1])
                      : SensorStatus.error;
                  sensorThreeStatus = event.impedance!.length >= 3
                      ? ImpHelper.getSensorStatusFromImpedance(event.impedance![2])
                      : SensorStatus.error;
                  sensorFourStatus = event.impedance!.length >= 4
                      ? ImpHelper.getSensorStatusFromImpedance(event.impedance![3])
                      : SensorStatus.error;
                  isTestFailed = _anySensorFailed();
                  notifyListeners();
                }
                else {
                  _setTestFailedOnNullValues();
                }
              } else {
                _setTestFailedOnNullValues();
              }
            }
            else {
              _setTestFailedOnNullValues();
            }
          } else {
            _setTestFailedOnNullValues();
          }
      //cancel this immediately because we got the result
      eegStreamSubscription?.cancel();
      if (!isTestFailed) {
        hardwareService.stopEEGAcquisition();
        Navigator.of(context)
            .pushReplacementNamed(Routes.HARDWARE_SETUP_CALIBRATION);
      } else {
        eegStreamSubscription?.cancel();
      }
    });

  }

  void _setTestFailedOnNullValues() {
    //TestRunning False
    //TestFailed true
    //SensorsStatus to inactive
    this.isTestRunning = false;
    this.isTestFailed = true;
    this.sensorOneStatus = SensorStatus.inactive;
    this.sensorTwoStatus = SensorStatus.inactive;
    this.sensorThreeStatus = SensorStatus.inactive;
    this.sensorFourStatus = SensorStatus.inactive;
    notifyListeners();
  }

  bool _anySensorFailed() {
    return sensorOneStatus != SensorStatus.active ||
        sensorTwoStatus != SensorStatus.active ||
        sensorThreeStatus != SensorStatus.active ||
        sensorFourStatus != SensorStatus.active;
  }
}

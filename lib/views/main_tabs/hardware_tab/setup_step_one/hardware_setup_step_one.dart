import 'dart:ui';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/headset/sensors_detector.dart';
import 'package:brain_trainer/elements/responsive.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;

class HardwareSetupStepOne extends StatefulWidget {
  @override
  _HardwareSetupStepOneState createState() => _HardwareSetupStepOneState();
}

class _HardwareSetupStepOneState extends State<HardwareSetupStepOne> {

  void _handleButtonTap() {
    //Navigator to Test Connection
    Navigator.of(context).pushNamed(Routes.HARDWARE_SETUP_TESTING);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    var detectorPathSize = 222.78;

    var bottomPadding = 228.0;

    return Scaffold(
      body: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 25, sigmaY: 25),
        child: Material(
          color: theme.primaryColor.withOpacity(0.5),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[

                MainAppbar(
                  onBack: (){},
                ),

                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 40),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        width: size.width,
                      ),
                      Text(
                        S.hardwareSetup,
                        style: textTheme.headline1,
                      ),
                      Text(
                        S.headsetFitting,
                        style: textTheme.headline3,
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 32,
                ),

                Container(
                    height: detectorPathSize,
                    width: detectorPathSize,
                    child: Center(child: SensorsDetector())),

                SizedBox(
                  height: 32,
                ),

                FractionallySizedBox(widthFactor: 334/768, child: Text(S.forAnOptimalConnection, style: textTheme.headline4,)),

                SizedBox(height: 20,),

                FractionallySizedBox(
                    widthFactor: 334/768,
                    child: Text(
                      S.stepOneInstructions,
                      style: textTheme.bodyText1,
                    )),

                // StepInstructions(headline: S.stepOne, instructions: S.stepOneInstructions,),

                SizedBox(
                  height: 32,
                ),

                _buildNextButton(context, textTheme),

                _buildLearnMoreInstructions(context, textTheme),

                SizedBox(
                  height: bottomPadding,
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }


  Widget _buildNextButton(BuildContext context, TextTheme textTheme) {
    return FractionallySizedBox(
      widthFactor: 334/768,
      child: SizedBox(
        height: 56,
        child: ElevatedButton(
          onPressed: _handleButtonTap,
          style: ElevatedButton.styleFrom(
            shape: StadiumBorder(),
          ),
          child: Center(
            child: Text(
              S.testConnection.toUpperCase(),
              style: textTheme.headline6,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildLearnMoreInstructions(
      BuildContext context, TextTheme textTheme) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 64,
          ),
          Text(
            S.learnMore,
            style: textTheme.headline3,
          ),
          SizedBox(
            height: 39,
          ),
          Container(
            child: IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  ///Box For Video Link
                  /*
                  Expanded(
                    child: GestureDetector(
                      onTap: () {},
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 15),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.16),
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                  color: Colors.white.withOpacity(0.24),
                                  width: 1)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                S.hardwareConnectingIssue,
                                style: textTheme.subtitle2?.merge(TextStyle(
                                    color:
                                    config.AppColors.placeholderColor)),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                S.hardwareConnectingIssueInstructions,
                                style: textTheme.bodyText1,
                              ),
                            ],
                          )),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  */

                  Spacer(),
                  ///Box For Video Link
                  SizedBox(
                    width: (MediaQuery.of(context).size.width / 2) - 30,
                    child: GestureDetector(
                      onTap: () {},
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 15),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.16),
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                  color: Colors.white.withOpacity(0.24),
                                  width: 1)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                S.hardwareSupport,
                                style: textTheme.subtitle2,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                '${S.hardwareSupportInstructions}${S.supportNumber}',
                                style: textTheme.bodyText1,
                              ),
                            ],
                          )),
                    ),
                  ),

                  Spacer(),

                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

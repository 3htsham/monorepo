import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
import 'package:brain_trainer/services/firebase/firestore_db_service.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'session_result/session_result.dart';
import 'session_result/session_result_provider.dart';

class ReportsTabPage extends StatefulWidget {
  static String tabName = S.reports;

  @override
  _ReportsTabPageState createState() => _ReportsTabPageState();
}

class _ReportsTabPageState extends State<ReportsTabPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (BuildContext context) => SessionResultProvider(
            firebaseAuthService: context.read<FirebaseAuthService>(),
            db: context.read<FirestoreDBService>(),
            trainingService: context.read<TrainingService>()),
        child: GameSessionResult());
  }
}

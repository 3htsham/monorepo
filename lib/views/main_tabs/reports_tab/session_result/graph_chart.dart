import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

class ResultGraph extends StatefulWidget {
  final int? maxValue;
  final double cap;
  final Color? chartColor;
  final double chartWidth;
  final bool showChartArea;
  final List<FlSpot>? chartValues;
  final double threshold;

  ResultGraph(
      {Key? key,
      @required this.maxValue,
      @required this.chartValues,
      this.cap = 5,
      this.chartColor = Colors.transparent,
      this.chartWidth = 4,
      this.showChartArea = false,
      this.threshold = 0})
      : super(key: key);

  @override
  _ResultGraphState createState() => _ResultGraphState();
}

class _ResultGraphState extends State<ResultGraph> {
  double maxTarget = 100;

  List<Color> gradientColors = [
    const Color(0xff02d39a),
  ];

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Stack(
          children: <Widget>[
            Container(
              // color: Color(0xff232d37),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      height: 49,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          S.aboveTarget,
                          style: Theme.of(context)
                              .textTheme
                              .headline4?.copyWith(color: AppColors.greenColor),
                        ),
                      )),
                  Expanded(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: LineChart(
                        mainData(context),
                      ),
                    ),
                  ),
                  Container(
                      height: 49,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(S.belowTarget,
                            style: Theme.of(context).textTheme.headline4),
                      )),
                ],
              ),
            ),
            //49 22 bottom padding, 40 is box height
            Positioned(
              right: 0,
              top: ((constraints.maxHeight - 49 - 22) *
                      (maxTarget - widget.threshold) /
                      maxTarget) -
                  40,
              child: Container(
                color: Theme.of(context).backgroundColor,
                width: 80,
                height: 100,
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(S.expertStateTarget,
                      style: Theme.of(context).textTheme.headline4),
                ),
              ),
            )
          ],
        );
      },
    );
  }

  LineChartData mainData(BuildContext context) {
    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: false,
        horizontalInterval: 10,
        getDrawingHorizontalLine: (value) {
          // custom return widget or show middle target
          if (value == widget.threshold) {
            return FlLine(color: Colors.white, strokeWidth: 4);
          }

          if (value == 0 || value == maxTarget) {
            return FlLine(
              color: Colors.white,
              strokeWidth: 4,
            );
          }
          return FlLine(
            color: Colors.white.withOpacity(0.3),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: Colors.transparent,
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        rightTitles: SideTitles(
          showTitles: false,
          reservedSize: 0,
          margin: 0,
        ),
        leftTitles: SideTitles(
          showTitles: false,
          reservedSize: 0,
          margin: 0,
        ),
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (value) => const TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),
          getTitles: (value) {
            if ((value.toInt() % widget.cap) == 0)
              return value.toInt().toString();
            else
              return "";
          },
          margin: 8,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border(
              top: BorderSide(
                color: Colors.white,
              ),
              bottom: BorderSide(
                color: Colors.white,
              ))),
      minX: 0,
      maxX: widget.maxValue!.toDouble(),
      minY: 0,
      maxY: maxTarget,
      lineTouchData: LineTouchData(
        touchTooltipData: LineTouchTooltipData(
            tooltipBgColor: Colors.white,
            getTooltipItems: (List<LineBarSpot> spots) {
              return spots.map((LineBarSpot spot) {
                return LineTooltipItem(
                    spot.y.toInt().toString(), //any text you want to show
                    Theme.of(context)
                        .textTheme
                        .headline6!.copyWith(color: widget.chartColor) //any style you want
                    );
              }).toList();
            }),
      ),
      lineBarsData: [
        LineChartBarData(
          gradientFrom: Offset(0, 1),
          gradientTo: Offset(0, -1),
          spots: widget.chartValues ?? [],
          isCurved: true,
          preventCurveOverShooting: true,
          colors: [
                Colors.white,
                Color(0xFF79B87B),
                widget.chartColor ?? Colors.transparent
              ],
          barWidth: widget.chartWidth,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: widget.showChartArea,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }
}

import 'dart:math';

// import 'package:flame/components/text_component.dart';
// import 'package:flame/text_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

// import '../../../../config/common_style.dart';
import '../../../../extensions/float32_circular_buffer.dart';
// : GET EVENT timestamp: {
// I/flutter ( 4820):   seconds: 1623687359
// I/flutter ( 4820):   nanos: 244378134
// I/flutter ( 4820): }
// I/flutter ( 4820): type: DEVICE_EVENT
// I/flutter ( 4820): deviceEvent: {
// I/flutter ( 4820):   type: DEVICE_ACQUISITION_SAMPLES
// I/flutter ( 4820):   device: {
// I/flutter ( 4820):     name: M4 Simulator
// I/flutter ( 4820):     hash: 192.168.31.104:36142
// I/flutter ( 4820):     address: 192.168.31.104:36142
// I/flutter ( 4820):     type: NETWORK
// I/flutter ( 4820):   }
// I/flutter ( 4820):   samples: {
// I/flutter ( 4820):     sample: {
// I/flutter ( 4820):       battery: 27.890625
// I/flutter ( 4820):       LPRP: 5499.57470703125
// I/flutter ( 4820):       LFLP: -32523.13671875
// I/flutter ( 4820):       RFRP: 32679.21484375
// I/flutter ( 4820):       pulseRate: 170.88070678710938
// I/flutter ( 4820):       alphaPower: 47314.765625
// I/flutter ( 4820):       alphaMean: 39041.15695714649
// I/flutter ( 4820):       normalizedZScore: 0.6183082512478966
// I/flutter ( 4820):     }
// I/flutter ( 4820):   }
// I/flutter ( 4820): }
// I/flutter ( 4820): Z SCORE FROM EVENT 0.6183082512478966

class GraphPainter extends CustomPainter {
  GraphPainter(this.zScores);
  List<Offset> points = List<Offset>.generate(100, (index) => Offset(0.0, 0.0));
  Float32CircularBuffer? zScores = new Float32CircularBuffer(100);
  DateTime lastEventTime = DateTime.now();
  // TextConfig regular;
  // TextConfig hudTextConfig;
  // TextConfig greenLabelTextConfig;
  // TextConfig redLabelTextConfig;

  // TextComponent scoreText, timeText, aboveThresholdText, belowThresholdText;

  Random rnd = Random();

  num maxSamples = 1536;
  double circleSize = 10.0;

  double zScoreScaleFactor = 631.0;

  double? graphAreaStart;
  double threshold = 50;
  Rect? graphRect;

  Duration? gameTime;
  DateTime? endTime;
  final graphPointOffset = 100;

  Color optiosCorporateDarkerBlue = const Color(0xff003743);
  Color optiosCorporateDarkTeal = const Color(0xff008899);

  Function? exitFunction;

  @override
  void paint(Canvas canvas, Size size) {
    // int index = points.length - 1;
    // graphAreaStart = size.height * 0.3;
    // graphRect = Rect.fromLTWH(
    //     0,
    //     size.height - graphAreaStart - zScoreScaleFactor,
    //     size.width,
    //     zScoreScaleFactor);
    // // zScores.insert(0.5);
    //
    // zScores?.forEach((score) {
    //   double dy = graphRect.bottom - score * zScoreScaleFactor;
    //   if (dy < graphRect.top) {
    //     dy = graphRect.top;
    //   }
    //   var dx = (graphRect.size.width / zScores.length) * index;
    //   points[index] = Offset(dx, dy);
    //   print(points[index]);
    //   index--;
    // });
    // renderBack(canvas, size);
    // renderGraph(canvas);
    // renderHUD(canvas);
  }

  void renderHUD(Canvas canvas) {
    //scoreText?.render(canvas);
    //timeText?.render(canvas);
  }

  void renderGraph(Canvas canvas) {
    // Paint lineAbovePaint = Paint()
    //   ..color = optiosFunctionalGreen
    //   ..strokeWidth = 4;
    //
    // Paint lineBelowPaint = Paint()
    //   ..color = Colors.white
    //   ..strokeWidth = 4;
    //
    // Offset lastPoint;
    //
    // points.forEach((element) {
    //   if (lastPoint != null) {
    //     if (element.dy < thresholdY()) {
    //       canvas.drawLine(lastPoint, element, lineAbovePaint);
    //     } else {
    //       canvas.drawLine(lastPoint, element, lineAbovePaint);
    //     }
    //   }
    //   lastPoint = element;
    // });

    //canvas.drawPoints(PointMode.lines, points, linePaint);
  }

  // double thresholdY() {
    // return graphRect.bottom - threshold / 100.0 * zScoreScaleFactor;
  // }

  void renderBack(Canvas canvas, Size size) {
    // Rect bgRect = Rect.fromLTWH(0, 0, size.width, size.height);
    //
    // Paint linePaint = Paint()
    //   ..color = optiosAdditionalLightGrey
    //   ..strokeWidth = 1;
    //
    // for (double dy = graphRect.top;
    //     dy <= graphRect.bottom;
    //     dy = dy + zScoreScaleFactor / 10.0) {
    //   canvas.drawLine(Offset(0, dy), Offset(size.width, dy), linePaint);
    // }
    // Paint borderPaint = linePaint;
    // borderPaint.strokeWidth = 4;
    // canvas.drawLine(graphRect.topLeft, graphRect.topRight, borderPaint);
    // canvas.drawLine(graphRect.bottomLeft, graphRect.bottomRight, borderPaint);
    //
    // // render threshold
    // Paint thresholdPaint = Paint()
    //   ..color = Colors.white
    //   ..strokeWidth = 4;
    //
    // if (threshold != null) {
    //   canvas.drawLine(Offset(0.0, thresholdY()),
    //       Offset(size.width, thresholdY()), thresholdPaint);
    // }
  }

  @override
  bool shouldRepaint(GraphPainter oldDelegate) => false;
}

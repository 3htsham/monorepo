import 'dart:math';
import 'dart:ui';
import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/models/route_argument.dart';
import 'package:brain_trainer/models/training_session.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:brain_trainer/views/main_tabs/reports_tab/session_result/session_result_provider.dart';
import 'package:catcher/core/catcher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'package:brain_trainer/elements/others/date_range_picker.dart';
import 'package:brain_trainer/extensions/float32_circular_buffer.dart';
import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:brain_trainer/helper/datetime_helper.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'graph_chart.dart';
import 'package:date_time_format/date_time_format.dart';

class GameSessionResult extends StatefulWidget {
  ///Will receive onFinishCallback (optional) and gameType from 'arguments'
  RouteArgument? arguments;

  GameSessionResult({this.arguments});

  @override
  _GameSessionResultState createState() => _GameSessionResultState();
}

class _GameSessionResultState extends State<GameSessionResult> {
  final GlobalKey gameChangeDropdownKey = GlobalKey();

  Float32CircularBuffer zScores = new Float32CircularBuffer(100);
  Random rnd = Random();
  List<FlSpot> graphPoints = [];

  @override
  void initState() {
    super.initState();
  }

  fetchTrainingResult(TrainingSession? trainingSession) {
    if (trainingSession == null) return;

    List<double> z = trainingSession.summaryZScores;

    //List<double>.generate(30, (index) => rnd.nextInt(100) / 100.0);

    zScores.reset();
    graphPoints.clear();

    for (var i = 0; i < z.length; i++) {
      zScores.insert(z[i]);
      graphPoints.add(FlSpot(i.toDouble(), z[i]));
    }
  }

  void _handleTryAgain() {
    var gameType = context.read<SessionResultProvider>().gameType;
    String launpageRoute;
    // TrainingService trainingService = context.read<TrainingService>();
    // if (gameType == TrainingType.trainingType_Graph) {
    //   launpageRoute = Routes.GREEN_SEEKER_LAUNCH;
    // } else if (gameType == TrainingType.trainingType_PSR) {
    //   launpageRoute = Routes.PSR_LAUNCH;
    // } else if (gameType == TrainingType.trainingType_SpaceGame) {
    //   launpageRoute = Routes.UNIVERSE_LAUNCH;
    //   // routeName = Routes.UNIVERSE_MASTER_TRAIN;
    // }
    // Navigator.of(context).pushReplacementNamed(Routes.TRAINING_TIMER,
    //     arguments: RouteArgument(gameName: gameName, routeName: routeName));
    Catcher.navigatorKey?.currentState?.pop();
  }

  void _handleFinish() {
    if (widget.arguments?.onFinishCallBack != null) {
      widget.arguments?.onFinishCallBack?.call();
    }
  }

  @override
  Widget build(BuildContext context) {
    var gameType = context
        .select<SessionResultProvider, TrainingType?>((value) => value.gameType);
    // latestSession will be the latest session of the given game type
    fetchTrainingResult(context.read<SessionResultProvider>().latestSession);
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: MainAppbar(
          showBatteryIndicator: true,
          dropDownGameButton:
              _buildGameTypeSelection(context, key: gameChangeDropdownKey),
        ),
        body: Material(
            color: Theme.of(context).backgroundColor,
            child: SingleChildScrollView(
              primary: true,
              padding:
                  EdgeInsets.symmetric(horizontal: 40).copyWith(bottom: 100),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 42,
                  ),
                  Text(
                    S.gameSessionResult.toUpperCase(),
                    style: Theme.of(context)
                        .textTheme
                        .headline1
                        ?.copyWith(fontSize: 80),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  _buildLatestTrainingSessionReport(context),
                  SizedBox(
                    height: 96,
                  ),
                  Text(
                    S.expertStateMagnitudeOverTime,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(fontSize: 32, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 36,
                  ),
                  SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 631,
                      child: ResultGraph(
                          chartColor: AppColors.greenColor,
                          chartValues: graphPoints,
                          maxValue: graphPoints.length,
                          threshold: 50)),
                  SizedBox(
                    height: 34,
                  ),
                  if (widget.arguments?.onFinishCallBack != null)
                    buildTrainAgainAndFinish(),
                  if (widget.arguments?.onFinishCallBack != null)
                    SizedBox(height: 95),
//                  gameType == TrainingType.trainingType_PSR
//                      ? _buildPSRecent(context)
//                      : _buildAverageRecent(gameType),
                  _buildAverageRecent(gameType ?? TrainingType.trainingType_Graph),
                  SizedBox(
                    height: 96,
                  ),
                  _buildPastReports(context)
                ],
              ),
            )));
  }

  Widget _buildPSRecent(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          S.recentSections,
          style: Theme.of(context)
              .textTheme
              .bodyText1
              ?.copyWith(fontSize: 32, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 30),
        _buildRecentSectionInfoRow(
            [S.recentSections, S.duration, S.endedInExpertState],
            Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 16)),
        SizedBox(
          height: 23,
        ),
        _buildRecentSections(context),
      ],
    );
  }

  Widget _buildAverageRecent(TrainingType gameType) {
    return Column(
      children: <Widget>[
        Text(
          S.mostRecentAverageTime(
              TrainingSession.trainingTypeValue.reverse[gameType] ?? ""),
          style: Theme.of(context)
              .textTheme
              .bodyText1
              ?.copyWith(fontSize: 32, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 31),
        _buildDateRangePickerButton(context),
        SizedBox(height: 32),
        _buildMostRecentAverageBarChart(context),
      ],
    );
  }

  Widget _buildLatestTrainingSessionReport(BuildContext context) {
    var latestSession = context.watch<SessionResultProvider>().latestSession;
    var date = latestSession?.startTime ?? DateTime.now();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          date.format(AppDateFormat.normalDateFormat) + ' Overview',
          style: Theme.of(context)
              .textTheme
              .bodyText1
              ?.copyWith(fontSize: 32, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 30,
        ),
        _buildTrainingTimeInfo(context, latestSession),
      ],
    );
  }

  Widget _buildTrainingTimeInfo(
      BuildContext context, TrainingSession? session) {
    var trainingTime = session == null
        ? 0
        : session.endTime
            ?.difference(session.startTime ?? DateTime.now())
            .inSeconds;
    var timeInZone = session == null ? 0 : session.timeInZone?.inSeconds ?? 0;
    var percent = session == null || trainingTime == 0
        ? 0
        : timeInZone / (trainingTime ?? 1);
    return Row(children: [
      Expanded(
          child: _buildInfoBox(context,
              title: S.trainingTime, time: '$trainingTime sec')),
      SizedBox(width: 20),
      Expanded(
          child: _buildInfoBox(context,
              title: S.timeInExpertState,
              time: '$timeInZone sec',
              percent: '(${(percent * 100.0).round()}%)'))
    ]);
  }

  Widget _buildInfoBox(BuildContext context,
      {String title = "", String time = "", String? percent = ""}) {
    var textTheme = Theme.of(context).textTheme;
    return Container(
        // height: 114,
        padding: EdgeInsets.symmetric(horizontal: 32).copyWith(top: 12),
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.12),
            borderRadius: BorderRadius.circular(15),
            border:
                Border.all(color: Colors.white.withOpacity(0.24), width: 1)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: textTheme.bodyText1?.copyWith(fontSize: 16),
              maxLines: 1,
            ),
            SizedBox(
              height: 14,
            ),
            Text(
              time,
              style: textTheme.subtitle2?.copyWith(fontSize: 32),
              maxLines: 1,
            ),
            SizedBox(
              height: 6,
            ),
            percent != null
                ? Text(
                    percent,
                    style: textTheme.bodyText1
                        ?.copyWith(fontSize: 14, color: AppColors.yellow),
                  )
                : SizedBox()
          ],
        ));
  }

  Widget buildTrainAgainAndFinish() {
    return Row(children: [
      Expanded(
        child: MainAppButton(
          onTap: () {
            // AppKeys.mainTabsKey.currentState?.onTabTapped(1);
            _handleTryAgain();
          },
          title: S.trainAgain.toUpperCase(),
          width: 191,
          height: 56,
        ),
      ),
      SizedBox(width: 20),
      Expanded(
        child: MainAppButton(
          onTap: () {
            _handleFinish();
          },
          title: S.finish.toUpperCase(),
          width: 191,
          height: 56,
        ),
      )
    ]);
  }

  Widget _buildRecentSections(BuildContext context) {
    var filteredSessions =
        context.select<SessionResultProvider, List<TrainingSession>>(
            (value) => (value.allSessions ?? <TrainingSession>[]));
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        primary: false,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (ctx, index) =>
            _buildRecentSectionsItem(filteredSessions[index]),
        itemCount: filteredSessions.length,
        scrollDirection: Axis.horizontal,
      ),
    );
  }

  Widget _buildRecentSectionsItem(TrainingSession? session) {
    var trainingTime = session == null
        ? 0
        : session.endTime?.difference(session.startTime!).inSeconds;
    var endedInExpertState = true;
    return InkWell(
        onTap: () {
          Navigator.of(context)
              .pushNamed(Routes.GAMESESSIONRESULT_DETAIL, arguments: session);
        },
        child: Column(
          children: <Widget>[
            UIHelper.getHorizontalDivider(),
            SizedBox(
              height: 9,
            ),
            _buildRecentSectionInfoRow(
                [
                  DateTimeHelper.formatMMdd(
                      session?.startTime! ?? DateTime.now()),
                  trainingTime.toString() + ' sec',
                  endedInExpertState ? 'Yes' : 'No'
                ],
                Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.copyWith(fontSize: 16, fontWeight: FontWeight.bold)),
            SizedBox(
              height: 23,
            )
          ],
        ));
  }

  Widget _buildRecentSectionInfoRow(List<String> texts, TextStyle? textStyle) {
    return Row(
      children: texts
          .map((e) => Expanded(
                  child: Text(
                e,
                style: textStyle,
                textAlign: TextAlign.left,
              )))
          .toList(),
    );
  }

  Widget _buildMostRecentAverageBarChart(BuildContext context) {
    var barChartData = context.watch<SessionResultProvider>().barChartData;
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(S.percentTimeInExpertState,
                  style: Theme.of(context).textTheme.headline6),
              Expanded(
                child: SizedBox(),
              ),
              _buildBarchartLegend(S.good, Colors.grey, context),
              _buildBarchartLegend(S.excellent, AppColors.yellow, context),
              _buildBarchartLegend(S.superior, AppColors.greenColor, context)
            ],
          ),
          SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: barChartData != null
                ? Container(
                    height: 360,
                    width: MediaQuery.of(context).size.width,
                    child: BarChart(barChartData))
                : SizedBox(),
          ),
          const SizedBox(
            height: 12,
          ),
        ],
      ),
    );
  }

  //past reports
  Widget _buildPastReports(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
            child: Text(
          S.pastReport,
          style: Theme.of(context)
              .textTheme
              .bodyText1
              ?.copyWith(fontSize: 32, fontWeight: FontWeight.bold),
        )),
        Expanded(child: _buildListPastReports())
      ],
    );
  }

  Widget _buildListPastReports() {
    var sessions = context.watch<SessionResultProvider>().allSessions?.reversed.toList();
    return ListView.builder(
        primary: false,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (ctx, index) {
          return _buildPastReportItem(sessions?[index]);
        },
        itemCount: sessions?.length ?? 0,
        shrinkWrap: true);
  }

  Widget _buildPastReportItem(TrainingSession? session) {
    if (session == null) return SizedBox();
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .pushNamed(Routes.GAMESESSIONRESULT_DETAIL, arguments: session);
      },
      child: Column(
        children: <Widget>[
          Container(height: 1, color: Colors.white),
          Container(
              padding: EdgeInsets.symmetric(vertical: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                      '${DateTimeHelper.formatNormalDate(session.startTime ?? DateTime.now())}: ${TrainingSession.trainingTypeValue.reverse[TrainingSession.trainingTypesEnum[session.trainingType]]}',
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(
                          fontSize: 16, fontWeight: FontWeight.bold)),
                  Image.asset(AppAssets.rightArrow,
                      width: 12, height: 12, color: Colors.white)
                ],
              ))
        ],
      ),
    );
  }

  Widget _buildBarchartLegend(String title, Color color, BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Row(children: <Widget>[
        Container(
            width: 20,
            height: 20,
            decoration: BoxDecoration(color: color, shape: BoxShape.circle)),
        SizedBox(width: 4),
        Text(title, style: Theme.of(context).textTheme.headline6)
      ], mainAxisSize: MainAxisSize.min),
    );
  }

  //game select
  Widget _buildGameTypeSelection(BuildContext context, {GlobalKey? key}) {
    var currentGameType = context
        .select<SessionResultProvider, TrainingType?>((value) => value.gameType);
    return TextButton(
        key: key,
        onPressed: () {
          _showGameSelectionDropdown(key, context, currentGameType ?? TrainingType.trainingType_Graph);
        },
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Text(
            TrainingSession.trainingTypeValue.reverse[currentGameType] ?? "",
            style:
                Theme.of(context).textTheme.headline4?.copyWith(fontSize: 20),
          ),
          SizedBox(width: 12),
          Image.asset(AppAssets.arrowDownIcon, width: 24, height: 24)
        ]));
  }

  void _showGameSelectionDropdown(
      GlobalKey? key, BuildContext context, TrainingType gameType) {
    RenderBox renderBox = key?.currentContext?.findRenderObject() as RenderBox;
    var width = renderBox.size.width;
    UIHelper.showDialogWithPositionAndBlur(
        key!, context, _buildGameSelectDropDown(context, width, gameType));
  }

  Widget _buildGameSelectDropDown(
      BuildContext context, double width, TrainingType currentGameType) {
    var gameTypeOptions = TrainingSession.trainingTypeValue.map.values
        .where((element) => element != currentGameType)
        .toList();
    return Container(
        width: width,
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.16),
            borderRadius: BorderRadius.circular(15),
            border:
                Border.all(color: Colors.white.withOpacity(0.24), width: 1)),
        child: ListView.builder(
          shrinkWrap: true,
          primary: false,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (ctx, index) {
            var gameType = gameTypeOptions[index];
            return _buildGameSelectOption(context,
                TrainingSession.trainingTypeValue.reverse[gameType] ?? "", () {
              context.read<SessionResultProvider>().onGameChange(gameType);
              UIHelper.hideGeneralDialog();
            });
          },
          itemCount: gameTypeOptions.length,
        ));
  }

  Widget _buildGameSelectOption(
      BuildContext context, String title, Function? onTap) {
    return TextButton(
        style: TextButton.styleFrom(
          padding: EdgeInsets.all(8),
        ),
        onPressed: () {
          onTap?.call();
        },
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline4?.copyWith(fontSize: 20),
        ));
  }

  //date picker
  Widget _buildDateRangePickerButton(BuildContext context, {GlobalKey? key}) {
    var startDate = context.select<SessionResultProvider, DateTime>(
        (value) => value.reportStartDate);
    var endDate = context.select<SessionResultProvider, DateTime>(
        (value) => value.reportEndDate);
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        TextButton(
            style: TextButton.styleFrom(
              primary: Colors.white.withOpacity(0.08),
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Colors.white.withOpacity(0.16),
                      width: 1,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(8)),
            ),
            key: key,
            onPressed: () {
              showDateRangePicker(context);
            },
            child: Container(
              // width: 198,
              height: 49,
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Row(mainAxisSize: MainAxisSize.min, children: [
                Text(
                  '${DateTimeHelper.formatMMMdd(startDate)} - ${DateTimeHelper.formatSimpleMMddyy(endDate)}',
                  style: Theme.of(context)
                      .textTheme
                      .headline4
                      ?.copyWith(fontSize: 20),
                ),
                SizedBox(width: 12),
                Image.asset(AppAssets.arrowDownIcon, width: 24, height: 24)
              ]),
            )),
        Row(children: [
          Container(
            child: InkWell(
              child: Image.asset(AppAssets.leftArrow, height: 40, width: 40),
              onTap: () {
                context.read<SessionResultProvider>().changePage(-1);
              },
            ),
          ),
          SizedBox(
            width: 12,
          ),
          Container(
            width: 25,
            child: InkWell(
              child: Image.asset(AppAssets.rightArrow, height: 40, width: 40),
              onTap: () {
                context.read<SessionResultProvider>().changePage(1);
              },
            ),
          )
        ])
      ],
    );
  }

  void showDateRangePicker(BuildContext context) {
    UIHelper.showDialogBlur(
        context,
        DateRangePicker(
          currentDate: DateTime.now(),
          onDateRangeSelected:
              context.read<SessionResultProvider>().onDateRangeSelected,
        ));
  }
}

import 'dart:math';
import 'dart:ui';
import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/models/training_session.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/extensions/float32_circular_buffer.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:date_time_format/date_time_format.dart';
import 'graph_chart.dart';

class SessionResultDetail extends StatelessWidget {
  final GlobalKey gameChangeDropdownKey = GlobalKey();

  Float32CircularBuffer zScores = new Float32CircularBuffer(100);
  Random rnd = Random();
  List<FlSpot> graphPoints = [];

  TrainingSession? trainingSession;

  SessionResultDetail({@required this.trainingSession}) {
    fetchTrainingResult();
  }

  fetchTrainingResult() {
    List<double> z = trainingSession!.summaryZScores;

    zScores.reset();
    graphPoints.clear();

    //List<double>.generate(30, (index) => rnd.nextInt(100) / 100.0);
    for (var i = 0; i < z.length; i++) {
      zScores.insert(z[i]);
      graphPoints.add(FlSpot(i.toDouble(), z[i]));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: MainAppbar(
          onBack: () {},
          // TODO: wait for new mainappbar with title
          //temp commented
          dropDownGameButton:
              _buildGameTypeSelection(context, key: gameChangeDropdownKey),
        ),
        body: Material(
            color: Theme.of(context).backgroundColor,
            child: SingleChildScrollView(
              padding:
                  EdgeInsets.symmetric(horizontal: 40).copyWith(bottom: 100),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 42,
                  ),
                  Text(
                    S.gameSessionResult.toUpperCase(),
                    style: Theme.of(context)
                        .textTheme
                        .headline1
                        ?.copyWith(fontSize: 80),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  _buildLatestTrainingSessionReport(context),
                  SizedBox(
                    height: 38,
                  ),
                  SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 631,
                      child: ResultGraph(
                          chartColor: AppColors.greenColor,
                          chartValues: graphPoints,
                          maxValue: graphPoints.length,
                          threshold: 50)),
                ],
              ),
            )));
  }

  Widget _buildLatestTrainingSessionReport(BuildContext context) {
    var date = trainingSession?.startTime ?? DateTime.now();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          date.format(AppDateFormat.normalDateFormat) + ' Overview',
          style: Theme.of(context)
              .textTheme
              .bodyText1
              ?.copyWith(fontSize: 32, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 30,
        ),
        _buildTrainingTimeInfo(context, trainingSession),
      ],
    );
  }

  Widget _buildTrainingTimeInfo(
      BuildContext context, TrainingSession? session) {
    var trainingTime = session == null
        ? 0
        : session.endTime
            ?.difference(session.startTime ?? DateTime.now())
            .inSeconds;
    var timeInZone = session == null ? 0 : session.timeInZone?.inSeconds ?? 0;
    var percent = session == null || trainingTime == 0
        ? 0
        : timeInZone / (trainingTime ?? 1);
    return Row(children: [
      Expanded(
          child: _buildInfoBox(context,
              title: S.trainingTime, time: '$trainingTime sec')),
      SizedBox(width: 20),
      Expanded(
          child: _buildInfoBox(context,
              title: S.timeInExpertState,
              time: '$timeInZone sec',
              percent: '(${(percent * 100.0).round()}%)'))
    ]);
  }

  Widget _buildInfoBox(BuildContext context,
      {String title = "", String time = "", String? percent}) {
    var textTheme = Theme.of(context).textTheme;
    return Container(
        height: 167,
        padding: EdgeInsets.symmetric(horizontal: 32, vertical: 20),
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.16),
            borderRadius: BorderRadius.circular(15),
            border:
                Border.all(color: Colors.white.withOpacity(0.24), width: 1)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: textTheme.bodyText1?.copyWith(fontSize: 16),
              maxLines: 1,
            ),
            SizedBox(
              height: 14,
            ),
            Text(
              time,
              style: textTheme.subtitle2?.copyWith(fontSize: 32),
              maxLines: 1,
            ),
            SizedBox(
              height: 22,
            ),
            percent != null
                ? Text(
                    percent,
                    style: textTheme.bodyText1
                        ?.copyWith(fontSize: 14, color: AppColors.yellow),
                  )
                : SizedBox()
          ],
        ));
  }

  Widget _buildGameTypeSelection(BuildContext context, {GlobalKey? key}) {
    var currentGameType = TrainingType.trainingType_Graph;
    TrainingSession.trainingTypesStrings.forEach((key, value) {
      if (value == this.trainingSession?.trainingType) {
        currentGameType = key;
      }
    });
    return TextButton(
        key: key,
        onPressed: () {},
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Text(
            TrainingSession.trainingTypeValue.reverse[currentGameType] ?? "",
            style:
                Theme.of(context).textTheme.headline4?.copyWith(fontSize: 20),
          ),
        ]));
  }
}

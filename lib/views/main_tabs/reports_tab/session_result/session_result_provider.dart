import 'dart:async';
import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/helper/datetime_helper.dart';
import 'package:brain_trainer/models/training_session.dart';
import 'package:brain_trainer/services/firebase/firestore_db_service.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import '../../../../services/firebase/firebase_auth_service.dart';

class SessionResultProvider extends ChangeNotifier {
  //injected services
  FirebaseAuthService? firebaseAuthService;
  FirestoreDBService? db;
  TrainingService? trainingService;

  //current game type
  TrainingType? gameType = TrainingType.trainingType_Graph;

  //sessions
  List<TrainingSession>? allSessions;
  List<TrainingSession> get filteredSessions => _getSortedAndFilteredSession();
  TrainingSession? get latestSession =>
      filteredSessions.length > 0 ? filteredSessions.last : null;
  StreamSubscription? sessionsSubscription;

  //most average barchart data
  BarChartData? barChartData;
  DateTime reportStartDate = DateTime.now().subtract(Duration(days: 6));
  DateTime reportEndDate = DateTime.now();
  DateTime filteredStartDate = DateTime.now().subtract(Duration(days: 6));
  DateTime filteredEndDate = DateTime.now();
  int maxDays = 7;

  //init
  SessionResultProvider(
      {this.firebaseAuthService,
      this.db,
      this.trainingService,
      this.gameType = TrainingType.trainingType_Graph}) {
    listenSessionsChange();
  }
  @override
  void dispose() {
    sessionsSubscription?.cancel();
    super.dispose();
  }

  //game change action
  void onGameChange(TrainingType gameType) {
    this.gameType = gameType;
    fetchMainBarData();
    notifyListeners();
  }

  int currentPage = 0;

  onDateRangeSelected(DateTime start, DateTime end) {
    this.reportStartDate = start;
    this.reportEndDate = end;
    print('date range changed: $start : $end');
    //reset the page
    currentPage = 0;
    //fetch new data to bar chart
    fetchMainBarData();
    notifyListeners();
  }

  Future<void> listenSessionsChange() async {
    sessionsSubscription =
        trainingService?.trainingSessionsStream.stream.listen((event) {
      if (event != null) {
        //sort event
        if (event.length > 0) {
          this.allSessions = event
            ..sort((s1, s2) {
              if (s1.startTime == null || s2.startTime == null) return 0;
              return s2.startTime!.compareTo(s1.startTime!);
            });

          //change the current game type to latest game played
          onGameChange(TrainingSession.trainingTypesEnum[
              allSessions?.first.trainingType ?? 'Space Game']!);
        }
        notifyListeners();
      }
    });
  }

  List<TrainingSession> _getSortedAndFilteredSession({bool isLatest = true}) {
    if (allSessions == null) return [];
    var result = allSessions
      ?..sort((s1, s2) {
        if (s1.startTime == null || s2.startTime == null) return 0;
        return isLatest
            ? s1.startTime!.compareTo(s2.startTime!)
            : s2.startTime!.compareTo(s1.startTime!);
      });

    return result!
        .where((element) =>
            element.trainingType ==
            TrainingSession.trainingTypesStrings[gameType])
        .toList();
  }

  void fetchMainBarData() {
    this.barChartData = BarChartData(
      alignment: BarChartAlignment.spaceEvenly,
      maxY: 100,
      barTouchData: BarTouchData(
          enabled: false,
          touchTooltipData: BarTouchTooltipData(
            tooltipBgColor: Colors.transparent,
            tooltipPadding: const EdgeInsets.all(0),
            // tooltipBottomMargin: 5,
            getTooltipItem: (
              BarChartGroupData group,
              int groupIndex,
              BarChartRodData rod,
              int rodIndex,
            ) {
              return BarTooltipItem(
                rod.y == 0 ? '' : rod.y.round().toString() + ' %',
                TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              );
            },
          )),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14),
          margin: 20,
          getTitles: (double value) {
            return DateTimeHelper.formatMMMdd(
                filteredStartDate.add(Duration(days: value.toInt())));
          },
        ),
        leftTitles: SideTitles(
            reservedSize: 20,
            margin: 20,
            interval: 10,
            showTitles: true,
            getTextStyles: (value) =>
                const TextStyle(color: Colors.white, fontSize: 14),
            getTitles: (double value) {
              return value.toInt().toString();
            }),
      ),
      gridData: FlGridData(
          show: true, drawHorizontalLine: true, horizontalInterval: 10,
          getDrawingHorizontalLine: (e) => FlLine(color: Colors.white.withOpacity(0.5), strokeWidth: 0.5)),
      borderData: FlBorderData(
          show: true,
          border: const Border(
              bottom: BorderSide(
                color: Colors.white,
                width: 1,
              ),
              left: BorderSide(color: Colors.white, width: 1))),
      barGroups: getBarchartDataGroup(),
    );
  }

  void changePage(int diff) {
    if (currentPage == 0 && (diff < 0)) {
      print('first page');
      return;
    }
    if (reportEndDate.difference(reportStartDate).inDays <
        (currentPage + diff) * maxDays) {
      print('last page');
      return;
    }
    currentPage += diff;
    print('current page $currentPage');
    fetchMainBarData();
    notifyListeners();
  }

  List<BarChartGroupData> getBarchartDataGroup() {
    var barchartData = <BarChartGroupData>[];
    //get limited max days date range
    if (reportEndDate.difference(reportStartDate).inDays >=
        currentPage * maxDays) {
      filteredStartDate =
          reportStartDate.add(Duration(days: currentPage * maxDays));
      filteredEndDate =
          reportEndDate.difference(filteredStartDate).inDays > maxDays
              ? filteredStartDate.add(Duration(days: 6))
              : reportEndDate;
      print('getBarchartDataGroup: filtered start: $filteredStartDate');
      print('getBarchartDataGroup: filtered end: $filteredEndDate');
      for (var i = 0; i < maxDays; i++) {
        var iDate = filteredStartDate.add(Duration(days: i));

        double ?yValue = getAverageZoneTimeOnDay(iDate);

        barchartData.add(BarChartGroupData(x: i, barRods: [
          BarChartRodData(
              y: yValue == null?0.0:yValue,
              colors: yValue == null
                  ? []
                  : yValue <= 30
                      ? [AppColors.inactiveSensor]
                      : yValue <= 60
                          ? [AppColors.yellow]
                          : [AppColors.greenColor],
              width: 40,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8), topRight: Radius.circular(8)))
        ], showingTooltipIndicators: [
          0
        ]));
      }
      return barchartData;
    } else {
      return [];
    }
  }

  double? getAverageZoneTimeOnDay(DateTime iDate) {

    double average = 0.0;
    double sum=0.0;
    int n=0;

    for (var element in this.filteredSessions) {
      if(DateTimeHelper.checkSameDate(iDate, element.startTime!)) {
        sum+=element.averageZone;
        n++;
      }
    }

    if(n==0) return null;

    average = sum/n.toDouble();
    return average;

  }
}

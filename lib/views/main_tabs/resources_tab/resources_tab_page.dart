import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ResourcesTabPage extends StatefulWidget {
  static String tabName = S.resources;

  @override
  _ResourcesTabPageState createState() => _ResourcesTabPageState();
}

class _ResourcesTabPageState extends State<ResourcesTabPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: IntrinsicHeight(
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 41,
                    child: Stack(
                      children: <Widget>[
                        Image.asset(AppAssets.resourcesFeaturedImage,
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.fill),
                        Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                Colors.black.withOpacity(0.56),
                                Colors.transparent
                              ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.center)),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 75,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 66,
                          ),
                          Text(
                            S.trainingTipsAndResources.toUpperCase(),
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                          ),
                          Divider(
                            color: Colors.white,
                            thickness: 0.5,
                          ),
                          _buildResourceItem(S.settingUpTheHeadgear,
                              S.settingUpHeadgearDescription, () {
                            Navigator.of(context)
                                .pushNamed(Routes.RESOURCE_SETTINGUPHEADSET);
                          }),
                          Divider(
                            color: Colors.white,
                            thickness: 0.5,
                          ),
                          _buildResourceItem(S.enteringTheExpertState,
                              S.practiceTheseSkillsConsistentlyForBodyRelaxation,
                              () {
                            Navigator.of(context)
                                .pushNamed(Routes.RESOURCE_ENTERINGEXPERTSTATE);
                          }),
                          Divider(
                            color: Colors.white,
                            thickness: 0.5,
                          ),
                          _buildResourceItem(S.craftingAPreShotRoutine,
                              S.consistentlyPracticeASimpleSeriesOfSteps, () {
                            Navigator.of(context).pushNamed(
                                Routes.RESOURCE_CRAFTING_PRESHOT_ROUTINE);
                          }),
                          SizedBox(
                            height: 71,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: MainAppbar(
              showBatteryIndicator: true,
            ),
          )
        ],
      ),
    );
  }

  InkWell _buildResourceItem(
      String title, String description, Function? onTap) {
    return InkWell(
      onTap: () {
        onTap?.call();
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 14),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(title, style: Theme.of(context).textTheme.subtitle2),
                  SizedBox(
                    height: 15,
                  ),
                  Text(description,
                      maxLines: 2,
                      overflow: TextOverflow.clip,
                      style: Theme.of(context).textTheme.bodyText1),
                ],
              ),
            ),
            IconButton(
              icon: Icon(
                CupertinoIcons.forward,
                color: Colors.white,
              ),
              onPressed: () {
                onTap?.call();
              },
            )
          ],
        ),
      ),
    );
  }
}

import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:flutter/material.dart';

class SettingUpHeadgear extends StatefulWidget {
  @override
  _SettingUpHeadgearState createState() => _SettingUpHeadgearState();
}

class _SettingUpHeadgearState extends State<SettingUpHeadgear> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: IntrinsicHeight(
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 41,
                    child: Stack(
                      children: <Widget>[
                        Image.asset(AppAssets.settingUpHeadgearResource,
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.fill),
                        Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                Colors.black.withOpacity(0.56),
                                Colors.transparent
                              ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.center)),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 70,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 64,
                          ),
                          Text(
                            S.settingUpTheHeadgear.toUpperCase(),
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                          ),
                          SizedBox(
                            height: 26,
                          ),
                          _buildTipItem(S.settingUpHeadGearTipOne,
                              S.settingUpHeadGearTipOneDetails),
                          SizedBox(
                            height: 32,
                          ),
                          _buildTipItem(S.settingUpHeadGearTipTwo,
                              S.settingUpHeadGearTipTwoDetails),
                          SizedBox(
                            height: 32,
                          ),
                          _buildTipItem(S.settingUpHeadGearTipThree,
                              S.settingUpHeadGearTipThreeDetails),
                          SizedBox(
                            height: 32,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: MainAppbar(
              onBack: () {},
              showBatteryIndicator: true,
            ),
          )
        ],
      ),
    );
  }

  Row _buildTipItem(String title, String details) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Text(
            title,
            style:
                Theme.of(context).textTheme.subtitle1,
          ),
        ),
        Expanded(
          flex: 7,
          child: Text(
            details,
            style:
                Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ],
    );
  }
}

import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;

class SupportTabPage extends StatefulWidget {
  static String tabName = S.support;

  @override
  _SupportTabPageState createState() => _SupportTabPageState();
}

class _SupportTabPageState extends State<SupportTabPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: IntrinsicHeight(
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 41,
                    child: Stack(
                      children: <Widget>[
                        Image.asset(AppAssets.supportFeatureImage,
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.fill),
                        Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                Colors.black.withOpacity(0.56),
                                Colors.transparent
                              ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.center)),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 70,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 66,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                S.serviceAndSupport.toUpperCase(),
                                style: Theme.of(context)
                                    .textTheme
                                    .headline2,
                              ),
                              Text(
                                S.supportNumber.toUpperCase(),
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Text(
                            S.hardwareConnectionTips,
                            style: Theme.of(context)
                                .textTheme
                                .headline3,
                          ),
                          SizedBox(
                            height: 71,
                          ),
                          _buildTipItem(S.tipOne, S.tipOneDetails),
                          SizedBox(
                            height: 32,
                          ),
                          _buildTipItem(S.tipTwo, S.tipTwoDetails),
                          SizedBox(
                            height: 32,
                          ),
                          _buildTipItem(S.tipThree, S.tipThreeDetails),
                          SizedBox(
                            height: 168,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: MainAppbar(
              showBatteryIndicator: true,
            ),
          )
        ],
      ),
    );
  }

  Row _buildTipItem(String title, String details) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Text(
            title,
            style:
                Theme.of(context).textTheme.headline4,
          ),
        ),
        Expanded(
          flex: 7,
          child: Text(
            details,
            style:
                Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ],
    );
  }
}

import 'dart:async';
import 'dart:math';
import 'dart:ui';
import 'dart:ui' as ui;
import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/config/common_style.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:flame/components.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flame/game.dart';

class GraphFeedbackGame extends BaseGame {
  StreamSubscription<dynamic>? _zScoreSubscription;
  TrainingService? trainingService;

  TextPaintConfig? regular;
  TextPaintConfig? hudTextConfig;
  TextPaintConfig? greenLabelTextConfig;
  TextPaintConfig? whiteLabelTextConfig;
  TextPaintConfig? timeLabelTextConfig;
  TextPaintConfig? timeTextConfig;

  TextComponent? scoreText,
      timeText,
      aboveThresholdText,
      belowThresholdText,
      timeLabelText,
      thresholdText;

  Random? rnd;

  double zScoreScaleFactor = 500.0;

  DateTime? lastEventTime;
  double? graphAreaStart;
  double? threshold;
  Rect? graphRect;

  double _currentZScore = 0.0;

  Duration? gameTime;
  DateTime? endTime;

  Function? exitFunction;

  final _thermometerHeight = 500.0;
  final _thermometerWidth = 48.0;
  final _thermometerMarkWidth = 25.0;
  final _thermometerX = 360;
  final _thermometerY = 338;

  var _thermometerStart = 0.0;
  var _middle = 0.0;

  GraphFeedbackGame(Size _size, Stream<double> stream, this.exitFunction,
      this.trainingService) {
    _zScoreSubscription = stream.listen(_streamUpdate);
    lastEventTime = DateTime.now();
    onResize(Vector2(_size.width, _size.height));
    initialize();
  }

  @override
  void onDetach() {
    super.onDetach();
    _zScoreSubscription?.cancel();
  }

  void initialize() async {
    onResize(this.size);
    rnd = Random();

    _thermometerStart = _thermometerY + _thermometerHeight;
    _middle = _thermometerY + (_thermometerHeight / 2);


    gameTime = trainingService!.trainingDuration;

    endTime = DateTime.now().add(gameTime!);

    threshold = trainingService!.zScoreThreshold.toDouble();

    regular = TextPaintConfig(color: AppColors.white, fontFamily: 'ObjektivMk2' );
    hudTextConfig = regular!.withFontSize(24.0);
    greenLabelTextConfig =
        regular!.withFontSize(16.0).withColor(AppColors.greenColor);
    whiteLabelTextConfig =
        regular!.withFontSize(16.0).withColor(AppColors.white);
    timeLabelTextConfig =
        regular!.withFontSize(24.0).withColor(AppColors.white);
    timeTextConfig = regular!.withFontSize(48.0).withColor(AppColors.white).withFontFamily('BabesNeue');

    aboveThresholdText = TextComponent('Above Target',
        textRenderer: TextPaint(config: greenLabelTextConfig!))
      ..anchor = Anchor.topCenter
      ..x = 384
      ..y = 299;

    belowThresholdText = TextComponent('Below Target',
        textRenderer: TextPaint(config: whiteLabelTextConfig!))
      ..anchor = Anchor.topCenter
      ..x = 384
      ..y = 854;

    timeLabelText = TextComponent('Time Remaining',
        textRenderer: TextPaint(config: timeLabelTextConfig!))
      ..anchor = Anchor.topCenter
      ..x = 384
      ..y = 136;

    timeText =
        TextComponent('Time', textRenderer: TextPaint(config: timeTextConfig!))
          ..anchor = Anchor.topCenter
          ..x = 384
          ..y = 178;

    scoreText = TextComponent('score',
        textRenderer: TextPaint(config: whiteLabelTextConfig!))
      ..anchor = Anchor.topCenter
      ..x = 384
      ..y = 900;

    thresholdText = TextComponent(S.ogStateTarget,
        textRenderer: TextPaint(config: whiteLabelTextConfig!))
      ..anchor = Anchor.centerLeft
      ..x = 453
      ..y = 474;

    add(aboveThresholdText!);
    add(belowThresholdText!);
    add(timeText!);
    add(timeLabelText!);
    add(thresholdText!);
    if (!kReleaseMode) {
      add(scoreText!);
    }
  }

  @override
  @mustCallSuper
  void update(double dt) {
    super.update(dt);

    if (endTime != null) {
      if (DateTime.now().isAfter(endTime!)) {
        exitFunction!();
        return;
      }
    }

    scoreText?.text = 'Score: ${(_currentZScore.toInt().toString())}';

    if (endTime != null) {
      String timeRemaining =
          endTime!.difference(DateTime.now()).inSeconds.toString() + " SEC";
      timeText?.text = '$timeRemaining';
    }

    // OG-72 0 put threshold in the middle
    thresholdText?.y = _middle;
  }

  @override
  @mustCallSuper
  void render(Canvas canvas) {
    //If no size information -> leave
    if (size == null) {
      return;
    }

    renderBack(canvas);
    renderThermometer(canvas);
    renderHUD(canvas);
    super.render(canvas);
  }

  void renderHUD(Canvas canvas) {
    //scoreText?.render(canvas);
    //timeText?.render(canvas);
  }

  void renderThermometer(Canvas canvas) {
    Paint thermometerBackgroundPaint = Paint()
      ..color = Color(0x0FFFFFFF)
      ..strokeWidth = 1;

    Paint thermometerMarkPaint = Paint()
      ..color = AppColors.white
      ..strokeWidth = 1;

    Paint thermometerPaint = Paint()
      ..color = AppColors.greenColor
      ..strokeWidth = 1;



    // marks

    for (int i = 1; i < 10; ++i) {
      canvas.drawLine(
          Offset(_thermometerX + _thermometerWidth, _thermometerY + (i * _thermometerHeight / 10)),
          Offset(_thermometerX + _thermometerWidth + _thermometerMarkWidth,
              _thermometerY + (i * _thermometerHeight / 10)),
          thermometerMarkPaint);
    }

    // threshold mark
    thermometerMarkPaint.strokeWidth=2.0;

    canvas.drawLine(
        Offset(_thermometerX + _thermometerWidth, _thermometerY + ((100.0-50.0) * _thermometerHeight / 100)),
        Offset(_thermometerX + _thermometerWidth + _thermometerMarkWidth,
            _thermometerY + ((100.0-50.0) * _thermometerHeight / 100)),
        thermometerMarkPaint);

    // gray background
    canvas.drawRRect(RRect.fromRectAndCorners(Rect.fromLTWH(
        _thermometerX.toDouble(), 340, _thermometerWidth, _thermometerHeight),
        topRight: Radius.circular(28), topLeft: Radius.circular(28),
        bottomRight: Radius.circular(28), bottomLeft: Radius.circular(28)
    ), thermometerBackgroundPaint);

    //_currentZScore = 51.0;

    // actual z
    var zHeight = ((_currentZScore / 100.0) * _thermometerHeight).round();

    //this.threshold = 50.0;


    if(_currentZScore <= this.threshold!)
      {
        // bottom part - scale from zero to 50
        var pixelsPerPoint = (_thermometerHeight/2) / threshold!;
        zHeight = ((_currentZScore * pixelsPerPoint).round());
      }
    else
      {
        // top part
        var pixelsPerPoint = (_thermometerHeight/2) / (100-threshold!);
        zHeight = ((_thermometerHeight/2) +  (((_currentZScore - threshold!) * pixelsPerPoint))).round();

      }

    thermometerPaint = thermometerPaint
      ..shader = LinearGradient(
        colors: [
          AppColors.gradientLight1,
          AppColors.gradientLight2
        ],
        begin: Alignment.bottomCenter,
        end: Alignment.topCenter,
      ).createShader(Rect.fromLTRB(
          _thermometerX.toDouble(), 838.0 - zHeight.toDouble(), _thermometerX + _thermometerWidth, 838));

    Rect thermRect = Rect.fromLTRB(
        _thermometerX.toDouble(), 838.0 - zHeight.toDouble(), _thermometerX + _thermometerWidth, 838);

    canvas.drawRRect(RRect.fromRectAndCorners(thermRect, topRight: Radius.circular(28), topLeft: Radius.circular(28),
        bottomRight: Radius.circular(28), bottomLeft: Radius.circular(28)
    ), thermometerPaint);
  }

//  double thresholdY() =>
//      graphRect!.bottom - threshold! / 100.0 * zScoreScaleFactor;

  void renderBack(Canvas canvas) {
    Rect bgRect =
        Rect.fromLTWH(0, 0, size.toSize().width, size.toSize().height);
    Paint bgPaint = Paint()
      ..shader = LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment(0.8, 0.0), // 10% of the width, so there are ten blinds.
        colors: [
          AppColors.mainColorDark,
          AppColors.mainColorDark,
        ], // whitish to gray
        tileMode: TileMode.mirror, // repeats the gradient over the canvas
      ).createShader(bgRect);
    //bgPaint.color = Color(0xff101010);
    canvas.drawRect(bgRect, bgPaint);
  }

  @override
  @mustCallSuper
  void onResize(Vector2 _size) {
    super.onResize(_size);
//    print({"resize", size});
//
//    graphAreaStart = size
//        .toSize()
//        .height * 0.3;
//
//    // this is built oddly, from the bottom, where the graph value is zero
//    graphRect = Rect.fromLTWH(
//        0,
//        size
//            .toSize()
//            .height - graphAreaStart! - zScoreScaleFactor,
//        size
//            .toSize()
//            .width,
//        zScoreScaleFactor);
//
//    print(graphRect);
  }

  void _streamUpdate(double event) {
    double _zScore;

    _currentZScore = event;
  }
}

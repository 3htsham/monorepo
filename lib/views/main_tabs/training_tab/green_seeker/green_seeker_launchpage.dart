import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/models/route_argument.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:brain_trainer/config/app_keys.dart';
import '../../../../services/training/training_service.dart';

class GreenSeekerLaunchPage extends StatefulWidget {
  const GreenSeekerLaunchPage({Key? key}) : super(key: key);

  @override
  _GreenSeekerLaunchPageState createState() => _GreenSeekerLaunchPageState();
}

class _GreenSeekerLaunchPageState extends State<GreenSeekerLaunchPage> {
  final navigatorKey = GlobalKey<NavigatorState>();
  @override
  Widget build(BuildContext context) {
    var bottomPadding = 168.0;

    return LightingScaffold(
      resizeToAvoidBottomPadding: false,
      child: Stack(
        children: <Widget>[
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: IntrinsicHeight(
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Stack(
                        children: <Widget>[
                          Image.asset(AppAssets.greenSeekerGameBackground,
                              width: MediaQuery.of(context).size.width,
                              fit: BoxFit.fill),
                          Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    colors: [
                                  Colors.black.withOpacity(0.56),
                                  Colors.transparent
                                ],
                                    begin: Alignment.topCenter,
                                    end: Alignment.center)),
                          )
                        ],
                      ),
                      flex: 41),
                  Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 64,
                          ),
                          Text(
                            S.greenSeeker.toUpperCase(),
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                                ?.copyWith(fontSize: 64),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Text(
                            S.greenSeekerDescription,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1,
                          ),
                          SizedBox(
                            height: 32,
                          ),
                          MainAppButton(
                            onTap: () {
                              TrainingService trainingService = context.read<TrainingService>();
                              trainingService.playGraphGame();
                              Catcher.navigatorKey?.currentState?.pushNamed(
                                  Routes.TRAINING_TIMER,
                                  arguments: RouteArgument(
                                      gameName: S.greenSeeker,
                                      routeName: Routes.GREEN_SEEKER_TRAIN));

                            },
                            title: S.trainNow.toUpperCase(),
                            width: 191,
                            height: 56,
                          ),
                          SizedBox(
                            height: bottomPadding,
                          ),
                        ],
                      ),
                      flex: 59)
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: MainAppbar(
              onBack: (){},
              showBatteryIndicator: true,
            ),
          )
        ],
      ),
    );
  }
}

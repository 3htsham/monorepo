import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/models/route_argument.dart';
import 'package:brain_trainer/models/training_session.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:brain_trainer/views/main_tabs/reports_tab/session_result/session_result_provider.dart';
import 'package:catcher/catcher.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/green_seeker/graph_feedback.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GreenSeekerTrainPage extends StatefulWidget {
  const GreenSeekerTrainPage({Key? key}) : super(key: key);

  @override
  _GreenSeekerTrainPageState createState() => _GreenSeekerTrainPageState();
}

enum GSState { GSStateStart, GSStateInRound, GSStateEnd }

class _GreenSeekerTrainPageState extends State<GreenSeekerTrainPage>
    with WidgetsBindingObserver {
  GSState currentGSState = GSState.GSStateStart;

  AppLifecycleState? _notification;

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var trainingService = context.watch<TrainingService>();

    if (_notification != null && _notification == AppLifecycleState.inactive) {
      cancelTraining();
    }

    if (currentGSState == GSState.GSStateStart) {
      startTraining();
      currentGSState = GSState.GSStateInRound;
    }

    return LightingScaffold(
        resizeToAvoidBottomPadding: false,
        child: Container(
          child: Builder(builder: (context) {
            switch (currentGSState) {
              case GSState.GSStateStart:
                startTraining();
                return Container();
                break;
              case GSState.GSStateInRound:
                return Stack(
                  children: [
                    GameWidget(
                        game: GraphFeedbackGame(
                            Size(768, 952),
                            trainingService.onNewZScore,
                            endTraining,
                            trainingService)),
                    Positioned(
                      top: 0,
                      right: 0,
                      left: 0,
                      child: MainAppbar(
                          onBack: () {
                            context.read<TrainingService>().cancelTraining();
                          },
                          onAccountTap: () {},
                          showBatteryIndicator: true,
                          dropDownGameButton: TextButton(
                              style: TextButton.styleFrom(
                                padding: EdgeInsets.all(8),
                              ),
                              onPressed: () {},
                              child: Text(
                                S.greenSeeker,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    ?.copyWith(fontSize: 20),
                              ))),
                    )
                  ],
                );
                break;
              case GSState.GSStateEnd:
                return Container();
                break;
            }
          }),
        ));
  }

  void startTraining() {
    TrainingService trainingService = context.read<TrainingService>();
    trainingService.startSession();
  }

  void endTraining() {
    context.read<TrainingService>().endSession();
    context.read<TrainingService>().gameDone();
    Navigator.of(context).pushReplacementNamed(Routes.GAMESESSIONRESULT,
        arguments: RouteArgument(
            gameType: TrainingType.trainingType_Graph,
            onFinishCallBack: () {
              Catcher.navigatorKey?.currentState?.pop();
              //pop to trainig home page
              AppKeys.trainingNavigatorKey.currentState
                  ?.popUntil((route) => route.isFirst);
            }));
  }

  void cancelTraining() {
    context.read<TrainingService>().cancelTraining();
    WidgetsBinding.instance
        ?.addPostFrameCallback((_) => Navigator.pop(context));
  }

  Widget buildFinishButton(String title, double w, double h, Function onTap,
      Color backgroundColor, TextStyle textStyle) {
    return SizedBox(
      width: w,
      height: h,
      child: ElevatedButton(
        onPressed: () {
          onTap.call();
        },
        style: ElevatedButton.styleFrom(
          primary: backgroundColor,
          shape: StadiumBorder(),
        ),
        child: Center(
          child: Text(title, style: textStyle),
        ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _notification = state;
    });
  }
}

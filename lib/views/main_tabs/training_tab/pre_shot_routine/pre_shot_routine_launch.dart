import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/models/route_argument.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PreShotRoutineLaunchPage extends StatefulWidget {
  const PreShotRoutineLaunchPage({Key? key}) : super(key: key);

  @override
  _PreShotRoutineLaunchPageState createState() =>
      _PreShotRoutineLaunchPageState();
}

class _PreShotRoutineLaunchPageState extends State<PreShotRoutineLaunchPage> {
  @override
  Widget build(BuildContext context) {
    var trainingService = context.watch<TrainingService>();
    return LightingScaffold(
        resizeToAvoidBottomPadding: false,
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: IntrinsicHeight(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 41,
                      child: Stack(
                        children: <Widget>[
                          Image.asset(AppAssets.preShotRoutineGameBackground,
                              width: MediaQuery.of(context).size.width,
                              fit: BoxFit.fill),
                          Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    colors: [
                                  Colors.black.withOpacity(0.56),
                                  Colors.transparent
                                ],
                                    begin: Alignment.topCenter,
                                    end: Alignment.center)),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 59,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 64,
                          ),
                          Text(
                            S.preShotRoutine.toUpperCase(),
                            style: Theme.of(context)
                                .textTheme
                                .headline1
                                ?.copyWith(fontSize: 64),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Text(
                            S.preShotRoutineDescription,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                ?.copyWith(fontSize: 16),
                          ),
                          SizedBox(
                            height: 32,
                          ),
                          MainAppButton(
                            onTap: () {
                              TrainingService trainingService = context.read<TrainingService>();
                              trainingService.playPreShotRoutine();
                              Catcher.navigatorKey?.currentState?.pushNamed(
                                  Routes.TRAINING_TIMER,
                                  arguments: RouteArgument(
                                      gameName: S.preShotRoutine,
                                      routeName: Routes.PSR_TRAIN));
                            },
                            title: S.trainNow.toUpperCase(),
                            width: 191,
                            height: 56,
                          ),
                          SizedBox(
                            height: 168,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              left: 0,
              child: MainAppbar(
                onBack: () {},
                showBatteryIndicator: true,
              ),
            )
          ],
        ));
  }
}

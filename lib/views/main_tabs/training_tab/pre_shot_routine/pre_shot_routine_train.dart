import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/config/app_style.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/models/route_argument.dart';
import 'package:brain_trainer/models/training_session.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:brain_trainer/views/main_tabs/reports_tab/session_result/session_result_provider.dart';
import 'package:catcher/catcher.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PreShotRoutineTrainPage extends StatefulWidget {
  const PreShotRoutineTrainPage({Key? key}) : super(key: key);

  @override
  _PreShotRoutineTrainPageState createState() =>
      _PreShotRoutineTrainPageState();
}

enum PPRState {
  pprStateStart,
  pprStateInRound,
  pprStatePauseRound,
  pprStateEnd
}

class _PreShotRoutineTrainPageState extends State<PreShotRoutineTrainPage> with WidgetsBindingObserver {
  PPRState currentPPRState = PPRState.pprStateStart;

  AppLifecycleState? _notification;

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var trainingService = context.watch<TrainingService>();

    if(_notification != null && _notification == AppLifecycleState.inactive) {
      cancelTraining();
    }

    if (currentPPRState == PPRState.pprStateStart) {
      startTraining();
      currentPPRState = PPRState.pprStateInRound;
    }

    return LightingScaffold(
        resizeToAvoidBottomPadding: false,
        child: Container(
          child: Builder(builder: (context) {
            switch (currentPPRState) {
              case PPRState.pprStateStart:
                startTraining();
                return Container();
                break;
              case PPRState.pprStateInRound:
                return Stack(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        buildPreshotTrainingButton(
                            S.preShotPause.toUpperCase(),
                            334,
                            56,
                            pauseRound,
                            AppColors.orange,
                            AppStyle.mainAppTextStyleObjektive()
                                .copyWith(fontSize: 19)),
                        Container(
                          height: 20.0,
                        ),
                        buildPreshotTrainingButton(
                            S.preShotFinish.toUpperCase(),
                            334,
                            56,
                            endTraining,
                            AppColors.white,
                            AppStyle.mainAppTextStyleObjektive().copyWith(
                                fontSize: 19, color: AppColors.mainColorDark)),
                        Column(children: [
                          StreamBuilder(
                              stream:
                                  context.read<TrainingService>().onNewZScore,
                              builder:
                                  (context, AsyncSnapshot<double> snapshot) {
                                if (kDebugMode) {
                                  if (snapshot.hasData) {
                                    return Text(snapshot.data.toString());
                                  } else {
                                    return Text('No Connection');
                                  }
                                } else
                                  return Container();
                              }),
                        ]),
                      ],
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      left: 0,
                      child: MainAppbar(
                          onBack: () {context.read<TrainingService>().cancelTraining();},
                          onAccountTap: () {},
                          showBatteryIndicator: true,
                          dropDownGameButton: TextButton(
                              style: TextButton.styleFrom(
                                padding: EdgeInsets.all(8),
                              ),
                              onPressed: () {},
                              child: Text(
                                S.preShotRoutine,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    ?.copyWith(fontSize: 20),
                              ))),
                    )
                  ],
                );
                break;
              case PPRState.pprStatePauseRound:
                return Stack(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        buildPreshotTrainingButton(
                            S.preShotResume.toUpperCase(),
                            334,
                            56,
                            resumeRound,
                            AppColors.orange,
                            AppStyle.mainAppTextStyleObjektive()
                                .copyWith(fontSize: 19)),
                        Container(
                          height: 20.0,
                        ),
                        buildPreshotTrainingButton(
                            S.preShotFinish.toUpperCase(),
                            334,
                            56,
                            endTraining,
                            AppColors.white,
                            AppStyle.mainAppTextStyleObjektive().copyWith(
                                fontSize: 19, color: AppColors.mainColorDark)),
                      ],
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      left: 0,
                      child: MainAppbar(
                          onBack: () {context.read<TrainingService>().cancelTraining();},
                          onAccountTap: () {},
                          showBatteryIndicator: true,
                          dropDownGameButton: TextButton(
                              style: TextButton.styleFrom(
                                padding: EdgeInsets.all(8),
                              ),
                              onPressed: () {},
                              child: Text(
                                S.preShotRoutine,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    ?.copyWith(fontSize: 20),
                              ))),
                    )
                  ],
                );
                ;
                break;
              case PPRState.pprStateEnd:
                return Container();
                break;
            }
          }),
        ));
  }

  void startTraining() {
    TrainingService trainingService = context.read<TrainingService>();
    trainingService.startSession();
  }

  void cancelTraining() {
    context.read<TrainingService>().cancelTraining();
    WidgetsBinding.instance?.addPostFrameCallback((_) => Navigator.pop(context));
  }

  void endTraining() {
    context.read<TrainingService>().endSession();
    context.read<TrainingService>().gameDone();
    Navigator.of(context).pushReplacementNamed(Routes.GAMESESSIONRESULT,
        arguments: RouteArgument(
            gameType: TrainingType.trainingType_PSR,
            onFinishCallBack: () {
              Catcher.navigatorKey?.currentState?.pop();
              //pop to trainig home page
              AppKeys.trainingNavigatorKey.currentState
                  ?.popUntil((route) => route.isFirst);
            }));
  }

  pauseRound() {
    currentPPRState = PPRState.pprStatePauseRound;
    context.read<TrainingService>().pauseRound();
    setState(() {});
  }

  resumeRound() {
    currentPPRState = PPRState.pprStateInRound;
    context.read<TrainingService>().resumeRound();
    setState(() {});
  }

  Widget buildPreshotTrainingButton(String title, double w, double h,
      Function onTap, Color backgroundColor, TextStyle textStyle) {
    return SizedBox(
      width: w,
      height: h,
      child: ElevatedButton(
        onPressed: () {
          onTap.call();
        },
        style: ElevatedButton.styleFrom(
          primary: backgroundColor,
          shape: StadiumBorder(),
        ),
        child: Center(
          child: Text(title, style: textStyle),
        ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() { _notification = state; });
  }

}

import 'dart:convert';
import 'dart:ui';
import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/elements/responsive.dart';
import 'package:brain_trainer/enum/headband_state.dart';
import 'package:brain_trainer/helper/helpers.dart';
import 'package:brain_trainer/models/base_response.dart';
import 'package:brain_trainer/models/headband_models/cgx_eeg_data_model.dart';
import 'package:brain_trainer/models/route_argument.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/hardware/hardware_service.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

class TrainingTabPage extends StatefulWidget {
  static String tabName = S.training;

  @override
  _TrainingTabPageState createState() => _TrainingTabPageState();
}

class _TrainingTabPageState extends State<TrainingTabPage> {
  bool displayOverlay = false;

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  //TODO: This function will be called if the Hardware Connection is lost.
  _showHardwareDisconnectedOverlay() {
    showDialog(
        context: context,
        builder: (context) {
          return Helpers.hardwareDisconnected(context, onClose: () {
            Navigator.of(context).pop();
          }, onReset: () {
            Navigator.of(context).pop();
            AppKeys.mainTabsKey.currentState?.onTabTapped(0);
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    var bottomPadding = 228.0;

    var isFeedbackChecked = context.read<TrainingService>().feedbackOn;

    return StreamBuilder(
      stream: context.watch<HardwareService>().listenHardwareConnectionState(),
      builder: (context, AsyncSnapshot<HeadbandState> snapshot) {
        return Stack(
          children: [
            _userIgnorePointer(
                shouldUse: snapshot.data != HeadbandState.connected,
                // shouldUse: false,
                child: LightingScaffold(
                  scaffoldKey: this._scaffoldKey,
                  resizeToAvoidBottomPadding: false,
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        MainAppbar(
                          showBatteryIndicator: true,
                          // batteryIndicatorTitle: "100%",
                        ),

                        //Top headings
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal:
                                  Responsive.isMobile(context) ? 10 : 40),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: size.width,
                                height: 32,
                              ),
                              Text(
                                S.expertBrainStateTraining,
                                style:
                                    textTheme.headline1?.copyWith(fontSize: 48),
                              ),

                              SizedBox(
                                height: 32,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    S.training,
                                    style: textTheme.subtitle1,
                                  ),
                                  _buildRadioButton(
                                      textTheme, isFeedbackChecked, onTap: () {
                                    setState(() {
                                      context
                                          .read<TrainingService>()
                                          .feedbackOn = !isFeedbackChecked;
                                    });
                                  }),
                                ],
                              ),

                              SizedBox(
                                height: 16,
                              ),

                              //Training and Routine Items
                              _buildTrainingItems(size, textTheme),

                              SizedBox(
                                height: Responsive.isMobile(context) ? 10 : 40,
                              ),

                              _buildRoutineItems(size, textTheme),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: bottomPadding,
                        ),
                      ],
                    ),
                  ),
                )),
            snapshot.data != HeadbandState.connected
                ? Helpers.hardwareDisconnected(context, onClose: () {
                    // Navigator.of(context).pop();
                    AppKeys.mainTabsKey.currentState?.onTabTapped(0);
                  }, onReset: () {
                    // Navigator.of(context).pop();
                    AppKeys.mainTabsKey.currentState?.onTabTapped(0);
                  })
                : IgnorePointer(child: SizedBox())
          ],
        );
      },
    );
  }

  Widget _userIgnorePointer({bool shouldUse = false, @required Widget? child}) {
    return shouldUse
        ? IgnorePointer(
            child: child ?? Container(),
          )
        : child ?? Container();
  }

  Widget _buildTrainingItems(Size size, TextTheme textTheme) {
    return Container(
      width: size.width,
      child: Row(
        children: <Widget>[
          Expanded(
            child: _buildTrainingHomeItem(textTheme, AppAssets.greenSeekerImage,
                S.greenSeeker, S.learnHowToAccessThe, onTap: () {
              Navigator.of(context).pushNamed(Routes.GREEN_SEEKER_LAUNCH);
            }),
          ),
          SizedBox(
            width: Responsive.isMobile(context) ? 10 : 40,
          ),
          Expanded(
            child: _buildTrainingHomeItem(
                textTheme,
                AppAssets.universeMasterImage,
                S.universeMaster,
                S.gainControlOverEnteringTheExportStateInOrderToEasily,
                onTap: () {
              Navigator.of(context).pushNamed(Routes.UNIVERSE_LAUNCH);
            }),
          ),
        ],
      ),
    );
  }

  Widget _buildRoutineItems(Size size, TextTheme textTheme) {
    return Container(
      width: size.width,
      child: Row(
        children: <Widget>[
          Expanded(
            child: _buildTrainingHomeItem(
                textTheme,
                AppAssets.preShotRoutineImage,
                S.preShotRoutine,
                S.associateTheExpertBrainStateWithYourPreShot, onTap: () {
              Navigator.of(context).pushNamed(Routes.PSR_LAUNCH);
              // Navigator.of(context).pushNamed(Routes.GREEN_SEEKER_LAUNCH);
            }),
          ),
          SizedBox(
            width: Responsive.isMobile(context) ? 10 : 40,
          ),
          Expanded(
            child: SizedBox(),
          ),
        ],
      ),
    );
  }

  AspectRatio _buildTrainingHomeItem(
      TextTheme textTheme, String imageAsset, String title, String description,
      {VoidCallback? onTap}) {
    return AspectRatio(
      aspectRatio: 1 / 1,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: InkWell(
          onTap: onTap,
          highlightColor: Colors.transparent,
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(imageAsset), fit: BoxFit.cover),
            ),
            child: Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                Colors.transparent,
                Colors.transparent,
                Colors.black
              ], stops: [
                0,
                0.5,
                1
              ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    title,
                    style: textTheme.headline4,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Flexible(
                      child: Text(
                    description,
                    style: textTheme.bodyText1,
                    overflow: TextOverflow.fade,
                  )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _buildRadioButton(TextTheme textTheme, bool isChecked,
      {VoidCallback? onTap}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          S.feedback,
          style: textTheme.headline6,
        ),
        SizedBox(
          width: 10,
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: InkWell(
            onTap: () {
              onTap?.call();
            },
            child: AnimatedContainer(
              duration: Duration(milliseconds: 300),
              height: 31,
              width: 51,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: isChecked
                      ? Colors.green
                      : Colors.white.withOpacity(0.16)),
              child: Center(
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 300),
                  width: 27,
                  height: 27,
                  margin: EdgeInsets.only(
                      left: isChecked ? 20 : 0, right: isChecked ? 0 : 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

import 'dart:async';

import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/models/route_argument.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:flutter/scheduler.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class TrainingTimer extends StatefulWidget {
  RouteArgument? arguments;

  TrainingTimer({
    Key? key,
    @required this.arguments,
  }) : super(key: key);

  @override
  _TrainingTimerState createState() => _TrainingTimerState();
}

class _TrainingTimerState extends State<TrainingTimer> {
  int maxSeconds = 5;
  int seconds = 0;
  Timer? timer;
  Timer? navigationTimer;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  StreamController<int> _timerChangedController = StreamController<int>.broadcast();

  @override
  initState() {
    super.initState();
    startTimer();
  }

  startTimer() {
    SchedulerBinding.instance?.addPostFrameCallback((timeStamp) {
      timer = Timer.periodic(Duration(seconds: 1), (Timer t) {
        _timerChangedController.add(t.tick);
      });
      navigationTimer = Timer(Duration(seconds: maxSeconds), () {
        timer?.cancel();
        _timerChangedController.close();
        if(mounted) {
          //Navigate to next Route i.e. widget.routeName
          Navigator.of(context).pushReplacementNamed(widget.arguments?.routeName ?? "");
        }
      });
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    navigationTimer?.cancel();
    _timerChangedController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return LightingScaffold(
      scaffoldKey: this._scaffoldKey,
      resizeToAvoidBottomPadding: false,
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Container(
              height: size.height,
              width: size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    S.trainingBeginsIn,
                    style: textTheme.headline3?.copyWith(fontSize: 32),
                  ),
                  SizedBox(height: 30),

                  StreamBuilder(
                    stream: this._timerChangedController.stream,
                    builder: (context, AsyncSnapshot<int> seconds) {
                      return Container(
                        width: 333,
                        height: 333,
                        child: Stack(
                          children: [
                            SfRadialGauge(
                              axes: [
                                RadialAxis(
                                  minimum: 0,
                                  maximum: maxSeconds - 0,
                                  showFirstLabel: false,
                                  showLabels: false,
                                  showTicks: false,
                                  startAngle: 270,
                                  endAngle: 270,
                                  axisLineStyle: AxisLineStyle(
                                      thickness: 0.08,
                                      color: theme.scaffoldBackgroundColor,
                                      thicknessUnit: GaugeSizeUnit.factor),
                                  pointers: [
                                    RangePointer(
                                      value: double.parse(
                                          (this.maxSeconds - (seconds.data ?? 0))
                                              .toString()),
                                      width: 0.08,
                                      sizeUnit: GaugeSizeUnit.factor,
                                      cornerStyle: CornerStyle.bothCurve,
                                      gradient: SweepGradient(
                                        colors: [
                                          AppColors.gradientDark1,
                                          AppColors.gradientDark2,
                                        ],
                                        stops: [0.25, 0.75],
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                            Align(
                                alignment: Alignment.center,
                                child: Text(
                                  '${this.maxSeconds - (seconds.data ?? 0)}',
                                  style: textTheme.headline1?.copyWith(fontSize: 200),
                                )
                              // Text(
                              //   '${this.maxSeconds - this.seconds}',
                              //   style: textTheme.headline1?.copyWith(fontSize: 200),
                              // ),
                            )
                          ],
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: MainAppbar(
              showBatteryIndicator: true,
              onBack: () {},
              // batteryIndicatorTitle: "100%",
              dropDownGameButton: TextButton(
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.all(8),
                  ),
                  onPressed: () {},
                  child: Text(
                    widget.arguments?.gameName ?? "Universe Master",
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        ?.copyWith(fontSize: 20),
                  )),
            ),
          )
        ],
      ),
    );
  }
}

import 'dart:ui';
import 'dart:math';

import "package:flame/flame.dart";
import "package:flare_flutter/flare.dart";
import "package:flare_flutter/flare_actor.dart";

class FlareAnimation {
  final FlutterActorArtboard _artBoard;

  String _animationName = '';
  final List<FlareAnimationLayer> _animationLayers = [];

  double _width = 0.0, _height = 0.0, _xScale = 0.0, _yScale = 0.0;

  Picture? _picture;

  FlareAnimation(this._artBoard);

  static Future<FlareAnimation> load(String fileName) async {
    final actor = FlutterActor();
    await actor.loadFromBundle(Flame.bundle, fileName);
    await actor.loadImages();

    final artBoard = actor.artboard!.makeInstance() as FlutterActorArtboard;
    artBoard.makeInstance();
    artBoard.initializeGraphics();
    artBoard.advance(0.0);

    return FlareAnimation(artBoard);
  }

  double get width {
    return _width;
  }

  double get height {
    return _height;
  }

  set width(double newWidth) {
    _width = newWidth;
    _xScale = _width / _artBoard.width;
  }

  set height(double newHeight) {
    _height = newHeight;
    _yScale = _height / _artBoard.height;
  }

  void updateAnimation(String animationName) {
    _animationName = animationName;

    if (_animationName != null && _artBoard != null) {
      _animationLayers.clear();

      final ActorAnimation? animation = _artBoard.getAnimation(_animationName);
      if (animation != null) {
        _animationLayers.add(
            FlareAnimationLayer(_animationName, animation)..mixSeconds = 0.2);
        animation.apply(0.0, _artBoard, 1.0);
        _artBoard.advance(0.0);
      }
    }
  }

  void render(Canvas canvas, {double x = 0.0, double y = 0.0}) {
    if (_picture == null) {
      return;
    }

    canvas.save();
    canvas.translate(x, y);

    canvas.drawPicture(_picture!);
    canvas.restore();
  }

  void update(double elapsedSeconds) {
    int lastFullyMixed = -1;
    double lastMix = 0.0;

    final List<FlareAnimationLayer> completed = [];

    for (int i = 0; i < _animationLayers.length; i++) {
      final FlareAnimationLayer layer = _animationLayers[i];
      layer.mix += elapsedSeconds;
      layer.time += elapsedSeconds;

      lastMix = (layer.mixSeconds == null || layer.mixSeconds == 0.0)
          ? 1.0
          : min(1.0, layer.mix / layer.mixSeconds);
      if (layer.animation.isLooping) {
        layer.time %= layer.animation.duration;
      }
      layer.animation.apply(layer.time, _artBoard, lastMix);
      if (lastMix == 1.0) {
        lastFullyMixed = i;
      }
      if (layer.time > layer.animation.duration) {
        completed.add(layer);
      }
    }

    if (lastFullyMixed != -1) {
      _animationLayers.removeRange(0, lastFullyMixed);
    }
    if (_animationName == null &&
        _animationLayers.length == 1 &&
        lastMix == 1.0) {
      // Remove remaining animations.
      _animationLayers.removeAt(0);
    }

    if (_artBoard != null) {
      _artBoard.advance(elapsedSeconds);
    }

    // Memory render frame
    final r = PictureRecorder();
    final c = Canvas(r);

    c.scale(_xScale, _yScale);
    _artBoard.draw(c);

    _picture = r.endRecording();
  }
}

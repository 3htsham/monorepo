import 'dart:async';
import 'dart:math' as math;
import 'package:brain_trainer/config/app_colors.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/universe_master/flare_component.dart';
import 'package:flame/parallax.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flame/game.dart';
import 'package:flame/components.dart';

class SpaceGame extends BaseGame {
  TextPaintConfig? regularTextConfig;

  math.Random _random = math.Random(42);
  TrainingService trainingService;
  StreamSubscription<dynamic>? _zScoreSubscription;

  TextComponent? _debugText;

  FlareComponent? _playerShip;
  ParallaxComponent? _parallaxBackground;
  List _portals = <FlareComponent>[];

  Paint _portalLinePaint = Paint()
    ..color = Colors.white10
    ..style = PaintingStyle.stroke
    ..strokeWidth = 12.0;

  Offset _baseSpeed = Offset(0, 0);
  Offset _layerDelta = Offset(0, 0);

  static const _speedMax = 80.0;
  double _time = 0;
  double _shipAngle = 0;
  double _shipSpeed = 0;
  double _anglePCoeff = 0.5;
  double _speedPCoeff = 0.3;
  double _speedDesired = 0;
  double _angleDesired = 0;
  Offset _directionDesired = Offset(0.0, 1.0);

  double _zScore = 100.0;
  double _zScoreThreshold = 0;
  double _zScoreSum = 0;
  double _zScoreNumSamples = 0;
  double _zScoreAverage = 0;

  Function? _exitCallBack;

  bool _exitFlag = false;

  SpaceGame(Size _size, Stream<double> stream, this._exitCallBack,
      this.trainingService) {
    _zScoreSubscription = stream.listen(_zScoreStreamUpdate);
    onResize(Vector2(_size.width, _size.height));
  }

  @override
  Future<void> onLoad() async {
    int numberOfPortals = 6;

    _playerShip =
        FlareComponent("assets/animations/hero_pulse.flr", "AtSpeed", 75, 75);
    _playerShip!.anchor = Anchor.center;

    regularTextConfig = TextPaintConfig(
        color: AppColors.white, fontFamily: 'ObjektivMk2', fontSize: 12);
    _debugText = TextComponent('debug',
        textRenderer: TextPaint(config: regularTextConfig!))
      ..anchor = Anchor.topLeft
      ..x = 0
      ..y = 0;

    _portals = new List<PositionComponent>.generate(numberOfPortals, (index) {
      PortalComponent _portal = PortalComponent();
      _portal.x = -300.0 + _random.nextDouble() * 600.0;
      _portal.y = (index + 1) * -1100.0;
      return _portal;
    });

    _speedDesired = _speedMax;
    _shipAngle = _angleDesired = -(math.pi / 2.0);

    _zScoreThreshold = trainingService.zScoreThreshold.toDouble();

    var velVector = Vector2(0.0, 4.0);

    final l1 = await loadParallaxLayer(ParallaxImageData('hexagon_pattern.png'),
        repeat: ImageRepeat.repeat, alignment: Alignment.center);
    final l2 = await loadParallaxLayer(ParallaxImageData('planet.png'),
        repeat: ImageRepeat.noRepeat,
        alignment: Alignment.center,
        fill: LayerFill.none);
    final l3 = await loadParallaxLayer(
        ParallaxImageData('noise_texture_far.png'),
        repeat: ImageRepeat.repeat,
        alignment: Alignment.center,
        fill: LayerFill.none,
        velocityMultiplier: velVector);
    final l4 = await loadParallaxLayer(ParallaxImageData('star_field_far.png'),
        repeat: ImageRepeat.repeat,
        alignment: Alignment.center,
        fill: LayerFill.none,
        velocityMultiplier: velVector);
    final l5 = await loadParallaxLayer(
        ParallaxImageData('noise_texture_near.png'),
        repeat: ImageRepeat.repeat,
        alignment: Alignment.center,
        fill: LayerFill.none,
        velocityMultiplier: velVector);
    final l6 = await loadParallaxLayer(ParallaxImageData('star_field_near.png'),
        repeat: ImageRepeat.repeat,
        alignment: Alignment.center,
        fill: LayerFill.none,
        velocityMultiplier: velVector);

    final parallax =
        Parallax([l1, l2, l3, l4, l5, l6], baseVelocity: Vector2(0, -20));

    _parallaxBackground = ParallaxComponent.fromParallax(parallax);

    add(_parallaxBackground!);

    _portals.forEach((element) {
      add(element);
    });
    add(_playerShip!);

    if(kDebugMode) {
      add(_debugText!);
    }
  }

  @override
  void onDetach() {
    super.onDetach();
    _zScoreSubscription!.cancel();
  }

  void _zScoreStreamUpdate(double event) {
    _zScore = event;
    //_zScore = 100;
    _zScoreNumSamples++;
    _zScoreSum += _zScore;
    _zScoreAverage = _zScoreSum / _zScoreNumSamples;
  }

  @override
  void update(double dt) {
    super.update(dt);

    if (_exitFlag) {
      _exitCallBack!();
      _exitFlag = false;
    }

    _time = _time + dt;

    //    _angleDesired = _directionDesired.direction + (math.sin(1 * _time) + math.sin(math.pi / 2 * _time)) * 1.0;
    _angleDesired =
        _directionDesired.direction + (0.2 - (1.0 - (_zScore / 100.0)));
    _speedDesired = _speedMax;
    _speedDesired = math.min(_speedDesired, _speedMax);

    // _angleDesired = sin(1 * _time) + sin(pi / 2 * _time);
    // _angleDesired = pi;

    // _angle = _angle + t * ((_random.nextDouble() * 1.0) + -0.5);
    double angleError = _shipAngle - _angleDesired;
    double speedError = _shipSpeed - _speedDesired;

    _shipAngle = _shipAngle - dt * (angleError * _anglePCoeff);
    _shipSpeed = _shipSpeed - dt * (speedError * _speedPCoeff);

    // print([_speed, speedError]);

    _playerShip!.angle = _shipAngle;

    var offset = Offset.fromDirection(_shipAngle, _shipSpeed * (dt * 4.0));
    int index = 1;
    _parallaxBackground!.parallax!.layers.forEach((element) {
      element.velocityMultiplier = Vector2(-offset.dx, -offset.dy) * index.toDouble();
      index++;
    });
    _parallaxBackground?.parallax?.baseVelocity = Vector2(offset.dx, offset.dy);

    Offset _vel = Offset.fromDirection(_shipAngle, _shipSpeed * dt);
    _playerShip!.x += _vel.dx;
    _playerShip!.y += _vel.dy;

    camera.followComponent(_playerShip!);

//    Vector2 camPos = camera.position;

//    camPos.x = _playerShip!.x - (size.x / 2) * 1.0;
//    camPos.y = _playerShip!.y - (size.y / 2); // * (1.7 + _speed * 0.0);
    _parallaxBackground?.x = camera.position.x;
    _parallaxBackground?.y = camera.position.y;

    _debugText?.x = camera.position.x + 10.0;
    _debugText?.y = camera.position.y + 100.0;
    //_debugText?.text = 'z-score: ${_zScore.toString()}';

    var px = _portals[_currentTargetPortal]?.position.x;
    var py = _portals[_currentTargetPortal]?.position.y;

    _debugText?.text = '''
          ship_x: ${_playerShip?.position.x.toStringAsFixed(2)},
          ship_y: ${_playerShip?.position.y.toStringAsFixed(2)},
          vx: ${_vel.dx.toStringAsFixed(2)},
          vy: ${_vel.dy.toStringAsFixed(2)},
          a: ${(_shipAngle * 57.29).toStringAsFixed(2)},
          tp: ${_currentTargetPortal.toString()},
          tpx: ${px.toStringAsFixed(2)},
          tpy: ${py.toStringAsFixed(2)},
          s: ${_shipSpeed.toStringAsFixed(2)},
          des_dir.x: ${_directionDesired.dx.toStringAsFixed(2)},
          des_dir.y: ${_directionDesired.dy.toStringAsFixed(2)},
          size.x: ${size.x.toStringAsFixed(2)},
          size.y: ${size.y.toStringAsFixed(2)}
          cam.x: ${camera.position.x.toStringAsFixed(2)},
          cam.y: ${camera.position.y.toStringAsFixed(2)},
          zScore: ${_zScore.toStringAsFixed((2))},
          ''';
  }

  int _currentTargetPortal = 0;

  @override
  void render(Canvas canvas) {
    super.render(canvas);

    var circlePaint = Paint()
      ..color = Colors.white10
      ..style = PaintingStyle.stroke;
    canvas.drawCircle(Offset(0.0 - camera.position.x, 0.0 - camera.position.y),
        24.0 + math.sin(2.0 * _time) * 22.0, circlePaint);

    canvas.drawCircle(_playerShip!.position.toOffset(),
        24.0 + math.sin(2.0 * _time) * 22.0, circlePaint);

    Path path = Path();
    path.moveTo(0.0 - camera.position.x, 0.0 - camera.position.y);

    int targetPortal = 0;

    double dist = double.infinity;
    for (int i = 0; i < _portals.length; ++i) {
      var portal = _portals[i];
      if (i == 0) {
        path.lineTo(portal.x - camera.position.x, portal.y - camera.position.y);
      } else {
        Vector2 portal0 = Vector2.zero();
        Vector2 portal1 = Vector2.zero();
        if (i == 1) {
          portal0 = Vector2(0.0, 0.0);
          portal1 = _portals[i - 1].position;
        } else {
          portal0 = _portals[i - 2].position;
          portal1 = _portals[i - 1].position;
        }
        var prevDir = portal1 - portal0;
        var newDir = portal1 + prevDir.normalized() * 100.0;
        path.quadraticBezierTo(
          newDir.x - camera.position.x,
          newDir.y - camera.position.y,
          portal.x - camera.position.x,
          portal.y - camera.position.y,
        );
      }

      // Update ship desired direction
      if (_playerShip!.y > portal.y) {
        Vector2 dir = portal.position - _playerShip!.position;
        if (dir.length < dist) {
          dist = dir.length;
          _directionDesired = dir.toOffset();
          targetPortal = i;
        }
      } else if (portal == _portals.last) {
        print("Space Game: Past the Last Portal");
        _exitFlag = true;
      }
    }

    if (_currentTargetPortal != targetPortal) {
      print('$_zScoreAverage > $_zScoreThreshold');
      if (_zScoreAverage > _zScoreThreshold) {
        _portals[_currentTargetPortal].strokeColor = Colors.greenAccent;
      } else {
        _portals[_currentTargetPortal].strokeColor = Colors.redAccent;
      }
      _zScoreSum = 0;
      _zScoreNumSamples = 0;
      _currentTargetPortal = targetPortal;
    }

    canvas.drawPath(path, _portalLinePaint);
  }

  @override
  @mustCallSuper
  void onResize(Vector2 size) {
    print({"resize", size});
    super.onResize(size);

    Vector2 camPos = Vector2.zero();

    if (_playerShip != null) {
      camPos.x = _playerShip!.x - (size.x / 2);
      camPos.y = _playerShip!.y - (size.y / 2);
    }

    //camera.followVector2(camPos);

    _parallaxBackground?.x = camPos.x;
    _parallaxBackground?.y = camPos.y;
  }
}

class PortalComponent extends PositionComponent {
  double _time = 0.0;
  double _strokeWidth = 1.0;

  Color _strokeColor = Colors.white30;
  set strokeColor(Color color) => _strokeColor = color;

  @override
  void update(double dt) {
    _time += dt;
    _strokeWidth = 1.5 + math.sin(2.0 * _time);
  }

  @override
  void render(Canvas canvas) {
    Paint paint = Paint()
      ..color = _strokeColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = _strokeWidth;
    canvas.drawCircle(this.position.toOffset(), 50, paint);
  }
}

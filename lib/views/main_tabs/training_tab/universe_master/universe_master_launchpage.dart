import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/models/route_argument.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UniverseMasterLaunchPage extends StatefulWidget {
  const UniverseMasterLaunchPage({Key? key}) : super(key: key);

  @override
  _UniverseMasterLaunchPageState createState() =>
      _UniverseMasterLaunchPageState();
}

class _UniverseMasterLaunchPageState extends State<UniverseMasterLaunchPage> {
  @override
  Widget build(BuildContext context) {
    return LightingScaffold(
      resizeToAvoidBottomPadding: false,
      child: Stack(
        children: <Widget>[
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: IntrinsicHeight(
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 41,
                    child: Stack(
                      children: <Widget>[
                        Image.asset(AppAssets.universeMasterGameBackground,
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.fill),
                        Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                Colors.black.withOpacity(0.56),
                                Colors.transparent
                              ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.center)),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 59,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 64,
                        ),
                        Text(
                          S.universeMaster.toUpperCase(),
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              ?.copyWith(fontSize: 64),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Text(
                          S.universeMasterDescription,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        SizedBox(
                          height: 32,
                        ),
                        MainAppButton(
                          onTap: () {
                            context.read<TrainingService>().playSpaceGame();
                            Catcher.navigatorKey?.currentState?.pushNamed(
                                Routes.TRAINING_TIMER,
                                arguments: RouteArgument(
                                    gameName: S.universeMaster,
                                    routeName: Routes.UNIVERSE_TRAIN));

                          },
                          title: S.trainNow.toUpperCase(),
                          width: 191,
                          height: 56,
                        ),
                        SizedBox(
                          height: 168,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: MainAppbar(
              onBack: () {},
              showBatteryIndicator: true,
            ),
          )
        ],
      ),
    );
  }
}

import 'package:brain_trainer/config/app_keys.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/models/route_argument.dart';
import 'package:brain_trainer/models/training_session.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:brain_trainer/views/main_tabs/reports_tab/session_result/session_result_provider.dart';
import 'package:brain_trainer/views/main_tabs/training_tab/universe_master/space_game.dart';
import 'package:catcher/core/catcher.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UniverseMasterTrainPage extends StatefulWidget {
  const UniverseMasterTrainPage({Key? key}) : super(key: key);

  @override
  _UniverseMasterTrainPageState createState() =>
      _UniverseMasterTrainPageState();
}

enum HMState { HMStateStart, HMStateInRound, HMStateEnd }

class _UniverseMasterTrainPageState extends State<UniverseMasterTrainPage>
    with WidgetsBindingObserver {
  HMState currentHMState = HMState.HMStateStart;

  AppLifecycleState? _notification;

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var trainingService = context.watch<TrainingService>();

    if (_notification != null && _notification == AppLifecycleState.inactive) {
      cancelTraining();
    }

    if (currentHMState == HMState.HMStateStart) {
      startTraining();
      currentHMState = HMState.HMStateInRound;
    }

    return LightingScaffold(
        resizeToAvoidBottomPadding: false,
        child: Container(
          child: Builder(builder: (context) {
            switch (currentHMState) {
              case HMState.HMStateStart:
                startTraining();
                return Container();
                break;
              case HMState.HMStateInRound:
                return Stack(
                  children: [
                    GameWidget(
                        game: SpaceGame(
                            Size(768, 952),
                            trainingService.onNewZScore,
                            endTraining,
                            trainingService)),
                    Positioned(
                      top: 0,
                      right: 0,
                      left: 0,
                      child: MainAppbar(
                          onBack: () {
                            context.read<TrainingService>().cancelTraining();
                          },
                          onAccountTap: () {},
                          showBatteryIndicator: true,
                          dropDownGameButton: TextButton(
                              style: TextButton.styleFrom(
                                padding: EdgeInsets.all(8),
                              ),
                              onPressed: () {},
                              child: Text(
                                S.universeMaster,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    ?.copyWith(fontSize: 20),
                              ))),
                    )
                  ],
                );
                break;
              case HMState.HMStateEnd:
                return Container();
                break;
            }
          }),
        ));
  }

  void startTraining() {
    TrainingService trainingService = context.read<TrainingService>();
    trainingService.startSession();
  }

  void endTraining() {
    context.read<TrainingService>().endSession();
    context.read<TrainingService>().gameDone();
    Navigator.of(context).pushReplacementNamed(Routes.GAMESESSIONRESULT,
        arguments: RouteArgument(
            gameType: TrainingType.trainingType_SpaceGame,
            onFinishCallBack: () {
              Catcher.navigatorKey?.currentState?.pop();
              //pop to trainig home page
              AppKeys.trainingNavigatorKey.currentState
                  ?.popUntil((route) => route.isFirst);
            }));
  }

  void cancelTraining() {
    context.read<TrainingService>().cancelTraining();
    WidgetsBinding.instance
        ?.addPostFrameCallback((_) => Navigator.pop(context));
  }

  Widget buildFinishButton(String title, double w, double h, Function onTap,
      Color backgroundColor, TextStyle textStyle) {
    return SizedBox(
      width: w,
      height: h,
      child: ElevatedButton(
        onPressed: () {
          onTap.call();
        },
        style: ElevatedButton.styleFrom(
          primary: backgroundColor,
          shape: StadiumBorder(),
        ),
        child: Center(
          child: Text(title, style: textStyle),
        ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _notification = state;
    });
  }
}

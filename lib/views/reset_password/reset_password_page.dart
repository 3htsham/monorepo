import 'dart:ui';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/custom_text_form_field.dart';
import 'package:brain_trainer/elements/keyboard_dismiss.dart';
import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:brain_trainer/helper/ValidateHelper.dart';
import 'package:brain_trainer/views/reset_password/reset_password_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ResetPasswordPage extends StatefulWidget {
  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirmPasswordFocus = FocusNode();

  bool _isTenCharacters = false;
  bool _isOneUpperCase = false;
  bool _isOneLowercase = false;
  bool _isOneNumber = false;

  void _onPasswordChange(String? text) {
    if (text != null) {
      ///Check if Password contains
      ///- At least 10 characters
      ///- At least 1 uppercase
      ///- At least 1 lowercase
      ///- At least 1 number
      RegExp uppercaseRegex = RegExp(".*[A-Z].*");
      RegExp lowercaseRegex = RegExp(".*[a-z].*");
      RegExp numberRegex = RegExp(".*[1-9].*");

      this._isOneLowercase = lowercaseRegex.hasMatch(text);
      this._isOneUpperCase = uppercaseRegex.hasMatch(text);
      this._isOneNumber = numberRegex.hasMatch(text);
      this._isTenCharacters = text.length >= 10;
      setState(() {});
    }
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void _validateForm() {
    if (_formKey.currentState?.validate() ?? false) {
      //update password
      UIHelper.hideKeyboard(context);
      updatePasswordNow();
    }
  }

  void updatePasswordNow() async {
    var result = await context
        .read<ResetPasswordProvider>()
        .updateUserPassword(_passwordController.text);
    if (result is String) {
      UIHelper.showErrorDialog(result, context);
    } else if (result == null) {
      this._passwordController.clear();
      this._confirmPasswordController.clear();
      Navigator.of(context).pop(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    String spaces = "      ";
    String checkSpaces = "   ";
    String _tenCharsText = this._isTenCharacters
        ? "${S.check}$checkSpaces${S.atLeastTenChars}"
        : "${S.bullet}$spaces${S.atLeastTenChars}";
    String _oneUppercase = this._isOneUpperCase
        ? "${S.check}$checkSpaces${S.atLeastOneUppercase}"
        : "${S.bullet}$spaces${S.atLeastOneUppercase}";
    String _oneLowercase = this._isOneLowercase
        ? "${S.check}$checkSpaces${S.atLeastOneLowercase}"
        : "${S.bullet}$spaces${S.atLeastOneLowercase}";
    String _oneNum = this._isOneNumber
        ? "${S.check}$checkSpaces${S.atLeastOneNumber}"
        : "${S.bullet}$spaces${S.atLeastOneNumber}";

    var appBar = AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      leading: IconButton(
        icon: Icon(
          CupertinoIcons.back,
          color: Colors.white,
        ),
        onPressed: () {
          if (!context.read<ResetPasswordProvider>().isUpdating) {
            Navigator.of(context).pop();
          }
        },
      ),
    );

    return WillPopScope(
      onWillPop: () async {
        if (context.read<ResetPasswordProvider>().isUpdating) {
          return false;
        }
        return true;
      },
      child: KeyboardDismiss(
        child: Scaffold(
          appBar: appBar,
          body: Container(
            height: size.height,
            width: size.width,
            child: Center(
                child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      S.changePassword.toLowerCase(),
                      style: textTheme.headline2,
                      textAlign: TextAlign.start,
                    ),

                    SizedBox(
                      height: 25,
                    ),
                    _buildPasswordField(context),
                    SizedBox(
                      height: 10,
                    ),
                    _buildConfirmPasswordField(context),
                    SizedBox(
                      height: 10,
                    ),

                    ///Password Instructions
                    _buildPasswordInstructions(context, _tenCharsText,
                        textTheme, _oneUppercase, _oneLowercase, _oneNum),

                    SizedBox(height: 32),

                    _buildNextButton(textTheme, context)
                  ],
                ),
              ),
            )),
          ),
        ),
      ),
    );
  }

  CustomTextFormField _buildPasswordField(BuildContext context) {
    return CustomTextFormField(
      obscureText: true,
      focusNode: _passwordFocus,
      controller: _passwordController,
      width: 334,
      hint: "${S.newPassword}",
      inputType: TextInputType.text,
      validator: ValidateHelper.validatePassword,
      onSubmitted: (term) {
        _fieldFocusChange(context, _passwordFocus, _confirmPasswordFocus);
      },
      onChange: this._onPasswordChange,
      inputAction: TextInputAction.next,
    );
  }

  CustomTextFormField _buildConfirmPasswordField(BuildContext context) {
    return CustomTextFormField(
      obscureText: true,
      focusNode: _confirmPasswordFocus,
      controller: _confirmPasswordController,
      width: 334,
      hint: "${S.confirmNewPassword}",
      inputType: TextInputType.text,
      validator: (value) => ValidateHelper.validateConfirmPassword(
          _passwordController.text, value),
      inputAction: TextInputAction.done,
    );
  }

  AnimatedContainer _buildPasswordInstructions(
      BuildContext context,
      String _tenCharsText,
      TextTheme textTheme,
      String _oneUppercase,
      String _oneLowercase,
      String _oneNum) {
    return AnimatedContainer(
      height: !this._passwordFocus.hasFocus ? 0 : 80,
      width: 334,
      duration: Duration(milliseconds: 500),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              _tenCharsText,
              style: textTheme.bodyText2,
            ),
            Text(
              _oneUppercase,
              style: textTheme.bodyText2,
            ),
            Text(
              _oneLowercase,
              style: textTheme.bodyText2,
            ),
            Text(
              _oneNum,
              style: textTheme.bodyText2,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildNextButton(TextTheme textTheme, BuildContext context) {
    var isRegistering = context.watch<ResetPasswordProvider>().isUpdating;
    return isRegistering
        ? SizedBox(
            width: 334, child: Center(child: UIHelper.getCircularProgress()))
        : SizedBox(
            width: 334,
            height: 56,
            child: ElevatedButton(
                onPressed: () {
                  _validateForm();
                },
                style: ElevatedButton.styleFrom(
                  shape: StadiumBorder(),
                ),
                child: Center(
                  child: Text(
                    S.updatePassword.toUpperCase(),
                    style: textTheme.headline6,
                  ),
                )));
  }
}

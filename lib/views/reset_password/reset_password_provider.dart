import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
import 'package:flutter/material.dart';

class ResetPasswordProvider extends ChangeNotifier {
  FirebaseAuthService? firebaseAuthService;

  ResetPasswordProvider({this.firebaseAuthService});

  bool isUpdating = false;

  Future<dynamic> updateUserPassword(String newPassword) async {
    isUpdating = true;
    notifyListeners();
    var result = await firebaseAuthService?.updatePassword(newPassword);
    isUpdating = false;
    notifyListeners();
    return result;
  }
}

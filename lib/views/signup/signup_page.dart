import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/custom_text_form_field.dart';
import 'package:brain_trainer/elements/keyboard_dismiss.dart';
import 'package:brain_trainer/elements/radio_selection_button.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:brain_trainer/helper/ValidateHelper.dart';
import 'package:brain_trainer/helper/datetime_helper.dart';
import 'package:brain_trainer/models/auth_user.dart';
import 'package:brain_trainer/views/signup/signup_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  _SignUpPageState();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();
  final TextEditingController _handicapIndexController =
      TextEditingController();
  final TextEditingController _handicapGoalController = TextEditingController();
  final TextEditingController _primaryGolfCourseController =
      TextEditingController();
  final FocusNode _firstNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirmPasswordFocus = FocusNode();
  final FocusNode _handicapIndexFocus = FocusNode();
  final FocusNode _handicapGoalFocus = FocusNode();
  final FocusNode _primaryGolfCourseFocus = FocusNode();

  ///Added in Update
  final _form1Key = GlobalKey<FormState>();
  final _form2Key = GlobalKey<FormState>();
  final _form3Key = GlobalKey<FormState>();

  int _currentStep = 1;
  int _totalSteps = 3;

  bool _isTenCharacters = false;
  bool _isOneUpperCase = false;
  bool _isOneLowercase = false;
  bool _isOneNumber = false;

  bool isButtonPressed = false;

  @override
  void dispose() {
    super.dispose();
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void _onPasswordChange(String? text) {
    if (text != null) {
      this.isButtonPressed = false;
      _form2Key.currentState?.validate();
      ///Check if Password contains
      ///- At least 10 characters
      ///- At least 1 uppercase
      ///- At least 1 lowercase
      ///- At least 1 number
      RegExp uppercaseRegex = RegExp(".*[A-Z].*");
      RegExp lowercaseRegex = RegExp(".*[a-z].*");
      RegExp numberRegex = RegExp(".*[1-9].*");

      this._isOneLowercase = lowercaseRegex.hasMatch(text);
      this._isOneUpperCase = uppercaseRegex.hasMatch(text);
      this._isOneNumber = numberRegex.hasMatch(text);
      this._isTenCharacters = text.length >= 10;
      setState(() {});
    }
  }

  void _validateForms() async {
    if (_currentStep == 1) {
      if (_form1Key.currentState?.validate() ?? false) {
        setState(() {
          this._currentStep = 2;
        });
      }
    } else if (_currentStep == 2) {
      this.isButtonPressed = true;
      if (_form2Key.currentState?.validate() ?? false) {
        setState(() {
          this._currentStep = 3;
        });
      }
    } else if (_currentStep == 3) {
      if (_form3Key.currentState?.validate() ?? false) {
        //register and login
        UIHelper.hideKeyboard(context);
        signupNewAccountAction();
      }
    }
  }

  void signupNewAccountAction() async {
    var result = await context.read<SignUpProvider>().registerWithEmailPassword(
        email: _emailController.text,
        password: _passwordController.text,
        firstName: _firstNameController.text,
        lastName: _lastNameController.text,
        handicap: double.parse(_handicapIndexController.text),
        handicapGoal: _handicapGoalController.text.isEmpty
            ? null
            : int.parse(_handicapGoalController.text),
        homeCourse: _primaryGolfCourseController.text);
    if (result is String) {
      // UIHelper.showErrorDialog(result, context);

      if(result.toLowerCase().contains('password')) {
        //Password Error
        UIHelper.showFullScreenErrorDialog(context, S.opps, S.weakPasswordProvided, onClose: () {
          Navigator.of(context).pop();
        });
      } else if(result.toLowerCase().contains('email')) {
        //Email Error
        UIHelper.showFullScreenErrorDialog(context, '', S.emailAlreadyExists, onClose: () {
          Navigator.of(context).pop();
        });
      } else {
        UIHelper.showFullScreenErrorDialog(context, '', result, onClose: () {
          Navigator.of(context).pop();
        });
      }



    } else if (result is AuthUser) {
      //send a verification email
      await context.read<FirebaseAuthService>().sendVerificationEmail();

      UIHelper.showFullScreenErrorDialog(context, S.verifyEmailSentTitle, S.verifyEmailSentBody, onClose: () {
        Navigator.of(context).pop();
      }).then((value) => Navigator.of(context).pushNamedAndRemoveUntil(
          Routes.LOGIN, (Route<dynamic> route) => false));

      // UIHelper.showAlertDialog(
      //     context, S.verifyEmailSentTitle, S.verifyEmailSentBody, () {
      //   Navigator.of(context).pushNamedAndRemoveUntil(
      //       Routes.LOGIN, (Route<dynamic> route) => false);
      //   // Navigator.of(context).popUntil((route) => route.isFirst);
      // }).then((value) => Navigator.of(context).pushNamedAndRemoveUntil(
      //     Routes.LOGIN, (Route<dynamic> route) => false));
      // (value) => Navigator.of(context).popUntil((route) => route.isFirst));
    }
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var stepperWidth = 334.0;
    var stepperIndicatorWidth = stepperWidth / (_totalSteps / _currentStep);

    var appBar = AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      leading: IconButton(
        icon: Icon(
          CupertinoIcons.back,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );

    return WillPopScope(
        onWillPop: () async {
          if (this._currentStep > 1) {
            setState(() {
              this._currentStep--;
            });
            return false;
          }
          return true;
        },
        child: KeyboardDismiss(
          child: Scaffold(
            key: this._scaffoldKey,
            appBar: appBar,
            resizeToAvoidBottomInset: true,
            body: SafeArea(
              child: SingleChildScrollView(
                child: Center(
                  child: Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                            height: MediaQuery.of(context).size.height *
                                230 /
                                1024),
                        Text(
                          S.createAnAccount.toLowerCase(),
                          style: textTheme.headline2,
                          textAlign: TextAlign.start,
                        ),
                        this._currentStep == 3
                            ? Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Text(S.tellUsAboutYourGolfGame,
                                    style: textTheme.headline4,
                                    textAlign: TextAlign.start),
                              )
                            : SizedBox(),
                        SizedBox(height: 26),
                        this._currentStep == 1
                            ? _firstForm(context)
                            : this._currentStep == 2
                                ? _secondForm(context)
                                : _thirdForm(context),
                        _buildNextButton(textTheme, context),
                        SizedBox(height: 34),
                        _buildStepper(stepperWidth, stepperIndicatorWidth),
                        SizedBox(height: 16),
                        FractionallySizedBox(
                          widthFactor: 334 / 768,
                          child: Container(
                            child: Center(
                              child: Text(
                                "$_currentStep/$_totalSteps",
                                style: textTheme.caption,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: appBar.preferredSize.height,
                        ),
                        SizedBox(
                          height: appBar.preferredSize.height,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));
  }

  Widget _buildStepper(double stepperWidth, double stepperIndicatorWidth) {
    return FractionallySizedBox(
      widthFactor: 334 / 768,
      child: Container(
        height: 4,
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.08),
            borderRadius: BorderRadius.circular(100)),
        child: Align(
          alignment: Alignment.centerLeft,
          child: AnimatedContainer(
            width: stepperIndicatorWidth,
            duration: Duration(milliseconds: 100),
            decoration: BoxDecoration(
                gradient: config.AppColors.yellowGreenHorizontalGradient(),
                borderRadius: BorderRadius.circular(100)),
          ),
        ),
      ),
    );
  }

  Widget _buildNextButton(TextTheme textTheme, BuildContext context) {
    var isRegistering = context.watch<SignUpProvider>().isRegistering;
    return isRegistering
        ? FractionallySizedBox(
          widthFactor: 334 / 768,
          child: Center(child: UIHelper.getCircularProgress()),
        )
        : FractionallySizedBox(
            widthFactor: 334 / 768,
            child: SizedBox(
                // width: 334,
                height: 56,
                child: ElevatedButton(
                    onPressed: () {
                      _validateForms();
                    },
                    style: ElevatedButton.styleFrom(
                      shape: StadiumBorder(),
                    ),
                    child: Center(
                      child: Text(
                        _currentStep == 3
                            ? S.submit.toUpperCase()
                            : S.next.toUpperCase(),
                        style: textTheme.headline6,
                      ),
                    ))),
          );
  }

  Widget _firstForm(BuildContext context) {
    return Form(
      key: this._form1Key,
      child: Column(
        children: <Widget>[
          FractionallySizedBox(
            widthFactor: 334 / 768,
            child: Container(
              // width: 334,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(child: _buildFirstNameField(context)),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(child: _buildLastNameField(context))
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          _buildBirthdayField(context),
          SizedBox(height: 32),
        ],
      ),
    );
  }

  Widget _buildBirthdayField(BuildContext context) {
    var birthDay =
        context.select<SignUpProvider, DateTime?>((value) => value.birthDay);
    var bodyTextTheme = Theme.of(context).textTheme.bodyText1;
    final theme = Theme.of(context);
    return Container(
      height: 55,
      child: FractionallySizedBox(
        widthFactor: 334 / 768,
        child: Theme(
          data: ThemeData(
            primarySwatch: config.AppColors().scaffoldMaterialColor,
          ),
          child: Builder(
            builder: (ctx) => ElevatedButton(
              onPressed: () {
                context.read<SignUpProvider>().showBirthDayDatePicker(ctx);
              },
              style: ElevatedButton.styleFrom(
                primary: config.AppColors.mainColorDark,
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(16.0),
                    side: BorderSide(color: Colors.white)),
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  birthDay != null
                      ? DateTimeHelper.formatNormalDate(birthDay)
                      : "${S.birthday} (${S.optional.toLowerCase()})",
                  style: birthDay != null
                      ? bodyTextTheme
                      : bodyTextTheme?.copyWith(
                          color: Colors.white.withOpacity(0.7)),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  CustomTextFormField _buildLastNameField(BuildContext context) {
    return CustomTextFormField(
      focusNode: _lastNameFocus,
      controller: _lastNameController,
      width: 157,
      hint: S.lastName,
      inputType: TextInputType.text,
      validator: (value) => value == null || value.isEmpty ? S.required : null,
      onSubmitted: (term) {
        UIHelper.hideKeyboard(context);
      },
      inputAction: TextInputAction.next,
    );
  }

  CustomTextFormField _buildFirstNameField(BuildContext context) {
    return CustomTextFormField(
      focusNode: _firstNameFocus,
      controller: _firstNameController,
      width: 157,
      hint: S.firstName,
      inputType: TextInputType.text,
      validator: (value) => value == null || value.isEmpty ? S.required : null,
      onSubmitted: (term) {
        _fieldFocusChange(context, _firstNameFocus, _lastNameFocus);
      },
      inputAction: TextInputAction.next,
    );
  }

  Widget _secondForm(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    String spaces = "      ";
    String checkSpaces = "   ";
    String _tenCharsText = this._isTenCharacters
        ? "${S.check}$checkSpaces${S.atLeastTenChars}"
        : "${S.cross}$spaces${S.atLeastTenChars}";
    String _oneUppercase = this._isOneUpperCase
        ? "${S.check}$checkSpaces${S.atLeastOneUppercase}"
        : "${S.cross}$spaces${S.atLeastOneUppercase}";
    String _oneLowercase = this._isOneLowercase
        ? "${S.check}$checkSpaces${S.atLeastOneLowercase}"
        : "${S.cross}$spaces${S.atLeastOneLowercase}";
    String _oneNum = this._isOneNumber
        ? "${S.check}$checkSpaces${S.atLeastOneNumber}"
        : "${S.cross}$spaces${S.atLeastOneNumber}";

    return Form(
      key: this._form2Key,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildEmailField(context),
          SizedBox(
            height: 10,
          ),
          _buildPasswordField(context),
          SizedBox(
            height: 10,
          ),
          _buildConfirmPasswordField(context),
          SizedBox(
            height: 10,
          ),

          ///Password Instructions
          _buildPasswordInstructions(context, _tenCharsText, textTheme),
          SizedBox(height: 32),
        ],
      ),
    );
  }

  Widget _buildEmailField(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 334 / 768,
      child: CustomTextFormField(
        focusNode: _emailFocus,
        controller: _emailController,
        hint: "${S.email}",
        inputType: TextInputType.emailAddress,
        validator: (value) => ValidateHelper.validateEmail(value, isButtonPressed: this.isButtonPressed),
        onSubmitted: (term) {
          _fieldFocusChange(context, _emailFocus, _passwordFocus);
        },
        inputAction: TextInputAction.next,
      ),
    );
  }

  Widget _buildPasswordField(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 334 / 768,
      child: CustomTextFormField(
        obscureText: true,
        focusNode: _passwordFocus,
        controller: _passwordController,
        hint: "${S.password}",
        inputType: TextInputType.text,
        validator: (value) => ValidateHelper.validatePassword(value, isButtonPressed: this.isButtonPressed),
        onSubmitted: (term) {
          _fieldFocusChange(context, _passwordFocus, _confirmPasswordFocus);
        },
        onChange: this._onPasswordChange,
        inputAction: TextInputAction.next,
      ),
    );
  }

  Widget _buildPasswordInstructions(
      BuildContext context,
      String _tenCharsText,
      TextTheme textTheme,) {

    return FractionallySizedBox(
      widthFactor: 334 / 768,
      child: AnimatedContainer(
        height: !this._passwordFocus.hasFocus ? 0 : 100,
        duration: Duration(milliseconds: 500),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  Image.asset(this._isTenCharacters ? AppAssets.check : AppAssets.close, color: Colors.white, height: 24, width: 24,),
                  SizedBox(width: 8,),
                  Text(
                    S.atLeastTenChars,
                    style: textTheme.bodyText1,
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(this._isOneUpperCase ? AppAssets.check : AppAssets.close, color: Colors.white, height: 24, width: 24,),
                  SizedBox(width: 8,),
                  Text(
                    S.atLeastOneUppercase,
                    style: textTheme.bodyText1,
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(this._isOneLowercase ? AppAssets.check : AppAssets.close, color: Colors.white, height: 24, width: 24,),
                  SizedBox(width: 8,),
                  Text(
                    S.atLeastOneLowercase,
                    style: textTheme.bodyText1,
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(this._isOneNumber ? AppAssets.check : AppAssets.close, color: Colors.white, height: 24, width: 24,),
                  SizedBox(width: 8,),
                  Text(
                    S.atLeastOneNumber,
                    style: textTheme.bodyText1,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildConfirmPasswordField(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 334 / 768,
      child: CustomTextFormField(
        obscureText: true,
        focusNode: _confirmPasswordFocus,
        controller: _confirmPasswordController,
        hint: "${S.confirmPassword}",
        inputType: TextInputType.text,
        validator: (value) => ValidateHelper.validateConfirmPassword(
            _passwordController.text, value, isButtonPressed: this.isButtonPressed),
        inputAction: TextInputAction.done,
      ),
    );
  }

  Widget _thirdForm(BuildContext context) {
    return Form(
      key: this._form3Key,
      child: Column(
        children: <Widget>[
          _buildHandicapIndex(context),
          SizedBox(
            height: 10,
          ),
          _buildHandicapGoalField(context),
          SizedBox(
            height: 10,
          ),
          _buildPrimaryGolfCourse(context),
          _buildLeftRightHandSelection(context),
          SizedBox(height: 32),
        ],
      ),
    );
  }

  Widget _buildPrimaryGolfCourse(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 334 / 768,
      child: CustomTextFormField(
        focusNode: _primaryGolfCourseFocus,
        controller: _primaryGolfCourseController,
        // width: 334,
        hint: "${S.primaryGolfCourse} (${S.optional})",
        inputType: TextInputType.text,
        inputAction: TextInputAction.done,
      ),
    );
  }

  Widget _buildHandicapGoalField(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 334 / 768,
      child: CustomTextFormField(
        obscureText: false,
        focusNode: _handicapGoalFocus,
        controller: _handicapGoalController,
        // width: 334,
        hint: "${S.handicapGoal} (${S.optional})",
        inputType: TextInputType.number,
        inputFormatter: [
          FilteringTextInputFormatter.digitsOnly
          // WhitelistingTextInputFormatter.digitsOnly,
        ],
        onSubmitted: (term) {
          _fieldFocusChange(
              context, _handicapGoalFocus, _primaryGolfCourseFocus);
        },
        onChange: this._onPasswordChange,
        inputAction: TextInputAction.next,
      ),
    );
  }

  Widget _buildHandicapIndex(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 334 / 768,
      child: CustomTextFormField(
        focusNode: _handicapIndexFocus,
        controller: _handicapIndexController,
        // width: 334,
        hint: "${S.handicapIndex}",
        inputType: TextInputType.number,
        validator: (value) =>
            value == null || value.isEmpty ? S.fieldCantBeEmpty : null,
        inputFormatter: [
          FilteringTextInputFormatter.digitsOnly
          // WhitelistingTextInputFormatter.digitsOnly
        ],
        onSubmitted: (term) {
          _fieldFocusChange(context, _handicapIndexFocus, _handicapGoalFocus);
        },
        inputAction: TextInputAction.next,
      ),
    );
  }

  Widget _buildLeftRightHandSelection(BuildContext context) {
    var isLeftHand = context.watch<SignUpProvider>().isLeftHand;
    var selectedImage =
        Image.asset(AppAssets.icRadioSelected, width: 24, height: 24);
    var unSelectedImage =
        Image.asset(AppAssets.icRadioUnSelected, width: 24, height: 24);
    return FractionallySizedBox(
      widthFactor: 334 / 768,
      child: Row(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
        RadioSelectionButton(
            text: S.leftHand,
            onTap: () {
              context.read<SignUpProvider>().updateLeftHand(true);
            },
            image: isLeftHand ? selectedImage : unSelectedImage),
        // SizedBox(
        //   width: 74,
        // ),
        RadioSelectionButton(
            text: S.rightHand,
            onTap: () {
              context.read<SignUpProvider>().updateLeftHand(false);
            },
            image: isLeftHand ? unSelectedImage : selectedImage)
      ]),
    );
  }
}

import 'package:brain_trainer/services/firebase/firestore_db_service.dart';
import 'package:brain_trainer/models/auth_user.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:flutter/material.dart';

import '../../services/firebase/firebase_auth_service.dart';

class SignUpProvider extends ChangeNotifier {
  FirebaseAuthService? firebaseAuthService;
  FirestoreDBService? firestoreDBService;
  TrainingService? trainingService;

  SignUpProvider(
      {@required this.firebaseAuthService,
      @required this.firestoreDBService,
      @required this.trainingService,
      this.isLeftHand = true,
      this.isRegistering = false,
        this.birthDay
      });

  bool isLeftHand = true;
  bool isRegistering = false;
  DateTime? birthDay;

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> showBirthDayDatePicker(BuildContext context) async {
    var now = DateTime.now();
    var selectedDate = await showDatePicker(
      context: context,
      initialDate: DateTime(now.year - 10, now.month, now.day),
      firstDate: DateTime(now.year - 100, now.month, now.day),
      lastDate: now,
    );
    if (selectedDate != null) {
      birthDay = selectedDate;
      notifyListeners();
    }
  }

  Future<dynamic> registerWithEmailPassword(
      {String email = "",
      String password = "",
      String firstName = "",
      String lastName = "", double handicap = 0,
      int? handicapGoal,
      String? homeCourse}) async {
    isRegistering = true;
    notifyListeners();
    var result = await firebaseAuthService?.registerWithEmailPassword(
        email, password,
        displayName: (firstName) + ' ' + lastName);
    if (result is AuthUser) {
      // TODO: refactor the training service, also firestoredb to use DI Provider
      try {
        await trainingService?.buildNewSubject(firestoreDBService!,
            firstName: firstName,
            lastName: lastName,
            birthDay: birthDay,
            handicap: handicap,
            handicapGoal: handicapGoal,
            homeCourse: homeCourse ?? "",
            isLeftHand: isLeftHand);
      } catch (e) {
        isRegistering = false;
        notifyListeners();
        return e.toString();
      }
    }
    isRegistering = false;
    notifyListeners();
    return result;
  }

  //step 3 left right hand
  void updateLeftHand(bool isLeftHand) {
    this.isLeftHand = isLeftHand;
    notifyListeners();
  }
}

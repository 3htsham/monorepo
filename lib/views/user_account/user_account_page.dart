import 'package:brain_trainer/config/app_asset.dart';
import 'package:brain_trainer/config/constants.dart';
import 'package:brain_trainer/elements/app_bar/main_app_bar.dart';
import 'package:brain_trainer/elements/buttons/main_app_button.dart';
import 'package:brain_trainer/elements/others/lighting_scaffold.dart';
import 'package:brain_trainer/elements/responsive.dart';
import 'package:brain_trainer/helper/helpers.dart';
import 'package:brain_trainer/models/golf_subject.dart';
import 'package:brain_trainer/routes/route_name.dart';
import 'package:brain_trainer/helper/UIHelper.dart';
import 'package:brain_trainer/views/user_account/user_account_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:brain_trainer/config/app_colors.dart' as config;
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../services/firebase/firebase_auth_service.dart';
import '../../services/training/training_service.dart';

class UserAccountPage extends StatefulWidget {
  @override
  _UserAccountPageState createState() => _UserAccountPageState();
}

class _UserAccountPageState extends State<UserAccountPage> {
  GlobalKey<FormState>? _formKey;

  FocusNode? _emailFocus,
      _passwordFocus,
      _handicapIndexFocus,
      _homeCourseFocus,
      _focusNode;

  @override
  void initState() {
    _formKey = GlobalKey<FormState>();
    _emailFocus = FocusNode();
    _passwordFocus = FocusNode();
    _handicapIndexFocus = FocusNode();
    _homeCourseFocus = FocusNode();
    _focusNode = FocusNode();
    context.read<UserAccountProvider>().getUserTrainingProgram();
    context
        .read<UserAccountProvider>()
        .getCoachAndNeuroScientist(context.read<TrainingService>().subject);
    context.read<UserAccountProvider>().emailController.text =
        FirebaseAuthService.user?.email ?? "";
    setFakeData();
    super.initState();
  }

  setFakeData() {
    context.read<UserAccountProvider>().passwordController.text = "12345678";
  }

  bool validateLastField() {
    return this._formKey?.currentState?.validate() ?? false;
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode? currentFocus, FocusNode? nextFocus) {
    if (validateLastField()) {
      if (currentFocus != null) {
        currentFocus.unfocus();
      }
      _focusNode = nextFocus;
      FocusScope.of(context).requestFocus(nextFocus);
      setState(() {});
    }
  }

  void _updateEmail() async {
    if (FirebaseAuthService.user?.email.toLowerCase() !=
        context
            .read<UserAccountProvider>()
            .emailController
            .text
            .toString()
            .toLowerCase()) {
      var result = await context.read<UserAccountProvider>().updateUserEmail(
          context.read<UserAccountProvider>().emailController.text);
      if (result is PlatformException) {
        UIHelper.showErrorDialog(result.message ?? "", context,
            isRootNavigator: true);
        _fieldFocusChange(context, _focusNode, _emailFocus);
      } else {
        //send a verification email
        await context.read<FirebaseAuthService>().sendVerificationEmail();
        FirebaseAuthService.user =
            await context.read<FirebaseAuthService>().currentUser();
        UIHelper.showAlertDialog(
            context, S.verifyEmailSentTitle, S.verifyEmailSentBody, () {
          Navigator.of(context, rootNavigator: true).pop();
        });
      }
    }
  }

  void _updateHandicapIndexAndCourse(FocusNode _currentFocused) async {
    bool isUpdatedTextField = false;
    if (_currentFocused == _handicapIndexFocus &&
        context.read<UserAccountProvider>().trainingProgram?.handicap !=
            double.parse(context
                .read<UserAccountProvider>()
                .handicapIndexController
                .text
                .toString())) {
      isUpdatedTextField = true;
    } else if (_currentFocused == _homeCourseFocus &&
        context
                .read<UserAccountProvider>()
                .trainingProgram
                ?.homeCourse
                .toLowerCase() !=
            context
                .read<UserAccountProvider>()
                .homeCourseController
                .text
                .toString()
                .toLowerCase()) {
      isUpdatedTextField = true;
    }
    if (isUpdatedTextField) {
      try {
        await context.read<UserAccountProvider>().updateTrainingProgram();
      } catch (e) {
        UIHelper.showErrorDialog(e.toString(), context, isRootNavigator: true);
        _fieldFocusChange(context, _focusNode, _currentFocused);
      }
    }
  }

  @override
  void dispose() {
    _emailFocus?.dispose();
    _passwordFocus?.dispose();
    _handicapIndexFocus?.dispose();
    _homeCourseFocus?.dispose();
    _focusNode?.dispose();
    super.dispose();
  }

  _showImagePickerDialog() {
    UIHelper.showImagePickerDialog(context, onGallery: () async {
      dynamic _result = await context
          .read<UserAccountProvider>()
          .pickUserImage(ImageSource.gallery);
      _handleResult(_result);
    }, onCamera: () async {
      dynamic _result = await context
          .read<UserAccountProvider>()
          .pickUserImage(ImageSource.camera);
      _handleResult(_result);
    });
  }

  void _handleResult(dynamic _result) {
    if (_result is GolfSubject) {
      context.read<TrainingService>().subject = _result;
    }
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return LightingScaffold(
      resizeToAvoidBottomPadding: false,
      child: Container(
        height: size.height,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              MainAppbar(
                onBack: () {},
                onAccountTap: () {},
                showBatteryIndicator: true,
                batteryIndicatorTitle: "12 hr 24 min",
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      width: size.width,
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Text(
                      S.account,
                      style: textTheme.headline1,
                    ),
                    SizedBox(
                      height: 52,
                    ),
                    _buildUserProfile(theme),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        context.watch<UserAccountProvider>().coachProfile !=
                                null
                            ? Expanded(
                                child: _buildCoachesProfile(theme,
                                    imageUrl: context
                                            .watch<UserAccountProvider>()
                                            .coachProfile
                                            ?.imgUrl,
                                    name:
                                        '${context.watch<UserAccountProvider>().coachProfile?.name ?? " "}',
                                    profileTitle: S.yourPGACoach,
                                    email: context
                                            .watch<UserAccountProvider>()
                                            .coachProfile
                                            ?.email ??
                                        " "),
                              )
                            : SizedBox(),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: context
                                      .watch<UserAccountProvider>()
                                      .neuroScientistProfile !=
                                  null
                              ? _buildCoachesProfile(theme,
                                  imageUrl: context
                                          .watch<UserAccountProvider>()
                                          .neuroScientistProfile
                                          ?.imgUrl,
                                  name:
                                      '${context.watch<UserAccountProvider>().neuroScientistProfile?.name ?? " "}',
                                  profileTitle: S.yourNeuroScientist,
                                  email: context
                                          .watch<UserAccountProvider>()
                                          .neuroScientistProfile
                                          ?.email ??
                                      " ")
                              : SizedBox(),
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Center(
                      child: MainAppButton(
                          onTap: () async {
                            FirebaseAuthService _auth =
                                Provider.of<FirebaseAuthService>(context,
                                    listen: false);
                            // TODO - check that any data in the database streams up - it should
                            await _auth.signOut();
                            Navigator.of(context)
                                .popUntil((route) => route.isFirst);
                            Navigator.of(context).pushNamed(Routes.LOGIN);
                          },
                          title: S.logOut,
                          width: 191,
                          height: 56),
                    ),
                    SizedBox(height: 90),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildUserProfile(ThemeData theme) {
    var textTheme = theme.textTheme;
    var containerDecoration = BoxDecoration(
        color: Colors.white.withOpacity(0.16),
        borderRadius: BorderRadius.circular(16),
        border: Border.all(color: Colors.white.withOpacity(0.24), width: 1));

    return Container(
      padding: EdgeInsets.symmetric(vertical: 40, horizontal: 16),
      decoration: containerDecoration,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                _buildUserProfileImage(),
                Text(
                  '${context.watch<TrainingService>().subject?.firstName ?? ""} ${context.watch<TrainingService>().subject?.lastName ?? ""}',
                  textAlign: TextAlign.center,
                  style: textTheme.subtitle1,
                ),
                SizedBox(
                  height: 32,
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: _buildForm(theme),
          ),
        ],
      ),
    );
  }

  Widget _buildUserProfileImage() {
    return Container(
      width: 228,
      padding: EdgeInsets.only(top: 40),
      margin: EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage(AppAssets.userProfileImageBackgroundPattern),
      )),
      child: Center(
        child: Stack(children: <Widget>[
          Container(
            width: 157,
            height: 157,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                boxShadow: [
                  BoxShadow(
                      color: config.AppColors.shadowColor,
                      blurRadius: 62,
                      offset: Offset(0, 28))
                ],
                image: DecorationImage(
                  image: NetworkImage(context
                          .watch<TrainingService>()
                          .subject
                          ?.imgUrl ??
                      "https://golfdigest.sports.sndimg.com/content/dam/images/golfdigest/fullset/2018/01/08/5a52eec0357ff52ceab9a35b_dustin-johnson-sentry-tournament-of-champions-2018-sunday-wave-hat.jpg.rend.hgtvcom.966.725.suffix/1573233939175.jpeg"),
                  fit: BoxFit.cover,
                )),
            child: Center(
              child: context.watch<UserAccountProvider>().isUpdatingPhoto
                  ? UIHelper.getCircularProgress()
                  : context.watch<UserAccountProvider>().errorUploadingPhoto
                      ? Icon(
                          CupertinoIcons.restart,
                          color: Colors.red,
                        )
                      : SizedBox(),
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: IconButton(
              highlightColor: Theme.of(context).focusColor,
              icon: Image.asset(AppAssets.cameraIcon),
              iconSize: 40,
              onPressed: () {
                _showImagePickerDialog();
              },
            ),
          )
        ]),
      ),
    );
  }

  Widget _buildForm(ThemeData theme) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //Email
          _buildTextField(theme,
              isEnabled: _focusNode == _emailFocus,
              hint: S.emailAddress,
              controller: context.watch<UserAccountProvider>().emailController,
              focusNode: _emailFocus ?? FocusNode(),
              validator: (value) => _emailFocus != null &&
                      _emailFocus!.hasFocus &&
                      value != null &&
                      !value.contains("@")
                  ? S.enterValidEmail
                  : null,
              onSubmitted: (value) {
                if (_formKey?.currentState?.validate() ?? false) {
                  _fieldFocusChange(context, _emailFocus, FocusNode());
                  _updateEmail();
                }
              }),
          SizedBox(
            height: 8,
          ),
          //Password
          _buildTextField(theme,
              isEnabled: _focusNode == _passwordFocus,
              obscureText: true,
              hint: S.password,
              focusNode: _passwordFocus ?? FocusNode(),
              validator: (value) => _passwordFocus != null &&
                      _passwordFocus!.hasFocus &&
                      value != null &&
                      value.length < 6
                  ? S.pleaseEnterAValidPassword
                  : null,
              controller:
                  context.watch<UserAccountProvider>().passwordController,
              onSubmitted: (value) {
                if (_formKey?.currentState?.validate() ?? false) {
                  _fieldFocusChange(context, _passwordFocus, FocusNode());
                }
              }),
          SizedBox(
            height: 8,
          ),
          //Handicap Index
          _buildTextField(theme,
              isEnabled: _focusNode == _handicapIndexFocus,
              hint: S.userHandicapIndex,
              focusNode: _handicapIndexFocus ?? FocusNode(),
              validator: (value) => _handicapIndexFocus != null &&
                      _handicapIndexFocus!.hasFocus &&
                      value != null &&
                      value.isEmpty
                  ? S.handicapIndexCanNotBeEmpty
                  : null,
              controller:
                  context.watch<UserAccountProvider>().handicapIndexController,
              inputType: TextInputType.number,
              onSubmitted: (value) {
                if (_formKey?.currentState?.validate() ?? false) {
                  _fieldFocusChange(context, _handicapIndexFocus, FocusNode());
                  _updateHandicapIndexAndCourse(_handicapIndexFocus!);
                }
              }),
          SizedBox(
            height: 8,
          ),
          //Home Course
          _buildTextField(theme,
              isEnabled: _focusNode == _homeCourseFocus,
              hint: S.homeCourse,
              focusNode: _homeCourseFocus ?? FocusNode(),
              validator: (value) => _homeCourseFocus != null &&
                      _homeCourseFocus!.hasFocus &&
                      value != null &&
                      value.isEmpty
                  ? S.homeCourseCanNotBeEmpty
                  : null,
              controller:
                  context.watch<UserAccountProvider>().homeCourseController,
              inputType: TextInputType.text,
              onSubmitted: (value) {
                if (_formKey?.currentState?.validate() ?? false) {
                  _fieldFocusChange(context, _homeCourseFocus, FocusNode());
                  _updateHandicapIndexAndCourse(_homeCourseFocus!);
                }
              }),
        ],
      ),
    );
  }

  Widget _buildTextField(
    ThemeData theme, {
    TextEditingController? controller,
    bool? obscureText,
    bool? isEnabled,
    Function(String)? onSubmitted,
    @required String? hint,
    FocusNode? focusNode,
    TextInputType? inputType,
    String? Function(String?)? validator,
  }) {
    var textTheme = theme.textTheme;

    var border = InputBorder.none;
    // OutlineInputBorder(
    //     borderRadius: BorderRadius.circular(16),
    //     borderSide: BorderSide(width: 1, color: Colors.white)
    // );

    return Container(
      height: 56,
      decoration: isEnabled != null && !isEnabled
          ? BoxDecoration()
          : BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              border: Border.all(width: 1, color: Colors.white),
              color: Colors.white.withOpacity(0.08)),
      // padding: EdgeInsets.only(right: 15, left: 15),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: TextFormField(
                focusNode: focusNode ?? FocusNode(),
                controller: controller ?? TextEditingController(),
                textInputAction: TextInputAction.done,
                obscureText: obscureText ?? false,
                validator: validator ?? (value) => null,
                style: textTheme.subtitle2,
                keyboardType: inputType ?? TextInputType.emailAddress,
                autocorrect: false,
                autofocus: false,
                enabled: isEnabled ?? true,
                onSaved: (value) {},
                onFieldSubmitted: onSubmitted,
                decoration: InputDecoration(
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  focusColor: config.AppColors.mainColorDark,
                  contentPadding:
                      EdgeInsets.only(top: 5, right: 15, left: 15, bottom: 5),
                  enabledBorder: border,
                  disabledBorder: border,
                  focusedBorder: border,
                  errorBorder: border,
                  border: border,
                  focusedErrorBorder: border,
                  hintText: hint,
                  labelText: hint,
                  labelStyle: textTheme.bodyText2,
                  errorStyle: textTheme.caption?.merge(TextStyle(
                      color: theme.accentColor, fontSize: 10.0, height: 0.5)),
                  hintStyle: textTheme.subtitle1?.merge(TextStyle(
                    color: theme.hintColor,
                  )),
                )),
          ),
          isEnabled != null && !isEnabled
              ? IconButton(
                  icon: ImageIcon(AssetImage(AppAssets.editIcon), size: 24,),
                  color: Colors.white,
                  onPressed: () {
                    if (focusNode != _passwordFocus) {
                      _fieldFocusChange(context, _focusNode, focusNode);
                    } else {
                      Navigator.of(context)
                          .pushNamed(Routes.RESET_PASSWORD)
                          .then((value) {
                        if (value != null) {
                          if (value == true) {
                            //Display Password Saved Overlay
                            showDialog(
                              context: context,
                              builder: (context) {
                                return Helpers.overlayPasswordSaved(
                                    context, S.newPasswordSaved, onClose: () {
                                  Navigator.of(context).pop();
                                });
                              },
                            );
                          }
                        }
                      });
                    }
                  },
                )
              : SizedBox()
        ],
      ),
    );
  }

  Widget _buildCoachesProfile(ThemeData theme,
      {@required String? imageUrl,
      @required String? name,
      @required String? profileTitle,
      @required String? email}) {
    var textTheme = theme.textTheme;
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.16),
          borderRadius: BorderRadius.circular(16),
          border: Border.all(color: Colors.white.withOpacity(0.24), width: 1)),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Image.network(
                imageUrl ?? "",
                width: 96,
                height: 96,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: 16),
            Text(
              name ?? "",
              style: textTheme.subtitle1?.merge(TextStyle(fontSize: 24)),
            ),
            SizedBox(height: 6),
            Text(
              profileTitle ?? "",
              style: textTheme.subtitle1
                  ?.merge(TextStyle(color: config.AppColors.yellow)),
            ),
            SizedBox(height: 16),
            Text(email ?? "", style: textTheme.bodyText1),
          ],
        ),
      ),
    );
  }
}

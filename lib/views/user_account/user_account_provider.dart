import 'dart:io';

import 'package:brain_trainer/models/coach_tech_user.dart';
import 'package:brain_trainer/services/firebase/firebase_auth_service.dart';
import 'package:brain_trainer/services/firebase/firebase_storage_service.dart';
import 'package:brain_trainer/models/golf_subject.dart';
import 'package:brain_trainer/models/training_program.dart';
import 'package:brain_trainer/services/training/training_service.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../../services/firebase/firestore_db_service.dart';

class UserAccountProvider extends ChangeNotifier {
  FirebaseAuthService? firebaseAuthService;
  TrainingProgram? trainingProgram; //LoggedIn User's current training Program
  TrainingService? trainingService;
  CoachTechUser? coachProfile;
  CoachTechUser? neuroScientistProfile;
  FirestoreDBService? firestoreDBService;

  bool isUpdatingPhoto = false;
  bool errorUploadingPhoto = false;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController handicapIndexController = TextEditingController();
  TextEditingController homeCourseController = TextEditingController();

  UserAccountProvider({
    @required this.firebaseAuthService,
    @required this.trainingService,
    @required this.firestoreDBService,
    this.isUpdatingPhoto = false,
    this.errorUploadingPhoto = false,
  });

  Future<dynamic> updateUserEmail(String email) async {
    try {
      var result = await firebaseAuthService?.updateUserEmail(email);
      await firestoreDBService?.updateUserEmail(
          FirebaseAuthService.user?.uid ?? "", email);
      return result;
    } catch (e) {
      return e;
    }
  }

  Future<dynamic> getUserTrainingProgram() async {
    try {
      trainingProgram =
          await trainingService?.getUserTrainingProgram(firestoreDBService!);
    } catch (e) {
      print(e.toString());
    }
    this.handicapIndexController.text =
        trainingProgram?.handicap != null ? '${trainingProgram?.handicap}' : "";
    this.homeCourseController.text = trainingProgram?.homeCourse != null
        ? '${trainingProgram?.homeCourse}'
        : "";
  }

  Future<dynamic> getCoachAndNeuroScientist(GolfSubject? _subject) async {
    if (_subject?.coachID != null) {
      print(_subject?.coachID);
      this.coachProfile =
          await firestoreDBService?.getCoachTechUser(_subject?.coachID);
      notifyListeners();
    }
    if (_subject?.neuroID != null) {
      print(_subject?.neuroID);
      this.neuroScientistProfile =
          await firestoreDBService?.getCoachTechUser(_subject?.neuroID);
      notifyListeners();
    }
  }

  Future<dynamic> updateTrainingProgram() async {
    try {
      this.trainingProgram?.handicap =
          double.parse(this.handicapIndexController.text.toString());
      this.trainingProgram?.homeCourse =
          this.homeCourseController.text.toString();
      await trainingService?.updateUserTrainingProgram(
          firestoreDBService!, this.trainingProgram!);
    } catch (e) {
      throw (e);
    }
  }

  Future<dynamic> pickUserImage(ImageSource _source) async {
    errorUploadingPhoto = false;
    var image = await ImagePicker().getImage(source: _source);

    if (image != null) {
      isUpdatingPhoto = true;
      File imageFile = File(image.path);
      final imageExtension =
          image.path.substring(image.path.lastIndexOf("."), image.path.length);
      final imageName = '${FirebaseAuthService.user?.uid}$imageExtension';
      var imgRef = "${FirebaseStorageService.userPhotoRef}$imageName";
      notifyListeners();
      return await updateUserImage(imageFile, imgRef);
    }
  }

  Future<dynamic> updateUserImage(File imageFile, String imgRef) async {
    Reference ref = FirebaseStorage.instance.ref().child(imgRef);

    try {
      final task = await FirebaseStorageService().syncFile(imageFile, ref);
      final taskSnapshot = await task.whenComplete(() => true);

      if (task.snapshot.state == TaskState.success) {
        final String? photoUrl = await taskSnapshot.ref.getDownloadURL();
        if (photoUrl != null) {
          await firestoreDBService?.updateUserProfilePhoto(
              FirebaseAuthService.user?.uid ?? "", photoUrl);
          final subject = await firestoreDBService
              ?.getGolfSubject(FirebaseAuthService.user?.uid);
          isUpdatingPhoto = false;
          notifyListeners();
          return subject;
        }
      } else {
        isUpdatingPhoto = false;
      }
      return;
    } catch (e) {
      print(e);
      errorUploadingPhoto = true;
      notifyListeners();
    }
  }
}

# Golf Data Operations

This represents the scripts and schema to run the Golf Project
Firestore Database's connection to the BigQuery system.

# Setup

## Pre-req
Make sure that you have the following installed on your computer:
* [Node.js, NMP, and NPX](https://nodejs.org/en/download/)
* [Google Cloud CLI](https://cloud.google.com/sdk/docs/install)
* [Google Firestore CLI](https://github.com/firebase/firebase-tools)


## Extensions
This system uses the Firebase Extension [Export Collections to BigQuery](https://firebase.google.com/products/extensions/firestore-bigquery-export?authuser=0), which automatically moves new data in the given collections to the stated BigQuery data set.  

**Note:  An extension has to be installed for each collection that you wish to bring over.**

The three collections for this project are:
1. golf_subjects
2. training_programs
3. training_sessions

**Make note of the table names you move collection data to in BigQuery.  It is recommended that you use the same names as the collections**

## Load Existing Data

Once you install and configure the extensions, you must move the existing data over to the BigQuery system.  
To do this, use the script [fs-bq-import-collection](https://github.com/firebase/extensions/blob/master/firestore-bigquery-export/guides/IMPORT_EXISTING_DOCUMENTS.md).  Follow the instructions in that document.

Do this for each collection above.  Accept the defaults to any questions the scripts.

## Generate Tables for query

After the data is loaded, you may notice that the data in stored in unstructured JSON strings.  We must tell the
system how to map that JSON to typed fields in a queryable table.  To do this, we must create [scheme views](https://github.com/firebase/extensions/blob/master/firestore-bigquery-export/guides/GENERATE_SCHEMA_VIEWS.md).  

Each collection will need a JSON file that maps the fields to schema and types.  These are already in this directory, the three JSON files.

There is also a script file, loadschemas.sh, that will set these up.  Once they are setup once, you do not need to do it again.

**Note:  the names of the created, usable views will be long.  They are:**
1.  golf_subjects_schema_golf_subjects_schema_latest
2.  training_programs_schema_training_programs_schema_latest
3.  training_sessions_schema_training_sessions_schemea_latest

# Usage

Now that these views are up in BigQuery, they will be automatically up-to-date with all data changes made by the app users.  To 
get to this data, you can use a scheduled query to create tables of joined data for easy reporting.  Please read [the docs](https://cloud.google.com/bigquery/docs/scheduling-queries) on how to do this.  In the data directory is an example query from the last project that made a table for easy 
reporting in Googles Data Studio.


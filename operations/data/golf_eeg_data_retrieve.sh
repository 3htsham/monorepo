GOLF_DATA_BUCKET="gs://golf-brain-trainer.appspot.com/golf_*"
if [ $# = 0 ]; then
	echo "Need path for storage"
else
	echo $GOLF_DATA_BUCKET
	echo $1
	gsutil cp -rn $GOLF_DATA_BUCKET $1
fi
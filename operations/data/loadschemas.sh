PROJECT_ID=optios-golf
DATASET_ID=golf_data

echo Loading the schemas into $PROJECT_ID, data set $DATASET_ID


echo Loading golf_subjects schemas
npx @firebaseextensions/fs-bq-schema-views \
  --non-interactive \
  --project=$PROJECT_ID \
  --dataset=$DATASET_ID \
  --table-name-prefix=golf_subjects \
  --schema-files=./golf_subjects_schema.json

echo loading training_programs schemas

npx @firebaseextensions/fs-bq-schema-views \
  --non-interactive \
  --project=$PROJECT_ID \
  --dataset=$DATASET_ID \
  --table-name-prefix=training_programs \
  --schema-files=./training_programs_schema.json


echo loading training_sessions schemas
npx @firebaseextensions/fs-bq-schema-views \
  --non-interactive \
  --project=$PROJECT_ID \
  --dataset=$DATASET_ID \
  --table-name-prefix=training_sessions \
  --schema-files=./training_sessions_schema.json

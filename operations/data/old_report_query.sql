SELECT
  ROW_NUMBER() over ( PARTITION BY gs.email ORDER BY
  startTime ASC ) as session,
  SUBSTR(gs.email, 0, LENGTH(gs.email)-13) AS subject,
  ts.startTime,
  ts.endTime,
  TIMESTAMP_DIFF(ts.endTime, ts.startTime, SECOND) AS duration,
  ts.trainingType,
  ts.zoneThreshold,
  ts.averageZone,
  ts.timeInZone,
  ts.fileKey,
FROM
  `golf-brain-trainer.golf_data.training_sessions_schema_training_sessions_schema_latest` ts
JOIN
  golf-brain-trainer.golf_data.golf_subjects gs
ON
  ts.subjectID = gs.userAuthID
WHERE
  gs.groupID = 'subject'
ORDER BY
  startTime DESC
LIMIT
  1000
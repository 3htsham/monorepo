/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    giby.raphael@gmail.com, 7/21
    Feedback algorithm 
    - Remove baseline drift
    - Compute FFT 
    - Compute PSD 
    - Compute ALPHA, bandwidth 8-12Hz
    - Compute baseline Mean and SD
    - Compute Zscore
    - Compute Feedback 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <list>

using namespace std;

#define PI 3.1415926535897932384626434
#define SFREQ 500
#define MEDIAN_FILTER_ORDER 250
#define MEDIAN_INDEX MEDIAN_FILTER_ORDER/2
#define BASELINE_DUR 120 //in seconds
#define THRESHOLD 1

list<float> mFilterHKList;
list<float>::iterator it;
float mFilterHKArray[MEDIAN_FILTER_ORDER];
int mFilterHKArrayInd=0;
float mFilterOldestS=0;
bool Feedback = false;

typedef struct complex_t {
    double re;
    double im;
} complex;

//debug
FILE *fftout;
FILE *psdout;
FILE *alphaout;
FILE *medianout;
int epochT = 0;


//------------------------Complex number operations --------------------------//
complex conv_from_polar(double r, double radians) {
    complex result;
    result.re = r * cos(radians);
    result.im = r * sin(radians);
    return result;
}

complex add(complex left, complex right) {
    complex result;
    result.re = left.re + right.re;
    result.im = left.im + right.im;
    return result;
}

complex multiply(complex left, complex right) {
    complex result;
    result.re = left.re*right.re - left.im*right.im;
    result.im = left.re*right.im + left.im*right.re;
    return result;
}

//------------------------Cooley-Tukey FFT Algorithm --------------------------//
complex* DFT_naive(complex* x, int N) {
    complex* X = (complex*) malloc(sizeof(struct complex_t) * N);
    int k, n;
    for(k = 0; k < N; k++) {
        X[k].re = 0.0;
        X[k].im = 0.0;
        for(n = 0; n < N; n++) {
            X[k] = add(X[k], multiply(x[n], conv_from_polar(1, -2*PI*n*k/N)));
        }
    }
    
    return X;
}

complex* FFT(complex* input, int N, int N1, int N2) {
    int k1, k2;

    /* Allocate columnwise matrix */
    complex** columns = (complex**) malloc(sizeof(struct complex_t*) * N1);
    for(k1 = 0; k1 < N1; k1++) {
        columns[k1] = (complex*) malloc(sizeof(struct complex_t) * N2);
    }
    
    /* Allocate rowwise matrix */
    complex** rows = (complex**) malloc(sizeof(struct complex_t*) * N2);
    for(k2 = 0; k2 < N2; k2++) {
        rows[k2] = (complex*) malloc(sizeof(struct complex_t) * N1);
    }
    
    /* Reshape input into N1 columns */
    for (k1 = 0; k1 < N1; k1++) {
        for(k2 = 0; k2 < N2; k2++) {
            columns[k1][k2] = input[N1*k2 + k1];
        }
    }

    /* Compute N1 DFTs of length N2 using naive method */
    for (k1 = 0; k1 < N1; k1++) {
        columns[k1] = DFT_naive(columns[k1], N2);
        for (k2 = 0; k2 < N2; ++k2) {
              //printf(" - %d %d is %f, %f\n", k1, k2, columns[k1][k2].im, columns[k1][k2].re);
        }
    }
    
    /* Multiply by the twiddle factors  ( e^(-2*pi*j/N * k1*k2)) and transpose */
    for(k1 = 0; k1 < N1; k1++) {
        for (k2 = 0; k2 < N2; k2++) {
            rows[k2][k1] = multiply(conv_from_polar(1, -2.0*PI*k1*k2/N), columns[k1][k2]);
        }
    }
    
    /* Compute N2 DFTs of length N1 using naive method */
    for (k2 = 0; k2 < N2; k2++) {
        rows[k2] = DFT_naive(rows[k2], N1);
    }
    
    /* Flatten into single output */
    complex* output = (complex*) malloc(sizeof(struct complex_t) * N);
    for(k1 = 0; k1 < N1; k1++) {
        for (k2 = 0; k2 < N2; k2++) {
            output[N2*k1 + k2] = rows[k2][k1];
        }
    }

    /* Free all alocated memory except output and input arrays */
    for(k1 = 0; k1 < N1; k1++) {
        free(columns[k1]);
    }
    for(k2 = 0; k2 < N2; k2++) {
        free(rows[k2]);
    }
    free(columns);
    free(rows);
    return output;
}

//------------------------Bandwidth Algorithm --------------------------//
void PSD(complex* input, int N, double* psdv) {

    /* PSD 0Hz to 250Hz, 249 Real and 251 Img components */
    for(int k=0; k<=N/2; k++){
        psdv[k] = 2*((input[k].re*input[k].re) + (input[k].im*input[k].im))/N;
    }
}

double ALPHA(double* psdv){
    return log10((psdv[8]+psdv[9]+psdv[10]+psdv[11]+psdv[12])/5);
}

double BETA(double* psdv){
    double betav;
    double betat=0;

    /* BETA = 13-30Hz */
    betat=0;
    for(int k=13; k<=30; k++){
        betat += psdv[k];
    }
    betav = log10(betat/18);

    return betav;
}

double getAlpha(complex *input1){

    complex *fftA;
    double  psdA[250];
    double alphaV;
    double betaV;

    /* Do FFT */
    fftA = FFT(input1, SFREQ, 125, 4);

    
    //Debug
    for(int k=0; k<252; k++) {
          //printf("getAlpha - FFT - %d,%f,%f\n",k,fftA[k].re,fftA[k].im);
    }
    //epochT++;
    
 
    /* Do PSD */
    PSD(fftA,SFREQ,psdA);

    //Debug
    for(int k=0; k<=250; k++){
        //fprintf(psdout,"%d,%d,%f\n",epochT,k,psdA[k]);
    }
    

    /* Get ALPHA */
    alphaV = ALPHA(psdA);

    //debug
    printf(" *****************   Alpha Result  *****************\n    %d,%f\n",epochT,alphaV);

    /* Get BETA */
    betaV = BETA(psdA);

    return alphaV;
}

//------------------------------Baseline Filter ---------------------------//
double MEDIANFILTER(double sample){
    bool binsert=0;		
    mFilterOldestS = mFilterHKArray[mFilterHKArrayInd];
    mFilterHKArray[mFilterHKArrayInd] = sample; 
    mFilterHKArrayInd++; if (mFilterHKArrayInd>=MEDIAN_FILTER_ORDER) {mFilterHKArrayInd=0;}

    //remove			
    for (it=mFilterHKList.begin(); it!=mFilterHKList.end(); it++) {			
        if(*it == mFilterOldestS) {
            mFilterHKList.erase(it);
            break;
        }
    }

    //insert, remove after
    binsert=0;
    for (it=mFilterHKList.begin(); it!=mFilterHKList.end(); it++) {
        if(sample >= *it) {
            mFilterHKList.insert(it,sample);
            binsert=1;
            break;
        }
    }	
    if(!binsert) 			
			mFilterHKList.insert(mFilterHKList.end(),sample);


    //printf("size of list = %d\n",mFilterHKList.size());
		
    //flush median
    it=mFilterHKList.begin(); advance(it,MEDIAN_INDEX);

    //subtract dc
    return(sample - *it);

}

//-------------------------------Math Functions --------------------------------//
double calculateSD(double data[], int N) {
    double sum = 0.0, mean, SD = 0.0;
    int i;
    for (i = 0; i < N; ++i) {
        sum += data[i];
    }
    mean = sum / N;
    for (i = 0; i < N; ++i) {
        SD += pow(data[i] - mean, 2);
    }
    return sqrt(SD / N);
}


//-----------------------------------Baseline algorithm -----------------------------//
void getBaseline(double *data, int N, double *mean, double *sd){
    //get alpha
    double alphaT[N]; //2min baseline
    complex *input1 = (complex*) malloc(sizeof(struct complex_t) * SFREQ);
    for(int k=0; k<N; k++){
        for (int i=0; i < SFREQ; i++) {
            input1[i].re = data[k*SFREQ+i];
             input1[i].im = 0.0;
        }
        alphaT[k] = getAlpha(input1);
        //printf("getBaseline - alphaT[%d] = %f\n", k, alphaT[k]);

    }

    //compute baseline Mean and SD
    double baselineM = 0;
    double baselineSD = 0;
    for (int i=0; i<N; i++){
        baselineM = baselineM + alphaT[i];
    }
    baselineM = baselineM/N;
    baselineSD = calculateSD(alphaT, N);

    *mean = baselineM;
    *sd = baselineSD;

}


//------------------------------ZScore algorithm ------------------------------------//
double getZscore(double *data, double M, double SD){

    //get alpha 
    double alphaT = 0.0;
    complex *input1 = (complex*) malloc(sizeof(struct complex_t) * SFREQ);
    for (int i=0; i < SFREQ; i++) {
        input1[i].re = data[i];
        input1[i].im = 0.0;
    }
    alphaT = getAlpha(input1);

    //compute zscore
    return ((alphaT-M)/SD);


}



/*----------------------------------Entry point------------------------------------*/
int main() {
    printf("%s\n","Starting Main...");

    /*debug*/
    //fftout = fopen("fftC.csv","w+");
    //psdout = fopen("psdC.csv","w+");
    //alphaout = fopen("alphaC.csv","w+");
    //medianout = fopen("medianC.csv","w+");
    
    //----------For testing only----------------------------------//
    /*Read sim data*/
    int col = 44984; //5*60*SFREQ; //read 5 min data
    FILE *debugInFid = fopen("../data/giby_test2_input.csv","r");
    FILE *debugOutFid = fopen("../data/new_output.csv","w+");
    char buff[256];
    double dataRaw[col];
    double sum = 0.0f;
    for(int k=0; k<col; k++){
        dataRaw[k] = atof(fgets(buff,256,debugInFid));
    } 

    //---Replace with Real Time loop------------------------------------//
    //init 
    mFilterHKList.assign(MEDIAN_FILTER_ORDER,0);
    for(int i=0; i<MEDIAN_FILTER_ORDER; i++) {mFilterHKArray[i] = 0.0;}
    double rawForBaseline[BASELINE_DUR*SFREQ]; //2 min baseline
    int rawInd = 0;
    double baselineMean, baselineSD;
    double epoch[SFREQ];
    double zscore = 0;

    //loop
    for(int r=0; r<col; r++){

        /*Baseline filter*/    
        double before = dataRaw[r];

        // no filter for the giby test
        //dataRaw[r] = MEDIANFILTER(dataRaw[r]);


        // print post MEDIANFILTER
        // printf("Filtering of %d - b: %f a: %f\n", r, before, dataRaw[r]);
        // if (r > 10 * SFREQ) break;

        //printf("Baseline Start %d, end %d\n", 30*SFREQ, 30*SFREQ + BASELINE_DUR*SFREQ);

        // no baseline for giby test
        // if(r<30*SFREQ){ //ignore first 30 sec

        // }
        // else 
        // if(rawInd<BASELINE_DUR*SFREQ){ //compute baseline with next 2min data
        //     rawForBaseline[rawInd] = dataRaw[r];
        //     rawInd++;
        //     if(rawInd >= BASELINE_DUR*SFREQ){
        //         getBaseline(rawForBaseline,BASELINE_DUR,&baselineMean, &baselineSD);
        //         printf("getBaseline: mean = %f and sd = %f\n", baselineMean, baselineSD);

        //     }
        // }
        // else
        { //compute feedback
            epoch[r%SFREQ] = dataRaw[r];
            if((r%SFREQ)==0 && r>0){
                printf ("****************   Epoch %d   ****************\n", epochT);

                for(int e = 0; e < SFREQ; ++ e)
                {
                    //printf("  E[%03d] - %f\n", e, epoch[e]);
                }

               zscore =  getZscore(epoch,baselineMean, baselineSD);
               if(zscore > THRESHOLD){
                   Feedback = true;
               } else {Feedback = false;}
               printf("at %d - zscore = %f\t Feedback=%d\n", r ,zscore, Feedback);
               fprintf(debugOutFid,"%f,%d\n",zscore, Feedback);
               epochT++;
            }
        }
    }
    fclose(debugInFid);
    fclose(debugOutFid);

    return 0;
    
}

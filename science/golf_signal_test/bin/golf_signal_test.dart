import 'dart:convert';
import 'dart:io';

// ignore: avoid_relative_lib_imports
import '../../../lib/science/golf_signal_processing.dart';

void main(List<String> arguments) async {
  print('Hello world!');

  var dataRaw = List<double>.empty(growable: true);
  var rawDataFile = '../data/giby_test2_input.csv';
  var outputFile = '../data/dart_output.csv';

  print('Reading File $rawDataFile');

  final input_file = File(rawDataFile);
  Stream<String> lines =
      input_file.openRead().transform(Utf8Decoder()).transform(LineSplitter());

  await for (var line in lines) {
    dataRaw.add(double.parse(line));
  }

  var col = 0;

  print('Loaded raw data - ${dataRaw.length} entries');

  col = 5 * 60 * 500; //read 5 min data

  var computer = GolfSignalProcessor();

  computer.debugComputer = true;

  for (var r = 0; r < dataRaw.length; ++r) {
    // baseline filter
    var sample = dataRaw[r];

    // this is setup to read post-filtered data from Giby
    // and show the alpha value.  It skips baselining
    // and the median filter, thus the z score will be
    // trash.  WFM 8/29/2021

    // if (r == 30 * 500) {
    //   //baseline after 30 seconds
    //   computer.startBaseLine();
    // } else
    {
      var eegData = List<double>.generate(4, (index) => 0.0);
      eegData[3] = sample;
      var zScore = computer.addSample(eegData, true);
      if (r % 500 == 0 && r > 0) {
        print('${(r / 500)} - Alpha ${computer.lastAlphaT} - Zscore: $zScore');
      }
    }
  }
}
